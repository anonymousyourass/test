﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAssetBundle::GetAssetBundle(System.String,UnityEngine.CachedAssetBundle,System.UInt32)
extern void UnityWebRequestAssetBundle_GetAssetBundle_m2C155DBCEF9A2047E3E833A346B4BBBF8A4DA219 ();
// 0x00000002 System.IntPtr UnityEngine.Networking.DownloadHandlerAssetBundle::CreateCached(UnityEngine.Networking.DownloadHandlerAssetBundle,System.String,System.String,UnityEngine.Hash128,System.UInt32)
extern void DownloadHandlerAssetBundle_CreateCached_mE8DEDE3622B495AE1CA534BAD70F86A3AF41477D ();
// 0x00000003 System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::InternalCreateAssetBundleCached(System.String,System.String,UnityEngine.Hash128,System.UInt32)
extern void DownloadHandlerAssetBundle_InternalCreateAssetBundleCached_m2F4C9DD99E26350A7DC685CD918DE5792D9B7106 ();
// 0x00000004 System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::.ctor(System.String,UnityEngine.CachedAssetBundle,System.UInt32)
extern void DownloadHandlerAssetBundle__ctor_m491425D31D005ED9B500067D776DE58FD1A41D6E ();
// 0x00000005 System.Byte[] UnityEngine.Networking.DownloadHandlerAssetBundle::GetData()
extern void DownloadHandlerAssetBundle_GetData_m559C00EFAE6DAEFB753A247C6B842F32191923D3 ();
// 0x00000006 System.String UnityEngine.Networking.DownloadHandlerAssetBundle::GetText()
extern void DownloadHandlerAssetBundle_GetText_mF7330136649D26AACD775000E871F44B83891885 ();
// 0x00000007 UnityEngine.AssetBundle UnityEngine.Networking.DownloadHandlerAssetBundle::get_assetBundle()
extern void DownloadHandlerAssetBundle_get_assetBundle_m083E8230E8A5644AE6176135C86B4E1A6283190D ();
// 0x00000008 System.IntPtr UnityEngine.Networking.DownloadHandlerAssetBundle::CreateCached_Injected(UnityEngine.Networking.DownloadHandlerAssetBundle,System.String,System.String,UnityEngine.Hash128&,System.UInt32)
extern void DownloadHandlerAssetBundle_CreateCached_Injected_m225667904F10871FC65304B9DBCB4578FE9377D2 ();
static Il2CppMethodPointer s_methodPointers[8] = 
{
	UnityWebRequestAssetBundle_GetAssetBundle_m2C155DBCEF9A2047E3E833A346B4BBBF8A4DA219,
	DownloadHandlerAssetBundle_CreateCached_mE8DEDE3622B495AE1CA534BAD70F86A3AF41477D,
	DownloadHandlerAssetBundle_InternalCreateAssetBundleCached_m2F4C9DD99E26350A7DC685CD918DE5792D9B7106,
	DownloadHandlerAssetBundle__ctor_m491425D31D005ED9B500067D776DE58FD1A41D6E,
	DownloadHandlerAssetBundle_GetData_m559C00EFAE6DAEFB753A247C6B842F32191923D3,
	DownloadHandlerAssetBundle_GetText_mF7330136649D26AACD775000E871F44B83891885,
	DownloadHandlerAssetBundle_get_assetBundle_m083E8230E8A5644AE6176135C86B4E1A6283190D,
	DownloadHandlerAssetBundle_CreateCached_Injected_m225667904F10871FC65304B9DBCB4578FE9377D2,
};
static const int32_t s_InvokerIndices[8] = 
{
	1596,
	1597,
	1598,
	1599,
	14,
	14,
	14,
	1600,
};
extern const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestAssetBundleModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestAssetBundleModuleCodeGenModule = 
{
	"UnityEngine.UnityWebRequestAssetBundleModule.dll",
	8,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
