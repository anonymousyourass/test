﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4>
struct VirtFuncInvoker4
{
	typedef R (*Func)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
struct VirtFuncInvoker5
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
struct VirtFuncInvoker6
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, T6, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
struct VirtFuncInvoker7
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, T6, T7, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct GenericVirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericVirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4>
struct GenericVirtFuncInvoker4
{
	typedef R (*Func)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
struct GenericVirtFuncInvoker5
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
struct GenericVirtFuncInvoker6
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, T6, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
struct GenericVirtFuncInvoker7
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, T6, T7, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct InterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4>
struct InterfaceFuncInvoker4
{
	typedef R (*Func)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
struct InterfaceFuncInvoker5
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
struct InterfaceFuncInvoker6
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, T6, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
struct InterfaceFuncInvoker7
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, T6, T7, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct GenericInterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericInterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4>
struct GenericInterfaceFuncInvoker4
{
	typedef R (*Func)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
struct GenericInterfaceFuncInvoker5
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
struct GenericInterfaceFuncInvoker6
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, T6, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
struct GenericInterfaceFuncInvoker7
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, T6, T7, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, invokeData.method);
	}
};

// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UniRx.ICancelable
struct ICancelable_t17CBA667BF856A6EA2591122CE89C2105AE8B871;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// UniRx.Operators.IZipLatestObservable
struct IZipLatestObservable_tBC8E6DF6D33AD1BE66C2DD85334B17F8993603F7;
// UniRx.Operators.IZipObservable
struct IZipObservable_t0ECBDF53FECFF9407F1BE5BA0FD1156D07B45496;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC;
// System.String
struct String_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B;
// System.Func`2<System.Int64,System.Boolean>
struct Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8;
// System.Func`2<System.Object,System.Object>
struct Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436;
// System.Func`3<System.Boolean,System.Int32,System.Boolean>
struct Func_3_t3F60B00D281681419CA999636A62D3ED1B529778;
// System.Func`3<System.Int64,System.Int32,System.Boolean>
struct Func_3_t8FFBB3815B984FFFA353813F9E09AEA69D8DB623;
// System.Func`3<System.Object,System.Int32,System.Boolean>
struct Func_3_tCB498436E1553228C2C36F41918548DB2DF10E0A;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t707982BD768B18C51D263C759F33BCDBDFA44901;
// System.IObservable`1<System.Boolean>
struct IObservable_1_tD1F997DC99976CFA8E4ADBC96FD6CD9ED3629F98;
// System.IObservable`1<System.Int64>
struct IObservable_1_tD071D077B102D365D232E0A07A76B6FEA91797EE;
// System.IObservable`1<System.Object>
struct IObservable_1_tD298C95FFB6061193313830292FE8E817D37B5A4;
// System.IObserver`1<System.Boolean>
struct IObserver_1_t4F5148C316DBF705C535277299AA3CE650D666A2;
// System.IObserver`1<System.Int64>
struct IObserver_1_t54A626A6B9E891261BA1AC71BC4E7798F2DBAAF5;
// System.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t561C25E59CB3AAA46670ABBC269E0BBC41B771FB;
// System.IObserver`1<System.Object>
struct IObserver_1_t9EBB98F865B275FC177A4A094F9E254B58B8A962;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64;
// UniRx.Operators.WhereObservable`1<System.Boolean>
struct WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B;
// UniRx.Operators.WhereObservable`1<System.Int64>
struct WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F;
// UniRx.Operators.WhereObservable`1<System.Object>
struct WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01;
// UniRx.Operators.WhereSelectObservable`2/WhereSelect<System.Boolean,System.Boolean>
struct WhereSelect_t2C19B6051587E495A79CF7134957F51AB0F8CB99;
// UniRx.Operators.WhereSelectObservable`2/WhereSelect<System.Object,System.Boolean>
struct WhereSelect_t7CA94D47BE319C6E9D59A58019438DA0D06C8719;
// UniRx.Operators.WhereSelectObservable`2/WhereSelect<System.Object,System.Object>
struct WhereSelect_t6984636B2863BAE990462A289EC7A8841F77157D;
// UniRx.Operators.WhereSelectObservable`2<System.Boolean,System.Boolean>
struct WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C;
// UniRx.Operators.WhereSelectObservable`2<System.Object,System.Boolean>
struct WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870;
// UniRx.Operators.WhereSelectObservable`2<System.Object,System.Object>
struct WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F;
// UniRx.Operators.WhereObservable`1/Where_<System.Boolean>
struct Where__t4B5EA5BC9EA34076FAD0EE0F30CD814FA7506B60;
// UniRx.Operators.WhereObservable`1/Where_<System.Int64>
struct Where__t5E913FC3047D357D4D04A0BF656999DBCBF4C6C5;
// UniRx.Operators.WhereObservable`1/Where_<System.Object>
struct Where__tC4BFE268565371D15C47C91FF86D6E2F822710EB;
// UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<System.Object,System.Object,System.Object>
struct WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF;
// UniRx.Operators.WithLatestFromObservable`3<System.Object,System.Object,System.Object>
struct WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124;
// UniRx.Operators.ZipObservable`1/Zip<System.Object>
struct Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D;
// UniRx.Operators.ZipObservable`3/Zip<System.Object,System.Object,System.Object>
struct Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93;
// UniRx.Operators.ZipObservable`4/Zip<System.Object,System.Object,System.Object,System.Object>
struct Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1;
// UniRx.Operators.ZipObservable`5/Zip<System.Object,System.Object,System.Object,System.Object,System.Object>
struct Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808;
// UniRx.Operators.ZipObservable`6/Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09;
// UniRx.Operators.ZipObservable`7/Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75;
// UniRx.Operators.ZipObservable`8/Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Zip_tFE288625F1A712CC45FAE011730910DD457970CC;
// UniRx.Operators.ZipFunc`4<System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5;
// UniRx.Operators.ZipFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58;
// UniRx.Operators.ZipFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D;
// UniRx.Operators.ZipFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204;
// UniRx.Operators.ZipFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03;
// UniRx.Operators.ZipLatestObservable`1/ZipLatest<System.Object>
struct ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8;
// UniRx.Operators.ZipLatestObservable`3/ZipLatest<System.Object,System.Object,System.Object>
struct ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95;
// UniRx.Operators.ZipLatestObservable`4/ZipLatest<System.Object,System.Object,System.Object,System.Object>
struct ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D;
// UniRx.Operators.ZipLatestObservable`5/ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61;
// UniRx.Operators.ZipLatestObservable`6/ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565;
// UniRx.Operators.ZipLatestObservable`7/ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37;
// UniRx.Operators.ZipLatestObservable`8/ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821;
// UniRx.Operators.ZipLatestFunc`4<System.Object,System.Object,System.Object,System.Object>
struct ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652;
// UniRx.Operators.ZipLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90;
// UniRx.Operators.ZipLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F;
// UniRx.Operators.ZipLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606;
// UniRx.Operators.ZipLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA;
// UniRx.Operators.ZipLatestObservable`1<System.Object>
struct ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65;
// UniRx.Operators.ZipLatestObservable`3<System.Object,System.Object,System.Object>
struct ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493;
// UniRx.Operators.ZipLatestObservable`4<System.Object,System.Object,System.Object,System.Object>
struct ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0;
// UniRx.Operators.ZipLatestObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D;
// UniRx.Operators.ZipLatestObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645;
// UniRx.Operators.ZipLatestObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932;
// UniRx.Operators.ZipLatestObservable`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA;
// UniRx.Operators.ZipLatestObservable`1/ZipLatest/ZipLatestObserver<System.Object>
struct ZipLatestObserver_t1916CBEFD17AA1674F927430EF42C7EAEB835507;
// UniRx.Operators.ZipLatestObserver`1<System.Object>
struct ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8;
// UniRx.Operators.ZipObservable`1<System.Object>
struct ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B;
// UniRx.Operators.ZipObservable`3<System.Object,System.Object,System.Object>
struct ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838;
// UniRx.Operators.ZipObservable`4<System.Object,System.Object,System.Object,System.Object>
struct ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39;
// UniRx.Operators.ZipObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E;
// UniRx.Operators.ZipObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F;
// UniRx.Operators.ZipObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448;
// UniRx.Operators.ZipObservable`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F;
// UniRx.Operators.ZipObservable`1/Zip/ZipObserver<System.Object>
struct ZipObserver_tAE56C446B45ECA54A1360388514A7D8851F5CC31;
// UniRx.Operators.ZipObserver`1<System.Object>
struct ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.Boolean[]
struct BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.Exception
struct Exception_t;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.ICollection
struct ICollection_tC1E1DED86C0A66845675392606B302452210D5DA;
// System.Collections.ICollection[]
struct ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8;
// System.IDisposable
struct IDisposable_t099785737FC6A1E3699919A94109383715A8D807;
// System.IDisposable[]
struct IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65;
// System.IObservable`1<System.Object>[]
struct IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.Collections.Generic.Queue`1<System.Object>[]
struct Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;

IL2CPP_EXTERN_C RuntimeClass* Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Disposable_tFB6FF3E5C049CFE0B05B25A824CED890F91DC101_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IZipLatestObservable_tBC8E6DF6D33AD1BE66C2DD85334B17F8993603F7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IZipObservable_t0ECBDF53FECFF9407F1BE5BA0FD1156D07B45496_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const uint32_t WhereSelect_OnNext_m303FF425D2880CCE79EED7DFE611E91E614B1CA5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelect_OnNext_mAD438473015AD79949F4EB612BAFA5FB8ADE963D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelect_OnNext_mADF7FE4C82A7A66A749740E722021F4AF3970497_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Where__OnNext_m0CCB53E2FFB7B30017787F8F5BAB903579C7089A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Where__OnNext_m4B9950EDD00F399442B93F5C45CA5C3F09BCA73D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Where__OnNext_m7CAA64E5DF2545B8F7ECC5908026612D6A8C5A7B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WithLatestFrom_Run_mF7288626461BBE1475E42B8C399F4A023D3D1789_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WithLatestFrom__ctor_mA4980C7DD088FA5F41485AF5697C71D510054CA7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatestObserver_1_OnCompleted_m03112B32B92F0EA008AABD5AA38B40CFC5D64CB0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatestObserver_1_OnError_mC4873B44B318D8F81C20FBD7D2856F6D01556415_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatestObserver_1_OnNext_mA09C2EBC63D9C912D25DFC72BD868D15064C583A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatest_Publish_m703D1DBB95522C016CABF3180AFD11FB542148E0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatest_Run_m56F418E34A08A1994A5DCB220A456158252AF7BB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatest_Run_m6A33076CF7973B2DFACBAF48BBC09DB9DAE4D156_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatest_Run_mA5594F847273DBFAECA0FF9C0DE551CC7AEFA132_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatest_Run_mCFF5BFC3075B96740A882513D118F128E5704D27_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatest__ctor_m08EFFB073F24C1A73907F31BA2E36E55E85E0D26_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatest__ctor_m167F470A046891026C16D3C24E7B0103595B581D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatest__ctor_m256865DE087447D3480E2E8BC9FF352DA41B62DD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatest__ctor_m595FDEC88B829142A13F05D109DD42EEAFC3687E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatest__ctor_m6E374DD3DCBFC2A22BC03032A56830C4690B94F2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatest__ctor_mCED0BC1B2DFC1BA825C9D5FCB23BC7200DD385E0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipLatest__ctor_mDF54144032B9106F99FB1263F03EBC5ECE8985EC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipObserver_1_OnCompleted_m2C83E0107322DEEBE7FC6214DCAB4D098D43F67A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipObserver_1_OnError_mC8AFE6282FDE7D11733BB41711DBA564553A68EE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZipObserver_1_OnNext_mAFBA00C1A35721E923AEC7A296BD7F11E9B5E905_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip_Dequeue_m9811AB9BAC2D627ABEC3EA82ADCAE3ADCCA3B71E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip_Run_m00C2066ED34C8005D94090B88213979A8ABB81B5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip_Run_m35828CC23733F8BA2375FB3AA3E843EC06D68DB9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip_Run_m5D63082B93153494983F6C2415FAD6FB69239C94_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip_Run_m975CE4AFC3C1696C99E6D648A084FBB7769E0E5F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip_Run_m9F70121C3CA8F4F30D0EFAF999FB34EB12E7C09B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip_Run_mC67BEF9BD738899596EBC70FEBCEBE307CEBB64D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip_Run_mCCA3BEB63BD107249ABDB30A103E4FD27E2AD46F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip__ctor_m10F368F9A43890A63AC953BC023C90262A809EC0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip__ctor_m2219C780CE544D6FD405B8C996028EB8E8A5DA08_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip__ctor_m3EA93898D117E6409930CDBC327E300793D59461_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip__ctor_m4A60055901C0E8C8F796C30EBE4066E62C25617A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip__ctor_mA976EE453808F313AD9FBFFE54F95F200AA3C34F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip__ctor_mCB2E44FA936C74D8E5192B17163A4DB5DDFB178C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zip__ctor_mD100CA4C96D762F9368A7AA6324C8FCF65CE93A7_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C;
struct Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40;
struct ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8;
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
struct IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65;
struct IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.Queue`1<System.Object>
struct  Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_4;
	// System.Object System.Collections.Generic.Queue`1::_syncRoot
	RuntimeObject * ____syncRoot_5;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64, ____array_0)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__array_0() const { return ____array_0; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__tail_2() { return static_cast<int32_t>(offsetof(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64, ____tail_2)); }
	inline int32_t get__tail_2() const { return ____tail_2; }
	inline int32_t* get_address_of__tail_2() { return &____tail_2; }
	inline void set__tail_2(int32_t value)
	{
		____tail_2 = value;
	}

	inline static int32_t get_offset_of__size_3() { return static_cast<int32_t>(offsetof(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64, ____size_3)); }
	inline int32_t get__size_3() const { return ____size_3; }
	inline int32_t* get_address_of__size_3() { return &____size_3; }
	inline void set__size_3(int32_t value)
	{
		____size_3 = value;
	}

	inline static int32_t get_offset_of__version_4() { return static_cast<int32_t>(offsetof(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64, ____version_4)); }
	inline int32_t get__version_4() const { return ____version_4; }
	inline int32_t* get_address_of__version_4() { return &____version_4; }
	inline void set__version_4(int32_t value)
	{
		____version_4 = value;
	}

	inline static int32_t get_offset_of__syncRoot_5() { return static_cast<int32_t>(offsetof(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64, ____syncRoot_5)); }
	inline RuntimeObject * get__syncRoot_5() const { return ____syncRoot_5; }
	inline RuntimeObject ** get_address_of__syncRoot_5() { return &____syncRoot_5; }
	inline void set__syncRoot_5(RuntimeObject * value)
	{
		____syncRoot_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UniRx.Operators.OperatorObservableBase`1<System.Boolean>
struct  OperatorObservableBase_1_t530D2FE2B1B3F6717F04CEB073301DD8A6848C1B  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t530D2FE2B1B3F6717F04CEB073301DD8A6848C1B, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};


// UniRx.Operators.OperatorObservableBase`1<System.Collections.Generic.IList`1<System.Object>>
struct  OperatorObservableBase_1_tB16C98CA8ACB1947DC9798221FBEED187248DFB0  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_tB16C98CA8ACB1947DC9798221FBEED187248DFB0, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};


// UniRx.Operators.OperatorObservableBase`1<System.Int64>
struct  OperatorObservableBase_1_t59F31167F0E3596ED1B2C8679413A193C2D468FD  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t59F31167F0E3596ED1B2C8679413A193C2D468FD, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};


// UniRx.Operators.OperatorObservableBase`1<System.Object>
struct  OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};


// UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>
struct  OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4  : public RuntimeObject
{
public:
	// System.IObserver`1<TResult> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.OperatorObserverBase`2::observer
	RuntimeObject* ___observer_0;
	// System.IDisposable UniRx.Operators.OperatorObserverBase`2::cancel
	RuntimeObject* ___cancel_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___observer_0), (void*)value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4, ___cancel_1)); }
	inline RuntimeObject* get_cancel_1() const { return ___cancel_1; }
	inline RuntimeObject** get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(RuntimeObject* value)
	{
		___cancel_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cancel_1), (void*)value);
	}
};


// UniRx.Operators.OperatorObserverBase`2<System.Collections.Generic.IList`1<System.Object>,System.Collections.Generic.IList`1<System.Object>>
struct  OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6  : public RuntimeObject
{
public:
	// System.IObserver`1<TResult> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.OperatorObserverBase`2::observer
	RuntimeObject* ___observer_0;
	// System.IDisposable UniRx.Operators.OperatorObserverBase`2::cancel
	RuntimeObject* ___cancel_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___observer_0), (void*)value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6, ___cancel_1)); }
	inline RuntimeObject* get_cancel_1() const { return ___cancel_1; }
	inline RuntimeObject** get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(RuntimeObject* value)
	{
		___cancel_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cancel_1), (void*)value);
	}
};


// UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>
struct  OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A  : public RuntimeObject
{
public:
	// System.IObserver`1<TResult> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.OperatorObserverBase`2::observer
	RuntimeObject* ___observer_0;
	// System.IDisposable UniRx.Operators.OperatorObserverBase`2::cancel
	RuntimeObject* ___cancel_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___observer_0), (void*)value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A, ___cancel_1)); }
	inline RuntimeObject* get_cancel_1() const { return ___cancel_1; }
	inline RuntimeObject** get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(RuntimeObject* value)
	{
		___cancel_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cancel_1), (void*)value);
	}
};


// UniRx.Operators.OperatorObserverBase`2<System.Object,System.Boolean>
struct  OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00  : public RuntimeObject
{
public:
	// System.IObserver`1<TResult> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.OperatorObserverBase`2::observer
	RuntimeObject* ___observer_0;
	// System.IDisposable UniRx.Operators.OperatorObserverBase`2::cancel
	RuntimeObject* ___cancel_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___observer_0), (void*)value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00, ___cancel_1)); }
	inline RuntimeObject* get_cancel_1() const { return ___cancel_1; }
	inline RuntimeObject** get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(RuntimeObject* value)
	{
		___cancel_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cancel_1), (void*)value);
	}
};


// UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>
struct  OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0  : public RuntimeObject
{
public:
	// System.IObserver`1<TResult> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.OperatorObserverBase`2::observer
	RuntimeObject* ___observer_0;
	// System.IDisposable UniRx.Operators.OperatorObserverBase`2::cancel
	RuntimeObject* ___cancel_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___observer_0), (void*)value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0, ___cancel_1)); }
	inline RuntimeObject* get_cancel_1() const { return ___cancel_1; }
	inline RuntimeObject** get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(RuntimeObject* value)
	{
		___cancel_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cancel_1), (void*)value);
	}
};


// UniRx.Operators.WhereObservable`1_<>c__DisplayClass5_0<System.Boolean>
struct  U3CU3Ec__DisplayClass5_0_t33976DE27D27D56496F94D672F06437B92399781  : public RuntimeObject
{
public:
	// UniRx.Operators.WhereObservable`1<T> UniRx.Operators.WhereObservable`1_<>c__DisplayClass5_0::<>4__this
	WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * ___U3CU3E4__this_0;
	// System.Func`2<T,System.Boolean> UniRx.Operators.WhereObservable`1_<>c__DisplayClass5_0::combinePredicate
	Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * ___combinePredicate_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t33976DE27D27D56496F94D672F06437B92399781, ___U3CU3E4__this_0)); }
	inline WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_combinePredicate_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t33976DE27D27D56496F94D672F06437B92399781, ___combinePredicate_1)); }
	inline Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * get_combinePredicate_1() const { return ___combinePredicate_1; }
	inline Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B ** get_address_of_combinePredicate_1() { return &___combinePredicate_1; }
	inline void set_combinePredicate_1(Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * value)
	{
		___combinePredicate_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___combinePredicate_1), (void*)value);
	}
};


// UniRx.Operators.WhereObservable`1_<>c__DisplayClass5_0<System.Int64>
struct  U3CU3Ec__DisplayClass5_0_tEE3C846FB04A6F0261B3DD92D4D0D18439DBF468  : public RuntimeObject
{
public:
	// UniRx.Operators.WhereObservable`1<T> UniRx.Operators.WhereObservable`1_<>c__DisplayClass5_0::<>4__this
	WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * ___U3CU3E4__this_0;
	// System.Func`2<T,System.Boolean> UniRx.Operators.WhereObservable`1_<>c__DisplayClass5_0::combinePredicate
	Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * ___combinePredicate_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tEE3C846FB04A6F0261B3DD92D4D0D18439DBF468, ___U3CU3E4__this_0)); }
	inline WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_combinePredicate_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tEE3C846FB04A6F0261B3DD92D4D0D18439DBF468, ___combinePredicate_1)); }
	inline Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * get_combinePredicate_1() const { return ___combinePredicate_1; }
	inline Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 ** get_address_of_combinePredicate_1() { return &___combinePredicate_1; }
	inline void set_combinePredicate_1(Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * value)
	{
		___combinePredicate_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___combinePredicate_1), (void*)value);
	}
};


// UniRx.Operators.WhereObservable`1_<>c__DisplayClass5_0<System.Object>
struct  U3CU3Ec__DisplayClass5_0_t24107840801D474B1F350BA289D0DF100435FE33  : public RuntimeObject
{
public:
	// UniRx.Operators.WhereObservable`1<T> UniRx.Operators.WhereObservable`1_<>c__DisplayClass5_0::<>4__this
	WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * ___U3CU3E4__this_0;
	// System.Func`2<T,System.Boolean> UniRx.Operators.WhereObservable`1_<>c__DisplayClass5_0::combinePredicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___combinePredicate_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t24107840801D474B1F350BA289D0DF100435FE33, ___U3CU3E4__this_0)); }
	inline WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_combinePredicate_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t24107840801D474B1F350BA289D0DF100435FE33, ___combinePredicate_1)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_combinePredicate_1() const { return ___combinePredicate_1; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_combinePredicate_1() { return &___combinePredicate_1; }
	inline void set_combinePredicate_1(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___combinePredicate_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___combinePredicate_1), (void*)value);
	}
};


// UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom_LeftObserver<System.Object,System.Object,System.Object>
struct  LeftObserver_t5BAAD1BFF3A1BBEB6F0AE6F277E47434FF56E78C  : public RuntimeObject
{
public:
	// UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom<TLeft,TRight,TResult> UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom_LeftObserver::parent
	WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(LeftObserver_t5BAAD1BFF3A1BBEB6F0AE6F277E47434FF56E78C, ___parent_0)); }
	inline WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF * get_parent_0() const { return ___parent_0; }
	inline WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_0), (void*)value);
	}
};


// UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom_RightObserver<System.Object,System.Object,System.Object>
struct  RightObserver_t6CDD16943B9A79661E77D64D290EA3BFAEBC572B  : public RuntimeObject
{
public:
	// UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom<TLeft,TRight,TResult> UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom_RightObserver::parent
	WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF * ___parent_0;
	// System.IDisposable UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom_RightObserver::selfSubscription
	RuntimeObject* ___selfSubscription_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(RightObserver_t6CDD16943B9A79661E77D64D290EA3BFAEBC572B, ___parent_0)); }
	inline WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF * get_parent_0() const { return ___parent_0; }
	inline WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_0), (void*)value);
	}

	inline static int32_t get_offset_of_selfSubscription_1() { return static_cast<int32_t>(offsetof(RightObserver_t6CDD16943B9A79661E77D64D290EA3BFAEBC572B, ___selfSubscription_1)); }
	inline RuntimeObject* get_selfSubscription_1() const { return ___selfSubscription_1; }
	inline RuntimeObject** get_address_of_selfSubscription_1() { return &___selfSubscription_1; }
	inline void set_selfSubscription_1(RuntimeObject* value)
	{
		___selfSubscription_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selfSubscription_1), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`1_ZipLatest_ZipLatestObserver<System.Object>
struct  ZipLatestObserver_t1916CBEFD17AA1674F927430EF42C7EAEB835507  : public RuntimeObject
{
public:
	// UniRx.Operators.ZipLatestObservable`1_ZipLatest<T> UniRx.Operators.ZipLatestObservable`1_ZipLatest_ZipLatestObserver::parent
	ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * ___parent_0;
	// System.Int32 UniRx.Operators.ZipLatestObservable`1_ZipLatest_ZipLatestObserver::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(ZipLatestObserver_t1916CBEFD17AA1674F927430EF42C7EAEB835507, ___parent_0)); }
	inline ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * get_parent_0() const { return ___parent_0; }
	inline ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(ZipLatestObserver_t1916CBEFD17AA1674F927430EF42C7EAEB835507, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};


// UniRx.Operators.ZipLatestObservable`3_ZipLatest_LeftObserver<System.Object,System.Object,System.Object>
struct  LeftObserver_t7E253BA735D35FF82CE6EB117CCB0BC66532812E  : public RuntimeObject
{
public:
	// UniRx.Operators.ZipLatestObservable`3_ZipLatest<TLeft,TRight,TResult> UniRx.Operators.ZipLatestObservable`3_ZipLatest_LeftObserver::parent
	ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(LeftObserver_t7E253BA735D35FF82CE6EB117CCB0BC66532812E, ___parent_0)); }
	inline ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 * get_parent_0() const { return ___parent_0; }
	inline ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_0), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`3_ZipLatest_RightObserver<System.Object,System.Object,System.Object>
struct  RightObserver_tB3720D149C18F78108C0B9CBA08EE73EE963CE23  : public RuntimeObject
{
public:
	// UniRx.Operators.ZipLatestObservable`3_ZipLatest<TLeft,TRight,TResult> UniRx.Operators.ZipLatestObservable`3_ZipLatest_RightObserver::parent
	ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(RightObserver_tB3720D149C18F78108C0B9CBA08EE73EE963CE23, ___parent_0)); }
	inline ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 * get_parent_0() const { return ___parent_0; }
	inline ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_0), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObserver`1<System.Object>
struct  ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8  : public RuntimeObject
{
public:
	// System.Object UniRx.Operators.ZipLatestObserver`1::gate
	RuntimeObject * ___gate_0;
	// UniRx.Operators.IZipLatestObservable UniRx.Operators.ZipLatestObserver`1::parent
	RuntimeObject* ___parent_1;
	// System.Int32 UniRx.Operators.ZipLatestObserver`1::index
	int32_t ___index_2;
	// T UniRx.Operators.ZipLatestObserver`1::value
	RuntimeObject * ___value_3;

public:
	inline static int32_t get_offset_of_gate_0() { return static_cast<int32_t>(offsetof(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8, ___gate_0)); }
	inline RuntimeObject * get_gate_0() const { return ___gate_0; }
	inline RuntimeObject ** get_address_of_gate_0() { return &___gate_0; }
	inline void set_gate_0(RuntimeObject * value)
	{
		___gate_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_0), (void*)value);
	}

	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8, ___parent_1)); }
	inline RuntimeObject* get_parent_1() const { return ___parent_1; }
	inline RuntimeObject** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(RuntimeObject* value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_1), (void*)value);
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_3), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`1_Zip_ZipObserver<System.Object>
struct  ZipObserver_tAE56C446B45ECA54A1360388514A7D8851F5CC31  : public RuntimeObject
{
public:
	// UniRx.Operators.ZipObservable`1_Zip<T> UniRx.Operators.ZipObservable`1_Zip_ZipObserver::parent
	Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * ___parent_0;
	// System.Int32 UniRx.Operators.ZipObservable`1_Zip_ZipObserver::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(ZipObserver_tAE56C446B45ECA54A1360388514A7D8851F5CC31, ___parent_0)); }
	inline Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * get_parent_0() const { return ___parent_0; }
	inline Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(ZipObserver_tAE56C446B45ECA54A1360388514A7D8851F5CC31, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};


// UniRx.Operators.ZipObservable`3_Zip_LeftZipObserver<System.Object,System.Object,System.Object>
struct  LeftZipObserver_t3BEB841B64A20E855B75C20E8901F72597D033BA  : public RuntimeObject
{
public:
	// UniRx.Operators.ZipObservable`3_Zip<TLeft,TRight,TResult> UniRx.Operators.ZipObservable`3_Zip_LeftZipObserver::parent
	Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(LeftZipObserver_t3BEB841B64A20E855B75C20E8901F72597D033BA, ___parent_0)); }
	inline Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * get_parent_0() const { return ___parent_0; }
	inline Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_0), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`3_Zip_RightZipObserver<System.Object,System.Object,System.Object>
struct  RightZipObserver_t75FAA663E00278026507C33389FDDBA8FAFEFF80  : public RuntimeObject
{
public:
	// UniRx.Operators.ZipObservable`3_Zip<TLeft,TRight,TResult> UniRx.Operators.ZipObservable`3_Zip_RightZipObserver::parent
	Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(RightZipObserver_t75FAA663E00278026507C33389FDDBA8FAFEFF80, ___parent_0)); }
	inline Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * get_parent_0() const { return ___parent_0; }
	inline Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_0), (void*)value);
	}
};


// UniRx.Operators.ZipObserver`1<System.Object>
struct  ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78  : public RuntimeObject
{
public:
	// System.Object UniRx.Operators.ZipObserver`1::gate
	RuntimeObject * ___gate_0;
	// UniRx.Operators.IZipObservable UniRx.Operators.ZipObserver`1::parent
	RuntimeObject* ___parent_1;
	// System.Int32 UniRx.Operators.ZipObserver`1::index
	int32_t ___index_2;
	// System.Collections.Generic.Queue`1<T> UniRx.Operators.ZipObserver`1::queue
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___queue_3;

public:
	inline static int32_t get_offset_of_gate_0() { return static_cast<int32_t>(offsetof(ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78, ___gate_0)); }
	inline RuntimeObject * get_gate_0() const { return ___gate_0; }
	inline RuntimeObject ** get_address_of_gate_0() { return &___gate_0; }
	inline void set_gate_0(RuntimeObject * value)
	{
		___gate_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_0), (void*)value);
	}

	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78, ___parent_1)); }
	inline RuntimeObject* get_parent_1() const { return ___parent_1; }
	inline RuntimeObject** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(RuntimeObject* value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_1), (void*)value);
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_queue_3() { return static_cast<int32_t>(offsetof(ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78, ___queue_3)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_queue_3() const { return ___queue_3; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_queue_3() { return &___queue_3; }
	inline void set_queue_3(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___queue_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___queue_3), (void*)value);
	}
};


// UniRx.SingleAssignmentDisposable
struct  SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC  : public RuntimeObject
{
public:
	// System.Object UniRx.SingleAssignmentDisposable::gate
	RuntimeObject * ___gate_0;
	// System.IDisposable UniRx.SingleAssignmentDisposable::current
	RuntimeObject* ___current_1;
	// System.Boolean UniRx.SingleAssignmentDisposable::disposed
	bool ___disposed_2;

public:
	inline static int32_t get_offset_of_gate_0() { return static_cast<int32_t>(offsetof(SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC, ___gate_0)); }
	inline RuntimeObject * get_gate_0() const { return ___gate_0; }
	inline RuntimeObject ** get_address_of_gate_0() { return &___gate_0; }
	inline void set_gate_0(RuntimeObject * value)
	{
		___gate_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_0), (void*)value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC, ___current_1)); }
	inline RuntimeObject* get_current_1() const { return ___current_1; }
	inline RuntimeObject** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(RuntimeObject* value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_1), (void*)value);
	}

	inline static int32_t get_offset_of_disposed_2() { return static_cast<int32_t>(offsetof(SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC, ___disposed_2)); }
	inline bool get_disposed_2() const { return ___disposed_2; }
	inline bool* get_address_of_disposed_2() { return &___disposed_2; }
	inline void set_disposed_2(bool value)
	{
		___disposed_2 = value;
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int64
struct  Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UniRx.Operators.NthZipLatestObserverBase`1<System.Object>
struct  NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D  : public OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0
{
public:
	// System.Int32 UniRx.Operators.NthZipLatestObserverBase`1::length
	int32_t ___length_2;
	// System.Boolean[] UniRx.Operators.NthZipLatestObserverBase`1::isStarted
	BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* ___isStarted_3;
	// System.Boolean[] UniRx.Operators.NthZipLatestObserverBase`1::isCompleted
	BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* ___isCompleted_4;

public:
	inline static int32_t get_offset_of_length_2() { return static_cast<int32_t>(offsetof(NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D, ___length_2)); }
	inline int32_t get_length_2() const { return ___length_2; }
	inline int32_t* get_address_of_length_2() { return &___length_2; }
	inline void set_length_2(int32_t value)
	{
		___length_2 = value;
	}

	inline static int32_t get_offset_of_isStarted_3() { return static_cast<int32_t>(offsetof(NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D, ___isStarted_3)); }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* get_isStarted_3() const { return ___isStarted_3; }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C** get_address_of_isStarted_3() { return &___isStarted_3; }
	inline void set_isStarted_3(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* value)
	{
		___isStarted_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___isStarted_3), (void*)value);
	}

	inline static int32_t get_offset_of_isCompleted_4() { return static_cast<int32_t>(offsetof(NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D, ___isCompleted_4)); }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* get_isCompleted_4() const { return ___isCompleted_4; }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C** get_address_of_isCompleted_4() { return &___isCompleted_4; }
	inline void set_isCompleted_4(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* value)
	{
		___isCompleted_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___isCompleted_4), (void*)value);
	}
};


// UniRx.Operators.NthZipObserverBase`1<System.Object>
struct  NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677  : public OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0
{
public:
	// System.Collections.ICollection[] UniRx.Operators.NthZipObserverBase`1::queues
	ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* ___queues_2;
	// System.Boolean[] UniRx.Operators.NthZipObserverBase`1::isDone
	BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* ___isDone_3;
	// System.Int32 UniRx.Operators.NthZipObserverBase`1::length
	int32_t ___length_4;

public:
	inline static int32_t get_offset_of_queues_2() { return static_cast<int32_t>(offsetof(NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677, ___queues_2)); }
	inline ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* get_queues_2() const { return ___queues_2; }
	inline ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8** get_address_of_queues_2() { return &___queues_2; }
	inline void set_queues_2(ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* value)
	{
		___queues_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___queues_2), (void*)value);
	}

	inline static int32_t get_offset_of_isDone_3() { return static_cast<int32_t>(offsetof(NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677, ___isDone_3)); }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* get_isDone_3() const { return ___isDone_3; }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C** get_address_of_isDone_3() { return &___isDone_3; }
	inline void set_isDone_3(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* value)
	{
		___isDone_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___isDone_3), (void*)value);
	}

	inline static int32_t get_offset_of_length_4() { return static_cast<int32_t>(offsetof(NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677, ___length_4)); }
	inline int32_t get_length_4() const { return ___length_4; }
	inline int32_t* get_address_of_length_4() { return &___length_4; }
	inline void set_length_4(int32_t value)
	{
		___length_4 = value;
	}
};


// UniRx.Operators.WhereObservable`1_Where<System.Boolean>
struct  Where_tFCD764D70CF94AA9835D7814899E259A429FA17C  : public OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4
{
public:
	// UniRx.Operators.WhereObservable`1<T> UniRx.Operators.WhereObservable`1_Where::parent
	WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Where_tFCD764D70CF94AA9835D7814899E259A429FA17C, ___parent_2)); }
	inline WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * get_parent_2() const { return ___parent_2; }
	inline WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}
};


// UniRx.Operators.WhereObservable`1_Where<System.Int64>
struct  Where_t97078FF7350BA11174458F7A8E25B57004E2B900  : public OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A
{
public:
	// UniRx.Operators.WhereObservable`1<T> UniRx.Operators.WhereObservable`1_Where::parent
	WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Where_t97078FF7350BA11174458F7A8E25B57004E2B900, ___parent_2)); }
	inline WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * get_parent_2() const { return ___parent_2; }
	inline WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}
};


// UniRx.Operators.WhereObservable`1_Where<System.Object>
struct  Where_t6008305F5D07BBFE21D61C328E8EBD66A350D97B  : public OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0
{
public:
	// UniRx.Operators.WhereObservable`1<T> UniRx.Operators.WhereObservable`1_Where::parent
	WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Where_t6008305F5D07BBFE21D61C328E8EBD66A350D97B, ___parent_2)); }
	inline WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * get_parent_2() const { return ___parent_2; }
	inline WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}
};


// UniRx.Operators.WhereObservable`1_Where_<System.Boolean>
struct  Where__t4B5EA5BC9EA34076FAD0EE0F30CD814FA7506B60  : public OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4
{
public:
	// UniRx.Operators.WhereObservable`1<T> UniRx.Operators.WhereObservable`1_Where_::parent
	WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * ___parent_2;
	// System.Int32 UniRx.Operators.WhereObservable`1_Where_::index
	int32_t ___index_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Where__t4B5EA5BC9EA34076FAD0EE0F30CD814FA7506B60, ___parent_2)); }
	inline WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * get_parent_2() const { return ___parent_2; }
	inline WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(Where__t4B5EA5BC9EA34076FAD0EE0F30CD814FA7506B60, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}
};


// UniRx.Operators.WhereObservable`1_Where_<System.Int64>
struct  Where__t5E913FC3047D357D4D04A0BF656999DBCBF4C6C5  : public OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A
{
public:
	// UniRx.Operators.WhereObservable`1<T> UniRx.Operators.WhereObservable`1_Where_::parent
	WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * ___parent_2;
	// System.Int32 UniRx.Operators.WhereObservable`1_Where_::index
	int32_t ___index_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Where__t5E913FC3047D357D4D04A0BF656999DBCBF4C6C5, ___parent_2)); }
	inline WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * get_parent_2() const { return ___parent_2; }
	inline WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(Where__t5E913FC3047D357D4D04A0BF656999DBCBF4C6C5, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}
};


// UniRx.Operators.WhereObservable`1_Where_<System.Object>
struct  Where__tC4BFE268565371D15C47C91FF86D6E2F822710EB  : public OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0
{
public:
	// UniRx.Operators.WhereObservable`1<T> UniRx.Operators.WhereObservable`1_Where_::parent
	WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * ___parent_2;
	// System.Int32 UniRx.Operators.WhereObservable`1_Where_::index
	int32_t ___index_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Where__tC4BFE268565371D15C47C91FF86D6E2F822710EB, ___parent_2)); }
	inline WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * get_parent_2() const { return ___parent_2; }
	inline WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(Where__tC4BFE268565371D15C47C91FF86D6E2F822710EB, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}
};


// UniRx.Operators.WhereObservable`1<System.Boolean>
struct  WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B  : public OperatorObservableBase_1_t530D2FE2B1B3F6717F04CEB073301DD8A6848C1B
{
public:
	// System.IObservable`1<T> UniRx.Operators.WhereObservable`1::source
	RuntimeObject* ___source_1;
	// System.Func`2<T,System.Boolean> UniRx.Operators.WhereObservable`1::predicate
	Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * ___predicate_2;
	// System.Func`3<T,System.Int32,System.Boolean> UniRx.Operators.WhereObservable`1::predicateWithIndex
	Func_3_t3F60B00D281681419CA999636A62D3ED1B529778 * ___predicateWithIndex_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_1), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_2() { return static_cast<int32_t>(offsetof(WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B, ___predicate_2)); }
	inline Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * get_predicate_2() const { return ___predicate_2; }
	inline Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B ** get_address_of_predicate_2() { return &___predicate_2; }
	inline void set_predicate_2(Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * value)
	{
		___predicate_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_2), (void*)value);
	}

	inline static int32_t get_offset_of_predicateWithIndex_3() { return static_cast<int32_t>(offsetof(WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B, ___predicateWithIndex_3)); }
	inline Func_3_t3F60B00D281681419CA999636A62D3ED1B529778 * get_predicateWithIndex_3() const { return ___predicateWithIndex_3; }
	inline Func_3_t3F60B00D281681419CA999636A62D3ED1B529778 ** get_address_of_predicateWithIndex_3() { return &___predicateWithIndex_3; }
	inline void set_predicateWithIndex_3(Func_3_t3F60B00D281681419CA999636A62D3ED1B529778 * value)
	{
		___predicateWithIndex_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicateWithIndex_3), (void*)value);
	}
};


// UniRx.Operators.WhereObservable`1<System.Int64>
struct  WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F  : public OperatorObservableBase_1_t59F31167F0E3596ED1B2C8679413A193C2D468FD
{
public:
	// System.IObservable`1<T> UniRx.Operators.WhereObservable`1::source
	RuntimeObject* ___source_1;
	// System.Func`2<T,System.Boolean> UniRx.Operators.WhereObservable`1::predicate
	Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * ___predicate_2;
	// System.Func`3<T,System.Int32,System.Boolean> UniRx.Operators.WhereObservable`1::predicateWithIndex
	Func_3_t8FFBB3815B984FFFA353813F9E09AEA69D8DB623 * ___predicateWithIndex_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_1), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_2() { return static_cast<int32_t>(offsetof(WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F, ___predicate_2)); }
	inline Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * get_predicate_2() const { return ___predicate_2; }
	inline Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 ** get_address_of_predicate_2() { return &___predicate_2; }
	inline void set_predicate_2(Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * value)
	{
		___predicate_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_2), (void*)value);
	}

	inline static int32_t get_offset_of_predicateWithIndex_3() { return static_cast<int32_t>(offsetof(WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F, ___predicateWithIndex_3)); }
	inline Func_3_t8FFBB3815B984FFFA353813F9E09AEA69D8DB623 * get_predicateWithIndex_3() const { return ___predicateWithIndex_3; }
	inline Func_3_t8FFBB3815B984FFFA353813F9E09AEA69D8DB623 ** get_address_of_predicateWithIndex_3() { return &___predicateWithIndex_3; }
	inline void set_predicateWithIndex_3(Func_3_t8FFBB3815B984FFFA353813F9E09AEA69D8DB623 * value)
	{
		___predicateWithIndex_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicateWithIndex_3), (void*)value);
	}
};


// UniRx.Operators.WhereObservable`1<System.Object>
struct  WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<T> UniRx.Operators.WhereObservable`1::source
	RuntimeObject* ___source_1;
	// System.Func`2<T,System.Boolean> UniRx.Operators.WhereObservable`1::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_2;
	// System.Func`3<T,System.Int32,System.Boolean> UniRx.Operators.WhereObservable`1::predicateWithIndex
	Func_3_tCB498436E1553228C2C36F41918548DB2DF10E0A * ___predicateWithIndex_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_1), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_2() { return static_cast<int32_t>(offsetof(WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01, ___predicate_2)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_2() const { return ___predicate_2; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_2() { return &___predicate_2; }
	inline void set_predicate_2(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_2), (void*)value);
	}

	inline static int32_t get_offset_of_predicateWithIndex_3() { return static_cast<int32_t>(offsetof(WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01, ___predicateWithIndex_3)); }
	inline Func_3_tCB498436E1553228C2C36F41918548DB2DF10E0A * get_predicateWithIndex_3() const { return ___predicateWithIndex_3; }
	inline Func_3_tCB498436E1553228C2C36F41918548DB2DF10E0A ** get_address_of_predicateWithIndex_3() { return &___predicateWithIndex_3; }
	inline void set_predicateWithIndex_3(Func_3_tCB498436E1553228C2C36F41918548DB2DF10E0A * value)
	{
		___predicateWithIndex_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicateWithIndex_3), (void*)value);
	}
};


// UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Boolean,System.Boolean>
struct  WhereSelect_t2C19B6051587E495A79CF7134957F51AB0F8CB99  : public OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4
{
public:
	// UniRx.Operators.WhereSelectObservable`2<T,TR> UniRx.Operators.WhereSelectObservable`2_WhereSelect::parent
	WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(WhereSelect_t2C19B6051587E495A79CF7134957F51AB0F8CB99, ___parent_2)); }
	inline WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C * get_parent_2() const { return ___parent_2; }
	inline WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}
};


// UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Object,System.Boolean>
struct  WhereSelect_t7CA94D47BE319C6E9D59A58019438DA0D06C8719  : public OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00
{
public:
	// UniRx.Operators.WhereSelectObservable`2<T,TR> UniRx.Operators.WhereSelectObservable`2_WhereSelect::parent
	WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(WhereSelect_t7CA94D47BE319C6E9D59A58019438DA0D06C8719, ___parent_2)); }
	inline WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 * get_parent_2() const { return ___parent_2; }
	inline WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}
};


// UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Object,System.Object>
struct  WhereSelect_t6984636B2863BAE990462A289EC7A8841F77157D  : public OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0
{
public:
	// UniRx.Operators.WhereSelectObservable`2<T,TR> UniRx.Operators.WhereSelectObservable`2_WhereSelect::parent
	WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(WhereSelect_t6984636B2863BAE990462A289EC7A8841F77157D, ___parent_2)); }
	inline WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F * get_parent_2() const { return ___parent_2; }
	inline WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}
};


// UniRx.Operators.WhereSelectObservable`2<System.Boolean,System.Boolean>
struct  WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C  : public OperatorObservableBase_1_t530D2FE2B1B3F6717F04CEB073301DD8A6848C1B
{
public:
	// System.IObservable`1<T> UniRx.Operators.WhereSelectObservable`2::source
	RuntimeObject* ___source_1;
	// System.Func`2<T,System.Boolean> UniRx.Operators.WhereSelectObservable`2::predicate
	Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * ___predicate_2;
	// System.Func`2<T,TR> UniRx.Operators.WhereSelectObservable`2::selector
	Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * ___selector_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_1), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_2() { return static_cast<int32_t>(offsetof(WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C, ___predicate_2)); }
	inline Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * get_predicate_2() const { return ___predicate_2; }
	inline Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B ** get_address_of_predicate_2() { return &___predicate_2; }
	inline void set_predicate_2(Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * value)
	{
		___predicate_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_2), (void*)value);
	}

	inline static int32_t get_offset_of_selector_3() { return static_cast<int32_t>(offsetof(WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C, ___selector_3)); }
	inline Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * get_selector_3() const { return ___selector_3; }
	inline Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B ** get_address_of_selector_3() { return &___selector_3; }
	inline void set_selector_3(Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * value)
	{
		___selector_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_3), (void*)value);
	}
};


// UniRx.Operators.WhereSelectObservable`2<System.Object,System.Boolean>
struct  WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870  : public OperatorObservableBase_1_t530D2FE2B1B3F6717F04CEB073301DD8A6848C1B
{
public:
	// System.IObservable`1<T> UniRx.Operators.WhereSelectObservable`2::source
	RuntimeObject* ___source_1;
	// System.Func`2<T,System.Boolean> UniRx.Operators.WhereSelectObservable`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_2;
	// System.Func`2<T,TR> UniRx.Operators.WhereSelectObservable`2::selector
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___selector_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_1), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_2() { return static_cast<int32_t>(offsetof(WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870, ___predicate_2)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_2() const { return ___predicate_2; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_2() { return &___predicate_2; }
	inline void set_predicate_2(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_2), (void*)value);
	}

	inline static int32_t get_offset_of_selector_3() { return static_cast<int32_t>(offsetof(WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870, ___selector_3)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_selector_3() const { return ___selector_3; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_selector_3() { return &___selector_3; }
	inline void set_selector_3(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___selector_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_3), (void*)value);
	}
};


// UniRx.Operators.WhereSelectObservable`2<System.Object,System.Object>
struct  WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<T> UniRx.Operators.WhereSelectObservable`2::source
	RuntimeObject* ___source_1;
	// System.Func`2<T,System.Boolean> UniRx.Operators.WhereSelectObservable`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_2;
	// System.Func`2<T,TR> UniRx.Operators.WhereSelectObservable`2::selector
	Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_1), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_2() { return static_cast<int32_t>(offsetof(WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F, ___predicate_2)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_2() const { return ___predicate_2; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_2() { return &___predicate_2; }
	inline void set_predicate_2(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_2), (void*)value);
	}

	inline static int32_t get_offset_of_selector_3() { return static_cast<int32_t>(offsetof(WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F, ___selector_3)); }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * get_selector_3() const { return ___selector_3; }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 ** get_address_of_selector_3() { return &___selector_3; }
	inline void set_selector_3(Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * value)
	{
		___selector_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_3), (void*)value);
	}
};


// UniRx.Operators.WithLatestFromObservable`3<System.Object,System.Object,System.Object>
struct  WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<TLeft> UniRx.Operators.WithLatestFromObservable`3::left
	RuntimeObject* ___left_1;
	// System.IObservable`1<TRight> UniRx.Operators.WithLatestFromObservable`3::right
	RuntimeObject* ___right_2;
	// System.Func`3<TLeft,TRight,TResult> UniRx.Operators.WithLatestFromObservable`3::selector
	Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * ___selector_3;

public:
	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124, ___left_1)); }
	inline RuntimeObject* get_left_1() const { return ___left_1; }
	inline RuntimeObject** get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(RuntimeObject* value)
	{
		___left_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___left_1), (void*)value);
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124, ___right_2)); }
	inline RuntimeObject* get_right_2() const { return ___right_2; }
	inline RuntimeObject** get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(RuntimeObject* value)
	{
		___right_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___right_2), (void*)value);
	}

	inline static int32_t get_offset_of_selector_3() { return static_cast<int32_t>(offsetof(WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124, ___selector_3)); }
	inline Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * get_selector_3() const { return ___selector_3; }
	inline Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 ** get_address_of_selector_3() { return &___selector_3; }
	inline void set_selector_3(Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * value)
	{
		___selector_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_3), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`1_ZipLatest<System.Object>
struct  ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8  : public OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6
{
public:
	// UniRx.Operators.ZipLatestObservable`1<T> UniRx.Operators.ZipLatestObservable`1_ZipLatest::parent
	ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 * ___parent_2;
	// System.Object UniRx.Operators.ZipLatestObservable`1_ZipLatest::gate
	RuntimeObject * ___gate_3;
	// System.Int32 UniRx.Operators.ZipLatestObservable`1_ZipLatest::length
	int32_t ___length_4;
	// T[] UniRx.Operators.ZipLatestObservable`1_ZipLatest::values
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___values_5;
	// System.Boolean[] UniRx.Operators.ZipLatestObservable`1_ZipLatest::isStarted
	BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* ___isStarted_6;
	// System.Boolean[] UniRx.Operators.ZipLatestObservable`1_ZipLatest::isCompleted
	BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* ___isCompleted_7;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8, ___parent_2)); }
	inline ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 * get_parent_2() const { return ___parent_2; }
	inline ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8, ___gate_3)); }
	inline RuntimeObject * get_gate_3() const { return ___gate_3; }
	inline RuntimeObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(RuntimeObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_3), (void*)value);
	}

	inline static int32_t get_offset_of_length_4() { return static_cast<int32_t>(offsetof(ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8, ___length_4)); }
	inline int32_t get_length_4() const { return ___length_4; }
	inline int32_t* get_address_of_length_4() { return &___length_4; }
	inline void set_length_4(int32_t value)
	{
		___length_4 = value;
	}

	inline static int32_t get_offset_of_values_5() { return static_cast<int32_t>(offsetof(ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8, ___values_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_values_5() const { return ___values_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_values_5() { return &___values_5; }
	inline void set_values_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___values_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_5), (void*)value);
	}

	inline static int32_t get_offset_of_isStarted_6() { return static_cast<int32_t>(offsetof(ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8, ___isStarted_6)); }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* get_isStarted_6() const { return ___isStarted_6; }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C** get_address_of_isStarted_6() { return &___isStarted_6; }
	inline void set_isStarted_6(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* value)
	{
		___isStarted_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___isStarted_6), (void*)value);
	}

	inline static int32_t get_offset_of_isCompleted_7() { return static_cast<int32_t>(offsetof(ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8, ___isCompleted_7)); }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* get_isCompleted_7() const { return ___isCompleted_7; }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C** get_address_of_isCompleted_7() { return &___isCompleted_7; }
	inline void set_isCompleted_7(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* value)
	{
		___isCompleted_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___isCompleted_7), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`1<System.Object>
struct  ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65  : public OperatorObservableBase_1_tB16C98CA8ACB1947DC9798221FBEED187248DFB0
{
public:
	// System.IObservable`1<T>[] UniRx.Operators.ZipLatestObservable`1::sources
	IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* ___sources_1;

public:
	inline static int32_t get_offset_of_sources_1() { return static_cast<int32_t>(offsetof(ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65, ___sources_1)); }
	inline IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* get_sources_1() const { return ___sources_1; }
	inline IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B** get_address_of_sources_1() { return &___sources_1; }
	inline void set_sources_1(IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* value)
	{
		___sources_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sources_1), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`3_ZipLatest<System.Object,System.Object,System.Object>
struct  ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95  : public OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0
{
public:
	// UniRx.Operators.ZipLatestObservable`3<TLeft,TRight,TResult> UniRx.Operators.ZipLatestObservable`3_ZipLatest::parent
	ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 * ___parent_2;
	// System.Object UniRx.Operators.ZipLatestObservable`3_ZipLatest::gate
	RuntimeObject * ___gate_3;
	// TLeft UniRx.Operators.ZipLatestObservable`3_ZipLatest::leftValue
	RuntimeObject * ___leftValue_4;
	// System.Boolean UniRx.Operators.ZipLatestObservable`3_ZipLatest::leftStarted
	bool ___leftStarted_5;
	// System.Boolean UniRx.Operators.ZipLatestObservable`3_ZipLatest::leftCompleted
	bool ___leftCompleted_6;
	// TRight UniRx.Operators.ZipLatestObservable`3_ZipLatest::rightValue
	RuntimeObject * ___rightValue_7;
	// System.Boolean UniRx.Operators.ZipLatestObservable`3_ZipLatest::rightStarted
	bool ___rightStarted_8;
	// System.Boolean UniRx.Operators.ZipLatestObservable`3_ZipLatest::rightCompleted
	bool ___rightCompleted_9;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95, ___parent_2)); }
	inline ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 * get_parent_2() const { return ___parent_2; }
	inline ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95, ___gate_3)); }
	inline RuntimeObject * get_gate_3() const { return ___gate_3; }
	inline RuntimeObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(RuntimeObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_3), (void*)value);
	}

	inline static int32_t get_offset_of_leftValue_4() { return static_cast<int32_t>(offsetof(ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95, ___leftValue_4)); }
	inline RuntimeObject * get_leftValue_4() const { return ___leftValue_4; }
	inline RuntimeObject ** get_address_of_leftValue_4() { return &___leftValue_4; }
	inline void set_leftValue_4(RuntimeObject * value)
	{
		___leftValue_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___leftValue_4), (void*)value);
	}

	inline static int32_t get_offset_of_leftStarted_5() { return static_cast<int32_t>(offsetof(ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95, ___leftStarted_5)); }
	inline bool get_leftStarted_5() const { return ___leftStarted_5; }
	inline bool* get_address_of_leftStarted_5() { return &___leftStarted_5; }
	inline void set_leftStarted_5(bool value)
	{
		___leftStarted_5 = value;
	}

	inline static int32_t get_offset_of_leftCompleted_6() { return static_cast<int32_t>(offsetof(ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95, ___leftCompleted_6)); }
	inline bool get_leftCompleted_6() const { return ___leftCompleted_6; }
	inline bool* get_address_of_leftCompleted_6() { return &___leftCompleted_6; }
	inline void set_leftCompleted_6(bool value)
	{
		___leftCompleted_6 = value;
	}

	inline static int32_t get_offset_of_rightValue_7() { return static_cast<int32_t>(offsetof(ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95, ___rightValue_7)); }
	inline RuntimeObject * get_rightValue_7() const { return ___rightValue_7; }
	inline RuntimeObject ** get_address_of_rightValue_7() { return &___rightValue_7; }
	inline void set_rightValue_7(RuntimeObject * value)
	{
		___rightValue_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rightValue_7), (void*)value);
	}

	inline static int32_t get_offset_of_rightStarted_8() { return static_cast<int32_t>(offsetof(ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95, ___rightStarted_8)); }
	inline bool get_rightStarted_8() const { return ___rightStarted_8; }
	inline bool* get_address_of_rightStarted_8() { return &___rightStarted_8; }
	inline void set_rightStarted_8(bool value)
	{
		___rightStarted_8 = value;
	}

	inline static int32_t get_offset_of_rightCompleted_9() { return static_cast<int32_t>(offsetof(ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95, ___rightCompleted_9)); }
	inline bool get_rightCompleted_9() const { return ___rightCompleted_9; }
	inline bool* get_address_of_rightCompleted_9() { return &___rightCompleted_9; }
	inline void set_rightCompleted_9(bool value)
	{
		___rightCompleted_9 = value;
	}
};


// UniRx.Operators.ZipLatestObservable`3<System.Object,System.Object,System.Object>
struct  ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<TLeft> UniRx.Operators.ZipLatestObservable`3::left
	RuntimeObject* ___left_1;
	// System.IObservable`1<TRight> UniRx.Operators.ZipLatestObservable`3::right
	RuntimeObject* ___right_2;
	// System.Func`3<TLeft,TRight,TResult> UniRx.Operators.ZipLatestObservable`3::selector
	Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * ___selector_3;

public:
	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493, ___left_1)); }
	inline RuntimeObject* get_left_1() const { return ___left_1; }
	inline RuntimeObject** get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(RuntimeObject* value)
	{
		___left_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___left_1), (void*)value);
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493, ___right_2)); }
	inline RuntimeObject* get_right_2() const { return ___right_2; }
	inline RuntimeObject** get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(RuntimeObject* value)
	{
		___right_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___right_2), (void*)value);
	}

	inline static int32_t get_offset_of_selector_3() { return static_cast<int32_t>(offsetof(ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493, ___selector_3)); }
	inline Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * get_selector_3() const { return ___selector_3; }
	inline Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 ** get_address_of_selector_3() { return &___selector_3; }
	inline void set_selector_3(Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * value)
	{
		___selector_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_3), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`4<System.Object,System.Object,System.Object,System.Object>
struct  ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<T1> UniRx.Operators.ZipLatestObservable`4::source1
	RuntimeObject* ___source1_1;
	// System.IObservable`1<T2> UniRx.Operators.ZipLatestObservable`4::source2
	RuntimeObject* ___source2_2;
	// System.IObservable`1<T3> UniRx.Operators.ZipLatestObservable`4::source3
	RuntimeObject* ___source3_3;
	// UniRx.Operators.ZipLatestFunc`4<T1,T2,T3,TR> UniRx.Operators.ZipLatestObservable`4::resultSelector
	ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 * ___resultSelector_4;

public:
	inline static int32_t get_offset_of_source1_1() { return static_cast<int32_t>(offsetof(ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0, ___source1_1)); }
	inline RuntimeObject* get_source1_1() const { return ___source1_1; }
	inline RuntimeObject** get_address_of_source1_1() { return &___source1_1; }
	inline void set_source1_1(RuntimeObject* value)
	{
		___source1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source1_1), (void*)value);
	}

	inline static int32_t get_offset_of_source2_2() { return static_cast<int32_t>(offsetof(ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0, ___source2_2)); }
	inline RuntimeObject* get_source2_2() const { return ___source2_2; }
	inline RuntimeObject** get_address_of_source2_2() { return &___source2_2; }
	inline void set_source2_2(RuntimeObject* value)
	{
		___source2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source2_2), (void*)value);
	}

	inline static int32_t get_offset_of_source3_3() { return static_cast<int32_t>(offsetof(ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0, ___source3_3)); }
	inline RuntimeObject* get_source3_3() const { return ___source3_3; }
	inline RuntimeObject** get_address_of_source3_3() { return &___source3_3; }
	inline void set_source3_3(RuntimeObject* value)
	{
		___source3_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source3_3), (void*)value);
	}

	inline static int32_t get_offset_of_resultSelector_4() { return static_cast<int32_t>(offsetof(ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0, ___resultSelector_4)); }
	inline ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 * get_resultSelector_4() const { return ___resultSelector_4; }
	inline ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 ** get_address_of_resultSelector_4() { return &___resultSelector_4; }
	inline void set_resultSelector_4(ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 * value)
	{
		___resultSelector_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resultSelector_4), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<T1> UniRx.Operators.ZipLatestObservable`5::source1
	RuntimeObject* ___source1_1;
	// System.IObservable`1<T2> UniRx.Operators.ZipLatestObservable`5::source2
	RuntimeObject* ___source2_2;
	// System.IObservable`1<T3> UniRx.Operators.ZipLatestObservable`5::source3
	RuntimeObject* ___source3_3;
	// System.IObservable`1<T4> UniRx.Operators.ZipLatestObservable`5::source4
	RuntimeObject* ___source4_4;
	// UniRx.Operators.ZipLatestFunc`5<T1,T2,T3,T4,TR> UniRx.Operators.ZipLatestObservable`5::resultSelector
	ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 * ___resultSelector_5;

public:
	inline static int32_t get_offset_of_source1_1() { return static_cast<int32_t>(offsetof(ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D, ___source1_1)); }
	inline RuntimeObject* get_source1_1() const { return ___source1_1; }
	inline RuntimeObject** get_address_of_source1_1() { return &___source1_1; }
	inline void set_source1_1(RuntimeObject* value)
	{
		___source1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source1_1), (void*)value);
	}

	inline static int32_t get_offset_of_source2_2() { return static_cast<int32_t>(offsetof(ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D, ___source2_2)); }
	inline RuntimeObject* get_source2_2() const { return ___source2_2; }
	inline RuntimeObject** get_address_of_source2_2() { return &___source2_2; }
	inline void set_source2_2(RuntimeObject* value)
	{
		___source2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source2_2), (void*)value);
	}

	inline static int32_t get_offset_of_source3_3() { return static_cast<int32_t>(offsetof(ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D, ___source3_3)); }
	inline RuntimeObject* get_source3_3() const { return ___source3_3; }
	inline RuntimeObject** get_address_of_source3_3() { return &___source3_3; }
	inline void set_source3_3(RuntimeObject* value)
	{
		___source3_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source3_3), (void*)value);
	}

	inline static int32_t get_offset_of_source4_4() { return static_cast<int32_t>(offsetof(ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D, ___source4_4)); }
	inline RuntimeObject* get_source4_4() const { return ___source4_4; }
	inline RuntimeObject** get_address_of_source4_4() { return &___source4_4; }
	inline void set_source4_4(RuntimeObject* value)
	{
		___source4_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source4_4), (void*)value);
	}

	inline static int32_t get_offset_of_resultSelector_5() { return static_cast<int32_t>(offsetof(ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D, ___resultSelector_5)); }
	inline ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 * get_resultSelector_5() const { return ___resultSelector_5; }
	inline ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 ** get_address_of_resultSelector_5() { return &___resultSelector_5; }
	inline void set_resultSelector_5(ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 * value)
	{
		___resultSelector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resultSelector_5), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<T1> UniRx.Operators.ZipLatestObservable`6::source1
	RuntimeObject* ___source1_1;
	// System.IObservable`1<T2> UniRx.Operators.ZipLatestObservable`6::source2
	RuntimeObject* ___source2_2;
	// System.IObservable`1<T3> UniRx.Operators.ZipLatestObservable`6::source3
	RuntimeObject* ___source3_3;
	// System.IObservable`1<T4> UniRx.Operators.ZipLatestObservable`6::source4
	RuntimeObject* ___source4_4;
	// System.IObservable`1<T5> UniRx.Operators.ZipLatestObservable`6::source5
	RuntimeObject* ___source5_5;
	// UniRx.Operators.ZipLatestFunc`6<T1,T2,T3,T4,T5,TR> UniRx.Operators.ZipLatestObservable`6::resultSelector
	ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F * ___resultSelector_6;

public:
	inline static int32_t get_offset_of_source1_1() { return static_cast<int32_t>(offsetof(ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645, ___source1_1)); }
	inline RuntimeObject* get_source1_1() const { return ___source1_1; }
	inline RuntimeObject** get_address_of_source1_1() { return &___source1_1; }
	inline void set_source1_1(RuntimeObject* value)
	{
		___source1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source1_1), (void*)value);
	}

	inline static int32_t get_offset_of_source2_2() { return static_cast<int32_t>(offsetof(ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645, ___source2_2)); }
	inline RuntimeObject* get_source2_2() const { return ___source2_2; }
	inline RuntimeObject** get_address_of_source2_2() { return &___source2_2; }
	inline void set_source2_2(RuntimeObject* value)
	{
		___source2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source2_2), (void*)value);
	}

	inline static int32_t get_offset_of_source3_3() { return static_cast<int32_t>(offsetof(ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645, ___source3_3)); }
	inline RuntimeObject* get_source3_3() const { return ___source3_3; }
	inline RuntimeObject** get_address_of_source3_3() { return &___source3_3; }
	inline void set_source3_3(RuntimeObject* value)
	{
		___source3_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source3_3), (void*)value);
	}

	inline static int32_t get_offset_of_source4_4() { return static_cast<int32_t>(offsetof(ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645, ___source4_4)); }
	inline RuntimeObject* get_source4_4() const { return ___source4_4; }
	inline RuntimeObject** get_address_of_source4_4() { return &___source4_4; }
	inline void set_source4_4(RuntimeObject* value)
	{
		___source4_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source4_4), (void*)value);
	}

	inline static int32_t get_offset_of_source5_5() { return static_cast<int32_t>(offsetof(ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645, ___source5_5)); }
	inline RuntimeObject* get_source5_5() const { return ___source5_5; }
	inline RuntimeObject** get_address_of_source5_5() { return &___source5_5; }
	inline void set_source5_5(RuntimeObject* value)
	{
		___source5_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source5_5), (void*)value);
	}

	inline static int32_t get_offset_of_resultSelector_6() { return static_cast<int32_t>(offsetof(ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645, ___resultSelector_6)); }
	inline ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F * get_resultSelector_6() const { return ___resultSelector_6; }
	inline ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F ** get_address_of_resultSelector_6() { return &___resultSelector_6; }
	inline void set_resultSelector_6(ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F * value)
	{
		___resultSelector_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resultSelector_6), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<T1> UniRx.Operators.ZipLatestObservable`7::source1
	RuntimeObject* ___source1_1;
	// System.IObservable`1<T2> UniRx.Operators.ZipLatestObservable`7::source2
	RuntimeObject* ___source2_2;
	// System.IObservable`1<T3> UniRx.Operators.ZipLatestObservable`7::source3
	RuntimeObject* ___source3_3;
	// System.IObservable`1<T4> UniRx.Operators.ZipLatestObservable`7::source4
	RuntimeObject* ___source4_4;
	// System.IObservable`1<T5> UniRx.Operators.ZipLatestObservable`7::source5
	RuntimeObject* ___source5_5;
	// System.IObservable`1<T6> UniRx.Operators.ZipLatestObservable`7::source6
	RuntimeObject* ___source6_6;
	// UniRx.Operators.ZipLatestFunc`7<T1,T2,T3,T4,T5,T6,TR> UniRx.Operators.ZipLatestObservable`7::resultSelector
	ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 * ___resultSelector_7;

public:
	inline static int32_t get_offset_of_source1_1() { return static_cast<int32_t>(offsetof(ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932, ___source1_1)); }
	inline RuntimeObject* get_source1_1() const { return ___source1_1; }
	inline RuntimeObject** get_address_of_source1_1() { return &___source1_1; }
	inline void set_source1_1(RuntimeObject* value)
	{
		___source1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source1_1), (void*)value);
	}

	inline static int32_t get_offset_of_source2_2() { return static_cast<int32_t>(offsetof(ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932, ___source2_2)); }
	inline RuntimeObject* get_source2_2() const { return ___source2_2; }
	inline RuntimeObject** get_address_of_source2_2() { return &___source2_2; }
	inline void set_source2_2(RuntimeObject* value)
	{
		___source2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source2_2), (void*)value);
	}

	inline static int32_t get_offset_of_source3_3() { return static_cast<int32_t>(offsetof(ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932, ___source3_3)); }
	inline RuntimeObject* get_source3_3() const { return ___source3_3; }
	inline RuntimeObject** get_address_of_source3_3() { return &___source3_3; }
	inline void set_source3_3(RuntimeObject* value)
	{
		___source3_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source3_3), (void*)value);
	}

	inline static int32_t get_offset_of_source4_4() { return static_cast<int32_t>(offsetof(ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932, ___source4_4)); }
	inline RuntimeObject* get_source4_4() const { return ___source4_4; }
	inline RuntimeObject** get_address_of_source4_4() { return &___source4_4; }
	inline void set_source4_4(RuntimeObject* value)
	{
		___source4_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source4_4), (void*)value);
	}

	inline static int32_t get_offset_of_source5_5() { return static_cast<int32_t>(offsetof(ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932, ___source5_5)); }
	inline RuntimeObject* get_source5_5() const { return ___source5_5; }
	inline RuntimeObject** get_address_of_source5_5() { return &___source5_5; }
	inline void set_source5_5(RuntimeObject* value)
	{
		___source5_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source5_5), (void*)value);
	}

	inline static int32_t get_offset_of_source6_6() { return static_cast<int32_t>(offsetof(ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932, ___source6_6)); }
	inline RuntimeObject* get_source6_6() const { return ___source6_6; }
	inline RuntimeObject** get_address_of_source6_6() { return &___source6_6; }
	inline void set_source6_6(RuntimeObject* value)
	{
		___source6_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source6_6), (void*)value);
	}

	inline static int32_t get_offset_of_resultSelector_7() { return static_cast<int32_t>(offsetof(ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932, ___resultSelector_7)); }
	inline ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 * get_resultSelector_7() const { return ___resultSelector_7; }
	inline ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 ** get_address_of_resultSelector_7() { return &___resultSelector_7; }
	inline void set_resultSelector_7(ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 * value)
	{
		___resultSelector_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resultSelector_7), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<T1> UniRx.Operators.ZipLatestObservable`8::source1
	RuntimeObject* ___source1_1;
	// System.IObservable`1<T2> UniRx.Operators.ZipLatestObservable`8::source2
	RuntimeObject* ___source2_2;
	// System.IObservable`1<T3> UniRx.Operators.ZipLatestObservable`8::source3
	RuntimeObject* ___source3_3;
	// System.IObservable`1<T4> UniRx.Operators.ZipLatestObservable`8::source4
	RuntimeObject* ___source4_4;
	// System.IObservable`1<T5> UniRx.Operators.ZipLatestObservable`8::source5
	RuntimeObject* ___source5_5;
	// System.IObservable`1<T6> UniRx.Operators.ZipLatestObservable`8::source6
	RuntimeObject* ___source6_6;
	// System.IObservable`1<T7> UniRx.Operators.ZipLatestObservable`8::source7
	RuntimeObject* ___source7_7;
	// UniRx.Operators.ZipLatestFunc`8<T1,T2,T3,T4,T5,T6,T7,TR> UniRx.Operators.ZipLatestObservable`8::resultSelector
	ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA * ___resultSelector_8;

public:
	inline static int32_t get_offset_of_source1_1() { return static_cast<int32_t>(offsetof(ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA, ___source1_1)); }
	inline RuntimeObject* get_source1_1() const { return ___source1_1; }
	inline RuntimeObject** get_address_of_source1_1() { return &___source1_1; }
	inline void set_source1_1(RuntimeObject* value)
	{
		___source1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source1_1), (void*)value);
	}

	inline static int32_t get_offset_of_source2_2() { return static_cast<int32_t>(offsetof(ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA, ___source2_2)); }
	inline RuntimeObject* get_source2_2() const { return ___source2_2; }
	inline RuntimeObject** get_address_of_source2_2() { return &___source2_2; }
	inline void set_source2_2(RuntimeObject* value)
	{
		___source2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source2_2), (void*)value);
	}

	inline static int32_t get_offset_of_source3_3() { return static_cast<int32_t>(offsetof(ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA, ___source3_3)); }
	inline RuntimeObject* get_source3_3() const { return ___source3_3; }
	inline RuntimeObject** get_address_of_source3_3() { return &___source3_3; }
	inline void set_source3_3(RuntimeObject* value)
	{
		___source3_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source3_3), (void*)value);
	}

	inline static int32_t get_offset_of_source4_4() { return static_cast<int32_t>(offsetof(ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA, ___source4_4)); }
	inline RuntimeObject* get_source4_4() const { return ___source4_4; }
	inline RuntimeObject** get_address_of_source4_4() { return &___source4_4; }
	inline void set_source4_4(RuntimeObject* value)
	{
		___source4_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source4_4), (void*)value);
	}

	inline static int32_t get_offset_of_source5_5() { return static_cast<int32_t>(offsetof(ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA, ___source5_5)); }
	inline RuntimeObject* get_source5_5() const { return ___source5_5; }
	inline RuntimeObject** get_address_of_source5_5() { return &___source5_5; }
	inline void set_source5_5(RuntimeObject* value)
	{
		___source5_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source5_5), (void*)value);
	}

	inline static int32_t get_offset_of_source6_6() { return static_cast<int32_t>(offsetof(ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA, ___source6_6)); }
	inline RuntimeObject* get_source6_6() const { return ___source6_6; }
	inline RuntimeObject** get_address_of_source6_6() { return &___source6_6; }
	inline void set_source6_6(RuntimeObject* value)
	{
		___source6_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source6_6), (void*)value);
	}

	inline static int32_t get_offset_of_source7_7() { return static_cast<int32_t>(offsetof(ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA, ___source7_7)); }
	inline RuntimeObject* get_source7_7() const { return ___source7_7; }
	inline RuntimeObject** get_address_of_source7_7() { return &___source7_7; }
	inline void set_source7_7(RuntimeObject* value)
	{
		___source7_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source7_7), (void*)value);
	}

	inline static int32_t get_offset_of_resultSelector_8() { return static_cast<int32_t>(offsetof(ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA, ___resultSelector_8)); }
	inline ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA * get_resultSelector_8() const { return ___resultSelector_8; }
	inline ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA ** get_address_of_resultSelector_8() { return &___resultSelector_8; }
	inline void set_resultSelector_8(ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA * value)
	{
		___resultSelector_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resultSelector_8), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`1_Zip<System.Object>
struct  Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D  : public OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6
{
public:
	// UniRx.Operators.ZipObservable`1<T> UniRx.Operators.ZipObservable`1_Zip::parent
	ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B * ___parent_2;
	// System.Object UniRx.Operators.ZipObservable`1_Zip::gate
	RuntimeObject * ___gate_3;
	// System.Collections.Generic.Queue`1<T>[] UniRx.Operators.ZipObservable`1_Zip::queues
	Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40* ___queues_4;
	// System.Boolean[] UniRx.Operators.ZipObservable`1_Zip::isDone
	BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* ___isDone_5;
	// System.Int32 UniRx.Operators.ZipObservable`1_Zip::length
	int32_t ___length_6;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D, ___parent_2)); }
	inline ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B * get_parent_2() const { return ___parent_2; }
	inline ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D, ___gate_3)); }
	inline RuntimeObject * get_gate_3() const { return ___gate_3; }
	inline RuntimeObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(RuntimeObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_3), (void*)value);
	}

	inline static int32_t get_offset_of_queues_4() { return static_cast<int32_t>(offsetof(Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D, ___queues_4)); }
	inline Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40* get_queues_4() const { return ___queues_4; }
	inline Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40** get_address_of_queues_4() { return &___queues_4; }
	inline void set_queues_4(Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40* value)
	{
		___queues_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___queues_4), (void*)value);
	}

	inline static int32_t get_offset_of_isDone_5() { return static_cast<int32_t>(offsetof(Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D, ___isDone_5)); }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* get_isDone_5() const { return ___isDone_5; }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C** get_address_of_isDone_5() { return &___isDone_5; }
	inline void set_isDone_5(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* value)
	{
		___isDone_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___isDone_5), (void*)value);
	}

	inline static int32_t get_offset_of_length_6() { return static_cast<int32_t>(offsetof(Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D, ___length_6)); }
	inline int32_t get_length_6() const { return ___length_6; }
	inline int32_t* get_address_of_length_6() { return &___length_6; }
	inline void set_length_6(int32_t value)
	{
		___length_6 = value;
	}
};


// UniRx.Operators.ZipObservable`1<System.Object>
struct  ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B  : public OperatorObservableBase_1_tB16C98CA8ACB1947DC9798221FBEED187248DFB0
{
public:
	// System.IObservable`1<T>[] UniRx.Operators.ZipObservable`1::sources
	IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* ___sources_1;

public:
	inline static int32_t get_offset_of_sources_1() { return static_cast<int32_t>(offsetof(ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B, ___sources_1)); }
	inline IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* get_sources_1() const { return ___sources_1; }
	inline IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B** get_address_of_sources_1() { return &___sources_1; }
	inline void set_sources_1(IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* value)
	{
		___sources_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sources_1), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`3_Zip<System.Object,System.Object,System.Object>
struct  Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93  : public OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0
{
public:
	// UniRx.Operators.ZipObservable`3<TLeft,TRight,TResult> UniRx.Operators.ZipObservable`3_Zip::parent
	ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 * ___parent_2;
	// System.Object UniRx.Operators.ZipObservable`3_Zip::gate
	RuntimeObject * ___gate_3;
	// System.Collections.Generic.Queue`1<TLeft> UniRx.Operators.ZipObservable`3_Zip::leftQ
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___leftQ_4;
	// System.Boolean UniRx.Operators.ZipObservable`3_Zip::leftCompleted
	bool ___leftCompleted_5;
	// System.Collections.Generic.Queue`1<TRight> UniRx.Operators.ZipObservable`3_Zip::rightQ
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___rightQ_6;
	// System.Boolean UniRx.Operators.ZipObservable`3_Zip::rightCompleted
	bool ___rightCompleted_7;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93, ___parent_2)); }
	inline ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 * get_parent_2() const { return ___parent_2; }
	inline ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93, ___gate_3)); }
	inline RuntimeObject * get_gate_3() const { return ___gate_3; }
	inline RuntimeObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(RuntimeObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_3), (void*)value);
	}

	inline static int32_t get_offset_of_leftQ_4() { return static_cast<int32_t>(offsetof(Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93, ___leftQ_4)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_leftQ_4() const { return ___leftQ_4; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_leftQ_4() { return &___leftQ_4; }
	inline void set_leftQ_4(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___leftQ_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___leftQ_4), (void*)value);
	}

	inline static int32_t get_offset_of_leftCompleted_5() { return static_cast<int32_t>(offsetof(Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93, ___leftCompleted_5)); }
	inline bool get_leftCompleted_5() const { return ___leftCompleted_5; }
	inline bool* get_address_of_leftCompleted_5() { return &___leftCompleted_5; }
	inline void set_leftCompleted_5(bool value)
	{
		___leftCompleted_5 = value;
	}

	inline static int32_t get_offset_of_rightQ_6() { return static_cast<int32_t>(offsetof(Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93, ___rightQ_6)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_rightQ_6() const { return ___rightQ_6; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_rightQ_6() { return &___rightQ_6; }
	inline void set_rightQ_6(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___rightQ_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rightQ_6), (void*)value);
	}

	inline static int32_t get_offset_of_rightCompleted_7() { return static_cast<int32_t>(offsetof(Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93, ___rightCompleted_7)); }
	inline bool get_rightCompleted_7() const { return ___rightCompleted_7; }
	inline bool* get_address_of_rightCompleted_7() { return &___rightCompleted_7; }
	inline void set_rightCompleted_7(bool value)
	{
		___rightCompleted_7 = value;
	}
};


// UniRx.Operators.ZipObservable`3<System.Object,System.Object,System.Object>
struct  ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<TLeft> UniRx.Operators.ZipObservable`3::left
	RuntimeObject* ___left_1;
	// System.IObservable`1<TRight> UniRx.Operators.ZipObservable`3::right
	RuntimeObject* ___right_2;
	// System.Func`3<TLeft,TRight,TResult> UniRx.Operators.ZipObservable`3::selector
	Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * ___selector_3;

public:
	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838, ___left_1)); }
	inline RuntimeObject* get_left_1() const { return ___left_1; }
	inline RuntimeObject** get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(RuntimeObject* value)
	{
		___left_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___left_1), (void*)value);
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838, ___right_2)); }
	inline RuntimeObject* get_right_2() const { return ___right_2; }
	inline RuntimeObject** get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(RuntimeObject* value)
	{
		___right_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___right_2), (void*)value);
	}

	inline static int32_t get_offset_of_selector_3() { return static_cast<int32_t>(offsetof(ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838, ___selector_3)); }
	inline Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * get_selector_3() const { return ___selector_3; }
	inline Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 ** get_address_of_selector_3() { return &___selector_3; }
	inline void set_selector_3(Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * value)
	{
		___selector_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_3), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`4<System.Object,System.Object,System.Object,System.Object>
struct  ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<T1> UniRx.Operators.ZipObservable`4::source1
	RuntimeObject* ___source1_1;
	// System.IObservable`1<T2> UniRx.Operators.ZipObservable`4::source2
	RuntimeObject* ___source2_2;
	// System.IObservable`1<T3> UniRx.Operators.ZipObservable`4::source3
	RuntimeObject* ___source3_3;
	// UniRx.Operators.ZipFunc`4<T1,T2,T3,TR> UniRx.Operators.ZipObservable`4::resultSelector
	ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 * ___resultSelector_4;

public:
	inline static int32_t get_offset_of_source1_1() { return static_cast<int32_t>(offsetof(ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39, ___source1_1)); }
	inline RuntimeObject* get_source1_1() const { return ___source1_1; }
	inline RuntimeObject** get_address_of_source1_1() { return &___source1_1; }
	inline void set_source1_1(RuntimeObject* value)
	{
		___source1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source1_1), (void*)value);
	}

	inline static int32_t get_offset_of_source2_2() { return static_cast<int32_t>(offsetof(ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39, ___source2_2)); }
	inline RuntimeObject* get_source2_2() const { return ___source2_2; }
	inline RuntimeObject** get_address_of_source2_2() { return &___source2_2; }
	inline void set_source2_2(RuntimeObject* value)
	{
		___source2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source2_2), (void*)value);
	}

	inline static int32_t get_offset_of_source3_3() { return static_cast<int32_t>(offsetof(ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39, ___source3_3)); }
	inline RuntimeObject* get_source3_3() const { return ___source3_3; }
	inline RuntimeObject** get_address_of_source3_3() { return &___source3_3; }
	inline void set_source3_3(RuntimeObject* value)
	{
		___source3_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source3_3), (void*)value);
	}

	inline static int32_t get_offset_of_resultSelector_4() { return static_cast<int32_t>(offsetof(ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39, ___resultSelector_4)); }
	inline ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 * get_resultSelector_4() const { return ___resultSelector_4; }
	inline ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 ** get_address_of_resultSelector_4() { return &___resultSelector_4; }
	inline void set_resultSelector_4(ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 * value)
	{
		___resultSelector_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resultSelector_4), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<T1> UniRx.Operators.ZipObservable`5::source1
	RuntimeObject* ___source1_1;
	// System.IObservable`1<T2> UniRx.Operators.ZipObservable`5::source2
	RuntimeObject* ___source2_2;
	// System.IObservable`1<T3> UniRx.Operators.ZipObservable`5::source3
	RuntimeObject* ___source3_3;
	// System.IObservable`1<T4> UniRx.Operators.ZipObservable`5::source4
	RuntimeObject* ___source4_4;
	// UniRx.Operators.ZipFunc`5<T1,T2,T3,T4,TR> UniRx.Operators.ZipObservable`5::resultSelector
	ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 * ___resultSelector_5;

public:
	inline static int32_t get_offset_of_source1_1() { return static_cast<int32_t>(offsetof(ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E, ___source1_1)); }
	inline RuntimeObject* get_source1_1() const { return ___source1_1; }
	inline RuntimeObject** get_address_of_source1_1() { return &___source1_1; }
	inline void set_source1_1(RuntimeObject* value)
	{
		___source1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source1_1), (void*)value);
	}

	inline static int32_t get_offset_of_source2_2() { return static_cast<int32_t>(offsetof(ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E, ___source2_2)); }
	inline RuntimeObject* get_source2_2() const { return ___source2_2; }
	inline RuntimeObject** get_address_of_source2_2() { return &___source2_2; }
	inline void set_source2_2(RuntimeObject* value)
	{
		___source2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source2_2), (void*)value);
	}

	inline static int32_t get_offset_of_source3_3() { return static_cast<int32_t>(offsetof(ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E, ___source3_3)); }
	inline RuntimeObject* get_source3_3() const { return ___source3_3; }
	inline RuntimeObject** get_address_of_source3_3() { return &___source3_3; }
	inline void set_source3_3(RuntimeObject* value)
	{
		___source3_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source3_3), (void*)value);
	}

	inline static int32_t get_offset_of_source4_4() { return static_cast<int32_t>(offsetof(ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E, ___source4_4)); }
	inline RuntimeObject* get_source4_4() const { return ___source4_4; }
	inline RuntimeObject** get_address_of_source4_4() { return &___source4_4; }
	inline void set_source4_4(RuntimeObject* value)
	{
		___source4_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source4_4), (void*)value);
	}

	inline static int32_t get_offset_of_resultSelector_5() { return static_cast<int32_t>(offsetof(ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E, ___resultSelector_5)); }
	inline ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 * get_resultSelector_5() const { return ___resultSelector_5; }
	inline ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 ** get_address_of_resultSelector_5() { return &___resultSelector_5; }
	inline void set_resultSelector_5(ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 * value)
	{
		___resultSelector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resultSelector_5), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<T1> UniRx.Operators.ZipObservable`6::source1
	RuntimeObject* ___source1_1;
	// System.IObservable`1<T2> UniRx.Operators.ZipObservable`6::source2
	RuntimeObject* ___source2_2;
	// System.IObservable`1<T3> UniRx.Operators.ZipObservable`6::source3
	RuntimeObject* ___source3_3;
	// System.IObservable`1<T4> UniRx.Operators.ZipObservable`6::source4
	RuntimeObject* ___source4_4;
	// System.IObservable`1<T5> UniRx.Operators.ZipObservable`6::source5
	RuntimeObject* ___source5_5;
	// UniRx.Operators.ZipFunc`6<T1,T2,T3,T4,T5,TR> UniRx.Operators.ZipObservable`6::resultSelector
	ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D * ___resultSelector_6;

public:
	inline static int32_t get_offset_of_source1_1() { return static_cast<int32_t>(offsetof(ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F, ___source1_1)); }
	inline RuntimeObject* get_source1_1() const { return ___source1_1; }
	inline RuntimeObject** get_address_of_source1_1() { return &___source1_1; }
	inline void set_source1_1(RuntimeObject* value)
	{
		___source1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source1_1), (void*)value);
	}

	inline static int32_t get_offset_of_source2_2() { return static_cast<int32_t>(offsetof(ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F, ___source2_2)); }
	inline RuntimeObject* get_source2_2() const { return ___source2_2; }
	inline RuntimeObject** get_address_of_source2_2() { return &___source2_2; }
	inline void set_source2_2(RuntimeObject* value)
	{
		___source2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source2_2), (void*)value);
	}

	inline static int32_t get_offset_of_source3_3() { return static_cast<int32_t>(offsetof(ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F, ___source3_3)); }
	inline RuntimeObject* get_source3_3() const { return ___source3_3; }
	inline RuntimeObject** get_address_of_source3_3() { return &___source3_3; }
	inline void set_source3_3(RuntimeObject* value)
	{
		___source3_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source3_3), (void*)value);
	}

	inline static int32_t get_offset_of_source4_4() { return static_cast<int32_t>(offsetof(ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F, ___source4_4)); }
	inline RuntimeObject* get_source4_4() const { return ___source4_4; }
	inline RuntimeObject** get_address_of_source4_4() { return &___source4_4; }
	inline void set_source4_4(RuntimeObject* value)
	{
		___source4_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source4_4), (void*)value);
	}

	inline static int32_t get_offset_of_source5_5() { return static_cast<int32_t>(offsetof(ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F, ___source5_5)); }
	inline RuntimeObject* get_source5_5() const { return ___source5_5; }
	inline RuntimeObject** get_address_of_source5_5() { return &___source5_5; }
	inline void set_source5_5(RuntimeObject* value)
	{
		___source5_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source5_5), (void*)value);
	}

	inline static int32_t get_offset_of_resultSelector_6() { return static_cast<int32_t>(offsetof(ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F, ___resultSelector_6)); }
	inline ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D * get_resultSelector_6() const { return ___resultSelector_6; }
	inline ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D ** get_address_of_resultSelector_6() { return &___resultSelector_6; }
	inline void set_resultSelector_6(ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D * value)
	{
		___resultSelector_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resultSelector_6), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<T1> UniRx.Operators.ZipObservable`7::source1
	RuntimeObject* ___source1_1;
	// System.IObservable`1<T2> UniRx.Operators.ZipObservable`7::source2
	RuntimeObject* ___source2_2;
	// System.IObservable`1<T3> UniRx.Operators.ZipObservable`7::source3
	RuntimeObject* ___source3_3;
	// System.IObservable`1<T4> UniRx.Operators.ZipObservable`7::source4
	RuntimeObject* ___source4_4;
	// System.IObservable`1<T5> UniRx.Operators.ZipObservable`7::source5
	RuntimeObject* ___source5_5;
	// System.IObservable`1<T6> UniRx.Operators.ZipObservable`7::source6
	RuntimeObject* ___source6_6;
	// UniRx.Operators.ZipFunc`7<T1,T2,T3,T4,T5,T6,TR> UniRx.Operators.ZipObservable`7::resultSelector
	ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 * ___resultSelector_7;

public:
	inline static int32_t get_offset_of_source1_1() { return static_cast<int32_t>(offsetof(ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448, ___source1_1)); }
	inline RuntimeObject* get_source1_1() const { return ___source1_1; }
	inline RuntimeObject** get_address_of_source1_1() { return &___source1_1; }
	inline void set_source1_1(RuntimeObject* value)
	{
		___source1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source1_1), (void*)value);
	}

	inline static int32_t get_offset_of_source2_2() { return static_cast<int32_t>(offsetof(ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448, ___source2_2)); }
	inline RuntimeObject* get_source2_2() const { return ___source2_2; }
	inline RuntimeObject** get_address_of_source2_2() { return &___source2_2; }
	inline void set_source2_2(RuntimeObject* value)
	{
		___source2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source2_2), (void*)value);
	}

	inline static int32_t get_offset_of_source3_3() { return static_cast<int32_t>(offsetof(ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448, ___source3_3)); }
	inline RuntimeObject* get_source3_3() const { return ___source3_3; }
	inline RuntimeObject** get_address_of_source3_3() { return &___source3_3; }
	inline void set_source3_3(RuntimeObject* value)
	{
		___source3_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source3_3), (void*)value);
	}

	inline static int32_t get_offset_of_source4_4() { return static_cast<int32_t>(offsetof(ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448, ___source4_4)); }
	inline RuntimeObject* get_source4_4() const { return ___source4_4; }
	inline RuntimeObject** get_address_of_source4_4() { return &___source4_4; }
	inline void set_source4_4(RuntimeObject* value)
	{
		___source4_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source4_4), (void*)value);
	}

	inline static int32_t get_offset_of_source5_5() { return static_cast<int32_t>(offsetof(ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448, ___source5_5)); }
	inline RuntimeObject* get_source5_5() const { return ___source5_5; }
	inline RuntimeObject** get_address_of_source5_5() { return &___source5_5; }
	inline void set_source5_5(RuntimeObject* value)
	{
		___source5_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source5_5), (void*)value);
	}

	inline static int32_t get_offset_of_source6_6() { return static_cast<int32_t>(offsetof(ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448, ___source6_6)); }
	inline RuntimeObject* get_source6_6() const { return ___source6_6; }
	inline RuntimeObject** get_address_of_source6_6() { return &___source6_6; }
	inline void set_source6_6(RuntimeObject* value)
	{
		___source6_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source6_6), (void*)value);
	}

	inline static int32_t get_offset_of_resultSelector_7() { return static_cast<int32_t>(offsetof(ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448, ___resultSelector_7)); }
	inline ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 * get_resultSelector_7() const { return ___resultSelector_7; }
	inline ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 ** get_address_of_resultSelector_7() { return &___resultSelector_7; }
	inline void set_resultSelector_7(ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 * value)
	{
		___resultSelector_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resultSelector_7), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F  : public OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC
{
public:
	// System.IObservable`1<T1> UniRx.Operators.ZipObservable`8::source1
	RuntimeObject* ___source1_1;
	// System.IObservable`1<T2> UniRx.Operators.ZipObservable`8::source2
	RuntimeObject* ___source2_2;
	// System.IObservable`1<T3> UniRx.Operators.ZipObservable`8::source3
	RuntimeObject* ___source3_3;
	// System.IObservable`1<T4> UniRx.Operators.ZipObservable`8::source4
	RuntimeObject* ___source4_4;
	// System.IObservable`1<T5> UniRx.Operators.ZipObservable`8::source5
	RuntimeObject* ___source5_5;
	// System.IObservable`1<T6> UniRx.Operators.ZipObservable`8::source6
	RuntimeObject* ___source6_6;
	// System.IObservable`1<T7> UniRx.Operators.ZipObservable`8::source7
	RuntimeObject* ___source7_7;
	// UniRx.Operators.ZipFunc`8<T1,T2,T3,T4,T5,T6,T7,TR> UniRx.Operators.ZipObservable`8::resultSelector
	ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 * ___resultSelector_8;

public:
	inline static int32_t get_offset_of_source1_1() { return static_cast<int32_t>(offsetof(ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F, ___source1_1)); }
	inline RuntimeObject* get_source1_1() const { return ___source1_1; }
	inline RuntimeObject** get_address_of_source1_1() { return &___source1_1; }
	inline void set_source1_1(RuntimeObject* value)
	{
		___source1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source1_1), (void*)value);
	}

	inline static int32_t get_offset_of_source2_2() { return static_cast<int32_t>(offsetof(ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F, ___source2_2)); }
	inline RuntimeObject* get_source2_2() const { return ___source2_2; }
	inline RuntimeObject** get_address_of_source2_2() { return &___source2_2; }
	inline void set_source2_2(RuntimeObject* value)
	{
		___source2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source2_2), (void*)value);
	}

	inline static int32_t get_offset_of_source3_3() { return static_cast<int32_t>(offsetof(ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F, ___source3_3)); }
	inline RuntimeObject* get_source3_3() const { return ___source3_3; }
	inline RuntimeObject** get_address_of_source3_3() { return &___source3_3; }
	inline void set_source3_3(RuntimeObject* value)
	{
		___source3_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source3_3), (void*)value);
	}

	inline static int32_t get_offset_of_source4_4() { return static_cast<int32_t>(offsetof(ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F, ___source4_4)); }
	inline RuntimeObject* get_source4_4() const { return ___source4_4; }
	inline RuntimeObject** get_address_of_source4_4() { return &___source4_4; }
	inline void set_source4_4(RuntimeObject* value)
	{
		___source4_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source4_4), (void*)value);
	}

	inline static int32_t get_offset_of_source5_5() { return static_cast<int32_t>(offsetof(ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F, ___source5_5)); }
	inline RuntimeObject* get_source5_5() const { return ___source5_5; }
	inline RuntimeObject** get_address_of_source5_5() { return &___source5_5; }
	inline void set_source5_5(RuntimeObject* value)
	{
		___source5_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source5_5), (void*)value);
	}

	inline static int32_t get_offset_of_source6_6() { return static_cast<int32_t>(offsetof(ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F, ___source6_6)); }
	inline RuntimeObject* get_source6_6() const { return ___source6_6; }
	inline RuntimeObject** get_address_of_source6_6() { return &___source6_6; }
	inline void set_source6_6(RuntimeObject* value)
	{
		___source6_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source6_6), (void*)value);
	}

	inline static int32_t get_offset_of_source7_7() { return static_cast<int32_t>(offsetof(ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F, ___source7_7)); }
	inline RuntimeObject* get_source7_7() const { return ___source7_7; }
	inline RuntimeObject** get_address_of_source7_7() { return &___source7_7; }
	inline void set_source7_7(RuntimeObject* value)
	{
		___source7_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source7_7), (void*)value);
	}

	inline static int32_t get_offset_of_resultSelector_8() { return static_cast<int32_t>(offsetof(ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F, ___resultSelector_8)); }
	inline ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 * get_resultSelector_8() const { return ___resultSelector_8; }
	inline ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 ** get_address_of_resultSelector_8() { return &___resultSelector_8; }
	inline void set_resultSelector_8(ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 * value)
	{
		___resultSelector_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resultSelector_8), (void*)value);
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom<System.Object,System.Object,System.Object>
struct  WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF  : public OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0
{
public:
	// UniRx.Operators.WithLatestFromObservable`3<TLeft,TRight,TResult> UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom::parent
	WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 * ___parent_2;
	// System.Object UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom::gate
	RuntimeObject * ___gate_3;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom::hasLatest
	bool ___hasLatest_4;
	// TRight UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom::latestValue
	RuntimeObject * ___latestValue_5;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF, ___parent_2)); }
	inline WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 * get_parent_2() const { return ___parent_2; }
	inline WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_2), (void*)value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF, ___gate_3)); }
	inline RuntimeObject * get_gate_3() const { return ___gate_3; }
	inline RuntimeObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(RuntimeObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_3), (void*)value);
	}

	inline static int32_t get_offset_of_hasLatest_4() { return static_cast<int32_t>(offsetof(WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF, ___hasLatest_4)); }
	inline bool get_hasLatest_4() const { return ___hasLatest_4; }
	inline bool* get_address_of_hasLatest_4() { return &___hasLatest_4; }
	inline void set_hasLatest_4(bool value)
	{
		___hasLatest_4 = value;
	}

	inline static int32_t get_offset_of_latestValue_5() { return static_cast<int32_t>(offsetof(WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF, ___latestValue_5)); }
	inline RuntimeObject * get_latestValue_5() const { return ___latestValue_5; }
	inline RuntimeObject ** get_address_of_latestValue_5() { return &___latestValue_5; }
	inline void set_latestValue_5(RuntimeObject * value)
	{
		___latestValue_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___latestValue_5), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`4_ZipLatest<System.Object,System.Object,System.Object,System.Object>
struct  ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D  : public NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D
{
public:
	// UniRx.Operators.ZipLatestObservable`4<T1,T2,T3,TR> UniRx.Operators.ZipLatestObservable`4_ZipLatest::parent
	ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * ___parent_5;
	// System.Object UniRx.Operators.ZipLatestObservable`4_ZipLatest::gate
	RuntimeObject * ___gate_6;
	// UniRx.Operators.ZipLatestObserver`1<T1> UniRx.Operators.ZipLatestObservable`4_ZipLatest::c1
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c1_7;
	// UniRx.Operators.ZipLatestObserver`1<T2> UniRx.Operators.ZipLatestObservable`4_ZipLatest::c2
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c2_8;
	// UniRx.Operators.ZipLatestObserver`1<T3> UniRx.Operators.ZipLatestObservable`4_ZipLatest::c3
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c3_9;

public:
	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D, ___parent_5)); }
	inline ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * get_parent_5() const { return ___parent_5; }
	inline ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_5), (void*)value);
	}

	inline static int32_t get_offset_of_gate_6() { return static_cast<int32_t>(offsetof(ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D, ___gate_6)); }
	inline RuntimeObject * get_gate_6() const { return ___gate_6; }
	inline RuntimeObject ** get_address_of_gate_6() { return &___gate_6; }
	inline void set_gate_6(RuntimeObject * value)
	{
		___gate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_6), (void*)value);
	}

	inline static int32_t get_offset_of_c1_7() { return static_cast<int32_t>(offsetof(ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D, ___c1_7)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c1_7() const { return ___c1_7; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c1_7() { return &___c1_7; }
	inline void set_c1_7(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c1_7), (void*)value);
	}

	inline static int32_t get_offset_of_c2_8() { return static_cast<int32_t>(offsetof(ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D, ___c2_8)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c2_8() const { return ___c2_8; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c2_8() { return &___c2_8; }
	inline void set_c2_8(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c2_8), (void*)value);
	}

	inline static int32_t get_offset_of_c3_9() { return static_cast<int32_t>(offsetof(ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D, ___c3_9)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c3_9() const { return ___c3_9; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c3_9() { return &___c3_9; }
	inline void set_c3_9(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c3_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c3_9), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`5_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61  : public NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D
{
public:
	// UniRx.Operators.ZipLatestObservable`5<T1,T2,T3,T4,TR> UniRx.Operators.ZipLatestObservable`5_ZipLatest::parent
	ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * ___parent_5;
	// System.Object UniRx.Operators.ZipLatestObservable`5_ZipLatest::gate
	RuntimeObject * ___gate_6;
	// UniRx.Operators.ZipLatestObserver`1<T1> UniRx.Operators.ZipLatestObservable`5_ZipLatest::c1
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c1_7;
	// UniRx.Operators.ZipLatestObserver`1<T2> UniRx.Operators.ZipLatestObservable`5_ZipLatest::c2
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c2_8;
	// UniRx.Operators.ZipLatestObserver`1<T3> UniRx.Operators.ZipLatestObservable`5_ZipLatest::c3
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c3_9;
	// UniRx.Operators.ZipLatestObserver`1<T4> UniRx.Operators.ZipLatestObservable`5_ZipLatest::c4
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c4_10;

public:
	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61, ___parent_5)); }
	inline ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * get_parent_5() const { return ___parent_5; }
	inline ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_5), (void*)value);
	}

	inline static int32_t get_offset_of_gate_6() { return static_cast<int32_t>(offsetof(ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61, ___gate_6)); }
	inline RuntimeObject * get_gate_6() const { return ___gate_6; }
	inline RuntimeObject ** get_address_of_gate_6() { return &___gate_6; }
	inline void set_gate_6(RuntimeObject * value)
	{
		___gate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_6), (void*)value);
	}

	inline static int32_t get_offset_of_c1_7() { return static_cast<int32_t>(offsetof(ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61, ___c1_7)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c1_7() const { return ___c1_7; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c1_7() { return &___c1_7; }
	inline void set_c1_7(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c1_7), (void*)value);
	}

	inline static int32_t get_offset_of_c2_8() { return static_cast<int32_t>(offsetof(ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61, ___c2_8)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c2_8() const { return ___c2_8; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c2_8() { return &___c2_8; }
	inline void set_c2_8(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c2_8), (void*)value);
	}

	inline static int32_t get_offset_of_c3_9() { return static_cast<int32_t>(offsetof(ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61, ___c3_9)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c3_9() const { return ___c3_9; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c3_9() { return &___c3_9; }
	inline void set_c3_9(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c3_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c3_9), (void*)value);
	}

	inline static int32_t get_offset_of_c4_10() { return static_cast<int32_t>(offsetof(ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61, ___c4_10)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c4_10() const { return ___c4_10; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c4_10() { return &___c4_10; }
	inline void set_c4_10(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c4_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c4_10), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`6_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565  : public NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D
{
public:
	// UniRx.Operators.ZipLatestObservable`6<T1,T2,T3,T4,T5,TR> UniRx.Operators.ZipLatestObservable`6_ZipLatest::parent
	ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * ___parent_5;
	// System.Object UniRx.Operators.ZipLatestObservable`6_ZipLatest::gate
	RuntimeObject * ___gate_6;
	// UniRx.Operators.ZipLatestObserver`1<T1> UniRx.Operators.ZipLatestObservable`6_ZipLatest::c1
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c1_7;
	// UniRx.Operators.ZipLatestObserver`1<T2> UniRx.Operators.ZipLatestObservable`6_ZipLatest::c2
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c2_8;
	// UniRx.Operators.ZipLatestObserver`1<T3> UniRx.Operators.ZipLatestObservable`6_ZipLatest::c3
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c3_9;
	// UniRx.Operators.ZipLatestObserver`1<T4> UniRx.Operators.ZipLatestObservable`6_ZipLatest::c4
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c4_10;
	// UniRx.Operators.ZipLatestObserver`1<T5> UniRx.Operators.ZipLatestObservable`6_ZipLatest::c5
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c5_11;

public:
	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565, ___parent_5)); }
	inline ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * get_parent_5() const { return ___parent_5; }
	inline ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_5), (void*)value);
	}

	inline static int32_t get_offset_of_gate_6() { return static_cast<int32_t>(offsetof(ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565, ___gate_6)); }
	inline RuntimeObject * get_gate_6() const { return ___gate_6; }
	inline RuntimeObject ** get_address_of_gate_6() { return &___gate_6; }
	inline void set_gate_6(RuntimeObject * value)
	{
		___gate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_6), (void*)value);
	}

	inline static int32_t get_offset_of_c1_7() { return static_cast<int32_t>(offsetof(ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565, ___c1_7)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c1_7() const { return ___c1_7; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c1_7() { return &___c1_7; }
	inline void set_c1_7(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c1_7), (void*)value);
	}

	inline static int32_t get_offset_of_c2_8() { return static_cast<int32_t>(offsetof(ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565, ___c2_8)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c2_8() const { return ___c2_8; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c2_8() { return &___c2_8; }
	inline void set_c2_8(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c2_8), (void*)value);
	}

	inline static int32_t get_offset_of_c3_9() { return static_cast<int32_t>(offsetof(ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565, ___c3_9)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c3_9() const { return ___c3_9; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c3_9() { return &___c3_9; }
	inline void set_c3_9(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c3_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c3_9), (void*)value);
	}

	inline static int32_t get_offset_of_c4_10() { return static_cast<int32_t>(offsetof(ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565, ___c4_10)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c4_10() const { return ___c4_10; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c4_10() { return &___c4_10; }
	inline void set_c4_10(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c4_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c4_10), (void*)value);
	}

	inline static int32_t get_offset_of_c5_11() { return static_cast<int32_t>(offsetof(ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565, ___c5_11)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c5_11() const { return ___c5_11; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c5_11() { return &___c5_11; }
	inline void set_c5_11(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c5_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c5_11), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`7_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37  : public NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D
{
public:
	// UniRx.Operators.ZipLatestObservable`7<T1,T2,T3,T4,T5,T6,TR> UniRx.Operators.ZipLatestObservable`7_ZipLatest::parent
	ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * ___parent_5;
	// System.Object UniRx.Operators.ZipLatestObservable`7_ZipLatest::gate
	RuntimeObject * ___gate_6;
	// UniRx.Operators.ZipLatestObserver`1<T1> UniRx.Operators.ZipLatestObservable`7_ZipLatest::c1
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c1_7;
	// UniRx.Operators.ZipLatestObserver`1<T2> UniRx.Operators.ZipLatestObservable`7_ZipLatest::c2
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c2_8;
	// UniRx.Operators.ZipLatestObserver`1<T3> UniRx.Operators.ZipLatestObservable`7_ZipLatest::c3
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c3_9;
	// UniRx.Operators.ZipLatestObserver`1<T4> UniRx.Operators.ZipLatestObservable`7_ZipLatest::c4
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c4_10;
	// UniRx.Operators.ZipLatestObserver`1<T5> UniRx.Operators.ZipLatestObservable`7_ZipLatest::c5
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c5_11;
	// UniRx.Operators.ZipLatestObserver`1<T6> UniRx.Operators.ZipLatestObservable`7_ZipLatest::c6
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c6_12;

public:
	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37, ___parent_5)); }
	inline ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * get_parent_5() const { return ___parent_5; }
	inline ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_5), (void*)value);
	}

	inline static int32_t get_offset_of_gate_6() { return static_cast<int32_t>(offsetof(ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37, ___gate_6)); }
	inline RuntimeObject * get_gate_6() const { return ___gate_6; }
	inline RuntimeObject ** get_address_of_gate_6() { return &___gate_6; }
	inline void set_gate_6(RuntimeObject * value)
	{
		___gate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_6), (void*)value);
	}

	inline static int32_t get_offset_of_c1_7() { return static_cast<int32_t>(offsetof(ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37, ___c1_7)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c1_7() const { return ___c1_7; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c1_7() { return &___c1_7; }
	inline void set_c1_7(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c1_7), (void*)value);
	}

	inline static int32_t get_offset_of_c2_8() { return static_cast<int32_t>(offsetof(ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37, ___c2_8)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c2_8() const { return ___c2_8; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c2_8() { return &___c2_8; }
	inline void set_c2_8(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c2_8), (void*)value);
	}

	inline static int32_t get_offset_of_c3_9() { return static_cast<int32_t>(offsetof(ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37, ___c3_9)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c3_9() const { return ___c3_9; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c3_9() { return &___c3_9; }
	inline void set_c3_9(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c3_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c3_9), (void*)value);
	}

	inline static int32_t get_offset_of_c4_10() { return static_cast<int32_t>(offsetof(ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37, ___c4_10)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c4_10() const { return ___c4_10; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c4_10() { return &___c4_10; }
	inline void set_c4_10(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c4_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c4_10), (void*)value);
	}

	inline static int32_t get_offset_of_c5_11() { return static_cast<int32_t>(offsetof(ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37, ___c5_11)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c5_11() const { return ___c5_11; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c5_11() { return &___c5_11; }
	inline void set_c5_11(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c5_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c5_11), (void*)value);
	}

	inline static int32_t get_offset_of_c6_12() { return static_cast<int32_t>(offsetof(ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37, ___c6_12)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c6_12() const { return ___c6_12; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c6_12() { return &___c6_12; }
	inline void set_c6_12(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c6_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c6_12), (void*)value);
	}
};


// UniRx.Operators.ZipLatestObservable`8_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821  : public NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D
{
public:
	// UniRx.Operators.ZipLatestObservable`8<T1,T2,T3,T4,T5,T6,T7,TR> UniRx.Operators.ZipLatestObservable`8_ZipLatest::parent
	ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * ___parent_5;
	// System.Object UniRx.Operators.ZipLatestObservable`8_ZipLatest::gate
	RuntimeObject * ___gate_6;
	// UniRx.Operators.ZipLatestObserver`1<T1> UniRx.Operators.ZipLatestObservable`8_ZipLatest::c1
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c1_7;
	// UniRx.Operators.ZipLatestObserver`1<T2> UniRx.Operators.ZipLatestObservable`8_ZipLatest::c2
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c2_8;
	// UniRx.Operators.ZipLatestObserver`1<T3> UniRx.Operators.ZipLatestObservable`8_ZipLatest::c3
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c3_9;
	// UniRx.Operators.ZipLatestObserver`1<T4> UniRx.Operators.ZipLatestObservable`8_ZipLatest::c4
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c4_10;
	// UniRx.Operators.ZipLatestObserver`1<T5> UniRx.Operators.ZipLatestObservable`8_ZipLatest::c5
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c5_11;
	// UniRx.Operators.ZipLatestObserver`1<T6> UniRx.Operators.ZipLatestObservable`8_ZipLatest::c6
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c6_12;
	// UniRx.Operators.ZipLatestObserver`1<T7> UniRx.Operators.ZipLatestObservable`8_ZipLatest::c7
	ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * ___c7_13;

public:
	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821, ___parent_5)); }
	inline ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * get_parent_5() const { return ___parent_5; }
	inline ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_5), (void*)value);
	}

	inline static int32_t get_offset_of_gate_6() { return static_cast<int32_t>(offsetof(ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821, ___gate_6)); }
	inline RuntimeObject * get_gate_6() const { return ___gate_6; }
	inline RuntimeObject ** get_address_of_gate_6() { return &___gate_6; }
	inline void set_gate_6(RuntimeObject * value)
	{
		___gate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_6), (void*)value);
	}

	inline static int32_t get_offset_of_c1_7() { return static_cast<int32_t>(offsetof(ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821, ___c1_7)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c1_7() const { return ___c1_7; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c1_7() { return &___c1_7; }
	inline void set_c1_7(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c1_7), (void*)value);
	}

	inline static int32_t get_offset_of_c2_8() { return static_cast<int32_t>(offsetof(ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821, ___c2_8)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c2_8() const { return ___c2_8; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c2_8() { return &___c2_8; }
	inline void set_c2_8(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c2_8), (void*)value);
	}

	inline static int32_t get_offset_of_c3_9() { return static_cast<int32_t>(offsetof(ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821, ___c3_9)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c3_9() const { return ___c3_9; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c3_9() { return &___c3_9; }
	inline void set_c3_9(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c3_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c3_9), (void*)value);
	}

	inline static int32_t get_offset_of_c4_10() { return static_cast<int32_t>(offsetof(ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821, ___c4_10)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c4_10() const { return ___c4_10; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c4_10() { return &___c4_10; }
	inline void set_c4_10(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c4_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c4_10), (void*)value);
	}

	inline static int32_t get_offset_of_c5_11() { return static_cast<int32_t>(offsetof(ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821, ___c5_11)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c5_11() const { return ___c5_11; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c5_11() { return &___c5_11; }
	inline void set_c5_11(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c5_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c5_11), (void*)value);
	}

	inline static int32_t get_offset_of_c6_12() { return static_cast<int32_t>(offsetof(ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821, ___c6_12)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c6_12() const { return ___c6_12; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c6_12() { return &___c6_12; }
	inline void set_c6_12(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c6_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c6_12), (void*)value);
	}

	inline static int32_t get_offset_of_c7_13() { return static_cast<int32_t>(offsetof(ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821, ___c7_13)); }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * get_c7_13() const { return ___c7_13; }
	inline ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 ** get_address_of_c7_13() { return &___c7_13; }
	inline void set_c7_13(ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * value)
	{
		___c7_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___c7_13), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`4_Zip<System.Object,System.Object,System.Object,System.Object>
struct  Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1  : public NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677
{
public:
	// UniRx.Operators.ZipObservable`4<T1,T2,T3,TR> UniRx.Operators.ZipObservable`4_Zip::parent
	ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * ___parent_5;
	// System.Object UniRx.Operators.ZipObservable`4_Zip::gate
	RuntimeObject * ___gate_6;
	// System.Collections.Generic.Queue`1<T1> UniRx.Operators.ZipObservable`4_Zip::q1
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q1_7;
	// System.Collections.Generic.Queue`1<T2> UniRx.Operators.ZipObservable`4_Zip::q2
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q2_8;
	// System.Collections.Generic.Queue`1<T3> UniRx.Operators.ZipObservable`4_Zip::q3
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q3_9;

public:
	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1, ___parent_5)); }
	inline ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * get_parent_5() const { return ___parent_5; }
	inline ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_5), (void*)value);
	}

	inline static int32_t get_offset_of_gate_6() { return static_cast<int32_t>(offsetof(Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1, ___gate_6)); }
	inline RuntimeObject * get_gate_6() const { return ___gate_6; }
	inline RuntimeObject ** get_address_of_gate_6() { return &___gate_6; }
	inline void set_gate_6(RuntimeObject * value)
	{
		___gate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_6), (void*)value);
	}

	inline static int32_t get_offset_of_q1_7() { return static_cast<int32_t>(offsetof(Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1, ___q1_7)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q1_7() const { return ___q1_7; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q1_7() { return &___q1_7; }
	inline void set_q1_7(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q1_7), (void*)value);
	}

	inline static int32_t get_offset_of_q2_8() { return static_cast<int32_t>(offsetof(Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1, ___q2_8)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q2_8() const { return ___q2_8; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q2_8() { return &___q2_8; }
	inline void set_q2_8(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q2_8), (void*)value);
	}

	inline static int32_t get_offset_of_q3_9() { return static_cast<int32_t>(offsetof(Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1, ___q3_9)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q3_9() const { return ___q3_9; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q3_9() { return &___q3_9; }
	inline void set_q3_9(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q3_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q3_9), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`5_Zip<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808  : public NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677
{
public:
	// UniRx.Operators.ZipObservable`5<T1,T2,T3,T4,TR> UniRx.Operators.ZipObservable`5_Zip::parent
	ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * ___parent_5;
	// System.Object UniRx.Operators.ZipObservable`5_Zip::gate
	RuntimeObject * ___gate_6;
	// System.Collections.Generic.Queue`1<T1> UniRx.Operators.ZipObservable`5_Zip::q1
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q1_7;
	// System.Collections.Generic.Queue`1<T2> UniRx.Operators.ZipObservable`5_Zip::q2
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q2_8;
	// System.Collections.Generic.Queue`1<T3> UniRx.Operators.ZipObservable`5_Zip::q3
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q3_9;
	// System.Collections.Generic.Queue`1<T4> UniRx.Operators.ZipObservable`5_Zip::q4
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q4_10;

public:
	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808, ___parent_5)); }
	inline ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * get_parent_5() const { return ___parent_5; }
	inline ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_5), (void*)value);
	}

	inline static int32_t get_offset_of_gate_6() { return static_cast<int32_t>(offsetof(Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808, ___gate_6)); }
	inline RuntimeObject * get_gate_6() const { return ___gate_6; }
	inline RuntimeObject ** get_address_of_gate_6() { return &___gate_6; }
	inline void set_gate_6(RuntimeObject * value)
	{
		___gate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_6), (void*)value);
	}

	inline static int32_t get_offset_of_q1_7() { return static_cast<int32_t>(offsetof(Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808, ___q1_7)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q1_7() const { return ___q1_7; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q1_7() { return &___q1_7; }
	inline void set_q1_7(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q1_7), (void*)value);
	}

	inline static int32_t get_offset_of_q2_8() { return static_cast<int32_t>(offsetof(Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808, ___q2_8)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q2_8() const { return ___q2_8; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q2_8() { return &___q2_8; }
	inline void set_q2_8(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q2_8), (void*)value);
	}

	inline static int32_t get_offset_of_q3_9() { return static_cast<int32_t>(offsetof(Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808, ___q3_9)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q3_9() const { return ___q3_9; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q3_9() { return &___q3_9; }
	inline void set_q3_9(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q3_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q3_9), (void*)value);
	}

	inline static int32_t get_offset_of_q4_10() { return static_cast<int32_t>(offsetof(Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808, ___q4_10)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q4_10() const { return ___q4_10; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q4_10() { return &___q4_10; }
	inline void set_q4_10(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q4_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q4_10), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`6_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09  : public NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677
{
public:
	// UniRx.Operators.ZipObservable`6<T1,T2,T3,T4,T5,TR> UniRx.Operators.ZipObservable`6_Zip::parent
	ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * ___parent_5;
	// System.Object UniRx.Operators.ZipObservable`6_Zip::gate
	RuntimeObject * ___gate_6;
	// System.Collections.Generic.Queue`1<T1> UniRx.Operators.ZipObservable`6_Zip::q1
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q1_7;
	// System.Collections.Generic.Queue`1<T2> UniRx.Operators.ZipObservable`6_Zip::q2
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q2_8;
	// System.Collections.Generic.Queue`1<T3> UniRx.Operators.ZipObservable`6_Zip::q3
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q3_9;
	// System.Collections.Generic.Queue`1<T4> UniRx.Operators.ZipObservable`6_Zip::q4
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q4_10;
	// System.Collections.Generic.Queue`1<T5> UniRx.Operators.ZipObservable`6_Zip::q5
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q5_11;

public:
	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09, ___parent_5)); }
	inline ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * get_parent_5() const { return ___parent_5; }
	inline ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_5), (void*)value);
	}

	inline static int32_t get_offset_of_gate_6() { return static_cast<int32_t>(offsetof(Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09, ___gate_6)); }
	inline RuntimeObject * get_gate_6() const { return ___gate_6; }
	inline RuntimeObject ** get_address_of_gate_6() { return &___gate_6; }
	inline void set_gate_6(RuntimeObject * value)
	{
		___gate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_6), (void*)value);
	}

	inline static int32_t get_offset_of_q1_7() { return static_cast<int32_t>(offsetof(Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09, ___q1_7)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q1_7() const { return ___q1_7; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q1_7() { return &___q1_7; }
	inline void set_q1_7(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q1_7), (void*)value);
	}

	inline static int32_t get_offset_of_q2_8() { return static_cast<int32_t>(offsetof(Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09, ___q2_8)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q2_8() const { return ___q2_8; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q2_8() { return &___q2_8; }
	inline void set_q2_8(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q2_8), (void*)value);
	}

	inline static int32_t get_offset_of_q3_9() { return static_cast<int32_t>(offsetof(Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09, ___q3_9)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q3_9() const { return ___q3_9; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q3_9() { return &___q3_9; }
	inline void set_q3_9(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q3_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q3_9), (void*)value);
	}

	inline static int32_t get_offset_of_q4_10() { return static_cast<int32_t>(offsetof(Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09, ___q4_10)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q4_10() const { return ___q4_10; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q4_10() { return &___q4_10; }
	inline void set_q4_10(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q4_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q4_10), (void*)value);
	}

	inline static int32_t get_offset_of_q5_11() { return static_cast<int32_t>(offsetof(Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09, ___q5_11)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q5_11() const { return ___q5_11; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q5_11() { return &___q5_11; }
	inline void set_q5_11(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q5_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q5_11), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`7_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75  : public NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677
{
public:
	// UniRx.Operators.ZipObservable`7<T1,T2,T3,T4,T5,T6,TR> UniRx.Operators.ZipObservable`7_Zip::parent
	ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * ___parent_5;
	// System.Object UniRx.Operators.ZipObservable`7_Zip::gate
	RuntimeObject * ___gate_6;
	// System.Collections.Generic.Queue`1<T1> UniRx.Operators.ZipObservable`7_Zip::q1
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q1_7;
	// System.Collections.Generic.Queue`1<T2> UniRx.Operators.ZipObservable`7_Zip::q2
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q2_8;
	// System.Collections.Generic.Queue`1<T3> UniRx.Operators.ZipObservable`7_Zip::q3
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q3_9;
	// System.Collections.Generic.Queue`1<T4> UniRx.Operators.ZipObservable`7_Zip::q4
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q4_10;
	// System.Collections.Generic.Queue`1<T5> UniRx.Operators.ZipObservable`7_Zip::q5
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q5_11;
	// System.Collections.Generic.Queue`1<T6> UniRx.Operators.ZipObservable`7_Zip::q6
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q6_12;

public:
	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75, ___parent_5)); }
	inline ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * get_parent_5() const { return ___parent_5; }
	inline ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_5), (void*)value);
	}

	inline static int32_t get_offset_of_gate_6() { return static_cast<int32_t>(offsetof(Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75, ___gate_6)); }
	inline RuntimeObject * get_gate_6() const { return ___gate_6; }
	inline RuntimeObject ** get_address_of_gate_6() { return &___gate_6; }
	inline void set_gate_6(RuntimeObject * value)
	{
		___gate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_6), (void*)value);
	}

	inline static int32_t get_offset_of_q1_7() { return static_cast<int32_t>(offsetof(Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75, ___q1_7)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q1_7() const { return ___q1_7; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q1_7() { return &___q1_7; }
	inline void set_q1_7(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q1_7), (void*)value);
	}

	inline static int32_t get_offset_of_q2_8() { return static_cast<int32_t>(offsetof(Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75, ___q2_8)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q2_8() const { return ___q2_8; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q2_8() { return &___q2_8; }
	inline void set_q2_8(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q2_8), (void*)value);
	}

	inline static int32_t get_offset_of_q3_9() { return static_cast<int32_t>(offsetof(Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75, ___q3_9)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q3_9() const { return ___q3_9; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q3_9() { return &___q3_9; }
	inline void set_q3_9(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q3_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q3_9), (void*)value);
	}

	inline static int32_t get_offset_of_q4_10() { return static_cast<int32_t>(offsetof(Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75, ___q4_10)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q4_10() const { return ___q4_10; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q4_10() { return &___q4_10; }
	inline void set_q4_10(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q4_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q4_10), (void*)value);
	}

	inline static int32_t get_offset_of_q5_11() { return static_cast<int32_t>(offsetof(Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75, ___q5_11)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q5_11() const { return ___q5_11; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q5_11() { return &___q5_11; }
	inline void set_q5_11(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q5_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q5_11), (void*)value);
	}

	inline static int32_t get_offset_of_q6_12() { return static_cast<int32_t>(offsetof(Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75, ___q6_12)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q6_12() const { return ___q6_12; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q6_12() { return &___q6_12; }
	inline void set_q6_12(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q6_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q6_12), (void*)value);
	}
};


// UniRx.Operators.ZipObservable`8_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Zip_tFE288625F1A712CC45FAE011730910DD457970CC  : public NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677
{
public:
	// UniRx.Operators.ZipObservable`8<T1,T2,T3,T4,T5,T6,T7,TR> UniRx.Operators.ZipObservable`8_Zip::parent
	ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * ___parent_5;
	// System.Object UniRx.Operators.ZipObservable`8_Zip::gate
	RuntimeObject * ___gate_6;
	// System.Collections.Generic.Queue`1<T1> UniRx.Operators.ZipObservable`8_Zip::q1
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q1_7;
	// System.Collections.Generic.Queue`1<T2> UniRx.Operators.ZipObservable`8_Zip::q2
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q2_8;
	// System.Collections.Generic.Queue`1<T3> UniRx.Operators.ZipObservable`8_Zip::q3
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q3_9;
	// System.Collections.Generic.Queue`1<T4> UniRx.Operators.ZipObservable`8_Zip::q4
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q4_10;
	// System.Collections.Generic.Queue`1<T5> UniRx.Operators.ZipObservable`8_Zip::q5
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q5_11;
	// System.Collections.Generic.Queue`1<T6> UniRx.Operators.ZipObservable`8_Zip::q6
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q6_12;
	// System.Collections.Generic.Queue`1<T7> UniRx.Operators.ZipObservable`8_Zip::q7
	Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___q7_13;

public:
	inline static int32_t get_offset_of_parent_5() { return static_cast<int32_t>(offsetof(Zip_tFE288625F1A712CC45FAE011730910DD457970CC, ___parent_5)); }
	inline ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * get_parent_5() const { return ___parent_5; }
	inline ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F ** get_address_of_parent_5() { return &___parent_5; }
	inline void set_parent_5(ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * value)
	{
		___parent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_5), (void*)value);
	}

	inline static int32_t get_offset_of_gate_6() { return static_cast<int32_t>(offsetof(Zip_tFE288625F1A712CC45FAE011730910DD457970CC, ___gate_6)); }
	inline RuntimeObject * get_gate_6() const { return ___gate_6; }
	inline RuntimeObject ** get_address_of_gate_6() { return &___gate_6; }
	inline void set_gate_6(RuntimeObject * value)
	{
		___gate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_6), (void*)value);
	}

	inline static int32_t get_offset_of_q1_7() { return static_cast<int32_t>(offsetof(Zip_tFE288625F1A712CC45FAE011730910DD457970CC, ___q1_7)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q1_7() const { return ___q1_7; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q1_7() { return &___q1_7; }
	inline void set_q1_7(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q1_7), (void*)value);
	}

	inline static int32_t get_offset_of_q2_8() { return static_cast<int32_t>(offsetof(Zip_tFE288625F1A712CC45FAE011730910DD457970CC, ___q2_8)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q2_8() const { return ___q2_8; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q2_8() { return &___q2_8; }
	inline void set_q2_8(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q2_8), (void*)value);
	}

	inline static int32_t get_offset_of_q3_9() { return static_cast<int32_t>(offsetof(Zip_tFE288625F1A712CC45FAE011730910DD457970CC, ___q3_9)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q3_9() const { return ___q3_9; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q3_9() { return &___q3_9; }
	inline void set_q3_9(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q3_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q3_9), (void*)value);
	}

	inline static int32_t get_offset_of_q4_10() { return static_cast<int32_t>(offsetof(Zip_tFE288625F1A712CC45FAE011730910DD457970CC, ___q4_10)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q4_10() const { return ___q4_10; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q4_10() { return &___q4_10; }
	inline void set_q4_10(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q4_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q4_10), (void*)value);
	}

	inline static int32_t get_offset_of_q5_11() { return static_cast<int32_t>(offsetof(Zip_tFE288625F1A712CC45FAE011730910DD457970CC, ___q5_11)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q5_11() const { return ___q5_11; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q5_11() { return &___q5_11; }
	inline void set_q5_11(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q5_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q5_11), (void*)value);
	}

	inline static int32_t get_offset_of_q6_12() { return static_cast<int32_t>(offsetof(Zip_tFE288625F1A712CC45FAE011730910DD457970CC, ___q6_12)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q6_12() const { return ___q6_12; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q6_12() { return &___q6_12; }
	inline void set_q6_12(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q6_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q6_12), (void*)value);
	}

	inline static int32_t get_offset_of_q7_13() { return static_cast<int32_t>(offsetof(Zip_tFE288625F1A712CC45FAE011730910DD457970CC, ___q7_13)); }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * get_q7_13() const { return ___q7_13; }
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** get_address_of_q7_13() { return &___q7_13; }
	inline void set_q7_13(Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		___q7_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___q7_13), (void*)value);
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Action
struct  Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6  : public MulticastDelegate_t
{
public:

public:
};


// System.AsyncCallback
struct  AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Boolean,System.Boolean>
struct  Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int64,System.Boolean>
struct  Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Boolean>
struct  Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Object>
struct  Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`3<System.Boolean,System.Int32,System.Boolean>
struct  Func_3_t3F60B00D281681419CA999636A62D3ED1B529778  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`3<System.Int64,System.Int32,System.Boolean>
struct  Func_3_t8FFBB3815B984FFFA353813F9E09AEA69D8DB623  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`3<System.Object,System.Int32,System.Boolean>
struct  Func_3_tCB498436E1553228C2C36F41918548DB2DF10E0A  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`3<System.Object,System.Object,System.Object>
struct  Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27  : public MulticastDelegate_t
{
public:

public:
};


// UniRx.Operators.ZipFunc`4<System.Object,System.Object,System.Object,System.Object>
struct  ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5  : public MulticastDelegate_t
{
public:

public:
};


// UniRx.Operators.ZipFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58  : public MulticastDelegate_t
{
public:

public:
};


// UniRx.Operators.ZipFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D  : public MulticastDelegate_t
{
public:

public:
};


// UniRx.Operators.ZipFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204  : public MulticastDelegate_t
{
public:

public:
};


// UniRx.Operators.ZipFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03  : public MulticastDelegate_t
{
public:

public:
};


// UniRx.Operators.ZipLatestFunc`4<System.Object,System.Object,System.Object,System.Object>
struct  ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652  : public MulticastDelegate_t
{
public:

public:
};


// UniRx.Operators.ZipLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90  : public MulticastDelegate_t
{
public:

public:
};


// UniRx.Operators.ZipLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F  : public MulticastDelegate_t
{
public:

public:
};


// UniRx.Operators.ZipLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606  : public MulticastDelegate_t
{
public:

public:
};


// UniRx.Operators.ZipLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.IDisposable[]
struct IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.IObservable`1<System.Object>[]
struct IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Collections.Generic.Queue`1<System.Object>[]
struct Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * m_Items[1];

public:
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Boolean[]
struct BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Collections.ICollection[]
struct ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UniRx.SingleAssignmentDisposable::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SingleAssignmentDisposable__ctor_m7C40CDEFECC8BB2EAE4A236CFB987D2C942EEECF (SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC * __this, const RuntimeMethod* method);
// System.Void UniRx.SingleAssignmentDisposable::set_Disposable(System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SingleAssignmentDisposable_set_Disposable_m792C4CF29925CE53CE13F5C9F0FA7B4CB9BE5743 (SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// UniRx.ICancelable UniRx.StableCompositeDisposable::Create(System.IDisposable,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StableCompositeDisposable_Create_mFBB35C8BE58E02B49CB9A9CAEC34B451A7DD0122 (RuntimeObject* ___disposable10, RuntimeObject* ___disposable21, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.IDisposable UniRx.Disposable::Create(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Disposable_Create_m3E97031C0F83935B39EA43B34B685D80B644DCE2 (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___disposeAction0, const RuntimeMethod* method);
// UniRx.ICancelable UniRx.StableCompositeDisposable::CreateUnsafe(System.IDisposable[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StableCompositeDisposable_CreateUnsafe_mBCB7541CE148A6314AC85043A4C1515A050A793A (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* ___disposables0, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4 (RuntimeObject * ___obj0, bool* ___lockTaken1, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Exit(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A (RuntimeObject * ___obj0, const RuntimeMethod* method);
// UniRx.ICancelable UniRx.StableCompositeDisposable::Create(System.IDisposable,System.IDisposable,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StableCompositeDisposable_Create_m1E0EF5E19E06763B493586C330216A928AB5D7A5 (RuntimeObject* ___disposable10, RuntimeObject* ___disposable21, RuntimeObject* ___disposable32, const RuntimeMethod* method);
// UniRx.ICancelable UniRx.StableCompositeDisposable::Create(System.IDisposable,System.IDisposable,System.IDisposable,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StableCompositeDisposable_Create_mCA54324578190C14AC89AF3A5D1A8401AB4546D0 (RuntimeObject* ___disposable10, RuntimeObject* ___disposable21, RuntimeObject* ___disposable32, RuntimeObject* ___disposable43, const RuntimeMethod* method);
// UniRx.ICancelable UniRx.StableCompositeDisposable::Create(System.IDisposable[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StableCompositeDisposable_Create_mA26CD55619BE45D7F630D010C9BB47D84E83BCAA (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* ___disposables0, const RuntimeMethod* method);
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Clear_mEB42D172C5E0825D340F6209F28578BDDDDCE34F (RuntimeArray * ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WhereObservable`1<System.Boolean>::.ctor(System.IObservable`1<T>,System.Func`2<T,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereObservable_1__ctor_m5A1FEA2191895061BA73A5F52587A0BB35A846CF_gshared (WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * __this, RuntimeObject* ___source0, Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * ___predicate1, const RuntimeMethod* method)
{
	{
		// : base(source.IsRequiredSubscribeOnCurrentThread())
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		((  void (*) (OperatorObservableBase_1_t530D2FE2B1B3F6717F04CEB073301DD8A6848C1B *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((OperatorObservableBase_1_t530D2FE2B1B3F6717F04CEB073301DD8A6848C1B *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		// this.source = source;
		RuntimeObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		// this.predicate = predicate;
		Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * L_3 = ___predicate1;
		__this->set_predicate_2(L_3);
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereObservable`1<System.Boolean>::.ctor(System.IObservable`1<T>,System.Func`3<T,System.Int32,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereObservable_1__ctor_mAB5D7681D50623FD26B89DF8420F84BAF583FE9B_gshared (WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * __this, RuntimeObject* ___source0, Func_3_t3F60B00D281681419CA999636A62D3ED1B529778 * ___predicateWithIndex1, const RuntimeMethod* method)
{
	{
		// : base(source.IsRequiredSubscribeOnCurrentThread())
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		((  void (*) (OperatorObservableBase_1_t530D2FE2B1B3F6717F04CEB073301DD8A6848C1B *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((OperatorObservableBase_1_t530D2FE2B1B3F6717F04CEB073301DD8A6848C1B *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		// this.source = source;
		RuntimeObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		// this.predicateWithIndex = predicateWithIndex;
		Func_3_t3F60B00D281681419CA999636A62D3ED1B529778 * L_3 = ___predicateWithIndex1;
		__this->set_predicateWithIndex_3(L_3);
		// }
		return;
	}
}
// System.IObservable`1<T> UniRx.Operators.WhereObservable`1<System.Boolean>::CombinePredicate(System.Func`2<T,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereObservable_1_CombinePredicate_m28BAFD5EA1A87596E9932D04994A169BC1A16897_gshared (WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * __this, Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * ___combinePredicate0, const RuntimeMethod* method)
{
	U3CU3Ec__DisplayClass5_0_t33976DE27D27D56496F94D672F06437B92399781 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass5_0_t33976DE27D27D56496F94D672F06437B92399781 * L_0 = (U3CU3Ec__DisplayClass5_0_t33976DE27D27D56496F94D672F06437B92399781 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (U3CU3Ec__DisplayClass5_0_t33976DE27D27D56496F94D672F06437B92399781 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		V_0 = (U3CU3Ec__DisplayClass5_0_t33976DE27D27D56496F94D672F06437B92399781 *)L_0;
		U3CU3Ec__DisplayClass5_0_t33976DE27D27D56496F94D672F06437B92399781 * L_1 = V_0;
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass5_0_t33976DE27D27D56496F94D672F06437B92399781 * L_2 = V_0;
		Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * L_3 = ___combinePredicate0;
		L_2->set_combinePredicate_1(L_3);
		// if (this.predicate != null)
		Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * L_4 = (Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *)__this->get_predicate_2();
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		// return new WhereObservable<T>(source, x => this.predicate(x) && combinePredicate(x));
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_source_1();
		U3CU3Ec__DisplayClass5_0_t33976DE27D27D56496F94D672F06437B92399781 * L_6 = V_0;
		Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * L_7 = (Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6));
		((  void (*) (Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)(L_7, (RuntimeObject *)L_6, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * L_8 = (WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8));
		((  void (*) (WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B *, RuntimeObject*, Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)(L_8, (RuntimeObject*)L_5, (Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		return L_8;
	}

IL_0034:
	{
		// return new WhereObservable<T>(this, combinePredicate);
		U3CU3Ec__DisplayClass5_0_t33976DE27D27D56496F94D672F06437B92399781 * L_9 = V_0;
		Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * L_10 = (Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *)L_9->get_combinePredicate_1();
		WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * L_11 = (WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8));
		((  void (*) (WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B *, RuntimeObject*, Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)(L_11, (RuntimeObject*)__this, (Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		return L_11;
	}
}
// System.IDisposable UniRx.Operators.WhereObservable`1<System.Boolean>::SubscribeCore(System.IObserver`1<T>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereObservable_1_SubscribeCore_mE05843BBBF7F94522DC347E121FCE1F08D19C55C_gshared (WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// if (predicate != null)
		Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * L_0 = (Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *)__this->get_predicate_2();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		// return source.Subscribe(new Where(this, observer, cancel));
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_source_1();
		RuntimeObject* L_2 = ___observer0;
		RuntimeObject* L_3 = ___cancel1;
		Where_tFCD764D70CF94AA9835D7814899E259A429FA17C * L_4 = (Where_tFCD764D70CF94AA9835D7814899E259A429FA17C *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (Where_tFCD764D70CF94AA9835D7814899E259A429FA17C *, WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_4, (WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B *)__this, (RuntimeObject*)L_2, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		RuntimeObject* L_5 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Boolean>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 12), (RuntimeObject*)L_1, (RuntimeObject*)L_4);
		return L_5;
	}

IL_001c:
	{
		// return source.Subscribe(new Where_(this, observer, cancel));
		RuntimeObject* L_6 = (RuntimeObject*)__this->get_source_1();
		RuntimeObject* L_7 = ___observer0;
		RuntimeObject* L_8 = ___cancel1;
		Where__t4B5EA5BC9EA34076FAD0EE0F30CD814FA7506B60 * L_9 = (Where__t4B5EA5BC9EA34076FAD0EE0F30CD814FA7506B60 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 13));
		((  void (*) (Where__t4B5EA5BC9EA34076FAD0EE0F30CD814FA7506B60 *, WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14)->methodPointer)(L_9, (WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B *)__this, (RuntimeObject*)L_7, (RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14));
		RuntimeObject* L_10 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Boolean>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 12), (RuntimeObject*)L_6, (RuntimeObject*)L_9);
		return L_10;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WhereObservable`1<System.Int64>::.ctor(System.IObservable`1<T>,System.Func`2<T,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereObservable_1__ctor_m3B13D6CC2383CFBD60720DD646E3A57E67E70A9F_gshared (WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * __this, RuntimeObject* ___source0, Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * ___predicate1, const RuntimeMethod* method)
{
	{
		// : base(source.IsRequiredSubscribeOnCurrentThread())
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		((  void (*) (OperatorObservableBase_1_t59F31167F0E3596ED1B2C8679413A193C2D468FD *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((OperatorObservableBase_1_t59F31167F0E3596ED1B2C8679413A193C2D468FD *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		// this.source = source;
		RuntimeObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		// this.predicate = predicate;
		Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * L_3 = ___predicate1;
		__this->set_predicate_2(L_3);
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereObservable`1<System.Int64>::.ctor(System.IObservable`1<T>,System.Func`3<T,System.Int32,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereObservable_1__ctor_m771204228CA3BABCDD4B89ABD274BEBF440D2FE9_gshared (WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * __this, RuntimeObject* ___source0, Func_3_t8FFBB3815B984FFFA353813F9E09AEA69D8DB623 * ___predicateWithIndex1, const RuntimeMethod* method)
{
	{
		// : base(source.IsRequiredSubscribeOnCurrentThread())
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		((  void (*) (OperatorObservableBase_1_t59F31167F0E3596ED1B2C8679413A193C2D468FD *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((OperatorObservableBase_1_t59F31167F0E3596ED1B2C8679413A193C2D468FD *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		// this.source = source;
		RuntimeObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		// this.predicateWithIndex = predicateWithIndex;
		Func_3_t8FFBB3815B984FFFA353813F9E09AEA69D8DB623 * L_3 = ___predicateWithIndex1;
		__this->set_predicateWithIndex_3(L_3);
		// }
		return;
	}
}
// System.IObservable`1<T> UniRx.Operators.WhereObservable`1<System.Int64>::CombinePredicate(System.Func`2<T,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereObservable_1_CombinePredicate_m007CBF9710FF2B454A2BCF2E534093E1C06CB433_gshared (WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * __this, Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * ___combinePredicate0, const RuntimeMethod* method)
{
	U3CU3Ec__DisplayClass5_0_tEE3C846FB04A6F0261B3DD92D4D0D18439DBF468 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass5_0_tEE3C846FB04A6F0261B3DD92D4D0D18439DBF468 * L_0 = (U3CU3Ec__DisplayClass5_0_tEE3C846FB04A6F0261B3DD92D4D0D18439DBF468 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (U3CU3Ec__DisplayClass5_0_tEE3C846FB04A6F0261B3DD92D4D0D18439DBF468 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		V_0 = (U3CU3Ec__DisplayClass5_0_tEE3C846FB04A6F0261B3DD92D4D0D18439DBF468 *)L_0;
		U3CU3Ec__DisplayClass5_0_tEE3C846FB04A6F0261B3DD92D4D0D18439DBF468 * L_1 = V_0;
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass5_0_tEE3C846FB04A6F0261B3DD92D4D0D18439DBF468 * L_2 = V_0;
		Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * L_3 = ___combinePredicate0;
		L_2->set_combinePredicate_1(L_3);
		// if (this.predicate != null)
		Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * L_4 = (Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 *)__this->get_predicate_2();
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		// return new WhereObservable<T>(source, x => this.predicate(x) && combinePredicate(x));
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_source_1();
		U3CU3Ec__DisplayClass5_0_tEE3C846FB04A6F0261B3DD92D4D0D18439DBF468 * L_6 = V_0;
		Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * L_7 = (Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6));
		((  void (*) (Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)(L_7, (RuntimeObject *)L_6, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * L_8 = (WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8));
		((  void (*) (WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F *, RuntimeObject*, Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)(L_8, (RuntimeObject*)L_5, (Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		return L_8;
	}

IL_0034:
	{
		// return new WhereObservable<T>(this, combinePredicate);
		U3CU3Ec__DisplayClass5_0_tEE3C846FB04A6F0261B3DD92D4D0D18439DBF468 * L_9 = V_0;
		Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * L_10 = (Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 *)L_9->get_combinePredicate_1();
		WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * L_11 = (WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8));
		((  void (*) (WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F *, RuntimeObject*, Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)(L_11, (RuntimeObject*)__this, (Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		return L_11;
	}
}
// System.IDisposable UniRx.Operators.WhereObservable`1<System.Int64>::SubscribeCore(System.IObserver`1<T>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereObservable_1_SubscribeCore_m3FBEDA930956DEFC40977FC237A77E7B4FAC8D05_gshared (WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// if (predicate != null)
		Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 * L_0 = (Func_2_t03B9AF86FB7E93B62590F1CBD16276EF7AA1B215 *)__this->get_predicate_2();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		// return source.Subscribe(new Where(this, observer, cancel));
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_source_1();
		RuntimeObject* L_2 = ___observer0;
		RuntimeObject* L_3 = ___cancel1;
		Where_t97078FF7350BA11174458F7A8E25B57004E2B900 * L_4 = (Where_t97078FF7350BA11174458F7A8E25B57004E2B900 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (Where_t97078FF7350BA11174458F7A8E25B57004E2B900 *, WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_4, (WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F *)__this, (RuntimeObject*)L_2, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		RuntimeObject* L_5 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Int64>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 12), (RuntimeObject*)L_1, (RuntimeObject*)L_4);
		return L_5;
	}

IL_001c:
	{
		// return source.Subscribe(new Where_(this, observer, cancel));
		RuntimeObject* L_6 = (RuntimeObject*)__this->get_source_1();
		RuntimeObject* L_7 = ___observer0;
		RuntimeObject* L_8 = ___cancel1;
		Where__t5E913FC3047D357D4D04A0BF656999DBCBF4C6C5 * L_9 = (Where__t5E913FC3047D357D4D04A0BF656999DBCBF4C6C5 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 13));
		((  void (*) (Where__t5E913FC3047D357D4D04A0BF656999DBCBF4C6C5 *, WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14)->methodPointer)(L_9, (WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F *)__this, (RuntimeObject*)L_7, (RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14));
		RuntimeObject* L_10 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Int64>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 12), (RuntimeObject*)L_6, (RuntimeObject*)L_9);
		return L_10;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WhereObservable`1<System.Object>::.ctor(System.IObservable`1<T>,System.Func`2<T,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereObservable_1__ctor_mD4168051627CC09811C10CDC65B7BE4548469ABE_gshared (WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * __this, RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method)
{
	{
		// : base(source.IsRequiredSubscribeOnCurrentThread())
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		// this.source = source;
		RuntimeObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		// this.predicate = predicate;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_3 = ___predicate1;
		__this->set_predicate_2(L_3);
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereObservable`1<System.Object>::.ctor(System.IObservable`1<T>,System.Func`3<T,System.Int32,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereObservable_1__ctor_mFCA12B4A29818FCF44C8E6F3D6DDBAAD8F6E4BE0_gshared (WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * __this, RuntimeObject* ___source0, Func_3_tCB498436E1553228C2C36F41918548DB2DF10E0A * ___predicateWithIndex1, const RuntimeMethod* method)
{
	{
		// : base(source.IsRequiredSubscribeOnCurrentThread())
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		// this.source = source;
		RuntimeObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		// this.predicateWithIndex = predicateWithIndex;
		Func_3_tCB498436E1553228C2C36F41918548DB2DF10E0A * L_3 = ___predicateWithIndex1;
		__this->set_predicateWithIndex_3(L_3);
		// }
		return;
	}
}
// System.IObservable`1<T> UniRx.Operators.WhereObservable`1<System.Object>::CombinePredicate(System.Func`2<T,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereObservable_1_CombinePredicate_m2980620C8ED2CD39B76470FE93D0C2199F9FFC10_gshared (WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___combinePredicate0, const RuntimeMethod* method)
{
	U3CU3Ec__DisplayClass5_0_t24107840801D474B1F350BA289D0DF100435FE33 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass5_0_t24107840801D474B1F350BA289D0DF100435FE33 * L_0 = (U3CU3Ec__DisplayClass5_0_t24107840801D474B1F350BA289D0DF100435FE33 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (U3CU3Ec__DisplayClass5_0_t24107840801D474B1F350BA289D0DF100435FE33 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		V_0 = (U3CU3Ec__DisplayClass5_0_t24107840801D474B1F350BA289D0DF100435FE33 *)L_0;
		U3CU3Ec__DisplayClass5_0_t24107840801D474B1F350BA289D0DF100435FE33 * L_1 = V_0;
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass5_0_t24107840801D474B1F350BA289D0DF100435FE33 * L_2 = V_0;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_3 = ___combinePredicate0;
		L_2->set_combinePredicate_1(L_3);
		// if (this.predicate != null)
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_4 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_2();
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		// return new WhereObservable<T>(source, x => this.predicate(x) && combinePredicate(x));
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_source_1();
		U3CU3Ec__DisplayClass5_0_t24107840801D474B1F350BA289D0DF100435FE33 * L_6 = V_0;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6));
		((  void (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)(L_7, (RuntimeObject *)L_6, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * L_8 = (WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8));
		((  void (*) (WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)(L_8, (RuntimeObject*)L_5, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		return L_8;
	}

IL_0034:
	{
		// return new WhereObservable<T>(this, combinePredicate);
		U3CU3Ec__DisplayClass5_0_t24107840801D474B1F350BA289D0DF100435FE33 * L_9 = V_0;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_10 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_9->get_combinePredicate_1();
		WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * L_11 = (WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8));
		((  void (*) (WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)(L_11, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		return L_11;
	}
}
// System.IDisposable UniRx.Operators.WhereObservable`1<System.Object>::SubscribeCore(System.IObserver`1<T>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereObservable_1_SubscribeCore_mB573C881A1B2495F9A599D9728B35FFC3F618363_gshared (WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// if (predicate != null)
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_2();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		// return source.Subscribe(new Where(this, observer, cancel));
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_source_1();
		RuntimeObject* L_2 = ___observer0;
		RuntimeObject* L_3 = ___cancel1;
		Where_t6008305F5D07BBFE21D61C328E8EBD66A350D97B * L_4 = (Where_t6008305F5D07BBFE21D61C328E8EBD66A350D97B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (Where_t6008305F5D07BBFE21D61C328E8EBD66A350D97B *, WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_4, (WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 *)__this, (RuntimeObject*)L_2, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		RuntimeObject* L_5 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 12), (RuntimeObject*)L_1, (RuntimeObject*)L_4);
		return L_5;
	}

IL_001c:
	{
		// return source.Subscribe(new Where_(this, observer, cancel));
		RuntimeObject* L_6 = (RuntimeObject*)__this->get_source_1();
		RuntimeObject* L_7 = ___observer0;
		RuntimeObject* L_8 = ___cancel1;
		Where__tC4BFE268565371D15C47C91FF86D6E2F822710EB * L_9 = (Where__tC4BFE268565371D15C47C91FF86D6E2F822710EB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 13));
		((  void (*) (Where__tC4BFE268565371D15C47C91FF86D6E2F822710EB *, WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14)->methodPointer)(L_9, (WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 *)__this, (RuntimeObject*)L_7, (RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14));
		RuntimeObject* L_10 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 12), (RuntimeObject*)L_6, (RuntimeObject*)L_9);
		return L_10;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Boolean,System.Boolean>::.ctor(UniRx.Operators.WhereSelectObservable`2<T,TR>,System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelect__ctor_m02BE7EB65518794B3D4930B7833D6C0FBB780FE9_gshared (WhereSelect_t2C19B6051587E495A79CF7134957F51AB0F8CB99 * __this, WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	{
		// : base(observer, cancel)
		RuntimeObject* L_0 = ___observer1;
		RuntimeObject* L_1 = ___cancel2;
		((  void (*) (OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Boolean,System.Boolean>::OnNext(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelect_OnNext_mAD438473015AD79949F4EB612BAFA5FB8ADE963D_gshared (WhereSelect_t2C19B6051587E495A79CF7134957F51AB0F8CB99 * __this, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelect_OnNext_mAD438473015AD79949F4EB612BAFA5FB8ADE963D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t * V_1 = NULL;
	bool V_2 = false;
	Exception_t * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 6);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// var isPassed = false;
		V_0 = (bool)0;
	}

IL_0002:
	try
	{ // begin try (depth: 1)
		// isPassed = parent.predicate(value);
		WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C * L_0 = (WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C *)__this->get_parent_2();
		Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * L_1 = (Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *)L_0->get_predicate_2();
		bool L_2 = ___value0;
		bool L_3 = ((  bool (*) (Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *)L_1, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		V_0 = (bool)L_3;
		// }
		goto IL_0030;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0016;
		throw e;
	}

CATCH_0016:
	{ // begin catch(System.Exception)
		{
			// catch (Exception ex)
			V_1 = (Exception_t *)((Exception_t *)__exception_local);
		}

IL_0017:
		try
		{ // begin try (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			RuntimeObject* L_4 = (RuntimeObject*)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t * L_5 = V_1;
			InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_4, (Exception_t *)L_5);
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_LEAVE(0x2E, FINALLY_0027);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0027;
		}

FINALLY_0027:
		{ // begin finally (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			((  void (*) (OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_END_FINALLY(39)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(39)
		{
			IL2CPP_JUMP_TBL(0x2E, IL_002e)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_002e:
		{
			// return;
			goto IL_0077;
		}
	} // end catch (depth: 1)

IL_0030:
	{
		// if (isPassed)
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0077;
		}
	}
	{
		// var v = default(TR);
		il2cpp_codegen_initobj((&V_2), sizeof(bool));
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		// v = parent.selector(value);
		WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C * L_7 = (WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C *)__this->get_parent_2();
		Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * L_8 = (Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *)L_7->get_selector_3();
		bool L_9 = ___value0;
		bool L_10 = ((  bool (*) (Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B *)L_8, (bool)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_2 = (bool)L_10;
		// }
		goto IL_0069;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_004f;
		throw e;
	}

CATCH_004f:
	{ // begin catch(System.Exception)
		{
			// catch (Exception ex)
			V_3 = (Exception_t *)((Exception_t *)__exception_local);
		}

IL_0050:
		try
		{ // begin try (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			RuntimeObject* L_11 = (RuntimeObject*)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t * L_12 = V_3;
			InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_11, (Exception_t *)L_12);
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0060;
		}

FINALLY_0060:
		{ // begin finally (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			((  void (*) (OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_END_FINALLY(96)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(96)
		{
			IL2CPP_JUMP_TBL(0x67, IL_0067)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0067:
		{
			// return;
			goto IL_0077;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		// observer.OnNext(v);
		RuntimeObject* L_13 = (RuntimeObject*)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_14 = V_2;
		InterfaceActionInvoker1< bool >::Invoke(0 /* System.Void System.IObserver`1<System.Boolean>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_13, (bool)L_14);
	}

IL_0077:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Boolean,System.Boolean>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelect_OnError_m11C6AA854A4C0DC20E2FB5003195FAADB368BE3C_gshared (WhereSelect_t2C19B6051587E495A79CF7134957F51AB0F8CB99 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); } finally { Dispose(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); } finally { Dispose(); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// try { observer.OnError(error); } finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// try { observer.OnError(error); } finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Boolean,System.Boolean>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelect_OnCompleted_mCD0C926858D5A6ED1EB1F61ED8C2386D11E0FBE1_gshared (WhereSelect_t2C19B6051587E495A79CF7134957F51AB0F8CB99 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); } finally { Dispose(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); } finally { Dispose(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// try { observer.OnCompleted(); } finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// try { observer.OnCompleted(); } finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Object,System.Boolean>::.ctor(UniRx.Operators.WhereSelectObservable`2<T,TR>,System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelect__ctor_mC0227823ECF49FA253598F631813FB207D0ACAA0_gshared (WhereSelect_t7CA94D47BE319C6E9D59A58019438DA0D06C8719 * __this, WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	{
		// : base(observer, cancel)
		RuntimeObject* L_0 = ___observer1;
		RuntimeObject* L_1 = ___cancel2;
		((  void (*) (OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Object,System.Boolean>::OnNext(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelect_OnNext_m303FF425D2880CCE79EED7DFE611E91E614B1CA5_gshared (WhereSelect_t7CA94D47BE319C6E9D59A58019438DA0D06C8719 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelect_OnNext_m303FF425D2880CCE79EED7DFE611E91E614B1CA5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t * V_1 = NULL;
	bool V_2 = false;
	Exception_t * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 6);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// var isPassed = false;
		V_0 = (bool)0;
	}

IL_0002:
	try
	{ // begin try (depth: 1)
		// isPassed = parent.predicate(value);
		WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 * L_0 = (WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 *)__this->get_parent_2();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0->get_predicate_2();
		RuntimeObject * L_2 = ___value0;
		bool L_3 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		V_0 = (bool)L_3;
		// }
		goto IL_0030;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0016;
		throw e;
	}

CATCH_0016:
	{ // begin catch(System.Exception)
		{
			// catch (Exception ex)
			V_1 = (Exception_t *)((Exception_t *)__exception_local);
		}

IL_0017:
		try
		{ // begin try (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			RuntimeObject* L_4 = (RuntimeObject*)((OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t * L_5 = V_1;
			InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_4, (Exception_t *)L_5);
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_LEAVE(0x2E, FINALLY_0027);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0027;
		}

FINALLY_0027:
		{ // begin finally (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			((  void (*) (OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_END_FINALLY(39)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(39)
		{
			IL2CPP_JUMP_TBL(0x2E, IL_002e)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_002e:
		{
			// return;
			goto IL_0077;
		}
	} // end catch (depth: 1)

IL_0030:
	{
		// if (isPassed)
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0077;
		}
	}
	{
		// var v = default(TR);
		il2cpp_codegen_initobj((&V_2), sizeof(bool));
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		// v = parent.selector(value);
		WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 * L_7 = (WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 *)__this->get_parent_2();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_7->get_selector_3();
		RuntimeObject * L_9 = ___value0;
		bool L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_2 = (bool)L_10;
		// }
		goto IL_0069;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_004f;
		throw e;
	}

CATCH_004f:
	{ // begin catch(System.Exception)
		{
			// catch (Exception ex)
			V_3 = (Exception_t *)((Exception_t *)__exception_local);
		}

IL_0050:
		try
		{ // begin try (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			RuntimeObject* L_11 = (RuntimeObject*)((OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t * L_12 = V_3;
			InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_11, (Exception_t *)L_12);
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0060;
		}

FINALLY_0060:
		{ // begin finally (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			((  void (*) (OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_END_FINALLY(96)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(96)
		{
			IL2CPP_JUMP_TBL(0x67, IL_0067)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0067:
		{
			// return;
			goto IL_0077;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		// observer.OnNext(v);
		RuntimeObject* L_13 = (RuntimeObject*)((OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_14 = V_2;
		InterfaceActionInvoker1< bool >::Invoke(0 /* System.Void System.IObserver`1<System.Boolean>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_13, (bool)L_14);
	}

IL_0077:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Object,System.Boolean>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelect_OnError_m05FAC77E716381F228ACDEBA5ABD540483B827C8_gshared (WhereSelect_t7CA94D47BE319C6E9D59A58019438DA0D06C8719 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); } finally { Dispose(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); } finally { Dispose(); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// try { observer.OnError(error); } finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// try { observer.OnError(error); } finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Object,System.Boolean>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelect_OnCompleted_m8FBDF4625AB343CB076C10BC535B62E4120629C9_gshared (WhereSelect_t7CA94D47BE319C6E9D59A58019438DA0D06C8719 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); } finally { Dispose(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); } finally { Dispose(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// try { observer.OnCompleted(); } finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tD120CEEEE13341319ABD89B18028534743949C00 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// try { observer.OnCompleted(); } finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Object,System.Object>::.ctor(UniRx.Operators.WhereSelectObservable`2<T,TR>,System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelect__ctor_mB753D4B679552BCD8C9A113EC06D1F111738C0F3_gshared (WhereSelect_t6984636B2863BAE990462A289EC7A8841F77157D * __this, WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	{
		// : base(observer, cancel)
		RuntimeObject* L_0 = ___observer1;
		RuntimeObject* L_1 = ___cancel2;
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Object,System.Object>::OnNext(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelect_OnNext_mADF7FE4C82A7A66A749740E722021F4AF3970497_gshared (WhereSelect_t6984636B2863BAE990462A289EC7A8841F77157D * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelect_OnNext_mADF7FE4C82A7A66A749740E722021F4AF3970497_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t * V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	Exception_t * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 6);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// var isPassed = false;
		V_0 = (bool)0;
	}

IL_0002:
	try
	{ // begin try (depth: 1)
		// isPassed = parent.predicate(value);
		WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F * L_0 = (WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F *)__this->get_parent_2();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0->get_predicate_2();
		RuntimeObject * L_2 = ___value0;
		bool L_3 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		V_0 = (bool)L_3;
		// }
		goto IL_0030;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0016;
		throw e;
	}

CATCH_0016:
	{ // begin catch(System.Exception)
		{
			// catch (Exception ex)
			V_1 = (Exception_t *)((Exception_t *)__exception_local);
		}

IL_0017:
		try
		{ // begin try (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			RuntimeObject* L_4 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t * L_5 = V_1;
			InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_4, (Exception_t *)L_5);
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_LEAVE(0x2E, FINALLY_0027);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0027;
		}

FINALLY_0027:
		{ // begin finally (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_END_FINALLY(39)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(39)
		{
			IL2CPP_JUMP_TBL(0x2E, IL_002e)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_002e:
		{
			// return;
			goto IL_0077;
		}
	} // end catch (depth: 1)

IL_0030:
	{
		// if (isPassed)
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0077;
		}
	}
	{
		// var v = default(TR);
		il2cpp_codegen_initobj((&V_2), sizeof(RuntimeObject *));
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		// v = parent.selector(value);
		WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F * L_7 = (WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F *)__this->get_parent_2();
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_8 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_7->get_selector_3();
		RuntimeObject * L_9 = ___value0;
		RuntimeObject * L_10 = ((  RuntimeObject * (*) (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_2 = (RuntimeObject *)L_10;
		// }
		goto IL_0069;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_004f;
		throw e;
	}

CATCH_004f:
	{ // begin catch(System.Exception)
		{
			// catch (Exception ex)
			V_3 = (Exception_t *)((Exception_t *)__exception_local);
		}

IL_0050:
		try
		{ // begin try (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			RuntimeObject* L_11 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t * L_12 = V_3;
			InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_11, (Exception_t *)L_12);
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_LEAVE(0x67, FINALLY_0060);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0060;
		}

FINALLY_0060:
		{ // begin finally (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_END_FINALLY(96)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(96)
		{
			IL2CPP_JUMP_TBL(0x67, IL_0067)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0067:
		{
			// return;
			goto IL_0077;
		}
	} // end catch (depth: 1)

IL_0069:
	{
		// observer.OnNext(v);
		RuntimeObject* L_13 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_14 = V_2;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_13, (RuntimeObject *)L_14);
	}

IL_0077:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelect_OnError_mD76D0FCAD21FAE19054006A43EBEE901E8A601F0_gshared (WhereSelect_t6984636B2863BAE990462A289EC7A8841F77157D * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); } finally { Dispose(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); } finally { Dispose(); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// try { observer.OnError(error); } finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// try { observer.OnError(error); } finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereSelectObservable`2_WhereSelect<System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelect_OnCompleted_m0E8EBEBE82F53A8A094CCDD7830B692EC688180C_gshared (WhereSelect_t6984636B2863BAE990462A289EC7A8841F77157D * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); } finally { Dispose(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); } finally { Dispose(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// try { observer.OnCompleted(); } finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// try { observer.OnCompleted(); } finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WhereSelectObservable`2<System.Boolean,System.Boolean>::.ctor(System.IObservable`1<T>,System.Func`2<T,System.Boolean>,System.Func`2<T,TR>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectObservable_2__ctor_mE06AABD709CA113C38EB08C67D01CFA5BDE2E407_gshared (WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C * __this, RuntimeObject* ___source0, Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * ___predicate1, Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * ___selector2, const RuntimeMethod* method)
{
	{
		// : base(source.IsRequiredSubscribeOnCurrentThread())
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		((  void (*) (OperatorObservableBase_1_t530D2FE2B1B3F6717F04CEB073301DD8A6848C1B *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((OperatorObservableBase_1_t530D2FE2B1B3F6717F04CEB073301DD8A6848C1B *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		// this.source = source;
		RuntimeObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		// this.predicate = predicate;
		Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * L_3 = ___predicate1;
		__this->set_predicate_2(L_3);
		// this.selector = selector;
		Func_2_t50F598941CFDF02619DC5D52FFDBC329473C5F7B * L_4 = ___selector2;
		__this->set_selector_3(L_4);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.WhereSelectObservable`2<System.Boolean,System.Boolean>::SubscribeCore(System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectObservable_2_SubscribeCore_m88079FCF3FD415FFFFAB7D22363372E2B929D810_gshared (WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return source.Subscribe(new WhereSelect(this, observer, cancel));
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_1();
		RuntimeObject* L_1 = ___observer0;
		RuntimeObject* L_2 = ___cancel1;
		WhereSelect_t2C19B6051587E495A79CF7134957F51AB0F8CB99 * L_3 = (WhereSelect_t2C19B6051587E495A79CF7134957F51AB0F8CB99 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (WhereSelect_t2C19B6051587E495A79CF7134957F51AB0F8CB99 *, WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_3, (WhereSelectObservable_2_tD4BBEFAF0B9C378C01C06A07F55B88C25001F63C *)__this, (RuntimeObject*)L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		RuntimeObject* L_4 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Boolean>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_0, (RuntimeObject*)L_3);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WhereSelectObservable`2<System.Object,System.Boolean>::.ctor(System.IObservable`1<T>,System.Func`2<T,System.Boolean>,System.Func`2<T,TR>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectObservable_2__ctor_mB0813EE5F859D01472EA8857A9BCA7BC5CA7BA37_gshared (WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 * __this, RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___selector2, const RuntimeMethod* method)
{
	{
		// : base(source.IsRequiredSubscribeOnCurrentThread())
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		((  void (*) (OperatorObservableBase_1_t530D2FE2B1B3F6717F04CEB073301DD8A6848C1B *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((OperatorObservableBase_1_t530D2FE2B1B3F6717F04CEB073301DD8A6848C1B *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		// this.source = source;
		RuntimeObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		// this.predicate = predicate;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_3 = ___predicate1;
		__this->set_predicate_2(L_3);
		// this.selector = selector;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_4 = ___selector2;
		__this->set_selector_3(L_4);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.WhereSelectObservable`2<System.Object,System.Boolean>::SubscribeCore(System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectObservable_2_SubscribeCore_m5554E90A89D82890E39FC347DEB5FA14C45EBDC6_gshared (WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return source.Subscribe(new WhereSelect(this, observer, cancel));
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_1();
		RuntimeObject* L_1 = ___observer0;
		RuntimeObject* L_2 = ___cancel1;
		WhereSelect_t7CA94D47BE319C6E9D59A58019438DA0D06C8719 * L_3 = (WhereSelect_t7CA94D47BE319C6E9D59A58019438DA0D06C8719 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (WhereSelect_t7CA94D47BE319C6E9D59A58019438DA0D06C8719 *, WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_3, (WhereSelectObservable_2_t03C6DB90073F5F23807A9C358B8955474611A870 *)__this, (RuntimeObject*)L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		RuntimeObject* L_4 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_0, (RuntimeObject*)L_3);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WhereSelectObservable`2<System.Object,System.Object>::.ctor(System.IObservable`1<T>,System.Func`2<T,System.Boolean>,System.Func`2<T,TR>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectObservable_2__ctor_mE9C83F897373E41E0383E5BC7E67FF758A4894A1_gshared (WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F * __this, RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector2, const RuntimeMethod* method)
{
	{
		// : base(source.IsRequiredSubscribeOnCurrentThread())
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		// this.source = source;
		RuntimeObject* L_2 = ___source0;
		__this->set_source_1(L_2);
		// this.predicate = predicate;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_3 = ___predicate1;
		__this->set_predicate_2(L_3);
		// this.selector = selector;
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_4 = ___selector2;
		__this->set_selector_3(L_4);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.WhereSelectObservable`2<System.Object,System.Object>::SubscribeCore(System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectObservable_2_SubscribeCore_m75C852B34F3EA114039A0B1ACF7D0D18247A35A2_gshared (WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return source.Subscribe(new WhereSelect(this, observer, cancel));
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_1();
		RuntimeObject* L_1 = ___observer0;
		RuntimeObject* L_2 = ___cancel1;
		WhereSelect_t6984636B2863BAE990462A289EC7A8841F77157D * L_3 = (WhereSelect_t6984636B2863BAE990462A289EC7A8841F77157D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (WhereSelect_t6984636B2863BAE990462A289EC7A8841F77157D *, WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_3, (WhereSelectObservable_2_t6998AC695D0329ADB0A8D4DEF04F8438A304186F *)__this, (RuntimeObject*)L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		RuntimeObject* L_4 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_0, (RuntimeObject*)L_3);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WhereObservable`1_Where_<System.Boolean>::.ctor(UniRx.Operators.WhereObservable`1<T>,System.IObserver`1<T>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where___ctor_m4EEA64FD7484E3D667AC73B19880F8350584EB45_gshared (Where__t4B5EA5BC9EA34076FAD0EE0F30CD814FA7506B60 * __this, WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	{
		// : base(observer, cancel)
		RuntimeObject* L_0 = ___observer1;
		RuntimeObject* L_1 = ___cancel2;
		((  void (*) (OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		// this.index = 0;
		__this->set_index_3(0);
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereObservable`1_Where_<System.Boolean>::OnNext(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where__OnNext_m0CCB53E2FFB7B30017787F8F5BAB903579C7089A_gshared (Where__t4B5EA5BC9EA34076FAD0EE0F30CD814FA7506B60 * __this, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Where__OnNext_m0CCB53E2FFB7B30017787F8F5BAB903579C7089A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Exception_t * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// var isPassed = false;
		V_0 = (bool)0;
	}

IL_0002:
	try
	{ // begin try (depth: 1)
		// isPassed = parent.predicateWithIndex(value, index++);
		WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B * L_0 = (WhereObservable_1_t669637C945265C8B7B37847E335B119E7A24904B *)__this->get_parent_2();
		Func_3_t3F60B00D281681419CA999636A62D3ED1B529778 * L_1 = (Func_3_t3F60B00D281681419CA999636A62D3ED1B529778 *)L_0->get_predicateWithIndex_3();
		bool L_2 = ___value0;
		int32_t L_3 = (int32_t)__this->get_index_3();
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_1;
		__this->set_index_3(((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)));
		int32_t L_5 = V_1;
		bool L_6 = ((  bool (*) (Func_3_t3F60B00D281681419CA999636A62D3ED1B529778 *, bool, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Func_3_t3F60B00D281681419CA999636A62D3ED1B529778 *)L_1, (bool)L_2, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		V_0 = (bool)L_6;
		// }
		goto IL_0041;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0027;
		throw e;
	}

CATCH_0027:
	{ // begin catch(System.Exception)
		{
			// catch (Exception ex)
			V_2 = (Exception_t *)((Exception_t *)__exception_local);
		}

IL_0028:
		try
		{ // begin try (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			RuntimeObject* L_7 = (RuntimeObject*)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t * L_8 = V_2;
			InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_7, (Exception_t *)L_8);
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_LEAVE(0x3F, FINALLY_0038);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0038;
		}

FINALLY_0038:
		{ // begin finally (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			((  void (*) (OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_END_FINALLY(56)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(56)
		{
			IL2CPP_JUMP_TBL(0x3F, IL_003f)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_003f:
		{
			// return;
			goto IL_0052;
		}
	} // end catch (depth: 1)

IL_0041:
	{
		// if (isPassed)
		bool L_9 = V_0;
		if (!L_9)
		{
			goto IL_0052;
		}
	}
	{
		// observer.OnNext(value);
		RuntimeObject* L_10 = (RuntimeObject*)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		bool L_11 = ___value0;
		InterfaceActionInvoker1< bool >::Invoke(0 /* System.Void System.IObserver`1<System.Boolean>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_10, (bool)L_11);
	}

IL_0052:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereObservable`1_Where_<System.Boolean>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where__OnError_m73C68CC013329B549D900E25DC71FCD2BAEB22D3_gshared (Where__t4B5EA5BC9EA34076FAD0EE0F30CD814FA7506B60 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); } finally { Dispose(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); } finally { Dispose(); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// try { observer.OnError(error); } finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// try { observer.OnError(error); } finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereObservable`1_Where_<System.Boolean>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where__OnCompleted_m94D1C65D18A3DC5029A4D2A2E1650DE20800FAAD_gshared (Where__t4B5EA5BC9EA34076FAD0EE0F30CD814FA7506B60 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); } finally { Dispose(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); } finally { Dispose(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// try { observer.OnCompleted(); } finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_t2B5B49AB01E591EEA04A59FF0D61F84227BB45E4 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// try { observer.OnCompleted(); } finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WhereObservable`1_Where_<System.Int64>::.ctor(UniRx.Operators.WhereObservable`1<T>,System.IObserver`1<T>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where___ctor_m5ABEBD816E9090CD5890470DBF5809C0A0F433F6_gshared (Where__t5E913FC3047D357D4D04A0BF656999DBCBF4C6C5 * __this, WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	{
		// : base(observer, cancel)
		RuntimeObject* L_0 = ___observer1;
		RuntimeObject* L_1 = ___cancel2;
		((  void (*) (OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		// this.index = 0;
		__this->set_index_3(0);
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereObservable`1_Where_<System.Int64>::OnNext(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where__OnNext_m7CAA64E5DF2545B8F7ECC5908026612D6A8C5A7B_gshared (Where__t5E913FC3047D357D4D04A0BF656999DBCBF4C6C5 * __this, int64_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Where__OnNext_m7CAA64E5DF2545B8F7ECC5908026612D6A8C5A7B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Exception_t * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// var isPassed = false;
		V_0 = (bool)0;
	}

IL_0002:
	try
	{ // begin try (depth: 1)
		// isPassed = parent.predicateWithIndex(value, index++);
		WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F * L_0 = (WhereObservable_1_t53BDC3815CAAB73FC1C03005AB9A39B48A3DBF2F *)__this->get_parent_2();
		Func_3_t8FFBB3815B984FFFA353813F9E09AEA69D8DB623 * L_1 = (Func_3_t8FFBB3815B984FFFA353813F9E09AEA69D8DB623 *)L_0->get_predicateWithIndex_3();
		int64_t L_2 = ___value0;
		int32_t L_3 = (int32_t)__this->get_index_3();
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_1;
		__this->set_index_3(((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)));
		int32_t L_5 = V_1;
		bool L_6 = ((  bool (*) (Func_3_t8FFBB3815B984FFFA353813F9E09AEA69D8DB623 *, int64_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Func_3_t8FFBB3815B984FFFA353813F9E09AEA69D8DB623 *)L_1, (int64_t)L_2, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		V_0 = (bool)L_6;
		// }
		goto IL_0041;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0027;
		throw e;
	}

CATCH_0027:
	{ // begin catch(System.Exception)
		{
			// catch (Exception ex)
			V_2 = (Exception_t *)((Exception_t *)__exception_local);
		}

IL_0028:
		try
		{ // begin try (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			RuntimeObject* L_7 = (RuntimeObject*)((OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t * L_8 = V_2;
			InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_7, (Exception_t *)L_8);
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_LEAVE(0x3F, FINALLY_0038);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0038;
		}

FINALLY_0038:
		{ // begin finally (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			((  void (*) (OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_END_FINALLY(56)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(56)
		{
			IL2CPP_JUMP_TBL(0x3F, IL_003f)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_003f:
		{
			// return;
			goto IL_0052;
		}
	} // end catch (depth: 1)

IL_0041:
	{
		// if (isPassed)
		bool L_9 = V_0;
		if (!L_9)
		{
			goto IL_0052;
		}
	}
	{
		// observer.OnNext(value);
		RuntimeObject* L_10 = (RuntimeObject*)((OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		int64_t L_11 = ___value0;
		InterfaceActionInvoker1< int64_t >::Invoke(0 /* System.Void System.IObserver`1<System.Int64>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_10, (int64_t)L_11);
	}

IL_0052:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereObservable`1_Where_<System.Int64>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where__OnError_m46D112679EF32F3E8DD79BFB38B5FF8E65D9476B_gshared (Where__t5E913FC3047D357D4D04A0BF656999DBCBF4C6C5 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); } finally { Dispose(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); } finally { Dispose(); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// try { observer.OnError(error); } finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// try { observer.OnError(error); } finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereObservable`1_Where_<System.Int64>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where__OnCompleted_m6DFE65D3D94339B6AD0C305E67CA9CF98C9564A2_gshared (Where__t5E913FC3047D357D4D04A0BF656999DBCBF4C6C5 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); } finally { Dispose(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); } finally { Dispose(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// try { observer.OnCompleted(); } finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tA4288C558C1A419D8191BB6998F2EBD96C65D92A *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// try { observer.OnCompleted(); } finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WhereObservable`1_Where_<System.Object>::.ctor(UniRx.Operators.WhereObservable`1<T>,System.IObserver`1<T>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where___ctor_m67EDAE82ECCC9211F6CB5CBB88A82BD5B72D22C9_gshared (Where__tC4BFE268565371D15C47C91FF86D6E2F822710EB * __this, WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	{
		// : base(observer, cancel)
		RuntimeObject* L_0 = ___observer1;
		RuntimeObject* L_1 = ___cancel2;
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * L_2 = ___parent0;
		__this->set_parent_2(L_2);
		// this.index = 0;
		__this->set_index_3(0);
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereObservable`1_Where_<System.Object>::OnNext(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where__OnNext_m4B9950EDD00F399442B93F5C45CA5C3F09BCA73D_gshared (Where__tC4BFE268565371D15C47C91FF86D6E2F822710EB * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Where__OnNext_m4B9950EDD00F399442B93F5C45CA5C3F09BCA73D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Exception_t * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// var isPassed = false;
		V_0 = (bool)0;
	}

IL_0002:
	try
	{ // begin try (depth: 1)
		// isPassed = parent.predicateWithIndex(value, index++);
		WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 * L_0 = (WhereObservable_1_tB4B71FFCA52BF8EDCD4746D2FAC2EF485E609C01 *)__this->get_parent_2();
		Func_3_tCB498436E1553228C2C36F41918548DB2DF10E0A * L_1 = (Func_3_tCB498436E1553228C2C36F41918548DB2DF10E0A *)L_0->get_predicateWithIndex_3();
		RuntimeObject * L_2 = ___value0;
		int32_t L_3 = (int32_t)__this->get_index_3();
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_1;
		__this->set_index_3(((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)));
		int32_t L_5 = V_1;
		bool L_6 = ((  bool (*) (Func_3_tCB498436E1553228C2C36F41918548DB2DF10E0A *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Func_3_tCB498436E1553228C2C36F41918548DB2DF10E0A *)L_1, (RuntimeObject *)L_2, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		V_0 = (bool)L_6;
		// }
		goto IL_0041;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0027;
		throw e;
	}

CATCH_0027:
	{ // begin catch(System.Exception)
		{
			// catch (Exception ex)
			V_2 = (Exception_t *)((Exception_t *)__exception_local);
		}

IL_0028:
		try
		{ // begin try (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			RuntimeObject* L_7 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t * L_8 = V_2;
			InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_7, (Exception_t *)L_8);
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_LEAVE(0x3F, FINALLY_0038);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0038;
		}

FINALLY_0038:
		{ // begin finally (depth: 2)
			// try { observer.OnError(ex); } finally { Dispose(); }
			((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
			// try { observer.OnError(ex); } finally { Dispose(); }
			IL2CPP_END_FINALLY(56)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(56)
		{
			IL2CPP_JUMP_TBL(0x3F, IL_003f)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_003f:
		{
			// return;
			goto IL_0052;
		}
	} // end catch (depth: 1)

IL_0041:
	{
		// if (isPassed)
		bool L_9 = V_0;
		if (!L_9)
		{
			goto IL_0052;
		}
	}
	{
		// observer.OnNext(value);
		RuntimeObject* L_10 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_11 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_10, (RuntimeObject *)L_11);
	}

IL_0052:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereObservable`1_Where_<System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where__OnError_m07D352851B370EDEF50D0F8AD114D73F46DD7191_gshared (Where__tC4BFE268565371D15C47C91FF86D6E2F822710EB * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); } finally { Dispose(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); } finally { Dispose(); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// try { observer.OnError(error); } finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// try { observer.OnError(error); } finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.WhereObservable`1_Where_<System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Where__OnCompleted_m418D8C0ED6FAC8274FDF8E87CAD39525E3F73F40_gshared (Where__tC4BFE268565371D15C47C91FF86D6E2F822710EB * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); } finally { Dispose(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); } finally { Dispose(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// try { observer.OnCompleted(); } finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// try { observer.OnCompleted(); } finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.WithLatestFromObservable`3<TLeft,TRight,TResult>,System.IObserver`1<TResult>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WithLatestFrom__ctor_mA4980C7DD088FA5F41485AF5697C71D510054CA7_gshared (WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF * __this, WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WithLatestFrom__ctor_mA4980C7DD088FA5F41485AF5697C71D510054CA7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		// public WithLatestFrom(WithLatestFromObservable<TLeft, TRight, TResult> parent, IObserver<TResult> observer, IDisposable cancel) : base(observer, cancel)
		RuntimeObject* L_1 = ___observer1;
		RuntimeObject* L_2 = ___cancel2;
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, (RuntimeObject*)L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom<System.Object,System.Object,System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WithLatestFrom_Run_mF7288626461BBE1475E42B8C399F4A023D3D1789_gshared (WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WithLatestFrom_Run_mF7288626461BBE1475E42B8C399F4A023D3D1789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC * V_0 = NULL;
	{
		// var l = parent.left.Subscribe(new LeftObserver(this));
		WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 * L_0 = (WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 *)__this->get_parent_2();
		RuntimeObject* L_1 = (RuntimeObject*)L_0->get_left_1();
		LeftObserver_t5BAAD1BFF3A1BBEB6F0AE6F277E47434FF56E78C * L_2 = (LeftObserver_t5BAAD1BFF3A1BBEB6F0AE6F277E47434FF56E78C *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (LeftObserver_t5BAAD1BFF3A1BBEB6F0AE6F277E47434FF56E78C *, WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, (WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		RuntimeObject* L_3 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4), (RuntimeObject*)L_1, (RuntimeObject*)L_2);
		// var rSubscription = new SingleAssignmentDisposable();
		SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC * L_4 = (SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m7C40CDEFECC8BB2EAE4A236CFB987D2C942EEECF(L_4, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC *)L_4;
		// rSubscription.Disposable  = parent.right.Subscribe(new RightObserver(this, rSubscription));
		SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC * L_5 = V_0;
		WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 * L_6 = (WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 *)__this->get_parent_2();
		RuntimeObject* L_7 = (RuntimeObject*)L_6->get_right_2();
		SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC * L_8 = V_0;
		RightObserver_t6CDD16943B9A79661E77D64D290EA3BFAEBC572B * L_9 = (RightObserver_t6CDD16943B9A79661E77D64D290EA3BFAEBC572B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5));
		((  void (*) (RightObserver_t6CDD16943B9A79661E77D64D290EA3BFAEBC572B *, WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)(L_9, (WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF *)__this, (RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		RuntimeObject* L_10 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7), (RuntimeObject*)L_7, (RuntimeObject*)L_9);
		SingleAssignmentDisposable_set_Disposable_m792C4CF29925CE53CE13F5C9F0FA7B4CB9BE5743((SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC *)L_5, (RuntimeObject*)L_10, /*hidden argument*/NULL);
		// return StableCompositeDisposable.Create(l, rSubscription);
		SingleAssignmentDisposable_t7C740C406D0CF208B1E8C4DA1D530A50089F6DBC * L_11 = V_0;
		RuntimeObject* L_12 = StableCompositeDisposable_Create_mFBB35C8BE58E02B49CB9A9CAEC34B451A7DD0122((RuntimeObject*)L_3, (RuntimeObject*)L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom<System.Object,System.Object,System.Object>::OnNext(TResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WithLatestFrom_OnNext_m5B31A6AF6E6580749B51670C93F90C6D811C2BB0_gshared (WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom<System.Object,System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WithLatestFrom_OnError_m27B8EF3BB81C6FD584CD87C5B5C8E2A301F83B05_gshared (WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.WithLatestFromObservable`3_WithLatestFrom<System.Object,System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WithLatestFrom_OnCompleted_mC7BEFA83C9B15F70BE48A448BE2198E57FA0080E_gshared (WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.WithLatestFromObservable`3<System.Object,System.Object,System.Object>::.ctor(System.IObservable`1<TLeft>,System.IObservable`1<TRight>,System.Func`3<TLeft,TRight,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WithLatestFromObservable_3__ctor_m775AC64B0D080FA30B61463B293AEFFAA1965708_gshared (WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 * __this, RuntimeObject* ___left0, RuntimeObject* ___right1, Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * ___selector2, const RuntimeMethod* method)
{
	WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 * G_B2_0 = NULL;
	WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 * G_B3_1 = NULL;
	{
		// : base(left.IsRequiredSubscribeOnCurrentThread() || right.IsRequiredSubscribeOnCurrentThread())
		RuntimeObject* L_0 = ___left0;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		G_B1_0 = ((WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 *)(__this));
			goto IL_0011;
		}
	}
	{
		RuntimeObject* L_2 = ___right1;
		bool L_3 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 *)(G_B1_0));
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 1;
		G_B3_1 = ((WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 *)(G_B2_0));
	}

IL_0012:
	{
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		// this.left = left;
		RuntimeObject* L_4 = ___left0;
		__this->set_left_1(L_4);
		// this.right = right;
		RuntimeObject* L_5 = ___right1;
		__this->set_right_2(L_5);
		// this.selector = selector;
		Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * L_6 = ___selector2;
		__this->set_selector_3(L_6);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.WithLatestFromObservable`3<System.Object,System.Object,System.Object>::SubscribeCore(System.IObserver`1<TResult>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WithLatestFromObservable_3_SubscribeCore_mFA28FA03AF814EE497F61AD4405D873EDCA4D027_gshared (WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new WithLatestFrom(this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF * L_2 = (WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4));
		((  void (*) (WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF *, WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)(L_2, (WithLatestFromObservable_3_t6D7CB9C5E3FC6B6F16D7A9A4A324474E17389124 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((WithLatestFrom_t19566C946CB043DB582A1ACDBDDC2517DDD22CFF *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`1_Zip<System.Object>::.ctor(UniRx.Operators.ZipObservable`1<T>,System.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip__ctor_m4A60055901C0E8C8F796C30EBE4066E62C25617A_gshared (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * __this, ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip__ctor_m4A60055901C0E8C8F796C30EBE4066E62C25617A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		// public Zip(ZipObservable<T> parent, IObserver<IList<T>> observer, IDisposable cancel) : base(observer, cancel)
		RuntimeObject* L_1 = ___observer1;
		RuntimeObject* L_2 = ___cancel2;
		((  void (*) (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this, (RuntimeObject*)L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`1_Zip<System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Zip_Run_mC67BEF9BD738899596EBC70FEBCEBE307CEBB64D_gshared (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip_Run_mC67BEF9BD738899596EBC70FEBCEBE307CEBB64D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	RuntimeObject* V_3 = NULL;
	{
		// length = parent.sources.Length;
		ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B * L_0 = (ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B *)__this->get_parent_2();
		IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* L_1 = (IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B*)L_0->get_sources_1();
		__this->set_length_6((((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length)))));
		// queues = new Queue<T>[length];
		int32_t L_2 = (int32_t)__this->get_length_6();
		Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40* L_3 = (Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40*)(Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40*)SZArrayNew(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2), (uint32_t)L_2);
		__this->set_queues_4(L_3);
		// isDone = new bool[length];
		int32_t L_4 = (int32_t)__this->get_length_6();
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_5 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)SZArrayNew(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C_il2cpp_TypeInfo_var, (uint32_t)L_4);
		__this->set_isDone_5(L_5);
		// for (int i = 0; i < length; i++)
		V_1 = (int32_t)0;
		goto IL_004a;
	}

IL_0039:
	{
		// queues[i] = new Queue<T>();
		Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40* L_6 = (Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40*)__this->get_queues_4();
		int32_t L_7 = V_1;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_8 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7), (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_8);
		// for (int i = 0; i < length; i++)
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_004a:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_10 = V_1;
		int32_t L_11 = (int32_t)__this->get_length_6();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0039;
		}
	}
	{
		// var disposables = new IDisposable[length + 1];
		int32_t L_12 = (int32_t)__this->get_length_6();
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_13 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)SZArrayNew(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1)));
		V_0 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_13;
		// for (int i = 0; i < length; i++)
		V_2 = (int32_t)0;
		goto IL_0087;
	}

IL_0065:
	{
		// var source = parent.sources[i];
		ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B * L_14 = (ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B *)__this->get_parent_2();
		IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* L_15 = (IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B*)L_14->get_sources_1();
		int32_t L_16 = V_2;
		int32_t L_17 = L_16;
		RuntimeObject* L_18 = (RuntimeObject*)(L_15)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_17));
		V_3 = (RuntimeObject*)L_18;
		// disposables[i] = source.Subscribe(new ZipObserver(this, i));
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_19 = V_0;
		int32_t L_20 = V_2;
		RuntimeObject* L_21 = V_3;
		int32_t L_22 = V_2;
		ZipObserver_tAE56C446B45ECA54A1360388514A7D8851F5CC31 * L_23 = (ZipObserver_tAE56C446B45ECA54A1360388514A7D8851F5CC31 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5));
		((  void (*) (ZipObserver_tAE56C446B45ECA54A1360388514A7D8851F5CC31 *, Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)(L_23, (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)__this, (int32_t)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		RuntimeObject* L_24 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7), (RuntimeObject*)L_21, (RuntimeObject*)L_23);
		ArrayElementTypeCheck (L_19, L_24);
		(L_19)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_20), (RuntimeObject*)L_24);
		// for (int i = 0; i < length; i++)
		int32_t L_25 = V_2;
		V_2 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1));
	}

IL_0087:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_26 = V_2;
		int32_t L_27 = (int32_t)__this->get_length_6();
		if ((((int32_t)L_26) < ((int32_t)L_27)))
		{
			goto IL_0065;
		}
	}
	{
		// disposables[length] = Disposable.Create(() =>
		// {
		//     lock (gate)
		//     {
		//         for (int i = 0; i < length; i++)
		//         {
		//             var q = queues[i];
		//             q.Clear();
		//         }
		//     }
		// });
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_28 = V_0;
		int32_t L_29 = (int32_t)__this->get_length_6();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_30 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_30, (RuntimeObject *)__this, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_tFB6FF3E5C049CFE0B05B25A824CED890F91DC101_il2cpp_TypeInfo_var);
		RuntimeObject* L_31 = Disposable_Create_m3E97031C0F83935B39EA43B34B685D80B644DCE2((Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_30, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_28, L_31);
		(L_28)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_29), (RuntimeObject*)L_31);
		// return StableCompositeDisposable.CreateUnsafe(disposables);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_32 = V_0;
		RuntimeObject* L_33 = StableCompositeDisposable_CreateUnsafe_mBCB7541CE148A6314AC85043A4C1515A050A793A((IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_32, /*hidden argument*/NULL);
		return L_33;
	}
}
// System.Void UniRx.Operators.ZipObservable`1_Zip<System.Object>::Dequeue(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_Dequeue_m3D19E3196D60319F9D9D82AD589B7992687520C3_gshared (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	bool V_0 = false;
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* V_1 = NULL;
	int32_t V_2 = 0;
	bool V_3 = false;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// var allQueueHasValue = true;
		V_0 = (bool)1;
		// for (int i = 0; i < length; i++)
		V_2 = (int32_t)0;
		goto IL_001d;
	}

IL_0006:
	{
		// if (queues[i].Count == 0)
		Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40* L_0 = (Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40*)__this->get_queues_4();
		int32_t L_1 = V_2;
		int32_t L_2 = L_1;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_3 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)(L_0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_2));
		int32_t L_4 = ((  int32_t (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		if (L_4)
		{
			goto IL_0019;
		}
	}
	{
		// allQueueHasValue = false;
		V_0 = (bool)0;
		// break;
		goto IL_0026;
	}

IL_0019:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_5 = V_2;
		V_2 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_001d:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_6 = V_2;
		int32_t L_7 = (int32_t)__this->get_length_6();
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0006;
		}
	}

IL_0026:
	{
		// if (!allQueueHasValue)
		bool L_8 = V_0;
		if (L_8)
		{
			goto IL_006f;
		}
	}
	{
		// var allCompletedWithoutSelf = true;
		V_3 = (bool)1;
		// for (int i = 0; i < length; i++)
		V_4 = (int32_t)0;
		goto IL_004a;
	}

IL_0030:
	{
		// if (i == index) continue;
		int32_t L_9 = V_4;
		int32_t L_10 = ___index0;
		if ((((int32_t)L_9) == ((int32_t)L_10)))
		{
			goto IL_0044;
		}
	}
	{
		// if (!isDone[i])
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_11 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)__this->get_isDone_5();
		int32_t L_12 = V_4;
		int32_t L_13 = L_12;
		uint8_t L_14 = (uint8_t)(L_11)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_13));
		if (L_14)
		{
			goto IL_0044;
		}
	}
	{
		// allCompletedWithoutSelf = false;
		V_3 = (bool)0;
		// break;
		goto IL_0054;
	}

IL_0044:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_15 = V_4;
		V_4 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_004a:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_16 = V_4;
		int32_t L_17 = (int32_t)__this->get_length_6();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0030;
		}
	}

IL_0054:
	{
		// if (allCompletedWithoutSelf)
		bool L_18 = V_3;
		if (!L_18)
		{
			goto IL_006e;
		}
	}

IL_0057:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_19 = (RuntimeObject*)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10), (RuntimeObject*)L_19);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x6D, FINALLY_0066);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(102)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_006d:
	{
		// return;
		return;
	}

IL_006e:
	{
		// return;
		return;
	}

IL_006f:
	{
		// var array = new T[length];
		int32_t L_20 = (int32_t)__this->get_length_6();
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_21 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 12), (uint32_t)L_20);
		V_1 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_21;
		// for (int i = 0; i < length; i++)
		V_5 = (int32_t)0;
		goto IL_009c;
	}

IL_0080:
	{
		// array[i] = queues[i].Dequeue();
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_22 = V_1;
		int32_t L_23 = V_5;
		Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40* L_24 = (Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40*)__this->get_queues_4();
		int32_t L_25 = V_5;
		int32_t L_26 = L_25;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_27 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)(L_24)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_26));
		RuntimeObject * L_28 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_27, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13));
		(L_22)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_23), (RuntimeObject *)L_28);
		// for (int i = 0; i < length; i++)
		int32_t L_29 = V_5;
		V_5 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_009c:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_30 = V_5;
		int32_t L_31 = (int32_t)__this->get_length_6();
		if ((((int32_t)L_30) < ((int32_t)L_31)))
		{
			goto IL_0080;
		}
	}
	{
		// OnNext(array);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_32 = V_1;
		VirtActionInvoker1< RuntimeObject* >::Invoke(8 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Collections.Generic.IList`1<System.Object>,System.Collections.Generic.IList`1<System.Object>>::OnNext(TSource) */, (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this, (RuntimeObject*)(RuntimeObject*)L_32);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`1_Zip<System.Object>::OnNext(System.Collections.Generic.IList`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnNext_m2AAC060140BCECE60235C1EC92EE2D4E4A661641_gshared (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject* L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject* >::Invoke(0 /* System.Void System.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10), (RuntimeObject*)L_0, (RuntimeObject*)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`1_Zip<System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnError_m7491285D6C6BDE6D7555C58D7073EACE4BF5BA0A_gshared (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`1_Zip<System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnCompleted_m50852F772B6C4747E5B0F234FE3292B6BD85BCDC_gshared (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`1_Zip<System.Object>::<Run>b__6_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_U3CRunU3Eb__6_0_m7D4EEDC3CC61770BD3015F9EE0F4588DF69A0943_gshared (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (gate)
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_3();
		V_0 = (RuntimeObject *)L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_1 = V_0;
			Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_1, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
			// for (int i = 0; i < length; i++)
			V_2 = (int32_t)0;
			goto IL_0026;
		}

IL_0015:
		{
			// var q = queues[i];
			Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40* L_2 = (Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40*)__this->get_queues_4();
			int32_t L_3 = V_2;
			int32_t L_4 = L_3;
			Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_5 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)(L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
			// q.Clear();
			((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15));
			// for (int i = 0; i < length; i++)
			int32_t L_6 = V_2;
			V_2 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
		}

IL_0026:
		{
			// for (int i = 0; i < length; i++)
			int32_t L_7 = V_2;
			int32_t L_8 = (int32_t)__this->get_length_6();
			if ((((int32_t)L_7) < ((int32_t)L_8)))
			{
				goto IL_0015;
			}
		}

IL_002f:
		{
			// }
			IL2CPP_LEAVE(0x3B, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		{
			bool L_9 = V_1;
			if (!L_9)
			{
				goto IL_003a;
			}
		}

IL_0034:
		{
			RuntimeObject * L_10 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_10, /*hidden argument*/NULL);
		}

IL_003a:
		{
			IL2CPP_END_FINALLY(49)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_003b:
	{
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`3_Zip<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipObservable`3<TLeft,TRight,TResult>,System.IObserver`1<TResult>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip__ctor_mCB2E44FA936C74D8E5192B17163A4DB5DDFB178C_gshared (Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * __this, ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip__ctor_mCB2E44FA936C74D8E5192B17163A4DB5DDFB178C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		// readonly Queue<TLeft> leftQ = new Queue<TLeft>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_1 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		__this->set_leftQ_4(L_1);
		// readonly Queue<TRight> rightQ = new Queue<TRight>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		__this->set_rightQ_6(L_2);
		// : base(observer, cancel)
		RuntimeObject* L_3 = ___observer1;
		RuntimeObject* L_4 = ___cancel2;
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, (RuntimeObject*)L_3, (RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// this.parent = parent;
		ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 * L_5 = ___parent0;
		__this->set_parent_2(L_5);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`3_Zip<System.Object,System.Object,System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Zip_Run_m975CE4AFC3C1696C99E6D648A084FBB7769E0E5F_gshared (Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip_Run_m975CE4AFC3C1696C99E6D648A084FBB7769E0E5F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		// var l = parent.left.Subscribe(new LeftZipObserver(this));
		ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 * L_0 = (ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 *)__this->get_parent_2();
		RuntimeObject* L_1 = (RuntimeObject*)L_0->get_left_1();
		LeftZipObserver_t3BEB841B64A20E855B75C20E8901F72597D033BA * L_2 = (LeftZipObserver_t3BEB841B64A20E855B75C20E8901F72597D033BA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6));
		((  void (*) (LeftZipObserver_t3BEB841B64A20E855B75C20E8901F72597D033BA *, Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)(L_2, (Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		RuntimeObject* L_3 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8), (RuntimeObject*)L_1, (RuntimeObject*)L_2);
		// var r = parent.right.Subscribe(new RightZipObserver(this));
		ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 * L_4 = (ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 *)__this->get_parent_2();
		RuntimeObject* L_5 = (RuntimeObject*)L_4->get_right_2();
		RightZipObserver_t75FAA663E00278026507C33389FDDBA8FAFEFF80 * L_6 = (RightZipObserver_t75FAA663E00278026507C33389FDDBA8FAFEFF80 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (RightZipObserver_t75FAA663E00278026507C33389FDDBA8FAFEFF80 *, Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_6, (Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		RuntimeObject* L_7 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 11), (RuntimeObject*)L_5, (RuntimeObject*)L_6);
		V_0 = (RuntimeObject*)L_7;
		// return StableCompositeDisposable.Create(l, r, Disposable.Create(() =>
		// {
		//     lock (gate)
		//     {
		//         leftQ.Clear();
		//         rightQ.Clear();
		//     }
		// }));
		RuntimeObject* L_8 = V_0;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_9 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_9, (RuntimeObject *)__this, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_tFB6FF3E5C049CFE0B05B25A824CED890F91DC101_il2cpp_TypeInfo_var);
		RuntimeObject* L_10 = Disposable_Create_m3E97031C0F83935B39EA43B34B685D80B644DCE2((Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_9, /*hidden argument*/NULL);
		RuntimeObject* L_11 = StableCompositeDisposable_Create_m1E0EF5E19E06763B493586C330216A928AB5D7A5((RuntimeObject*)L_3, (RuntimeObject*)L_8, (RuntimeObject*)L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void UniRx.Operators.ZipObservable`3_Zip<System.Object,System.Object,System.Object>::Dequeue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_Dequeue_m9811AB9BAC2D627ABEC3EA82ADCAE3ADCCA3B71E_gshared (Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip_Dequeue_m9811AB9BAC2D627ABEC3EA82ADCAE3ADCCA3B71E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	Exception_t * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 4);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// if (leftQ.Count != 0 && rightQ.Count != 0)
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_0 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_leftQ_4();
		int32_t L_1 = ((  int32_t (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13));
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_rightQ_6();
		int32_t L_3 = ((  int32_t (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14));
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		// lv = leftQ.Dequeue();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_leftQ_4();
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15));
		V_0 = (RuntimeObject *)L_5;
		// rv = rightQ.Dequeue();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_rightQ_6();
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16));
		V_1 = (RuntimeObject *)L_7;
		// }
		goto IL_005d;
	}

IL_0034:
	{
		// else if (leftCompleted || rightCompleted)
		bool L_8 = (bool)__this->get_leftCompleted_5();
		if (L_8)
		{
			goto IL_0044;
		}
	}
	{
		bool L_9 = (bool)__this->get_rightCompleted_7();
		if (!L_9)
		{
			goto IL_005c;
		}
	}

IL_0044:
	{
	}

IL_0045:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_10 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 17), (RuntimeObject*)L_10);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x5B, FINALLY_0054);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0054;
	}

FINALLY_0054:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(84)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(84)
	{
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005b:
	{
		// return;
		return;
	}

IL_005c:
	{
		// return;
		return;
	}

IL_005d:
	{
	}

IL_005e:
	try
	{ // begin try (depth: 1)
		// v = parent.selector(lv, rv);
		ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 * L_11 = (ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 *)__this->get_parent_2();
		Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * L_12 = (Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 *)L_11->get_selector_3();
		RuntimeObject * L_13 = V_0;
		RuntimeObject * L_14 = V_1;
		RuntimeObject * L_15 = ((  RuntimeObject * (*) (Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19)->methodPointer)((Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 *)L_12, (RuntimeObject *)L_13, (RuntimeObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19));
		V_2 = (RuntimeObject *)L_15;
		// }
		goto IL_008d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0073;
		throw e;
	}

CATCH_0073:
	{ // begin catch(System.Exception)
		{
			// catch (Exception ex)
			V_3 = (Exception_t *)((Exception_t *)__exception_local);
		}

IL_0074:
		try
		{ // begin try (depth: 2)
			// try { observer.OnError(ex); }
			RuntimeObject* L_16 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t * L_17 = V_3;
			InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 17), (RuntimeObject*)L_16, (Exception_t *)L_17);
			// try { observer.OnError(ex); }
			IL2CPP_LEAVE(0x8B, FINALLY_0084);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0084;
		}

FINALLY_0084:
		{ // begin finally (depth: 2)
			// finally { Dispose(); }
			((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
			// finally { Dispose(); }
			IL2CPP_END_FINALLY(132)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(132)
		{
			IL2CPP_JUMP_TBL(0x8B, IL_008b)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_008b:
		{
			// return;
			goto IL_0094;
		}
	} // end catch (depth: 1)

IL_008d:
	{
		// OnNext(v);
		RuntimeObject * L_18 = V_2;
		VirtActionInvoker1< RuntimeObject * >::Invoke(8 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::OnNext(TSource) */, (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, (RuntimeObject *)L_18);
	}

IL_0094:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`3_Zip<System.Object,System.Object,System.Object>::OnNext(TResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnNext_m3A722C8E53096055A6C36321501667203227EAC8_gshared (Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 17), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`3_Zip<System.Object,System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnError_mBB2681A243F6B004B26BC31D3425953508A00AC5_gshared (Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 17), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`3_Zip<System.Object,System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnCompleted_m8410D84BC2EAB5D6EDC4A97E77EAEB1A9B2EB779_gshared (Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 17), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`3_Zip<System.Object,System.Object,System.Object>::<Run>b__7_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_U3CRunU3Eb__7_0_m217F4ED46D220111E32C0949A8F2449343BEAB54_gshared (Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (gate)
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_3();
		V_0 = (RuntimeObject *)L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_1, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// leftQ.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_leftQ_4();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21));
		// rightQ.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_3 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_rightQ_6();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22));
		// }
		IL2CPP_LEAVE(0x33, FINALLY_0029);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0029;
	}

FINALLY_0029:
	{ // begin finally (depth: 1)
		{
			bool L_4 = V_1;
			if (!L_4)
			{
				goto IL_0032;
			}
		}

IL_002c:
		{
			RuntimeObject * L_5 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_5, /*hidden argument*/NULL);
		}

IL_0032:
		{
			IL2CPP_END_FINALLY(41)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(41)
	{
		IL2CPP_JUMP_TBL(0x33, IL_0033)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0033:
	{
		// }));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`4_Zip<System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipObservable`4<T1,T2,T3,TR>,System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip__ctor_m3EA93898D117E6409930CDBC327E300793D59461_gshared (Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1 * __this, ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip__ctor_m3EA93898D117E6409930CDBC327E300793D59461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_6(L_0);
		// readonly Queue<T1> q1 = new Queue<T1>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_1 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		__this->set_q1_7(L_1);
		// readonly Queue<T2> q2 = new Queue<T2>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		__this->set_q2_8(L_2);
		// readonly Queue<T3> q3 = new Queue<T3>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_3 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		__this->set_q3_9(L_3);
		// : base(observer, cancel)
		RuntimeObject* L_4 = ___observer1;
		RuntimeObject* L_5 = ___cancel2;
		((  void (*) (NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *)__this, (RuntimeObject*)L_4, (RuntimeObject*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		// this.parent = parent;
		ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * L_6 = ___parent0;
		__this->set_parent_5(L_6);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`4_Zip<System.Object,System.Object,System.Object,System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Zip_Run_mCCA3BEB63BD107249ABDB30A103E4FD27E2AD46F_gshared (Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip_Run_mCCA3BEB63BD107249ABDB30A103E4FD27E2AD46F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		// base.SetQueue(new System.Collections.ICollection[] { q1, q2, q3 });
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_0 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)(ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)SZArrayNew(ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8_il2cpp_TypeInfo_var, (uint32_t)3);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_1 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_0;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_2);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_3 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_1;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_4);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_5 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_3;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_6);
		((  void (*) (NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *, ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *)__this, (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		// var s1 = parent.source1.Subscribe(new ZipObserver<T1>(gate, this, 0, q1));
		ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * L_7 = (ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 *)__this->get_parent_5();
		RuntimeObject* L_8 = (RuntimeObject*)L_7->get_source1_1();
		RuntimeObject * L_9 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_10 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_11 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_11, (RuntimeObject *)L_9, (RuntimeObject*)__this, (int32_t)0, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		RuntimeObject* L_12 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 12), (RuntimeObject*)L_8, (RuntimeObject*)L_11);
		// var s2 = parent.source2.Subscribe(new ZipObserver<T2>(gate, this, 1, q2));
		ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * L_13 = (ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 *)__this->get_parent_5();
		RuntimeObject* L_14 = (RuntimeObject*)L_13->get_source2_2();
		RuntimeObject * L_15 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_16 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_17 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 13));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14)->methodPointer)(L_17, (RuntimeObject *)L_15, (RuntimeObject*)__this, (int32_t)1, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14));
		RuntimeObject* L_18 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 15), (RuntimeObject*)L_14, (RuntimeObject*)L_17);
		V_0 = (RuntimeObject*)L_18;
		// var s3 = parent.source3.Subscribe(new ZipObserver<T3>(gate, this, 2, q3));
		ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * L_19 = (ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 *)__this->get_parent_5();
		RuntimeObject* L_20 = (RuntimeObject*)L_19->get_source3_3();
		RuntimeObject * L_21 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_22 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_23 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 16));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17)->methodPointer)(L_23, (RuntimeObject *)L_21, (RuntimeObject*)__this, (int32_t)2, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17));
		RuntimeObject* L_24 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 18), (RuntimeObject*)L_20, (RuntimeObject*)L_23);
		V_1 = (RuntimeObject*)L_24;
		// return StableCompositeDisposable.Create(s1, s2, s3, Disposable.Create(() =>
		// {
		//     lock (gate)
		//     {
		//         q1.Clear(); q2.Clear(); q3.Clear();
		//     }
		// }));
		RuntimeObject* L_25 = V_0;
		RuntimeObject* L_26 = V_1;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_27 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_27, (RuntimeObject *)__this, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_tFB6FF3E5C049CFE0B05B25A824CED890F91DC101_il2cpp_TypeInfo_var);
		RuntimeObject* L_28 = Disposable_Create_m3E97031C0F83935B39EA43B34B685D80B644DCE2((Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_27, /*hidden argument*/NULL);
		RuntimeObject* L_29 = StableCompositeDisposable_Create_mCA54324578190C14AC89AF3A5D1A8401AB4546D0((RuntimeObject*)L_12, (RuntimeObject*)L_25, (RuntimeObject*)L_26, (RuntimeObject*)L_28, /*hidden argument*/NULL);
		return L_29;
	}
}
// TR UniRx.Operators.ZipObservable`4_Zip<System.Object,System.Object,System.Object,System.Object>::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Zip_GetResult_m3100F68ABB4714324D08A25A80B698FD5285D8EA_gshared (Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1 * __this, const RuntimeMethod* method)
{
	{
		// return parent.resultSelector(q1.Dequeue(), q2.Dequeue(), q3.Dequeue());
		ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * L_0 = (ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 *)__this->get_parent_5();
		ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 * L_1 = (ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 *)L_0->get_resultSelector_4();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22));
		RuntimeObject * L_8 = ((  RuntimeObject * (*) (ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23)->methodPointer)((ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 *)L_1, (RuntimeObject *)L_3, (RuntimeObject *)L_5, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23));
		return L_8;
	}
}
// System.Void UniRx.Operators.ZipObservable`4_Zip<System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnNext_m288D838612CBC40E738B4177C00F3AD7EF9ED427_gshared (Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 24), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`4_Zip<System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnError_m9F37892B12892231F19C60BC395C05E4E1160D84_gshared (Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 24), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`4_Zip<System.Object,System.Object,System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnCompleted_mD7B0D8FD11284A6CB1ADCD45D2EF1EC93AC2E619_gshared (Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 24), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`4_Zip<System.Object,System.Object,System.Object,System.Object>::<Run>b__6_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_U3CRunU3Eb__6_0_mB54B97CDCDAA8D87CF7BD741F12E11872EF535D3_gshared (Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (gate)
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_6();
		V_0 = (RuntimeObject *)L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_1, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// q1.Clear(); q2.Clear(); q3.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 26)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 26));
		// q1.Clear(); q2.Clear(); q3.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_3 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27));
		// q1.Clear(); q2.Clear(); q3.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 28)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 28));
		// }
		IL2CPP_LEAVE(0x3E, FINALLY_0034);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		{
			bool L_5 = V_1;
			if (!L_5)
			{
				goto IL_003d;
			}
		}

IL_0037:
		{
			RuntimeObject * L_6 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_6, /*hidden argument*/NULL);
		}

IL_003d:
		{
			IL2CPP_END_FINALLY(52)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_003e:
	{
		// }));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`5_Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipObservable`5<T1,T2,T3,T4,TR>,System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip__ctor_mA976EE453808F313AD9FBFFE54F95F200AA3C34F_gshared (Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808 * __this, ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip__ctor_mA976EE453808F313AD9FBFFE54F95F200AA3C34F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_6(L_0);
		// readonly Queue<T1> q1 = new Queue<T1>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_1 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		__this->set_q1_7(L_1);
		// readonly Queue<T2> q2 = new Queue<T2>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		__this->set_q2_8(L_2);
		// readonly Queue<T3> q3 = new Queue<T3>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_3 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		__this->set_q3_9(L_3);
		// readonly Queue<T4> q4 = new Queue<T4>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		__this->set_q4_10(L_4);
		// : base(observer, cancel)
		RuntimeObject* L_5 = ___observer1;
		RuntimeObject* L_6 = ___cancel2;
		((  void (*) (NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *)__this, (RuntimeObject*)L_5, (RuntimeObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		// this.parent = parent;
		ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * L_7 = ___parent0;
		__this->set_parent_5(L_7);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`5_Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Zip_Run_m35828CC23733F8BA2375FB3AA3E843EC06D68DB9_gshared (Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip_Run_m35828CC23733F8BA2375FB3AA3E843EC06D68DB9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	{
		// base.SetQueue(new System.Collections.ICollection[] { q1, q2, q3, q4 });
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_0 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)(ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)SZArrayNew(ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8_il2cpp_TypeInfo_var, (uint32_t)4);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_1 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_0;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_2);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_3 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_1;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_4);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_5 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_3;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_6);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_7 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_5;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_8 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_8);
		((  void (*) (NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *, ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *)__this, (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		// var s1 = parent.source1.Subscribe(new ZipObserver<T1>(gate, this, 0, q1));
		ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * L_9 = (ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)__this->get_parent_5();
		RuntimeObject* L_10 = (RuntimeObject*)L_9->get_source1_1();
		RuntimeObject * L_11 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_12 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_13 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 12));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13)->methodPointer)(L_13, (RuntimeObject *)L_11, (RuntimeObject*)__this, (int32_t)0, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13));
		RuntimeObject* L_14 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 14), (RuntimeObject*)L_10, (RuntimeObject*)L_13);
		V_0 = (RuntimeObject*)L_14;
		// var s2 = parent.source2.Subscribe(new ZipObserver<T2>(gate, this, 1, q2));
		ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * L_15 = (ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)__this->get_parent_5();
		RuntimeObject* L_16 = (RuntimeObject*)L_15->get_source2_2();
		RuntimeObject * L_17 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_18 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_19 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 15));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16)->methodPointer)(L_19, (RuntimeObject *)L_17, (RuntimeObject*)__this, (int32_t)1, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16));
		RuntimeObject* L_20 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 17), (RuntimeObject*)L_16, (RuntimeObject*)L_19);
		V_1 = (RuntimeObject*)L_20;
		// var s3 = parent.source3.Subscribe(new ZipObserver<T3>(gate, this, 2, q3));
		ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * L_21 = (ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)__this->get_parent_5();
		RuntimeObject* L_22 = (RuntimeObject*)L_21->get_source3_3();
		RuntimeObject * L_23 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_24 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_25 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 18));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19)->methodPointer)(L_25, (RuntimeObject *)L_23, (RuntimeObject*)__this, (int32_t)2, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19));
		RuntimeObject* L_26 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 20), (RuntimeObject*)L_22, (RuntimeObject*)L_25);
		V_2 = (RuntimeObject*)L_26;
		// var s4 = parent.source4.Subscribe(new ZipObserver<T4>(gate, this, 3, q4));
		ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * L_27 = (ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)__this->get_parent_5();
		RuntimeObject* L_28 = (RuntimeObject*)L_27->get_source4_4();
		RuntimeObject * L_29 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_30 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_31 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 21));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22)->methodPointer)(L_31, (RuntimeObject *)L_29, (RuntimeObject*)__this, (int32_t)3, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22));
		RuntimeObject* L_32 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 23), (RuntimeObject*)L_28, (RuntimeObject*)L_31);
		V_3 = (RuntimeObject*)L_32;
		// return StableCompositeDisposable.Create(s1, s2, s3, s4, Disposable.Create(() =>
		// {
		//     lock (gate)
		//     {
		//         q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear();
		//     }
		// }));
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_33 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)SZArrayNew(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65_il2cpp_TypeInfo_var, (uint32_t)5);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_34 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_33;
		RuntimeObject* L_35 = V_0;
		ArrayElementTypeCheck (L_34, L_35);
		(L_34)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_35);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_36 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_34;
		RuntimeObject* L_37 = V_1;
		ArrayElementTypeCheck (L_36, L_37);
		(L_36)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_37);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_38 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_36;
		RuntimeObject* L_39 = V_2;
		ArrayElementTypeCheck (L_38, L_39);
		(L_38)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_39);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_40 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_38;
		RuntimeObject* L_41 = V_3;
		ArrayElementTypeCheck (L_40, L_41);
		(L_40)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_41);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_42 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_40;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_43 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_43, (RuntimeObject *)__this, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_tFB6FF3E5C049CFE0B05B25A824CED890F91DC101_il2cpp_TypeInfo_var);
		RuntimeObject* L_44 = Disposable_Create_m3E97031C0F83935B39EA43B34B685D80B644DCE2((Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_43, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_42, L_44);
		(L_42)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (RuntimeObject*)L_44);
		RuntimeObject* L_45 = StableCompositeDisposable_Create_mA26CD55619BE45D7F630D010C9BB47D84E83BCAA((IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_42, /*hidden argument*/NULL);
		return L_45;
	}
}
// TR UniRx.Operators.ZipObservable`5_Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Zip_GetResult_m51E15F624C0D2774F99CA7C60097FE54BD60E130_gshared (Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808 * __this, const RuntimeMethod* method)
{
	{
		// return parent.resultSelector(q1.Dequeue(), q2.Dequeue(), q3.Dequeue(), q4.Dequeue());
		ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * L_0 = (ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)__this->get_parent_5();
		ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 * L_1 = (ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 *)L_0->get_resultSelector_5();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 26)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 26));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_8 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 28)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 28));
		RuntimeObject * L_10 = ((  RuntimeObject * (*) (ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29)->methodPointer)((ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 *)L_1, (RuntimeObject *)L_3, (RuntimeObject *)L_5, (RuntimeObject *)L_7, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29));
		return L_10;
	}
}
// System.Void UniRx.Operators.ZipObservable`5_Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnNext_m7E041170EDBFC07BA7E83C216423A576B34B3DB0_gshared (Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 30), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`5_Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnError_m1EE6540C6E5F087D4B8F1B5880CE4E2050F006C2_gshared (Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 30), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`5_Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnCompleted_m5FECA3E30B5A31CA041195D5BD3DD4AD62F86518_gshared (Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 30), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`5_Zip<System.Object,System.Object,System.Object,System.Object,System.Object>::<Run>b__7_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_U3CRunU3Eb__7_0_mB410764DB8F5ED2BD0A67E67B79C8479C9DA0187_gshared (Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (gate)
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_6();
		V_0 = (RuntimeObject *)L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_1, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 32)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 32));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_3 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 33)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 33));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 34)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 34));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_5 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 35)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 35));
		// }
		IL2CPP_LEAVE(0x49, FINALLY_003f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		{
			bool L_6 = V_1;
			if (!L_6)
			{
				goto IL_0048;
			}
		}

IL_0042:
		{
			RuntimeObject * L_7 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_7, /*hidden argument*/NULL);
		}

IL_0048:
		{
			IL2CPP_END_FINALLY(63)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0049:
	{
		// }));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`6_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipObservable`6<T1,T2,T3,T4,T5,TR>,System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip__ctor_m10F368F9A43890A63AC953BC023C90262A809EC0_gshared (Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09 * __this, ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip__ctor_m10F368F9A43890A63AC953BC023C90262A809EC0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_6(L_0);
		// readonly Queue<T1> q1 = new Queue<T1>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_1 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		__this->set_q1_7(L_1);
		// readonly Queue<T2> q2 = new Queue<T2>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		__this->set_q2_8(L_2);
		// readonly Queue<T3> q3 = new Queue<T3>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_3 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		__this->set_q3_9(L_3);
		// readonly Queue<T4> q4 = new Queue<T4>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		__this->set_q4_10(L_4);
		// readonly Queue<T5> q5 = new Queue<T5>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_5 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)(L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		__this->set_q5_11(L_5);
		// : base(observer, cancel)
		RuntimeObject* L_6 = ___observer1;
		RuntimeObject* L_7 = ___cancel2;
		((  void (*) (NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *)__this, (RuntimeObject*)L_6, (RuntimeObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		// this.parent = parent;
		ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * L_8 = ___parent0;
		__this->set_parent_5(L_8);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`6_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Zip_Run_m00C2066ED34C8005D94090B88213979A8ABB81B5_gshared (Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip_Run_m00C2066ED34C8005D94090B88213979A8ABB81B5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	{
		// base.SetQueue(new System.Collections.ICollection[] { q1, q2, q3, q4, q5 });
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_0 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)(ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)SZArrayNew(ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8_il2cpp_TypeInfo_var, (uint32_t)5);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_1 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_0;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_2);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_3 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_1;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_4);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_5 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_3;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_6);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_7 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_5;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_8 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_8);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_9 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_7;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_10 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q5_11();
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (RuntimeObject*)L_10);
		((  void (*) (NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *, ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13)->methodPointer)((NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *)__this, (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13));
		// var s1 = parent.source1.Subscribe(new ZipObserver<T1>(gate, this, 0, q1));
		ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * L_11 = (ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)__this->get_parent_5();
		RuntimeObject* L_12 = (RuntimeObject*)L_11->get_source1_1();
		RuntimeObject * L_13 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_14 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_15 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 14));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15)->methodPointer)(L_15, (RuntimeObject *)L_13, (RuntimeObject*)__this, (int32_t)0, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15));
		RuntimeObject* L_16 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 16), (RuntimeObject*)L_12, (RuntimeObject*)L_15);
		V_0 = (RuntimeObject*)L_16;
		// var s2 = parent.source2.Subscribe(new ZipObserver<T2>(gate, this, 1, q2));
		ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * L_17 = (ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)__this->get_parent_5();
		RuntimeObject* L_18 = (RuntimeObject*)L_17->get_source2_2();
		RuntimeObject * L_19 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_20 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_21 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 17));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)(L_21, (RuntimeObject *)L_19, (RuntimeObject*)__this, (int32_t)1, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		RuntimeObject* L_22 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 19), (RuntimeObject*)L_18, (RuntimeObject*)L_21);
		V_1 = (RuntimeObject*)L_22;
		// var s3 = parent.source3.Subscribe(new ZipObserver<T3>(gate, this, 2, q3));
		ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * L_23 = (ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)__this->get_parent_5();
		RuntimeObject* L_24 = (RuntimeObject*)L_23->get_source3_3();
		RuntimeObject * L_25 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_26 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_27 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 20));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21)->methodPointer)(L_27, (RuntimeObject *)L_25, (RuntimeObject*)__this, (int32_t)2, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21));
		RuntimeObject* L_28 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 22), (RuntimeObject*)L_24, (RuntimeObject*)L_27);
		V_2 = (RuntimeObject*)L_28;
		// var s4 = parent.source4.Subscribe(new ZipObserver<T4>(gate, this, 3, q4));
		ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * L_29 = (ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)__this->get_parent_5();
		RuntimeObject* L_30 = (RuntimeObject*)L_29->get_source4_4();
		RuntimeObject * L_31 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_32 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_33 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 23));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24)->methodPointer)(L_33, (RuntimeObject *)L_31, (RuntimeObject*)__this, (int32_t)3, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24));
		RuntimeObject* L_34 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 25), (RuntimeObject*)L_30, (RuntimeObject*)L_33);
		V_3 = (RuntimeObject*)L_34;
		// var s5 = parent.source5.Subscribe(new ZipObserver<T5>(gate, this, 4, q5));
		ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * L_35 = (ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)__this->get_parent_5();
		RuntimeObject* L_36 = (RuntimeObject*)L_35->get_source5_5();
		RuntimeObject * L_37 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_38 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q5_11();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_39 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 26));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27)->methodPointer)(L_39, (RuntimeObject *)L_37, (RuntimeObject*)__this, (int32_t)4, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_38, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27));
		RuntimeObject* L_40 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 28), (RuntimeObject*)L_36, (RuntimeObject*)L_39);
		V_4 = (RuntimeObject*)L_40;
		// return StableCompositeDisposable.Create(s1, s2, s3, s4, s5, Disposable.Create(() =>
		// {
		//     lock (gate)
		//     {
		//         q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear();
		//     }
		// }));
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_41 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)SZArrayNew(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65_il2cpp_TypeInfo_var, (uint32_t)6);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_42 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_41;
		RuntimeObject* L_43 = V_0;
		ArrayElementTypeCheck (L_42, L_43);
		(L_42)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_43);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_44 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_42;
		RuntimeObject* L_45 = V_1;
		ArrayElementTypeCheck (L_44, L_45);
		(L_44)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_45);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_46 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_44;
		RuntimeObject* L_47 = V_2;
		ArrayElementTypeCheck (L_46, L_47);
		(L_46)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_47);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_48 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_46;
		RuntimeObject* L_49 = V_3;
		ArrayElementTypeCheck (L_48, L_49);
		(L_48)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_49);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_50 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_48;
		RuntimeObject* L_51 = V_4;
		ArrayElementTypeCheck (L_50, L_51);
		(L_50)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (RuntimeObject*)L_51);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_52 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_50;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_53 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_53, (RuntimeObject *)__this, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_tFB6FF3E5C049CFE0B05B25A824CED890F91DC101_il2cpp_TypeInfo_var);
		RuntimeObject* L_54 = Disposable_Create_m3E97031C0F83935B39EA43B34B685D80B644DCE2((Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_53, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_52, L_54);
		(L_52)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (RuntimeObject*)L_54);
		RuntimeObject* L_55 = StableCompositeDisposable_Create_mA26CD55619BE45D7F630D010C9BB47D84E83BCAA((IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_52, /*hidden argument*/NULL);
		return L_55;
	}
}
// TR UniRx.Operators.ZipObservable`6_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Zip_GetResult_m183B16BABEB13C6CC1FC4FCF8963E621DAEB3D14_gshared (Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09 * __this, const RuntimeMethod* method)
{
	{
		// return parent.resultSelector(q1.Dequeue(), q2.Dequeue(), q3.Dequeue(), q4.Dequeue(), q5.Dequeue());
		ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * L_0 = (ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)__this->get_parent_5();
		ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D * L_1 = (ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D *)L_0->get_resultSelector_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 30)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 30));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 32)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 32));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_8 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 33)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 33));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_10 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q5_11();
		RuntimeObject * L_11 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 34)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 34));
		RuntimeObject * L_12 = ((  RuntimeObject * (*) (ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 35)->methodPointer)((ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D *)L_1, (RuntimeObject *)L_3, (RuntimeObject *)L_5, (RuntimeObject *)L_7, (RuntimeObject *)L_9, (RuntimeObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 35));
		return L_12;
	}
}
// System.Void UniRx.Operators.ZipObservable`6_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnNext_m2E52C4176EBA1729B176FF99908212D9219DB813_gshared (Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 36), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`6_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnError_m17EB82ECC07DD0915A35172C84F8D5441D7B203D_gshared (Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 36), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 37)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 37));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`6_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnCompleted_mD588AD2F4D4E6D39FD9EE8EFDF0662B084E962D5_gshared (Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 36), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 37)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 37));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`6_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::<Run>b__8_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_U3CRunU3Eb__8_0_mDA1A0BFC7F7259F6D51311693C502B0619C4C52A_gshared (Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (gate)
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_6();
		V_0 = (RuntimeObject *)L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_1, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 38)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 38));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_3 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 39)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 39));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 40)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 40));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_5 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 41)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 41));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q5_11();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 42)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 42));
		// }
		IL2CPP_LEAVE(0x54, FINALLY_004a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004a;
	}

FINALLY_004a:
	{ // begin finally (depth: 1)
		{
			bool L_7 = V_1;
			if (!L_7)
			{
				goto IL_0053;
			}
		}

IL_004d:
		{
			RuntimeObject * L_8 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_8, /*hidden argument*/NULL);
		}

IL_0053:
		{
			IL2CPP_END_FINALLY(74)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(74)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0054:
	{
		// }));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`7_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipObservable`7<T1,T2,T3,T4,T5,T6,TR>,System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip__ctor_m2219C780CE544D6FD405B8C996028EB8E8A5DA08_gshared (Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75 * __this, ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip__ctor_m2219C780CE544D6FD405B8C996028EB8E8A5DA08_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_6(L_0);
		// readonly Queue<T1> q1 = new Queue<T1>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_1 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		__this->set_q1_7(L_1);
		// readonly Queue<T2> q2 = new Queue<T2>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		__this->set_q2_8(L_2);
		// readonly Queue<T3> q3 = new Queue<T3>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_3 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		__this->set_q3_9(L_3);
		// readonly Queue<T4> q4 = new Queue<T4>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		__this->set_q4_10(L_4);
		// readonly Queue<T5> q5 = new Queue<T5>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_5 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)(L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		__this->set_q5_11(L_5);
		// readonly Queue<T6> q6 = new Queue<T6>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		__this->set_q6_12(L_6);
		// : base(observer, cancel)
		RuntimeObject* L_7 = ___observer1;
		RuntimeObject* L_8 = ___cancel2;
		((  void (*) (NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)->methodPointer)((NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *)__this, (RuntimeObject*)L_7, (RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12));
		// this.parent = parent;
		ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * L_9 = ___parent0;
		__this->set_parent_5(L_9);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`7_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Zip_Run_m9F70121C3CA8F4F30D0EFAF999FB34EB12E7C09B_gshared (Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip_Run_m9F70121C3CA8F4F30D0EFAF999FB34EB12E7C09B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	RuntimeObject* V_5 = NULL;
	{
		// base.SetQueue(new System.Collections.ICollection[] { q1, q2, q3, q4, q5, q6 });
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_0 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)(ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)SZArrayNew(ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8_il2cpp_TypeInfo_var, (uint32_t)6);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_1 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_0;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_2);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_3 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_1;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_4);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_5 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_3;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_6);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_7 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_5;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_8 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_8);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_9 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_7;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_10 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q5_11();
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (RuntimeObject*)L_10);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_11 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_9;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_12 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q6_12();
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (RuntimeObject*)L_12);
		((  void (*) (NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *, ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15)->methodPointer)((NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *)__this, (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15));
		// var s1 = parent.source1.Subscribe(new ZipObserver<T1>(gate, this, 0, q1));
		ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * L_13 = (ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)__this->get_parent_5();
		RuntimeObject* L_14 = (RuntimeObject*)L_13->get_source1_1();
		RuntimeObject * L_15 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_16 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_17 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 16));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17)->methodPointer)(L_17, (RuntimeObject *)L_15, (RuntimeObject*)__this, (int32_t)0, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17));
		RuntimeObject* L_18 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 18), (RuntimeObject*)L_14, (RuntimeObject*)L_17);
		V_0 = (RuntimeObject*)L_18;
		// var s2 = parent.source2.Subscribe(new ZipObserver<T2>(gate, this, 1, q2));
		ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * L_19 = (ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)__this->get_parent_5();
		RuntimeObject* L_20 = (RuntimeObject*)L_19->get_source2_2();
		RuntimeObject * L_21 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_22 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_23 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 19));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20)->methodPointer)(L_23, (RuntimeObject *)L_21, (RuntimeObject*)__this, (int32_t)1, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20));
		RuntimeObject* L_24 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 21), (RuntimeObject*)L_20, (RuntimeObject*)L_23);
		V_1 = (RuntimeObject*)L_24;
		// var s3 = parent.source3.Subscribe(new ZipObserver<T3>(gate, this, 2, q3));
		ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * L_25 = (ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)__this->get_parent_5();
		RuntimeObject* L_26 = (RuntimeObject*)L_25->get_source3_3();
		RuntimeObject * L_27 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_28 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_29 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 22));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23)->methodPointer)(L_29, (RuntimeObject *)L_27, (RuntimeObject*)__this, (int32_t)2, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23));
		RuntimeObject* L_30 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 24), (RuntimeObject*)L_26, (RuntimeObject*)L_29);
		V_2 = (RuntimeObject*)L_30;
		// var s4 = parent.source4.Subscribe(new ZipObserver<T4>(gate, this, 3, q4));
		ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * L_31 = (ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)__this->get_parent_5();
		RuntimeObject* L_32 = (RuntimeObject*)L_31->get_source4_4();
		RuntimeObject * L_33 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_34 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_35 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 25));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 26)->methodPointer)(L_35, (RuntimeObject *)L_33, (RuntimeObject*)__this, (int32_t)3, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 26));
		RuntimeObject* L_36 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 27), (RuntimeObject*)L_32, (RuntimeObject*)L_35);
		V_3 = (RuntimeObject*)L_36;
		// var s5 = parent.source5.Subscribe(new ZipObserver<T5>(gate, this, 4, q5));
		ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * L_37 = (ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)__this->get_parent_5();
		RuntimeObject* L_38 = (RuntimeObject*)L_37->get_source5_5();
		RuntimeObject * L_39 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_40 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q5_11();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_41 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 28));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29)->methodPointer)(L_41, (RuntimeObject *)L_39, (RuntimeObject*)__this, (int32_t)4, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_40, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29));
		RuntimeObject* L_42 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 30), (RuntimeObject*)L_38, (RuntimeObject*)L_41);
		V_4 = (RuntimeObject*)L_42;
		// var s6 = parent.source6.Subscribe(new ZipObserver<T6>(gate, this, 5, q6));
		ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * L_43 = (ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)__this->get_parent_5();
		RuntimeObject* L_44 = (RuntimeObject*)L_43->get_source6_6();
		RuntimeObject * L_45 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_46 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q6_12();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_47 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 31));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 32)->methodPointer)(L_47, (RuntimeObject *)L_45, (RuntimeObject*)__this, (int32_t)5, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_46, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 32));
		RuntimeObject* L_48 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 33), (RuntimeObject*)L_44, (RuntimeObject*)L_47);
		V_5 = (RuntimeObject*)L_48;
		// return StableCompositeDisposable.Create(s1, s2, s3, s4, s5, s6, Disposable.Create(() =>
		// {
		//     lock (gate)
		//     {
		//         q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear();
		//     }
		// }));
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_49 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)SZArrayNew(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65_il2cpp_TypeInfo_var, (uint32_t)7);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_50 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_49;
		RuntimeObject* L_51 = V_0;
		ArrayElementTypeCheck (L_50, L_51);
		(L_50)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_51);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_52 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_50;
		RuntimeObject* L_53 = V_1;
		ArrayElementTypeCheck (L_52, L_53);
		(L_52)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_53);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_54 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_52;
		RuntimeObject* L_55 = V_2;
		ArrayElementTypeCheck (L_54, L_55);
		(L_54)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_55);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_56 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_54;
		RuntimeObject* L_57 = V_3;
		ArrayElementTypeCheck (L_56, L_57);
		(L_56)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_57);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_58 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_56;
		RuntimeObject* L_59 = V_4;
		ArrayElementTypeCheck (L_58, L_59);
		(L_58)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (RuntimeObject*)L_59);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_60 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_58;
		RuntimeObject* L_61 = V_5;
		ArrayElementTypeCheck (L_60, L_61);
		(L_60)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (RuntimeObject*)L_61);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_62 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_60;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_63 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_63, (RuntimeObject *)__this, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 34)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_tFB6FF3E5C049CFE0B05B25A824CED890F91DC101_il2cpp_TypeInfo_var);
		RuntimeObject* L_64 = Disposable_Create_m3E97031C0F83935B39EA43B34B685D80B644DCE2((Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_63, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_62, L_64);
		(L_62)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (RuntimeObject*)L_64);
		RuntimeObject* L_65 = StableCompositeDisposable_Create_mA26CD55619BE45D7F630D010C9BB47D84E83BCAA((IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_62, /*hidden argument*/NULL);
		return L_65;
	}
}
// TR UniRx.Operators.ZipObservable`7_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Zip_GetResult_m90131109D8B02FD31BD2FB0931113D1FD0E8C6CE_gshared (Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75 * __this, const RuntimeMethod* method)
{
	{
		// return parent.resultSelector(q1.Dequeue(), q2.Dequeue(), q3.Dequeue(), q4.Dequeue(), q5.Dequeue(), q6.Dequeue());
		ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * L_0 = (ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)__this->get_parent_5();
		ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 * L_1 = (ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 *)L_0->get_resultSelector_7();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 35)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 35));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 36)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 36));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 37)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 37));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_8 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 38)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 38));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_10 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q5_11();
		RuntimeObject * L_11 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 39)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 39));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_12 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q6_12();
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 40)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 40));
		RuntimeObject * L_14 = ((  RuntimeObject * (*) (ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 41)->methodPointer)((ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 *)L_1, (RuntimeObject *)L_3, (RuntimeObject *)L_5, (RuntimeObject *)L_7, (RuntimeObject *)L_9, (RuntimeObject *)L_11, (RuntimeObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 41));
		return L_14;
	}
}
// System.Void UniRx.Operators.ZipObservable`7_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnNext_m86389159634E5559F25D41B0157C197455C3971C_gshared (Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 42), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`7_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnError_mA2A0AB8DDBB0C7B4728E68100D57A3B30F0D436B_gshared (Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 42), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 43)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 43));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`7_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnCompleted_m732311BAC853BC7091A208C9040B297B31AA3D55_gshared (Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 42), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 43)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 43));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`7_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::<Run>b__9_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_U3CRunU3Eb__9_0_mA2B62565B8F0D7F047FBB42A50A48466A787C241_gshared (Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (gate)
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_6();
		V_0 = (RuntimeObject *)L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_1, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 44)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 44));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_3 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 45)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 45));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 46)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 46));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_5 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 47)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 47));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q5_11();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 48)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 48));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_7 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q6_12();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 49)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 49));
		// }
		IL2CPP_LEAVE(0x5F, FINALLY_0055);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0055;
	}

FINALLY_0055:
	{ // begin finally (depth: 1)
		{
			bool L_8 = V_1;
			if (!L_8)
			{
				goto IL_005e;
			}
		}

IL_0058:
		{
			RuntimeObject * L_9 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_9, /*hidden argument*/NULL);
		}

IL_005e:
		{
			IL2CPP_END_FINALLY(85)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(85)
	{
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005f:
	{
		// }));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`8_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipObservable`8<T1,T2,T3,T4,T5,T6,T7,TR>,System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip__ctor_mD100CA4C96D762F9368A7AA6324C8FCF65CE93A7_gshared (Zip_tFE288625F1A712CC45FAE011730910DD457970CC * __this, ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip__ctor_mD100CA4C96D762F9368A7AA6324C8FCF65CE93A7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_6(L_0);
		// readonly Queue<T1> q1 = new Queue<T1>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_1 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		__this->set_q1_7(L_1);
		// readonly Queue<T2> q2 = new Queue<T2>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		__this->set_q2_8(L_2);
		// readonly Queue<T3> q3 = new Queue<T3>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_3 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		__this->set_q3_9(L_3);
		// readonly Queue<T4> q4 = new Queue<T4>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		__this->set_q4_10(L_4);
		// readonly Queue<T5> q5 = new Queue<T5>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_5 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)(L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		__this->set_q5_11(L_5);
		// readonly Queue<T6> q6 = new Queue<T6>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		__this->set_q6_12(L_6);
		// readonly Queue<T7> q7 = new Queue<T7>();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_7 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 12));
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13)->methodPointer)(L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13));
		__this->set_q7_13(L_7);
		// : base(observer, cancel)
		RuntimeObject* L_8 = ___observer1;
		RuntimeObject* L_9 = ___cancel2;
		((  void (*) (NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14)->methodPointer)((NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *)__this, (RuntimeObject*)L_8, (RuntimeObject*)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14));
		// this.parent = parent;
		ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * L_10 = ___parent0;
		__this->set_parent_5(L_10);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`8_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Zip_Run_m5D63082B93153494983F6C2415FAD6FB69239C94_gshared (Zip_tFE288625F1A712CC45FAE011730910DD457970CC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zip_Run_m5D63082B93153494983F6C2415FAD6FB69239C94_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	RuntimeObject* V_5 = NULL;
	RuntimeObject* V_6 = NULL;
	{
		// base.SetQueue(new System.Collections.ICollection[] { q1, q2, q3, q4, q5, q6, q7 });
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_0 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)(ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)SZArrayNew(ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8_il2cpp_TypeInfo_var, (uint32_t)7);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_1 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_0;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_2);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_3 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_1;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_4);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_5 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_3;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_6);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_7 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_5;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_8 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_8);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_9 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_7;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_10 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q5_11();
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (RuntimeObject*)L_10);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_11 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_9;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_12 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q6_12();
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (RuntimeObject*)L_12);
		ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8* L_13 = (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_11;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_14 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q7_13();
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (RuntimeObject*)L_14);
		((  void (*) (NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *, ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17)->methodPointer)((NthZipObserverBase_1_t656950E06BB2693C561C7D731C13A93A99EAE677 *)__this, (ICollectionU5BU5D_t4DA75741BFADECB3D4A64252B6081852D5AE25F8*)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17));
		// var s1 = parent.source1.Subscribe(new ZipObserver<T1>(gate, this, 0, q1));
		ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * L_15 = (ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)__this->get_parent_5();
		RuntimeObject* L_16 = (RuntimeObject*)L_15->get_source1_1();
		RuntimeObject * L_17 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_18 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_19 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 18));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19)->methodPointer)(L_19, (RuntimeObject *)L_17, (RuntimeObject*)__this, (int32_t)0, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19));
		RuntimeObject* L_20 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 20), (RuntimeObject*)L_16, (RuntimeObject*)L_19);
		V_0 = (RuntimeObject*)L_20;
		// var s2 = parent.source2.Subscribe(new ZipObserver<T2>(gate, this, 1, q2));
		ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * L_21 = (ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)__this->get_parent_5();
		RuntimeObject* L_22 = (RuntimeObject*)L_21->get_source2_2();
		RuntimeObject * L_23 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_24 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_25 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 21));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22)->methodPointer)(L_25, (RuntimeObject *)L_23, (RuntimeObject*)__this, (int32_t)1, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22));
		RuntimeObject* L_26 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 23), (RuntimeObject*)L_22, (RuntimeObject*)L_25);
		V_1 = (RuntimeObject*)L_26;
		// var s3 = parent.source3.Subscribe(new ZipObserver<T3>(gate, this, 2, q3));
		ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * L_27 = (ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)__this->get_parent_5();
		RuntimeObject* L_28 = (RuntimeObject*)L_27->get_source3_3();
		RuntimeObject * L_29 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_30 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_31 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 24));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)(L_31, (RuntimeObject *)L_29, (RuntimeObject*)__this, (int32_t)2, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		RuntimeObject* L_32 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 26), (RuntimeObject*)L_28, (RuntimeObject*)L_31);
		V_2 = (RuntimeObject*)L_32;
		// var s4 = parent.source4.Subscribe(new ZipObserver<T4>(gate, this, 3, q4));
		ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * L_33 = (ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)__this->get_parent_5();
		RuntimeObject* L_34 = (RuntimeObject*)L_33->get_source4_4();
		RuntimeObject * L_35 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_36 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_37 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 27));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 28)->methodPointer)(L_37, (RuntimeObject *)L_35, (RuntimeObject*)__this, (int32_t)3, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 28));
		RuntimeObject* L_38 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 29), (RuntimeObject*)L_34, (RuntimeObject*)L_37);
		V_3 = (RuntimeObject*)L_38;
		// var s5 = parent.source5.Subscribe(new ZipObserver<T5>(gate, this, 4, q5));
		ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * L_39 = (ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)__this->get_parent_5();
		RuntimeObject* L_40 = (RuntimeObject*)L_39->get_source5_5();
		RuntimeObject * L_41 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_42 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q5_11();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_43 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 30));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31)->methodPointer)(L_43, (RuntimeObject *)L_41, (RuntimeObject*)__this, (int32_t)4, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_42, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31));
		RuntimeObject* L_44 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 32), (RuntimeObject*)L_40, (RuntimeObject*)L_43);
		V_4 = (RuntimeObject*)L_44;
		// var s6 = parent.source6.Subscribe(new ZipObserver<T6>(gate, this, 5, q6));
		ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * L_45 = (ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)__this->get_parent_5();
		RuntimeObject* L_46 = (RuntimeObject*)L_45->get_source6_6();
		RuntimeObject * L_47 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_48 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q6_12();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_49 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 33));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 34)->methodPointer)(L_49, (RuntimeObject *)L_47, (RuntimeObject*)__this, (int32_t)5, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 34));
		RuntimeObject* L_50 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 35), (RuntimeObject*)L_46, (RuntimeObject*)L_49);
		V_5 = (RuntimeObject*)L_50;
		// var s7 = parent.source7.Subscribe(new ZipObserver<T7>(gate, this, 6, q7));
		ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * L_51 = (ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)__this->get_parent_5();
		RuntimeObject* L_52 = (RuntimeObject*)L_51->get_source7_7();
		RuntimeObject * L_53 = (RuntimeObject *)__this->get_gate_6();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_54 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q7_13();
		ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * L_55 = (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 36));
		((  void (*) (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 *, RuntimeObject *, RuntimeObject*, int32_t, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 37)->methodPointer)(L_55, (RuntimeObject *)L_53, (RuntimeObject*)__this, (int32_t)6, (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_54, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 37));
		RuntimeObject* L_56 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 38), (RuntimeObject*)L_52, (RuntimeObject*)L_55);
		V_6 = (RuntimeObject*)L_56;
		// return StableCompositeDisposable.Create(s1, s2, s3, s4, s5, s6, s7, Disposable.Create(() =>
		// {
		//     lock (gate)
		//     {
		//         q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear(); q7.Clear();
		//     }
		// }));
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_57 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)SZArrayNew(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65_il2cpp_TypeInfo_var, (uint32_t)8);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_58 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_57;
		RuntimeObject* L_59 = V_0;
		ArrayElementTypeCheck (L_58, L_59);
		(L_58)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_59);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_60 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_58;
		RuntimeObject* L_61 = V_1;
		ArrayElementTypeCheck (L_60, L_61);
		(L_60)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_61);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_62 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_60;
		RuntimeObject* L_63 = V_2;
		ArrayElementTypeCheck (L_62, L_63);
		(L_62)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_63);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_64 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_62;
		RuntimeObject* L_65 = V_3;
		ArrayElementTypeCheck (L_64, L_65);
		(L_64)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_65);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_66 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_64;
		RuntimeObject* L_67 = V_4;
		ArrayElementTypeCheck (L_66, L_67);
		(L_66)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (RuntimeObject*)L_67);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_68 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_66;
		RuntimeObject* L_69 = V_5;
		ArrayElementTypeCheck (L_68, L_69);
		(L_68)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (RuntimeObject*)L_69);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_70 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_68;
		RuntimeObject* L_71 = V_6;
		ArrayElementTypeCheck (L_70, L_71);
		(L_70)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (RuntimeObject*)L_71);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_72 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_70;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_73 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_73, (RuntimeObject *)__this, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 39)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_tFB6FF3E5C049CFE0B05B25A824CED890F91DC101_il2cpp_TypeInfo_var);
		RuntimeObject* L_74 = Disposable_Create_m3E97031C0F83935B39EA43B34B685D80B644DCE2((Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_73, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_72, L_74);
		(L_72)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(7), (RuntimeObject*)L_74);
		RuntimeObject* L_75 = StableCompositeDisposable_Create_mA26CD55619BE45D7F630D010C9BB47D84E83BCAA((IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_72, /*hidden argument*/NULL);
		return L_75;
	}
}
// TR UniRx.Operators.ZipObservable`8_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Zip_GetResult_m63D375A5D2C33E62A0CC9E1D09412071D6EFC6D0_gshared (Zip_tFE288625F1A712CC45FAE011730910DD457970CC * __this, const RuntimeMethod* method)
{
	{
		// return parent.resultSelector(q1.Dequeue(), q2.Dequeue(), q3.Dequeue(), q4.Dequeue(), q5.Dequeue(), q6.Dequeue(), q7.Dequeue());
		ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * L_0 = (ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)__this->get_parent_5();
		ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 * L_1 = (ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 *)L_0->get_resultSelector_8();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 40)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 40));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 41)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 41));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 42)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 42));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_8 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 43)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 43));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_10 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q5_11();
		RuntimeObject * L_11 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 44)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 44));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_12 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q6_12();
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 45)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 45));
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_14 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q7_13();
		RuntimeObject * L_15 = ((  RuntimeObject * (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 46)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 46));
		RuntimeObject * L_16 = ((  RuntimeObject * (*) (ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 47)->methodPointer)((ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 *)L_1, (RuntimeObject *)L_3, (RuntimeObject *)L_5, (RuntimeObject *)L_7, (RuntimeObject *)L_9, (RuntimeObject *)L_11, (RuntimeObject *)L_13, (RuntimeObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 47));
		return L_16;
	}
}
// System.Void UniRx.Operators.ZipObservable`8_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnNext_mE442FAB3DE94BD3B2E3364060A1D00922A40BDB2_gshared (Zip_tFE288625F1A712CC45FAE011730910DD457970CC * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 48), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`8_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnError_m405421AF13F2133D088EDA95A8B596B36E7F68A0_gshared (Zip_tFE288625F1A712CC45FAE011730910DD457970CC * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 48), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 49)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 49));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`8_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_OnCompleted_m6E20CB16DCEC4F99D6CC1536FF5A69314EF1B86E_gshared (Zip_tFE288625F1A712CC45FAE011730910DD457970CC * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 48), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 49)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 49));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`8_Zip<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::<Run>b__10_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zip_U3CRunU3Eb__10_0_m6068AB5029A7DE2627F845EBAA0D9192026BEF75_gshared (Zip_tFE288625F1A712CC45FAE011730910DD457970CC * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (gate)
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_6();
		V_0 = (RuntimeObject *)L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_1, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear(); q7.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q1_7();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 50)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 50));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear(); q7.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_3 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q2_8();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 51)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 51));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear(); q7.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_4 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q3_9();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 52)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 52));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear(); q7.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_5 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q4_10();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 53)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 53));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear(); q7.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_6 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q5_11();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 54)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 54));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear(); q7.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_7 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q6_12();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 55)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 55));
		// q1.Clear(); q2.Clear(); q3.Clear(); q4.Clear(); q5.Clear(); q6.Clear(); q7.Clear();
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_8 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_q7_13();
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 56)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 56));
		// }
		IL2CPP_LEAVE(0x6A, FINALLY_0060);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		{
			bool L_9 = V_1;
			if (!L_9)
			{
				goto IL_0069;
			}
		}

IL_0063:
		{
			RuntimeObject * L_10 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_10, /*hidden argument*/NULL);
		}

IL_0069:
		{
			IL2CPP_END_FINALLY(96)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x6A, IL_006a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_006a:
	{
		// }));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipFunc`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipFunc_4__ctor_mEDC6B8380531D47F92CC77166DB215C0A8AC5CCA_gshared (ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.ZipFunc`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipFunc_4_Invoke_m57749AE54EB7719204B79C174AB3C69CF62D4AE8_gshared (ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 3)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, targetMethod);
			}
		}
		else if (___parameterCount != 3)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32);
					else
						result = GenericVirtFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21, ___arg32);
					else
						result = VirtFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21, ___arg32);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg21) - 1), ___arg32, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, targetMethod);
				}
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32);
					else
						result = GenericVirtFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21, ___arg32);
					else
						result = VirtFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21, ___arg32);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg10) - 1), ___arg21, ___arg32, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult UniRx.Operators.ZipFunc`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipFunc_4_BeginInvoke_m44A26E6E46297748FD2453B5C33E10906066E334_gshared (ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// TR UniRx.Operators.ZipFunc`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipFunc_4_EndInvoke_m616108235B9E3AFD3E188B147E699108AA07B34A_gshared (ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipFunc_5__ctor_m77F9A85D1504AC6CF776BF30F1299D7CCC9A2C11_gshared (ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.ZipFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipFunc_5_Invoke_m2285547C1AE9E36AB901C7CC07796E4523B31DD2_gshared (ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 4)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, targetMethod);
			}
		}
		else if (___parameterCount != 4)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43);
					else
						result = GenericVirtFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43);
					else
						result = VirtFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg21) - 1), ___arg32, ___arg43, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, targetMethod);
				}
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43);
					else
						result = GenericVirtFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43);
					else
						result = VirtFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg10) - 1), ___arg21, ___arg32, ___arg43, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult UniRx.Operators.ZipFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipFunc_5_BeginInvoke_m40501B8A7950C38E13B1670976916C84FD48E0BF_gshared (ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback4, RuntimeObject * ___object5, const RuntimeMethod* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback4, (RuntimeObject*)___object5);
}
// TR UniRx.Operators.ZipFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipFunc_5_EndInvoke_m5F807CEA066FBAEC41FD5B1CFB51B9368294A639_gshared (ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipFunc_6__ctor_m4E31839BF3396E7F4F8E954A005D999B55FE9331_gshared (ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.ZipFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipFunc_6_Invoke_m9C14DC567D752923ACB96315157E7B753F94A706_gshared (ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, RuntimeObject * ___arg54, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 5)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, targetMethod);
			}
		}
		else if (___parameterCount != 5)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
					else
						result = GenericVirtFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
					else
						result = VirtFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg21) - 1), ___arg32, ___arg43, ___arg54, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, targetMethod);
				}
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
					else
						result = GenericVirtFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
					else
						result = VirtFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg10) - 1), ___arg21, ___arg32, ___arg43, ___arg54, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult UniRx.Operators.ZipFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipFunc_6_BeginInvoke_mA503F4D30AA0446CCE031CDDECC6F7E431755D2D_gshared (ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, RuntimeObject * ___arg54, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback5, RuntimeObject * ___object6, const RuntimeMethod* method)
{
	void *__d_args[6] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback5, (RuntimeObject*)___object6);
}
// TR UniRx.Operators.ZipFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipFunc_6_EndInvoke_m77BAEB6BCE0CCCF2580264B1F2FA505863396887_gshared (ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipFunc_7__ctor_m5810315ADAF5D9A80E6F80A1101C6D2E1B299BD5_gshared (ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.ZipFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipFunc_7_Invoke_m9BE58EC476D40ACBDDA5C5B7FECF2288EFE64907_gshared (ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, RuntimeObject * ___arg54, RuntimeObject * ___arg65, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 6)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
			}
		}
		else if (___parameterCount != 6)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
					else
						result = GenericVirtFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
					else
						result = VirtFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg21) - 1), ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
				}
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
					else
						result = GenericVirtFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
					else
						result = VirtFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg10) - 1), ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult UniRx.Operators.ZipFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipFunc_7_BeginInvoke_m50CCEF583BEBE566EF773C45D1234094EA6CAD40_gshared (ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, RuntimeObject * ___arg54, RuntimeObject * ___arg65, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback6, RuntimeObject * ___object7, const RuntimeMethod* method)
{
	void *__d_args[7] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	__d_args[5] = ___arg65;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback6, (RuntimeObject*)___object7);
}
// TR UniRx.Operators.ZipFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipFunc_7_EndInvoke_m140ACCECC30291E8A4D4BD2B6E6FA9C5D7DADB4D_gshared (ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipFunc_8__ctor_m8B5C917D3711703D873ABF174DF33CD350FF90E6_gshared (ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.ZipFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6,T7)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipFunc_8_Invoke_m43FB5C69CD8388D1A80793E13D11C65723C87698_gshared (ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, RuntimeObject * ___arg54, RuntimeObject * ___arg65, RuntimeObject * ___arg76, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 7)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
			}
		}
		else if (___parameterCount != 7)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
					else
						result = GenericVirtFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
					else
						result = VirtFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg21) - 1), ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
				}
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker7< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
					else
						result = GenericVirtFuncInvoker7< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker7< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
					else
						result = VirtFuncInvoker7< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg10) - 1), ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult UniRx.Operators.ZipFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,T7,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipFunc_8_BeginInvoke_m827E8FCAB4F92EFEF2896D7FD3DCC609C47C0CE4_gshared (ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, RuntimeObject * ___arg54, RuntimeObject * ___arg65, RuntimeObject * ___arg76, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback7, RuntimeObject * ___object8, const RuntimeMethod* method)
{
	void *__d_args[8] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	__d_args[5] = ___arg65;
	__d_args[6] = ___arg76;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback7, (RuntimeObject*)___object8);
}
// TR UniRx.Operators.ZipFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipFunc_8_EndInvoke_m4DCC9306C457172756C1C98D9B44177B20CEC19E_gshared (ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`1_ZipLatest<System.Object>::.ctor(UniRx.Operators.ZipLatestObservable`1<T>,System.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest__ctor_mDF54144032B9106F99FB1263F03EBC5ECE8985EC_gshared (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * __this, ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatest__ctor_mDF54144032B9106F99FB1263F03EBC5ECE8985EC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		// public ZipLatest(ZipLatestObservable<T> parent, IObserver<IList<T>> observer, IDisposable cancel) : base(observer, cancel)
		RuntimeObject* L_1 = ___observer1;
		RuntimeObject* L_2 = ___cancel2;
		((  void (*) (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this, (RuntimeObject*)L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`1_ZipLatest<System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatest_Run_mCFF5BFC3075B96740A882513D118F128E5704D27_gshared (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatest_Run_mCFF5BFC3075B96740A882513D118F128E5704D27_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject* V_2 = NULL;
	{
		// length = parent.sources.Length;
		ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 * L_0 = (ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 *)__this->get_parent_2();
		IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* L_1 = (IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B*)L_0->get_sources_1();
		__this->set_length_4((((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length)))));
		// values = new T[length];
		int32_t L_2 = (int32_t)__this->get_length_4();
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_3 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2), (uint32_t)L_2);
		__this->set_values_5(L_3);
		// isStarted = new bool[length];
		int32_t L_4 = (int32_t)__this->get_length_4();
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_5 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)SZArrayNew(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C_il2cpp_TypeInfo_var, (uint32_t)L_4);
		__this->set_isStarted_6(L_5);
		// isCompleted = new bool[length];
		int32_t L_6 = (int32_t)__this->get_length_4();
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_7 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)SZArrayNew(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C_il2cpp_TypeInfo_var, (uint32_t)L_6);
		__this->set_isCompleted_7(L_7);
		// var disposables = new IDisposable[length];
		int32_t L_8 = (int32_t)__this->get_length_4();
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_9 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)SZArrayNew(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65_il2cpp_TypeInfo_var, (uint32_t)L_8);
		V_0 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_9;
		// for (int i = 0; i < length; i++)
		V_1 = (int32_t)0;
		goto IL_0078;
	}

IL_0056:
	{
		// var source = parent.sources[i];
		ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 * L_10 = (ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 *)__this->get_parent_2();
		IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* L_11 = (IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B*)L_10->get_sources_1();
		int32_t L_12 = V_1;
		int32_t L_13 = L_12;
		RuntimeObject* L_14 = (RuntimeObject*)(L_11)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_13));
		V_2 = (RuntimeObject*)L_14;
		// disposables[i] = source.Subscribe(new ZipLatestObserver(this, i));
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_15 = V_0;
		int32_t L_16 = V_1;
		RuntimeObject* L_17 = V_2;
		int32_t L_18 = V_1;
		ZipLatestObserver_t1916CBEFD17AA1674F927430EF42C7EAEB835507 * L_19 = (ZipLatestObserver_t1916CBEFD17AA1674F927430EF42C7EAEB835507 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (ZipLatestObserver_t1916CBEFD17AA1674F927430EF42C7EAEB835507 *, ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_19, (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)__this, (int32_t)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		RuntimeObject* L_20 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_17, (RuntimeObject*)L_19);
		ArrayElementTypeCheck (L_15, L_20);
		(L_15)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_16), (RuntimeObject*)L_20);
		// for (int i = 0; i < length; i++)
		int32_t L_21 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_0078:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_22 = V_1;
		int32_t L_23 = (int32_t)__this->get_length_4();
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0056;
		}
	}
	{
		// return StableCompositeDisposable.CreateUnsafe(disposables);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_24 = V_0;
		RuntimeObject* L_25 = StableCompositeDisposable_CreateUnsafe_mBCB7541CE148A6314AC85043A4C1515A050A793A((IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_24, /*hidden argument*/NULL);
		return L_25;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`1_ZipLatest<System.Object>::Publish(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_Publish_mEE1C0DAC75771FA19101F06D645CE3CB182879C8_gshared (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// isStarted[index] = true;
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_0 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)__this->get_isStarted_6();
		int32_t L_1 = ___index0;
		(L_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_1), (bool)1);
		// var hasOnCompleted = false;
		V_0 = (bool)0;
		// var allValueStarted = true;
		V_1 = (bool)1;
		// for (int i = 0; i < length; i++)
		V_2 = (int32_t)0;
		goto IL_0033;
	}

IL_0011:
	{
		// if (!isStarted[i])
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_2 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)__this->get_isStarted_6();
		int32_t L_3 = V_2;
		int32_t L_4 = L_3;
		uint8_t L_5 = (uint8_t)(L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		// allValueStarted = false;
		V_1 = (bool)0;
		// break;
		goto IL_003c;
	}

IL_001f:
	{
		// if (i == index) continue;
		int32_t L_6 = V_2;
		int32_t L_7 = ___index0;
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_002f;
		}
	}
	{
		// if (isCompleted[i]) hasOnCompleted = true;
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_8 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)__this->get_isCompleted_7();
		int32_t L_9 = V_2;
		int32_t L_10 = L_9;
		uint8_t L_11 = (uint8_t)(L_8)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_10));
		if (!L_11)
		{
			goto IL_002f;
		}
	}
	{
		// if (isCompleted[i]) hasOnCompleted = true;
		V_0 = (bool)1;
	}

IL_002f:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_12 = V_2;
		V_2 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_0033:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_13 = V_2;
		int32_t L_14 = (int32_t)__this->get_length_4();
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0011;
		}
	}

IL_003c:
	{
		// if (allValueStarted)
		bool L_15 = V_1;
		if (!L_15)
		{
			goto IL_007d;
		}
	}
	{
		// OnNext(new List<T>(values));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_16 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get_values_5();
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_17 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6));
		((  void (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)(L_17, (RuntimeObject*)(RuntimeObject*)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		VirtActionInvoker1< RuntimeObject* >::Invoke(8 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Collections.Generic.IList`1<System.Object>,System.Collections.Generic.IList`1<System.Object>>::OnNext(TSource) */, (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this, (RuntimeObject*)L_17);
		// if (hasOnCompleted)
		bool L_18 = V_0;
		if (!L_18)
		{
			goto IL_006a;
		}
	}

IL_0053:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_19 = (RuntimeObject*)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9), (RuntimeObject*)L_19);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x69, FINALLY_0062);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0062;
	}

FINALLY_0062:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(98)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(98)
	{
		IL2CPP_JUMP_TBL(0x69, IL_0069)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0069:
	{
		// return;
		return;
	}

IL_006a:
	{
		// Array.Clear(isStarted, 0, length); // reset
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_20 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)__this->get_isStarted_6();
		int32_t L_21 = (int32_t)__this->get_length_4();
		Array_Clear_mEB42D172C5E0825D340F6209F28578BDDDDCE34F((RuntimeArray *)(RuntimeArray *)L_20, (int32_t)0, (int32_t)L_21, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_007d:
	{
		// for (int i = 0; i < length; i++)
		V_3 = (int32_t)0;
		goto IL_00b4;
	}

IL_0081:
	{
		// if (i == index) continue;
		int32_t L_22 = V_3;
		int32_t L_23 = ___index0;
		if ((((int32_t)L_22) == ((int32_t)L_23)))
		{
			goto IL_00b0;
		}
	}
	{
		// if (isCompleted[i] && !isStarted[i])
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_24 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)__this->get_isCompleted_7();
		int32_t L_25 = V_3;
		int32_t L_26 = L_25;
		uint8_t L_27 = (uint8_t)(L_24)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_26));
		if (!L_27)
		{
			goto IL_00b0;
		}
	}
	{
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_28 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)__this->get_isStarted_6();
		int32_t L_29 = V_3;
		int32_t L_30 = L_29;
		uint8_t L_31 = (uint8_t)(L_28)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_30));
		if (L_31)
		{
			goto IL_00b0;
		}
	}

IL_0099:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_32 = (RuntimeObject*)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9), (RuntimeObject*)L_32);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0xAF, FINALLY_00a8);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00a8;
	}

FINALLY_00a8:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(168)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(168)
	{
		IL2CPP_JUMP_TBL(0xAF, IL_00af)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00af:
	{
		// return;
		return;
	}

IL_00b0:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_33 = V_3;
		V_3 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_00b4:
	{
		// for (int i = 0; i < length; i++)
		int32_t L_34 = V_3;
		int32_t L_35 = (int32_t)__this->get_length_4();
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_0081;
		}
	}
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`1_ZipLatest<System.Object>::OnNext(System.Collections.Generic.IList`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnNext_m313E0E9F2E88700E85BE467BBB95C563EA37ECBE_gshared (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject* L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject* >::Invoke(0 /* System.Void System.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9), (RuntimeObject*)L_0, (RuntimeObject*)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`1_ZipLatest<System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnError_m64EBEFFEBA8A3442C557BA51EC58A689F3FC4456_gshared (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`1_ZipLatest<System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnCompleted_m660C607649117667DA466916F66C1075D4FEF491_gshared (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`3_ZipLatest<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipLatestObservable`3<TLeft,TRight,TResult>,System.IObserver`1<TResult>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest__ctor_m6E374DD3DCBFC2A22BC03032A56830C4690B94F2_gshared (ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 * __this, ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 * ___parent0, RuntimeObject* ___observer1, RuntimeObject* ___cancel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatest__ctor_m6E374DD3DCBFC2A22BC03032A56830C4690B94F2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_3(L_0);
		// public ZipLatest(ZipLatestObservable<TLeft, TRight, TResult> parent, IObserver<TResult> observer, IDisposable cancel) : base(observer, cancel)
		RuntimeObject* L_1 = ___observer1;
		RuntimeObject* L_2 = ___cancel2;
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, (RuntimeObject*)L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 * L_3 = ___parent0;
		__this->set_parent_2(L_3);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`3_ZipLatest<System.Object,System.Object,System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatest_Run_m3870F78C0436AC6CCAF506EC29382161695A7DF1_gshared (ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		// var l = parent.left.Subscribe(new LeftObserver(this));
		ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 * L_0 = (ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 *)__this->get_parent_2();
		RuntimeObject* L_1 = (RuntimeObject*)L_0->get_left_1();
		LeftObserver_t7E253BA735D35FF82CE6EB117CCB0BC66532812E * L_2 = (LeftObserver_t7E253BA735D35FF82CE6EB117CCB0BC66532812E *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (LeftObserver_t7E253BA735D35FF82CE6EB117CCB0BC66532812E *, ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, (ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		RuntimeObject* L_3 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4), (RuntimeObject*)L_1, (RuntimeObject*)L_2);
		// var r = parent.right.Subscribe(new RightObserver(this));
		ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 * L_4 = (ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 *)__this->get_parent_2();
		RuntimeObject* L_5 = (RuntimeObject*)L_4->get_right_2();
		RightObserver_tB3720D149C18F78108C0B9CBA08EE73EE963CE23 * L_6 = (RightObserver_tB3720D149C18F78108C0B9CBA08EE73EE963CE23 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5));
		((  void (*) (RightObserver_tB3720D149C18F78108C0B9CBA08EE73EE963CE23 *, ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)(L_6, (ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		RuntimeObject* L_7 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7), (RuntimeObject*)L_5, (RuntimeObject*)L_6);
		V_0 = (RuntimeObject*)L_7;
		// return StableCompositeDisposable.Create(l, r);
		RuntimeObject* L_8 = V_0;
		RuntimeObject* L_9 = StableCompositeDisposable_Create_mFBB35C8BE58E02B49CB9A9CAEC34B451A7DD0122((RuntimeObject*)L_3, (RuntimeObject*)L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`3_ZipLatest<System.Object,System.Object,System.Object>::Publish()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_Publish_m703D1DBB95522C016CABF3180AFD11FB542148E0_gshared (ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatest_Publish_m703D1DBB95522C016CABF3180AFD11FB542148E0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 5);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// if ((leftCompleted && !leftStarted) || (rightCompleted && !rightStarted))
		bool L_0 = (bool)__this->get_leftCompleted_6();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_leftStarted_5();
		if (!L_1)
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		bool L_2 = (bool)__this->get_rightCompleted_9();
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		bool L_3 = (bool)__this->get_rightStarted_8();
		if (L_3)
		{
			goto IL_0038;
		}
	}

IL_0020:
	{
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_4 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8), (RuntimeObject*)L_4);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x37, FINALLY_0030);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(48)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x37, IL_0037)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0037:
	{
		// return;
		return;
	}

IL_0038:
	{
		// else if (!(leftStarted && rightStarted))
		bool L_5 = (bool)__this->get_leftStarted_5();
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		bool L_6 = (bool)__this->get_rightStarted_8();
		if (L_6)
		{
			goto IL_0049;
		}
	}

IL_0048:
	{
		// return;
		return;
	}

IL_0049:
	{
	}

IL_004a:
	try
	{ // begin try (depth: 1)
		// v = parent.selector(leftValue, rightValue);
		ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 * L_7 = (ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 *)__this->get_parent_2();
		Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * L_8 = (Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 *)L_7->get_selector_3();
		RuntimeObject * L_9 = (RuntimeObject *)__this->get_leftValue_4();
		RuntimeObject * L_10 = (RuntimeObject *)__this->get_rightValue_7();
		RuntimeObject * L_11 = ((  RuntimeObject * (*) (Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 *)L_8, (RuntimeObject *)L_9, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		V_0 = (RuntimeObject *)L_11;
		// }
		goto IL_0083;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0069;
		throw e;
	}

CATCH_0069:
	{ // begin catch(System.Exception)
		{
			// catch (Exception ex)
			V_1 = (Exception_t *)((Exception_t *)__exception_local);
		}

IL_006a:
		try
		{ // begin try (depth: 2)
			// try { observer.OnError(ex); }
			RuntimeObject* L_12 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
			il2cpp_codegen_memory_barrier();
			Exception_t * L_13 = V_1;
			InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8), (RuntimeObject*)L_12, (Exception_t *)L_13);
			// try { observer.OnError(ex); }
			IL2CPP_LEAVE(0x81, FINALLY_007a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_007a;
		}

FINALLY_007a:
		{ // begin finally (depth: 2)
			// finally { Dispose(); }
			((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
			// finally { Dispose(); }
			IL2CPP_END_FINALLY(122)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(122)
		{
			IL2CPP_JUMP_TBL(0x81, IL_0081)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0081:
		{
			// return;
			goto IL_00c0;
		}
	} // end catch (depth: 1)

IL_0083:
	{
		// OnNext(v);
		RuntimeObject * L_14 = V_0;
		VirtActionInvoker1< RuntimeObject * >::Invoke(8 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>::OnNext(TSource) */, (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, (RuntimeObject *)L_14);
		// leftStarted = false;
		__this->set_leftStarted_5((bool)0);
		// rightStarted = false;
		__this->set_rightStarted_8((bool)0);
		// if (leftCompleted || rightCompleted)
		bool L_15 = (bool)__this->get_leftCompleted_6();
		if (L_15)
		{
			goto IL_00a8;
		}
	}
	{
		bool L_16 = (bool)__this->get_rightCompleted_9();
		if (!L_16)
		{
			goto IL_00c0;
		}
	}

IL_00a8:
	{
	}

IL_00a9:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_17 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8), (RuntimeObject*)L_17);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0xBF, FINALLY_00b8);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00b8;
	}

FINALLY_00b8:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(184)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(184)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00bf:
	{
		// return;
		return;
	}

IL_00c0:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`3_ZipLatest<System.Object,System.Object,System.Object>::OnNext(TResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnNext_mFC0F536938FEFE3DB76A254B2425D33899305736_gshared (ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`3_ZipLatest<System.Object,System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnError_m16F27B39346CB573E30AFA7CB5FD99ADE4E89134_gshared (ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`3_ZipLatest<System.Object,System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnCompleted_m6F8F0EB3E726BDC6C1E7ACB5C5BB1D9832D0A438_gshared (ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`4_ZipLatest<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32,UniRx.Operators.ZipLatestObservable`4<T1,T2,T3,TR>,System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest__ctor_m08EFFB073F24C1A73907F31BA2E36E55E85E0D26_gshared (ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D * __this, int32_t ___length0, ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * ___parent1, RuntimeObject* ___observer2, RuntimeObject* ___cancel3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatest__ctor_m08EFFB073F24C1A73907F31BA2E36E55E85E0D26_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_6(L_0);
		// : base(length, observer, cancel)
		int32_t L_1 = ___length0;
		RuntimeObject* L_2 = ___observer2;
		RuntimeObject* L_3 = ___cancel3;
		((  void (*) (NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D *, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D *)__this, (int32_t)L_1, (RuntimeObject*)L_2, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * L_4 = ___parent1;
		__this->set_parent_5(L_4);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`4_ZipLatest<System.Object,System.Object,System.Object,System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatest_Run_m653657FA8CF9C2AB1980B50104CD778793DB5CCF_gshared (ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		// c1 = new ZipLatestObserver<T1>(gate, this, 0);
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_1 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_1, (RuntimeObject *)L_0, (RuntimeObject*)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_c1_7(L_1);
		// c2 = new ZipLatestObserver<T2>(gate, this, 1);
		RuntimeObject * L_2 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_3 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)(L_3, (RuntimeObject *)L_2, (RuntimeObject*)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		__this->set_c2_8(L_3);
		// c3 = new ZipLatestObserver<T3>(gate, this, 2);
		RuntimeObject * L_4 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_5 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_5, (RuntimeObject *)L_4, (RuntimeObject*)__this, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		__this->set_c3_9(L_5);
		// var s1 = parent.source1.Subscribe(c1);
		ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * L_6 = (ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 *)__this->get_parent_5();
		RuntimeObject* L_7 = (RuntimeObject*)L_6->get_source1_1();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_8 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c1_7();
		RuntimeObject* L_9 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9), (RuntimeObject*)L_7, (RuntimeObject*)L_8);
		// var s2 = parent.source2.Subscribe(c2);
		ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * L_10 = (ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 *)__this->get_parent_5();
		RuntimeObject* L_11 = (RuntimeObject*)L_10->get_source2_2();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_12 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c2_8();
		RuntimeObject* L_13 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10), (RuntimeObject*)L_11, (RuntimeObject*)L_12);
		V_0 = (RuntimeObject*)L_13;
		// var s3 = parent.source3.Subscribe(c3);
		ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * L_14 = (ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 *)__this->get_parent_5();
		RuntimeObject* L_15 = (RuntimeObject*)L_14->get_source3_3();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_16 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c3_9();
		RuntimeObject* L_17 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 11), (RuntimeObject*)L_15, (RuntimeObject*)L_16);
		V_1 = (RuntimeObject*)L_17;
		// return StableCompositeDisposable.Create(s1, s2, s3);
		RuntimeObject* L_18 = V_0;
		RuntimeObject* L_19 = V_1;
		RuntimeObject* L_20 = StableCompositeDisposable_Create_m1E0EF5E19E06763B493586C330216A928AB5D7A5((RuntimeObject*)L_9, (RuntimeObject*)L_18, (RuntimeObject*)L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// TR UniRx.Operators.ZipLatestObservable`4_ZipLatest<System.Object,System.Object,System.Object,System.Object>::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatest_GetResult_m751513ADBF0552E0FA1CCCD6A83C5C88D1F0F796_gshared (ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D * __this, const RuntimeMethod* method)
{
	{
		// return parent.resultSelector(c1.Value, c2.Value, c3.Value);
		ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * L_0 = (ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 *)__this->get_parent_5();
		ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 * L_1 = (ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 *)L_0->get_resultSelector_4();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_2 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c1_7();
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_4 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c2_8();
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_6 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c3_9();
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14));
		RuntimeObject * L_8 = ((  RuntimeObject * (*) (ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15)->methodPointer)((ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 *)L_1, (RuntimeObject *)L_3, (RuntimeObject *)L_5, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15));
		return L_8;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`4_ZipLatest<System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnNext_mB7B0012708E2E7252BB95A196634E59757237D5B_gshared (ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 16), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`4_ZipLatest<System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnError_m95528872882726F3D0EAED78E4DFA2DCC0614BC0_gshared (ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 16), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`4_ZipLatest<System.Object,System.Object,System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnCompleted_m8172F66E3C2A87C357BEE0CBB01FF53423EAEC8C_gshared (ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 16), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`5_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32,UniRx.Operators.ZipLatestObservable`5<T1,T2,T3,T4,TR>,System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest__ctor_m167F470A046891026C16D3C24E7B0103595B581D_gshared (ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61 * __this, int32_t ___length0, ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * ___parent1, RuntimeObject* ___observer2, RuntimeObject* ___cancel3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatest__ctor_m167F470A046891026C16D3C24E7B0103595B581D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_6(L_0);
		// : base(length, observer, cancel)
		int32_t L_1 = ___length0;
		RuntimeObject* L_2 = ___observer2;
		RuntimeObject* L_3 = ___cancel3;
		((  void (*) (NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D *, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D *)__this, (int32_t)L_1, (RuntimeObject*)L_2, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * L_4 = ___parent1;
		__this->set_parent_5(L_4);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`5_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatest_Run_m33F303B4C9ADCB7FBCE43C299AB312A7F83BA307_gshared (ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	{
		// c1 = new ZipLatestObserver<T1>(gate, this, 0);
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_1 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_1, (RuntimeObject *)L_0, (RuntimeObject*)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_c1_7(L_1);
		// c2 = new ZipLatestObserver<T2>(gate, this, 1);
		RuntimeObject * L_2 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_3 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)(L_3, (RuntimeObject *)L_2, (RuntimeObject*)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		__this->set_c2_8(L_3);
		// c3 = new ZipLatestObserver<T3>(gate, this, 2);
		RuntimeObject * L_4 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_5 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_5, (RuntimeObject *)L_4, (RuntimeObject*)__this, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		__this->set_c3_9(L_5);
		// c4 = new ZipLatestObserver<T4>(gate, this, 3);
		RuntimeObject * L_6 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_7 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_7, (RuntimeObject *)L_6, (RuntimeObject*)__this, (int32_t)3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		__this->set_c4_10(L_7);
		// var s1 = parent.source1.Subscribe(c1);
		ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * L_8 = (ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)__this->get_parent_5();
		RuntimeObject* L_9 = (RuntimeObject*)L_8->get_source1_1();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_10 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c1_7();
		RuntimeObject* L_11 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 11), (RuntimeObject*)L_9, (RuntimeObject*)L_10);
		// var s2 = parent.source2.Subscribe(c2);
		ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * L_12 = (ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)__this->get_parent_5();
		RuntimeObject* L_13 = (RuntimeObject*)L_12->get_source2_2();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_14 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c2_8();
		RuntimeObject* L_15 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 12), (RuntimeObject*)L_13, (RuntimeObject*)L_14);
		V_0 = (RuntimeObject*)L_15;
		// var s3 = parent.source3.Subscribe(c3);
		ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * L_16 = (ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)__this->get_parent_5();
		RuntimeObject* L_17 = (RuntimeObject*)L_16->get_source3_3();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_18 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c3_9();
		RuntimeObject* L_19 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 13), (RuntimeObject*)L_17, (RuntimeObject*)L_18);
		V_1 = (RuntimeObject*)L_19;
		// var s4 = parent.source4.Subscribe(c4);
		ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * L_20 = (ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)__this->get_parent_5();
		RuntimeObject* L_21 = (RuntimeObject*)L_20->get_source4_4();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_22 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c4_10();
		RuntimeObject* L_23 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 14), (RuntimeObject*)L_21, (RuntimeObject*)L_22);
		V_2 = (RuntimeObject*)L_23;
		// return StableCompositeDisposable.Create(s1, s2, s3, s4);
		RuntimeObject* L_24 = V_0;
		RuntimeObject* L_25 = V_1;
		RuntimeObject* L_26 = V_2;
		RuntimeObject* L_27 = StableCompositeDisposable_Create_mCA54324578190C14AC89AF3A5D1A8401AB4546D0((RuntimeObject*)L_11, (RuntimeObject*)L_24, (RuntimeObject*)L_25, (RuntimeObject*)L_26, /*hidden argument*/NULL);
		return L_27;
	}
}
// TR UniRx.Operators.ZipLatestObservable`5_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatest_GetResult_m9D759898A7083733DD01259227B659845FB89D49_gshared (ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61 * __this, const RuntimeMethod* method)
{
	{
		// return parent.resultSelector(c1.Value, c2.Value, c3.Value, c4.Value);
		ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * L_0 = (ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)__this->get_parent_5();
		ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 * L_1 = (ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 *)L_0->get_resultSelector_5();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_2 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c1_7();
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_4 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c2_8();
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_6 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c3_9();
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_8 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c4_10();
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		RuntimeObject * L_10 = ((  RuntimeObject * (*) (ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19)->methodPointer)((ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 *)L_1, (RuntimeObject *)L_3, (RuntimeObject *)L_5, (RuntimeObject *)L_7, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19));
		return L_10;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`5_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnNext_m1EC585339E326018624D491E5FF892A5EE2A6FC9_gshared (ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 20), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`5_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnError_m49856D8F1D7808D19ECAA72C99B8411EC07F4792_gshared (ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 20), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`5_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnCompleted_m2C8BE894B9878F30C11923D353702AA426789E0A_gshared (ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 20), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`6_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32,UniRx.Operators.ZipLatestObservable`6<T1,T2,T3,T4,T5,TR>,System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest__ctor_m256865DE087447D3480E2E8BC9FF352DA41B62DD_gshared (ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565 * __this, int32_t ___length0, ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * ___parent1, RuntimeObject* ___observer2, RuntimeObject* ___cancel3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatest__ctor_m256865DE087447D3480E2E8BC9FF352DA41B62DD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_6(L_0);
		// : base(length, observer, cancel)
		int32_t L_1 = ___length0;
		RuntimeObject* L_2 = ___observer2;
		RuntimeObject* L_3 = ___cancel3;
		((  void (*) (NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D *, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D *)__this, (int32_t)L_1, (RuntimeObject*)L_2, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * L_4 = ___parent1;
		__this->set_parent_5(L_4);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`6_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatest_Run_mA5594F847273DBFAECA0FF9C0DE551CC7AEFA132_gshared (ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatest_Run_mA5594F847273DBFAECA0FF9C0DE551CC7AEFA132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	{
		// c1 = new ZipLatestObserver<T1>(gate, this, 0);
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_1 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_1, (RuntimeObject *)L_0, (RuntimeObject*)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_c1_7(L_1);
		// c2 = new ZipLatestObserver<T2>(gate, this, 1);
		RuntimeObject * L_2 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_3 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)(L_3, (RuntimeObject *)L_2, (RuntimeObject*)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		__this->set_c2_8(L_3);
		// c3 = new ZipLatestObserver<T3>(gate, this, 2);
		RuntimeObject * L_4 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_5 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_5, (RuntimeObject *)L_4, (RuntimeObject*)__this, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		__this->set_c3_9(L_5);
		// c4 = new ZipLatestObserver<T4>(gate, this, 3);
		RuntimeObject * L_6 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_7 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_7, (RuntimeObject *)L_6, (RuntimeObject*)__this, (int32_t)3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		__this->set_c4_10(L_7);
		// c5 = new ZipLatestObserver<T5>(gate, this, 4);
		RuntimeObject * L_8 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_9 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 11));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)->methodPointer)(L_9, (RuntimeObject *)L_8, (RuntimeObject*)__this, (int32_t)4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12));
		__this->set_c5_11(L_9);
		// var s1 = parent.source1.Subscribe(c1);
		ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * L_10 = (ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)__this->get_parent_5();
		RuntimeObject* L_11 = (RuntimeObject*)L_10->get_source1_1();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_12 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c1_7();
		RuntimeObject* L_13 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 13), (RuntimeObject*)L_11, (RuntimeObject*)L_12);
		V_0 = (RuntimeObject*)L_13;
		// var s2 = parent.source2.Subscribe(c2);
		ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * L_14 = (ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)__this->get_parent_5();
		RuntimeObject* L_15 = (RuntimeObject*)L_14->get_source2_2();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_16 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c2_8();
		RuntimeObject* L_17 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 14), (RuntimeObject*)L_15, (RuntimeObject*)L_16);
		V_1 = (RuntimeObject*)L_17;
		// var s3 = parent.source3.Subscribe(c3);
		ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * L_18 = (ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)__this->get_parent_5();
		RuntimeObject* L_19 = (RuntimeObject*)L_18->get_source3_3();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_20 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c3_9();
		RuntimeObject* L_21 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 15), (RuntimeObject*)L_19, (RuntimeObject*)L_20);
		V_2 = (RuntimeObject*)L_21;
		// var s4 = parent.source4.Subscribe(c4);
		ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * L_22 = (ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)__this->get_parent_5();
		RuntimeObject* L_23 = (RuntimeObject*)L_22->get_source4_4();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_24 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c4_10();
		RuntimeObject* L_25 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 16), (RuntimeObject*)L_23, (RuntimeObject*)L_24);
		V_3 = (RuntimeObject*)L_25;
		// var s5 = parent.source5.Subscribe(c5);
		ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * L_26 = (ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)__this->get_parent_5();
		RuntimeObject* L_27 = (RuntimeObject*)L_26->get_source5_5();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_28 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c5_11();
		RuntimeObject* L_29 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 17), (RuntimeObject*)L_27, (RuntimeObject*)L_28);
		V_4 = (RuntimeObject*)L_29;
		// return StableCompositeDisposable.Create(s1, s2, s3, s4, s5);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_30 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)SZArrayNew(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65_il2cpp_TypeInfo_var, (uint32_t)5);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_31 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_30;
		RuntimeObject* L_32 = V_0;
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_32);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_33 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_31;
		RuntimeObject* L_34 = V_1;
		ArrayElementTypeCheck (L_33, L_34);
		(L_33)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_34);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_35 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_33;
		RuntimeObject* L_36 = V_2;
		ArrayElementTypeCheck (L_35, L_36);
		(L_35)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_36);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_37 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_35;
		RuntimeObject* L_38 = V_3;
		ArrayElementTypeCheck (L_37, L_38);
		(L_37)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_38);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_39 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_37;
		RuntimeObject* L_40 = V_4;
		ArrayElementTypeCheck (L_39, L_40);
		(L_39)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (RuntimeObject*)L_40);
		RuntimeObject* L_41 = StableCompositeDisposable_Create_mA26CD55619BE45D7F630D010C9BB47D84E83BCAA((IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_39, /*hidden argument*/NULL);
		return L_41;
	}
}
// TR UniRx.Operators.ZipLatestObservable`6_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatest_GetResult_mD243843276AFF137E5825D5A1EA639A2DB58DC89_gshared (ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565 * __this, const RuntimeMethod* method)
{
	{
		// return parent.resultSelector(c1.Value, c2.Value, c3.Value, c4.Value, c5.Value);
		ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * L_0 = (ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)__this->get_parent_5();
		ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F * L_1 = (ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F *)L_0->get_resultSelector_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_2 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c1_7();
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_4 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c2_8();
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_6 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c3_9();
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_8 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c4_10();
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_10 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c5_11();
		RuntimeObject * L_11 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22));
		RuntimeObject * L_12 = ((  RuntimeObject * (*) (ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23)->methodPointer)((ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F *)L_1, (RuntimeObject *)L_3, (RuntimeObject *)L_5, (RuntimeObject *)L_7, (RuntimeObject *)L_9, (RuntimeObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23));
		return L_12;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`6_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnNext_mEB513FBBC908E4EB8C9A7DF0CD333DF334164F8A_gshared (ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 24), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`6_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnError_m0855B8891D8955A1DE9489493DA3ED14820554E8_gshared (ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 24), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`6_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnCompleted_mAF322DAB6EBDC5C1EE6EF5FA6DA08A211F3A4740_gshared (ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 24), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`7_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32,UniRx.Operators.ZipLatestObservable`7<T1,T2,T3,T4,T5,T6,TR>,System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest__ctor_m595FDEC88B829142A13F05D109DD42EEAFC3687E_gshared (ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37 * __this, int32_t ___length0, ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * ___parent1, RuntimeObject* ___observer2, RuntimeObject* ___cancel3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatest__ctor_m595FDEC88B829142A13F05D109DD42EEAFC3687E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_6(L_0);
		// : base(length, observer, cancel)
		int32_t L_1 = ___length0;
		RuntimeObject* L_2 = ___observer2;
		RuntimeObject* L_3 = ___cancel3;
		((  void (*) (NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D *, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D *)__this, (int32_t)L_1, (RuntimeObject*)L_2, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * L_4 = ___parent1;
		__this->set_parent_5(L_4);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`7_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatest_Run_m6A33076CF7973B2DFACBAF48BBC09DB9DAE4D156_gshared (ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatest_Run_m6A33076CF7973B2DFACBAF48BBC09DB9DAE4D156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	RuntimeObject* V_5 = NULL;
	{
		// c1 = new ZipLatestObserver<T1>(gate, this, 0);
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_1 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_1, (RuntimeObject *)L_0, (RuntimeObject*)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_c1_7(L_1);
		// c2 = new ZipLatestObserver<T2>(gate, this, 1);
		RuntimeObject * L_2 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_3 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)(L_3, (RuntimeObject *)L_2, (RuntimeObject*)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		__this->set_c2_8(L_3);
		// c3 = new ZipLatestObserver<T3>(gate, this, 2);
		RuntimeObject * L_4 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_5 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_5, (RuntimeObject *)L_4, (RuntimeObject*)__this, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		__this->set_c3_9(L_5);
		// c4 = new ZipLatestObserver<T4>(gate, this, 3);
		RuntimeObject * L_6 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_7 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_7, (RuntimeObject *)L_6, (RuntimeObject*)__this, (int32_t)3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		__this->set_c4_10(L_7);
		// c5 = new ZipLatestObserver<T5>(gate, this, 4);
		RuntimeObject * L_8 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_9 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 11));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)->methodPointer)(L_9, (RuntimeObject *)L_8, (RuntimeObject*)__this, (int32_t)4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12));
		__this->set_c5_11(L_9);
		// c6 = new ZipLatestObserver<T6>(gate, this, 5);
		RuntimeObject * L_10 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_11 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 13));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14)->methodPointer)(L_11, (RuntimeObject *)L_10, (RuntimeObject*)__this, (int32_t)5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14));
		__this->set_c6_12(L_11);
		// var s1 = parent.source1.Subscribe(c1);
		ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * L_12 = (ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)__this->get_parent_5();
		RuntimeObject* L_13 = (RuntimeObject*)L_12->get_source1_1();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_14 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c1_7();
		RuntimeObject* L_15 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 15), (RuntimeObject*)L_13, (RuntimeObject*)L_14);
		V_0 = (RuntimeObject*)L_15;
		// var s2 = parent.source2.Subscribe(c2);
		ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * L_16 = (ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)__this->get_parent_5();
		RuntimeObject* L_17 = (RuntimeObject*)L_16->get_source2_2();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_18 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c2_8();
		RuntimeObject* L_19 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 16), (RuntimeObject*)L_17, (RuntimeObject*)L_18);
		V_1 = (RuntimeObject*)L_19;
		// var s3 = parent.source3.Subscribe(c3);
		ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * L_20 = (ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)__this->get_parent_5();
		RuntimeObject* L_21 = (RuntimeObject*)L_20->get_source3_3();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_22 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c3_9();
		RuntimeObject* L_23 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 17), (RuntimeObject*)L_21, (RuntimeObject*)L_22);
		V_2 = (RuntimeObject*)L_23;
		// var s4 = parent.source4.Subscribe(c4);
		ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * L_24 = (ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)__this->get_parent_5();
		RuntimeObject* L_25 = (RuntimeObject*)L_24->get_source4_4();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_26 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c4_10();
		RuntimeObject* L_27 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 18), (RuntimeObject*)L_25, (RuntimeObject*)L_26);
		V_3 = (RuntimeObject*)L_27;
		// var s5 = parent.source5.Subscribe(c5);
		ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * L_28 = (ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)__this->get_parent_5();
		RuntimeObject* L_29 = (RuntimeObject*)L_28->get_source5_5();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_30 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c5_11();
		RuntimeObject* L_31 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 19), (RuntimeObject*)L_29, (RuntimeObject*)L_30);
		V_4 = (RuntimeObject*)L_31;
		// var s6 = parent.source6.Subscribe(c6);
		ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * L_32 = (ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)__this->get_parent_5();
		RuntimeObject* L_33 = (RuntimeObject*)L_32->get_source6_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_34 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c6_12();
		RuntimeObject* L_35 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 20), (RuntimeObject*)L_33, (RuntimeObject*)L_34);
		V_5 = (RuntimeObject*)L_35;
		// return StableCompositeDisposable.Create(s1, s2, s3, s4, s5, s6);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_36 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)SZArrayNew(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65_il2cpp_TypeInfo_var, (uint32_t)6);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_37 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_36;
		RuntimeObject* L_38 = V_0;
		ArrayElementTypeCheck (L_37, L_38);
		(L_37)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_38);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_39 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_37;
		RuntimeObject* L_40 = V_1;
		ArrayElementTypeCheck (L_39, L_40);
		(L_39)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_40);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_41 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_39;
		RuntimeObject* L_42 = V_2;
		ArrayElementTypeCheck (L_41, L_42);
		(L_41)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_42);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_43 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_41;
		RuntimeObject* L_44 = V_3;
		ArrayElementTypeCheck (L_43, L_44);
		(L_43)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_44);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_45 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_43;
		RuntimeObject* L_46 = V_4;
		ArrayElementTypeCheck (L_45, L_46);
		(L_45)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (RuntimeObject*)L_46);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_47 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_45;
		RuntimeObject* L_48 = V_5;
		ArrayElementTypeCheck (L_47, L_48);
		(L_47)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (RuntimeObject*)L_48);
		RuntimeObject* L_49 = StableCompositeDisposable_Create_mA26CD55619BE45D7F630D010C9BB47D84E83BCAA((IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_47, /*hidden argument*/NULL);
		return L_49;
	}
}
// TR UniRx.Operators.ZipLatestObservable`7_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatest_GetResult_m2AE3E89DE8B79EF47E3842B024D8199554E4B62E_gshared (ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37 * __this, const RuntimeMethod* method)
{
	{
		// return parent.resultSelector(c1.Value, c2.Value, c3.Value, c4.Value, c5.Value, c6.Value);
		ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * L_0 = (ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)__this->get_parent_5();
		ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 * L_1 = (ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 *)L_0->get_resultSelector_7();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_2 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c1_7();
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_4 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c2_8();
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_6 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c3_9();
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_8 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c4_10();
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_10 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c5_11();
		RuntimeObject * L_11 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_12 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c6_12();
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 26)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 26));
		RuntimeObject * L_14 = ((  RuntimeObject * (*) (ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27)->methodPointer)((ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 *)L_1, (RuntimeObject *)L_3, (RuntimeObject *)L_5, (RuntimeObject *)L_7, (RuntimeObject *)L_9, (RuntimeObject *)L_11, (RuntimeObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27));
		return L_14;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`7_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnNext_m1332D701B90CD076020AD58ADD2E42C56F47594D_gshared (ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 28), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`7_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnError_mC632C9AAA133999D2FBA7484EFBE7B61339FD984_gshared (ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 28), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`7_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnCompleted_mDD95471A752FB249C0401467FB09F6404EDF202C_gshared (ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 28), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`8_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32,UniRx.Operators.ZipLatestObservable`8<T1,T2,T3,T4,T5,T6,T7,TR>,System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest__ctor_mCED0BC1B2DFC1BA825C9D5FCB23BC7200DD385E0_gshared (ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821 * __this, int32_t ___length0, ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * ___parent1, RuntimeObject* ___observer2, RuntimeObject* ___cancel3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatest__ctor_mCED0BC1B2DFC1BA825C9D5FCB23BC7200DD385E0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// readonly object gate = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_gate_6(L_0);
		// : base(length, observer, cancel)
		int32_t L_1 = ___length0;
		RuntimeObject* L_2 = ___observer2;
		RuntimeObject* L_3 = ___cancel3;
		((  void (*) (NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D *, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((NthZipLatestObserverBase_1_t2976FC9EF30CE751E0C203A114D8695E217B3F3D *)__this, (int32_t)L_1, (RuntimeObject*)L_2, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.parent = parent;
		ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * L_4 = ___parent1;
		__this->set_parent_5(L_4);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`8_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatest_Run_m56F418E34A08A1994A5DCB220A456158252AF7BB_gshared (ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatest_Run_m56F418E34A08A1994A5DCB220A456158252AF7BB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	RuntimeObject* V_5 = NULL;
	RuntimeObject* V_6 = NULL;
	{
		// c1 = new ZipLatestObserver<T1>(gate, this, 0);
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_1 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_1, (RuntimeObject *)L_0, (RuntimeObject*)__this, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_c1_7(L_1);
		// c2 = new ZipLatestObserver<T2>(gate, this, 1);
		RuntimeObject * L_2 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_3 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)(L_3, (RuntimeObject *)L_2, (RuntimeObject*)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		__this->set_c2_8(L_3);
		// c3 = new ZipLatestObserver<T3>(gate, this, 2);
		RuntimeObject * L_4 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_5 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_5, (RuntimeObject *)L_4, (RuntimeObject*)__this, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		__this->set_c3_9(L_5);
		// c4 = new ZipLatestObserver<T4>(gate, this, 3);
		RuntimeObject * L_6 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_7 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_7, (RuntimeObject *)L_6, (RuntimeObject*)__this, (int32_t)3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		__this->set_c4_10(L_7);
		// c5 = new ZipLatestObserver<T5>(gate, this, 4);
		RuntimeObject * L_8 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_9 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 11));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)->methodPointer)(L_9, (RuntimeObject *)L_8, (RuntimeObject*)__this, (int32_t)4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12));
		__this->set_c5_11(L_9);
		// c6 = new ZipLatestObserver<T6>(gate, this, 5);
		RuntimeObject * L_10 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_11 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 13));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14)->methodPointer)(L_11, (RuntimeObject *)L_10, (RuntimeObject*)__this, (int32_t)5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14));
		__this->set_c6_12(L_11);
		// c7 = new ZipLatestObserver<T7>(gate, this, 6);
		RuntimeObject * L_12 = (RuntimeObject *)__this->get_gate_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_13 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 15));
		((  void (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, RuntimeObject *, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16)->methodPointer)(L_13, (RuntimeObject *)L_12, (RuntimeObject*)__this, (int32_t)6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16));
		__this->set_c7_13(L_13);
		// var s1 = parent.source1.Subscribe(c1);
		ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * L_14 = (ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)__this->get_parent_5();
		RuntimeObject* L_15 = (RuntimeObject*)L_14->get_source1_1();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_16 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c1_7();
		RuntimeObject* L_17 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 17), (RuntimeObject*)L_15, (RuntimeObject*)L_16);
		V_0 = (RuntimeObject*)L_17;
		// var s2 = parent.source2.Subscribe(c2);
		ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * L_18 = (ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)__this->get_parent_5();
		RuntimeObject* L_19 = (RuntimeObject*)L_18->get_source2_2();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_20 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c2_8();
		RuntimeObject* L_21 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 18), (RuntimeObject*)L_19, (RuntimeObject*)L_20);
		V_1 = (RuntimeObject*)L_21;
		// var s3 = parent.source3.Subscribe(c3);
		ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * L_22 = (ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)__this->get_parent_5();
		RuntimeObject* L_23 = (RuntimeObject*)L_22->get_source3_3();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_24 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c3_9();
		RuntimeObject* L_25 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 19), (RuntimeObject*)L_23, (RuntimeObject*)L_24);
		V_2 = (RuntimeObject*)L_25;
		// var s4 = parent.source4.Subscribe(c4);
		ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * L_26 = (ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)__this->get_parent_5();
		RuntimeObject* L_27 = (RuntimeObject*)L_26->get_source4_4();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_28 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c4_10();
		RuntimeObject* L_29 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 20), (RuntimeObject*)L_27, (RuntimeObject*)L_28);
		V_3 = (RuntimeObject*)L_29;
		// var s5 = parent.source5.Subscribe(c5);
		ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * L_30 = (ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)__this->get_parent_5();
		RuntimeObject* L_31 = (RuntimeObject*)L_30->get_source5_5();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_32 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c5_11();
		RuntimeObject* L_33 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 21), (RuntimeObject*)L_31, (RuntimeObject*)L_32);
		V_4 = (RuntimeObject*)L_33;
		// var s6 = parent.source6.Subscribe(c6);
		ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * L_34 = (ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)__this->get_parent_5();
		RuntimeObject* L_35 = (RuntimeObject*)L_34->get_source6_6();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_36 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c6_12();
		RuntimeObject* L_37 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 22), (RuntimeObject*)L_35, (RuntimeObject*)L_36);
		V_5 = (RuntimeObject*)L_37;
		// var s7 = parent.source7.Subscribe(c7);
		ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * L_38 = (ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)__this->get_parent_5();
		RuntimeObject* L_39 = (RuntimeObject*)L_38->get_source7_7();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_40 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c7_13();
		RuntimeObject* L_41 = InterfaceFuncInvoker1< RuntimeObject*, RuntimeObject* >::Invoke(0 /* System.IDisposable System.IObservable`1<System.Object>::Subscribe(System.IObserver`1<!0>) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 23), (RuntimeObject*)L_39, (RuntimeObject*)L_40);
		V_6 = (RuntimeObject*)L_41;
		// return StableCompositeDisposable.Create(s1, s2, s3, s4, s5, s6, s7);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_42 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)SZArrayNew(IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65_il2cpp_TypeInfo_var, (uint32_t)7);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_43 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_42;
		RuntimeObject* L_44 = V_0;
		ArrayElementTypeCheck (L_43, L_44);
		(L_43)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_44);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_45 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_43;
		RuntimeObject* L_46 = V_1;
		ArrayElementTypeCheck (L_45, L_46);
		(L_45)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_46);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_47 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_45;
		RuntimeObject* L_48 = V_2;
		ArrayElementTypeCheck (L_47, L_48);
		(L_47)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_48);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_49 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_47;
		RuntimeObject* L_50 = V_3;
		ArrayElementTypeCheck (L_49, L_50);
		(L_49)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_50);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_51 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_49;
		RuntimeObject* L_52 = V_4;
		ArrayElementTypeCheck (L_51, L_52);
		(L_51)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (RuntimeObject*)L_52);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_53 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_51;
		RuntimeObject* L_54 = V_5;
		ArrayElementTypeCheck (L_53, L_54);
		(L_53)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (RuntimeObject*)L_54);
		IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65* L_55 = (IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_53;
		RuntimeObject* L_56 = V_6;
		ArrayElementTypeCheck (L_55, L_56);
		(L_55)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (RuntimeObject*)L_56);
		RuntimeObject* L_57 = StableCompositeDisposable_Create_mA26CD55619BE45D7F630D010C9BB47D84E83BCAA((IDisposableU5BU5D_tA118A91699805C6191EC7EC5368D525034AB6E65*)L_55, /*hidden argument*/NULL);
		return L_57;
	}
}
// TR UniRx.Operators.ZipLatestObservable`8_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatest_GetResult_m6764BFD47F1C9A5C9DA5A84D8AF9943834FD77C9_gshared (ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821 * __this, const RuntimeMethod* method)
{
	{
		// return parent.resultSelector(c1.Value, c2.Value, c3.Value, c4.Value, c5.Value, c6.Value, c7.Value);
		ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * L_0 = (ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)__this->get_parent_5();
		ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA * L_1 = (ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA *)L_0->get_resultSelector_8();
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_2 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c1_7();
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_4 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c2_8();
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_6 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c3_9();
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 26)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 26));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_8 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c4_10();
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_10 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c5_11();
		RuntimeObject * L_11 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 28)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 28));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_12 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c6_12();
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29));
		ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * L_14 = (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)__this->get_c7_13();
		RuntimeObject * L_15 = ((  RuntimeObject * (*) (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 30)->methodPointer)((ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 30));
		RuntimeObject * L_16 = ((  RuntimeObject * (*) (ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31)->methodPointer)((ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA *)L_1, (RuntimeObject *)L_3, (RuntimeObject *)L_5, (RuntimeObject *)L_7, (RuntimeObject *)L_9, (RuntimeObject *)L_11, (RuntimeObject *)L_13, (RuntimeObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31));
		return L_16;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`8_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnNext_m836BF0DBF56BFD58EAE62B416AAFBD72EAD1D21B_gshared (ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		// base.observer.OnNext(value);
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		RuntimeObject * L_1 = ___value0;
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(0 /* System.Void System.IObserver`1<System.Object>::OnNext(!0) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 32), (RuntimeObject*)L_0, (RuntimeObject *)L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`8_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnError_m9545CFF86FECFDB856788C06664FA1BE88F076C6_gshared (ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnError(error); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		Exception_t * L_1 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void System.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 32), (RuntimeObject*)L_0, (Exception_t *)L_1);
		// try { observer.OnError(error); }
		IL2CPP_LEAVE(0x17, FINALLY_0010);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0010;
	}

FINALLY_0010:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 33)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 33));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(16)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(16)
	{
		IL2CPP_JUMP_TBL(0x17, IL_0017)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`8_ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatest_OnCompleted_mEF295E2187992AA8EE7D27E09EF4A7677C9F6FBC_gshared (ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// try { observer.OnCompleted(); }
		RuntimeObject* L_0 = (RuntimeObject*)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this)->get_observer_0();
		il2cpp_codegen_memory_barrier();
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 32), (RuntimeObject*)L_0);
		// try { observer.OnCompleted(); }
		IL2CPP_LEAVE(0x16, FINALLY_000f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000f;
	}

FINALLY_000f:
	{ // begin finally (depth: 1)
		// finally { Dispose(); }
		((  void (*) (OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 33)->methodPointer)((OperatorObserverBase_2_tB93B26BFFDF6C6075F82E69B0A061C77D77A67C0 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 33));
		// finally { Dispose(); }
		IL2CPP_END_FINALLY(15)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(15)
	{
		IL2CPP_JUMP_TBL(0x16, IL_0016)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0016:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestFunc_4__ctor_mD6ECEFEA44A36F2DA64A97D52F37D25828D39577_gshared (ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.ZipLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatestFunc_4_Invoke_mE08B6FEBCEC03323EB13CE5A4E14D62E82684785_gshared (ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 3)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, targetMethod);
			}
		}
		else if (___parameterCount != 3)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32);
					else
						result = GenericVirtFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21, ___arg32);
					else
						result = VirtFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21, ___arg32);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg21) - 1), ___arg32, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, targetMethod);
				}
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32);
					else
						result = GenericVirtFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21, ___arg32);
					else
						result = VirtFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21, ___arg32);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg10) - 1), ___arg21, ___arg32, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult UniRx.Operators.ZipLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatestFunc_4_BeginInvoke_mFB35F2494FA87E2740491A70B8CE468A9B8BF71F_gshared (ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// TR UniRx.Operators.ZipLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatestFunc_4_EndInvoke_m955CEA6D97300DBF8B1C1CFAC6FB374DF7B648C5_gshared (ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestFunc_5__ctor_mDD830D5D09377FB0604497BC3B31ECACB75A791E_gshared (ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.ZipLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatestFunc_5_Invoke_m1E19AA08654BB44FAC01F801186DC114B78DB0F4_gshared (ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 4)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, targetMethod);
			}
		}
		else if (___parameterCount != 4)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43);
					else
						result = GenericVirtFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43);
					else
						result = VirtFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg21) - 1), ___arg32, ___arg43, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, targetMethod);
				}
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43);
					else
						result = GenericVirtFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43);
					else
						result = VirtFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg10) - 1), ___arg21, ___arg32, ___arg43, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult UniRx.Operators.ZipLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatestFunc_5_BeginInvoke_m8CD706CAB2FCAB53A0FFF4B593FFE7787713882D_gshared (ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback4, RuntimeObject * ___object5, const RuntimeMethod* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback4, (RuntimeObject*)___object5);
}
// TR UniRx.Operators.ZipLatestFunc`5<System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatestFunc_5_EndInvoke_mCE992CE8D8EB0940CB4253A5CAACD914635A9E66_gshared (ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestFunc_6__ctor_m9F82723151EC66F667A41D88078F5FB2D632775F_gshared (ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.ZipLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatestFunc_6_Invoke_m67A99D631C9F6B300886FB475179FAF620A7DD9A_gshared (ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, RuntimeObject * ___arg54, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 5)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, targetMethod);
			}
		}
		else if (___parameterCount != 5)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
					else
						result = GenericVirtFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
					else
						result = VirtFuncInvoker4< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg21) - 1), ___arg32, ___arg43, ___arg54, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, targetMethod);
				}
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
					else
						result = GenericVirtFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
					else
						result = VirtFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg10) - 1), ___arg21, ___arg32, ___arg43, ___arg54, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult UniRx.Operators.ZipLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatestFunc_6_BeginInvoke_m6A1E35F8B66D3944B0D7044995DCD54126CB6D05_gshared (ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, RuntimeObject * ___arg54, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback5, RuntimeObject * ___object6, const RuntimeMethod* method)
{
	void *__d_args[6] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback5, (RuntimeObject*)___object6);
}
// TR UniRx.Operators.ZipLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatestFunc_6_EndInvoke_mC911AC5379185AA9C041390DBD7F21F096230599_gshared (ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestFunc_7__ctor_mB68ED4148ECCA6CDA8DF550F2E8132F43E330472_gshared (ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.ZipLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatestFunc_7_Invoke_m630B0B8B3B386AB4983F88F522ED5BA73B9FA3FA_gshared (ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, RuntimeObject * ___arg54, RuntimeObject * ___arg65, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 6)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
			}
		}
		else if (___parameterCount != 6)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
					else
						result = GenericVirtFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
					else
						result = VirtFuncInvoker5< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg21) - 1), ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
				}
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
					else
						result = GenericVirtFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
					else
						result = VirtFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg10) - 1), ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult UniRx.Operators.ZipLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatestFunc_7_BeginInvoke_m387490A97DDDD37A8A1CD4F2003AC76DBF4AA749_gshared (ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, RuntimeObject * ___arg54, RuntimeObject * ___arg65, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback6, RuntimeObject * ___object7, const RuntimeMethod* method)
{
	void *__d_args[7] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	__d_args[5] = ___arg65;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback6, (RuntimeObject*)___object7);
}
// TR UniRx.Operators.ZipLatestFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatestFunc_7_EndInvoke_m61B12C90E04C4EEA202CAB88440DB4AE7A282523_gshared (ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestFunc_8__ctor_mDFE4EE3BB8F75CACC0E24F6F009C928E9BC14F45_gshared (ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TR UniRx.Operators.ZipLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6,T7)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatestFunc_8_Invoke_mEAFBBE815E16DE891087497E7C40697A64ADB65E_gshared (ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, RuntimeObject * ___arg54, RuntimeObject * ___arg65, RuntimeObject * ___arg76, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 7)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
			}
		}
		else if (___parameterCount != 7)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
					else
						result = GenericVirtFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
					else
						result = VirtFuncInvoker6< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg21) - 1), ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
				}
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker7< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
					else
						result = GenericVirtFuncInvoker7< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker7< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
					else
						result = VirtFuncInvoker7< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___arg10) - 1), ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
				}
				else
				{
					typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult UniRx.Operators.ZipLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,T7,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatestFunc_8_BeginInvoke_m6CE0DB893F37BBF39DB061D8211B1FE55BFCE523_gshared (ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, RuntimeObject * ___arg43, RuntimeObject * ___arg54, RuntimeObject * ___arg65, RuntimeObject * ___arg76, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback7, RuntimeObject * ___object8, const RuntimeMethod* method)
{
	void *__d_args[8] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	__d_args[4] = ___arg54;
	__d_args[5] = ___arg65;
	__d_args[6] = ___arg76;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback7, (RuntimeObject*)___object8);
}
// TR UniRx.Operators.ZipLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatestFunc_8_EndInvoke_m83E6FB45BEFBB6472D89649750C7F37A14CCA61C_gshared (ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`1<System.Object>::.ctor(System.IObservable`1<T>[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObservable_1__ctor_mB091D7A2BCD74401E78F30B211CE18963792295C_gshared (ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 * __this, IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* ___sources0, const RuntimeMethod* method)
{
	{
		// : base(true)
		((  void (*) (OperatorObservableBase_1_tB16C98CA8ACB1947DC9798221FBEED187248DFB0 *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((OperatorObservableBase_1_tB16C98CA8ACB1947DC9798221FBEED187248DFB0 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.sources = sources;
		IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* L_0 = ___sources0;
		__this->set_sources_1(L_0);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`1<System.Object>::SubscribeCore(System.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatestObservable_1_SubscribeCore_m837D25972EFE41C541C4A2ABBB55377590604687_gshared (ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new ZipLatest(this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * L_2 = (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *, ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, (ZipLatestObservable_1_t77A3A4D5BF1D37F40302B8E71C277D99A6B1FB65 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`3<System.Object,System.Object,System.Object>::.ctor(System.IObservable`1<TLeft>,System.IObservable`1<TRight>,System.Func`3<TLeft,TRight,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObservable_3__ctor_mBA0B65CF8A77A108CBAF0BB509B067707CDF93D9_gshared (ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 * __this, RuntimeObject* ___left0, RuntimeObject* ___right1, Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * ___selector2, const RuntimeMethod* method)
{
	ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 * G_B2_0 = NULL;
	ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 * G_B3_1 = NULL;
	{
		// : base(left.IsRequiredSubscribeOnCurrentThread() || right.IsRequiredSubscribeOnCurrentThread())
		RuntimeObject* L_0 = ___left0;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		G_B1_0 = ((ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 *)(__this));
			goto IL_0011;
		}
	}
	{
		RuntimeObject* L_2 = ___right1;
		bool L_3 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 *)(G_B1_0));
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 1;
		G_B3_1 = ((ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 *)(G_B2_0));
	}

IL_0012:
	{
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		// this.left = left;
		RuntimeObject* L_4 = ___left0;
		__this->set_left_1(L_4);
		// this.right = right;
		RuntimeObject* L_5 = ___right1;
		__this->set_right_2(L_5);
		// this.selector = selector;
		Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * L_6 = ___selector2;
		__this->set_selector_3(L_6);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`3<System.Object,System.Object,System.Object>::SubscribeCore(System.IObserver`1<TResult>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatestObservable_3_SubscribeCore_mCF5770ED952D29FB4E6A76D7F07BF5BDE30926AB_gshared (ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new ZipLatest(this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 * L_2 = (ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4));
		((  void (*) (ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 *, ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)(L_2, (ZipLatestObservable_3_tC856D30C75DF83D2D9AE455AC5B19F42C72CB493 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((ZipLatest_t06450034633EBC97DA2254E3C47FC5C6C2D76F95 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.IObservable`1<T1>,System.IObservable`1<T2>,System.IObservable`1<T3>,UniRx.Operators.ZipLatestFunc`4<T1,T2,T3,TR>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObservable_4__ctor_mE4B7AE0EB6D8AF5F8592BCA2A4ED7561C512023E_gshared (ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * __this, RuntimeObject* ___source10, RuntimeObject* ___source21, RuntimeObject* ___source32, ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 * ___resultSelector3, const RuntimeMethod* method)
{
	ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * G_B3_0 = NULL;
	ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * G_B1_0 = NULL;
	ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * G_B4_1 = NULL;
	{
		// : base(
		//     source1.IsRequiredSubscribeOnCurrentThread() ||
		//     source2.IsRequiredSubscribeOnCurrentThread() ||
		//     source3.IsRequiredSubscribeOnCurrentThread() ||
		//     false)
		RuntimeObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		G_B1_0 = ((ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 *)(__this));
		if (L_1)
		{
			G_B3_0 = ((ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 *)(__this));
			goto IL_0019;
		}
	}
	{
		RuntimeObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		G_B2_0 = ((ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 *)(G_B1_0));
		if (L_3)
		{
			G_B3_0 = ((ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 *)(G_B1_0));
			goto IL_0019;
		}
	}
	{
		RuntimeObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		G_B4_0 = ((int32_t)(L_5));
		G_B4_1 = ((ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 *)(G_B2_0));
		goto IL_001a;
	}

IL_0019:
	{
		G_B4_0 = 1;
		G_B4_1 = ((ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 *)(G_B3_0));
	}

IL_001a:
	{
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)G_B4_1, (bool)G_B4_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		// this.source1 = source1;
		RuntimeObject* L_6 = ___source10;
		__this->set_source1_1(L_6);
		// this.source2 = source2;
		RuntimeObject* L_7 = ___source21;
		__this->set_source2_2(L_7);
		// this.source3 = source3;
		RuntimeObject* L_8 = ___source32;
		__this->set_source3_3(L_8);
		// this.resultSelector = resultSelector;
		ZipLatestFunc_4_tC144B415D312C406B735513B41EF25D11EEA4652 * L_9 = ___resultSelector3;
		__this->set_resultSelector_4(L_9);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`4<System.Object,System.Object,System.Object,System.Object>::SubscribeCore(System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatestObservable_4_SubscribeCore_m5C297E37F13D16BBF36EB22F5D283B88B4AE7304_gshared (ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new ZipLatest(3, this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D * L_2 = (ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5));
		((  void (*) (ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D *, int32_t, ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)(L_2, (int32_t)3, (ZipLatestObservable_4_t98ABBA62DCEED28AB507C59489499724657894B0 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((ZipLatest_t3DEB1AF84F592F3C49A6654B8941E642D089748D *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.IObservable`1<T1>,System.IObservable`1<T2>,System.IObservable`1<T3>,System.IObservable`1<T4>,UniRx.Operators.ZipLatestFunc`5<T1,T2,T3,T4,TR>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObservable_5__ctor_m809B5845629EE902FED8517B016E1F82E76E657A_gshared (ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * __this, RuntimeObject* ___source10, RuntimeObject* ___source21, RuntimeObject* ___source32, RuntimeObject* ___source43, ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 * ___resultSelector4, const RuntimeMethod* method)
{
	ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * G_B4_0 = NULL;
	ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * G_B1_0 = NULL;
	ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * G_B2_0 = NULL;
	ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * G_B5_1 = NULL;
	{
		// : base(
		//     source1.IsRequiredSubscribeOnCurrentThread() ||
		//     source2.IsRequiredSubscribeOnCurrentThread() ||
		//     source3.IsRequiredSubscribeOnCurrentThread() ||
		//     source4.IsRequiredSubscribeOnCurrentThread() ||
		//     false)
		RuntimeObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		G_B1_0 = ((ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)(__this));
		if (L_1)
		{
			G_B4_0 = ((ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)(__this));
			goto IL_0022;
		}
	}
	{
		RuntimeObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		G_B2_0 = ((ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)(G_B1_0));
		if (L_3)
		{
			G_B4_0 = ((ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)(G_B1_0));
			goto IL_0022;
		}
	}
	{
		RuntimeObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		G_B3_0 = ((ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)(G_B2_0));
		if (L_5)
		{
			G_B4_0 = ((ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)(G_B2_0));
			goto IL_0022;
		}
	}
	{
		RuntimeObject* L_6 = ___source43;
		bool L_7 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((RuntimeObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		G_B5_0 = ((int32_t)(L_7));
		G_B5_1 = ((ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)(G_B3_0));
		goto IL_0023;
	}

IL_0022:
	{
		G_B5_0 = 1;
		G_B5_1 = ((ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)(G_B4_0));
	}

IL_0023:
	{
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)G_B5_1, (bool)G_B5_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// this.source1 = source1;
		RuntimeObject* L_8 = ___source10;
		__this->set_source1_1(L_8);
		// this.source2 = source2;
		RuntimeObject* L_9 = ___source21;
		__this->set_source2_2(L_9);
		// this.source3 = source3;
		RuntimeObject* L_10 = ___source32;
		__this->set_source3_3(L_10);
		// this.source4 = source4;
		RuntimeObject* L_11 = ___source43;
		__this->set_source4_4(L_11);
		// this.resultSelector = resultSelector;
		ZipLatestFunc_5_t8CB24E5B43B4C1CC44453A08ECFF76CF98C95C90 * L_12 = ___resultSelector4;
		__this->set_resultSelector_5(L_12);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatestObservable_5_SubscribeCore_m4BCA9D795873F87F210250DF54E2196DE7CBF9C3_gshared (ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new ZipLatest(4, this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61 * L_2 = (ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6));
		((  void (*) (ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61 *, int32_t, ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)(L_2, (int32_t)4, (ZipLatestObservable_5_tA377A73DC51F9D3F11AC153C320291FD079D7C7D *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((ZipLatest_t553DC9783A4F6DD38E71DA28E72FCE44B1E6CE61 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.IObservable`1<T1>,System.IObservable`1<T2>,System.IObservable`1<T3>,System.IObservable`1<T4>,System.IObservable`1<T5>,UniRx.Operators.ZipLatestFunc`6<T1,T2,T3,T4,T5,TR>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObservable_6__ctor_m31079A5BA4EAB1CAAA568E65E192CC743A451A75_gshared (ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * __this, RuntimeObject* ___source10, RuntimeObject* ___source21, RuntimeObject* ___source32, RuntimeObject* ___source43, RuntimeObject* ___source54, ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F * ___resultSelector5, const RuntimeMethod* method)
{
	ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * G_B5_0 = NULL;
	ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * G_B1_0 = NULL;
	ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * G_B2_0 = NULL;
	ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * G_B3_0 = NULL;
	ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * G_B6_1 = NULL;
	{
		// : base(
		//     source1.IsRequiredSubscribeOnCurrentThread() ||
		//     source2.IsRequiredSubscribeOnCurrentThread() ||
		//     source3.IsRequiredSubscribeOnCurrentThread() ||
		//     source4.IsRequiredSubscribeOnCurrentThread() ||
		//     source5.IsRequiredSubscribeOnCurrentThread() ||
		//     false)
		RuntimeObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		G_B1_0 = ((ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)(__this));
		if (L_1)
		{
			G_B5_0 = ((ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)(__this));
			goto IL_002b;
		}
	}
	{
		RuntimeObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		G_B2_0 = ((ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)(G_B1_0));
		if (L_3)
		{
			G_B5_0 = ((ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)(G_B1_0));
			goto IL_002b;
		}
	}
	{
		RuntimeObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		G_B3_0 = ((ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)(G_B2_0));
		if (L_5)
		{
			G_B5_0 = ((ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)(G_B2_0));
			goto IL_002b;
		}
	}
	{
		RuntimeObject* L_6 = ___source43;
		bool L_7 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((RuntimeObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		G_B4_0 = ((ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)(G_B3_0));
		if (L_7)
		{
			G_B5_0 = ((ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)(G_B3_0));
			goto IL_002b;
		}
	}
	{
		RuntimeObject* L_8 = ___source54;
		bool L_9 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		G_B6_0 = ((int32_t)(L_9));
		G_B6_1 = ((ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)(G_B4_0));
		goto IL_002c;
	}

IL_002b:
	{
		G_B6_0 = 1;
		G_B6_1 = ((ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)(G_B5_0));
	}

IL_002c:
	{
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)G_B6_1, (bool)G_B6_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		// this.source1 = source1;
		RuntimeObject* L_10 = ___source10;
		__this->set_source1_1(L_10);
		// this.source2 = source2;
		RuntimeObject* L_11 = ___source21;
		__this->set_source2_2(L_11);
		// this.source3 = source3;
		RuntimeObject* L_12 = ___source32;
		__this->set_source3_3(L_12);
		// this.source4 = source4;
		RuntimeObject* L_13 = ___source43;
		__this->set_source4_4(L_13);
		// this.source5 = source5;
		RuntimeObject* L_14 = ___source54;
		__this->set_source5_5(L_14);
		// this.resultSelector = resultSelector;
		ZipLatestFunc_6_tE751D6A3F172D070978A746E5B49C8DE35B2329F * L_15 = ___resultSelector5;
		__this->set_resultSelector_6(L_15);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatestObservable_6_SubscribeCore_m365E70A741B30117DADF8ABA985C7DADE3F7B27B_gshared (ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new ZipLatest(5, this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565 * L_2 = (ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565 *, int32_t, ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_2, (int32_t)5, (ZipLatestObservable_6_tAD1A02D818E20E691F7D2D61B9CE4A320771E645 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((ZipLatest_t4A460D47B0F39AFCA0CDB0EBF82A850A4F91F565 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.IObservable`1<T1>,System.IObservable`1<T2>,System.IObservable`1<T3>,System.IObservable`1<T4>,System.IObservable`1<T5>,System.IObservable`1<T6>,UniRx.Operators.ZipLatestFunc`7<T1,T2,T3,T4,T5,T6,TR>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObservable_7__ctor_m42911EDEE63CA6685844F8FD07B9D0E71444EC92_gshared (ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * __this, RuntimeObject* ___source10, RuntimeObject* ___source21, RuntimeObject* ___source32, RuntimeObject* ___source43, RuntimeObject* ___source54, RuntimeObject* ___source65, ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 * ___resultSelector6, const RuntimeMethod* method)
{
	ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * G_B6_0 = NULL;
	ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * G_B1_0 = NULL;
	ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * G_B2_0 = NULL;
	ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * G_B3_0 = NULL;
	ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * G_B4_0 = NULL;
	ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * G_B7_1 = NULL;
	{
		// : base(
		//     source1.IsRequiredSubscribeOnCurrentThread() ||
		//     source2.IsRequiredSubscribeOnCurrentThread() ||
		//     source3.IsRequiredSubscribeOnCurrentThread() ||
		//     source4.IsRequiredSubscribeOnCurrentThread() ||
		//     source5.IsRequiredSubscribeOnCurrentThread() ||
		//     source6.IsRequiredSubscribeOnCurrentThread() ||
		//     false)
		RuntimeObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		G_B1_0 = ((ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)(__this));
		if (L_1)
		{
			G_B6_0 = ((ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)(__this));
			goto IL_0034;
		}
	}
	{
		RuntimeObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		G_B2_0 = ((ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)(G_B1_0));
		if (L_3)
		{
			G_B6_0 = ((ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)(G_B1_0));
			goto IL_0034;
		}
	}
	{
		RuntimeObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		G_B3_0 = ((ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)(G_B2_0));
		if (L_5)
		{
			G_B6_0 = ((ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)(G_B2_0));
			goto IL_0034;
		}
	}
	{
		RuntimeObject* L_6 = ___source43;
		bool L_7 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((RuntimeObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		G_B4_0 = ((ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)(G_B3_0));
		if (L_7)
		{
			G_B6_0 = ((ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)(G_B3_0));
			goto IL_0034;
		}
	}
	{
		RuntimeObject* L_8 = ___source54;
		bool L_9 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		G_B5_0 = ((ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)(G_B4_0));
		if (L_9)
		{
			G_B6_0 = ((ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)(G_B4_0));
			goto IL_0034;
		}
	}
	{
		RuntimeObject* L_10 = ___source65;
		bool L_11 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((RuntimeObject*)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		G_B7_0 = ((int32_t)(L_11));
		G_B7_1 = ((ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)(G_B5_0));
		goto IL_0035;
	}

IL_0034:
	{
		G_B7_0 = 1;
		G_B7_1 = ((ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)(G_B6_0));
	}

IL_0035:
	{
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)G_B7_1, (bool)G_B7_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		// this.source1 = source1;
		RuntimeObject* L_12 = ___source10;
		__this->set_source1_1(L_12);
		// this.source2 = source2;
		RuntimeObject* L_13 = ___source21;
		__this->set_source2_2(L_13);
		// this.source3 = source3;
		RuntimeObject* L_14 = ___source32;
		__this->set_source3_3(L_14);
		// this.source4 = source4;
		RuntimeObject* L_15 = ___source43;
		__this->set_source4_4(L_15);
		// this.source5 = source5;
		RuntimeObject* L_16 = ___source54;
		__this->set_source5_5(L_16);
		// this.source6 = source6;
		RuntimeObject* L_17 = ___source65;
		__this->set_source6_6(L_17);
		// this.resultSelector = resultSelector;
		ZipLatestFunc_7_t26DD573480A9A11CA833BAC7F7CDC9C25387C606 * L_18 = ___resultSelector6;
		__this->set_resultSelector_7(L_18);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatestObservable_7_SubscribeCore_m0B7FEC877161E541632E5D96A27E10127C9C48FD_gshared (ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new ZipLatest(6, this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37 * L_2 = (ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8));
		((  void (*) (ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37 *, int32_t, ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)(L_2, (int32_t)6, (ZipLatestObservable_7_tD0ED12A808A02226198628FC3969FDEA48DB8932 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((ZipLatest_t111921563CCB0CE5C57BD84F4E1C1824C1B17E37 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.IObservable`1<T1>,System.IObservable`1<T2>,System.IObservable`1<T3>,System.IObservable`1<T4>,System.IObservable`1<T5>,System.IObservable`1<T6>,System.IObservable`1<T7>,UniRx.Operators.ZipLatestFunc`8<T1,T2,T3,T4,T5,T6,T7,TR>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObservable_8__ctor_mDAFDBEE183079C4F04B2568FE79DE621A2AD6F83_gshared (ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * __this, RuntimeObject* ___source10, RuntimeObject* ___source21, RuntimeObject* ___source32, RuntimeObject* ___source43, RuntimeObject* ___source54, RuntimeObject* ___source65, RuntimeObject* ___source76, ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA * ___resultSelector7, const RuntimeMethod* method)
{
	ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * G_B7_0 = NULL;
	ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * G_B1_0 = NULL;
	ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * G_B2_0 = NULL;
	ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * G_B3_0 = NULL;
	ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * G_B4_0 = NULL;
	ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * G_B5_0 = NULL;
	ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * G_B6_0 = NULL;
	int32_t G_B8_0 = 0;
	ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * G_B8_1 = NULL;
	{
		// : base(
		//     source1.IsRequiredSubscribeOnCurrentThread() ||
		//     source2.IsRequiredSubscribeOnCurrentThread() ||
		//     source3.IsRequiredSubscribeOnCurrentThread() ||
		//     source4.IsRequiredSubscribeOnCurrentThread() ||
		//     source5.IsRequiredSubscribeOnCurrentThread() ||
		//     source6.IsRequiredSubscribeOnCurrentThread() ||
		//     source7.IsRequiredSubscribeOnCurrentThread() ||
		//     false)
		RuntimeObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		G_B1_0 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(__this));
		if (L_1)
		{
			G_B7_0 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(__this));
			goto IL_003d;
		}
	}
	{
		RuntimeObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		G_B2_0 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(G_B1_0));
		if (L_3)
		{
			G_B7_0 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(G_B1_0));
			goto IL_003d;
		}
	}
	{
		RuntimeObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		G_B3_0 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(G_B2_0));
		if (L_5)
		{
			G_B7_0 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(G_B2_0));
			goto IL_003d;
		}
	}
	{
		RuntimeObject* L_6 = ___source43;
		bool L_7 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((RuntimeObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		G_B4_0 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(G_B3_0));
		if (L_7)
		{
			G_B7_0 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(G_B3_0));
			goto IL_003d;
		}
	}
	{
		RuntimeObject* L_8 = ___source54;
		bool L_9 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		G_B5_0 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(G_B4_0));
		if (L_9)
		{
			G_B7_0 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(G_B4_0));
			goto IL_003d;
		}
	}
	{
		RuntimeObject* L_10 = ___source65;
		bool L_11 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((RuntimeObject*)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		G_B6_0 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(G_B5_0));
		if (L_11)
		{
			G_B7_0 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(G_B5_0));
			goto IL_003d;
		}
	}
	{
		RuntimeObject* L_12 = ___source76;
		bool L_13 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((RuntimeObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		G_B8_0 = ((int32_t)(L_13));
		G_B8_1 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(G_B6_0));
		goto IL_003e;
	}

IL_003d:
	{
		G_B8_0 = 1;
		G_B8_1 = ((ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)(G_B7_0));
	}

IL_003e:
	{
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)G_B8_1, (bool)G_B8_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		// this.source1 = source1;
		RuntimeObject* L_14 = ___source10;
		__this->set_source1_1(L_14);
		// this.source2 = source2;
		RuntimeObject* L_15 = ___source21;
		__this->set_source2_2(L_15);
		// this.source3 = source3;
		RuntimeObject* L_16 = ___source32;
		__this->set_source3_3(L_16);
		// this.source4 = source4;
		RuntimeObject* L_17 = ___source43;
		__this->set_source4_4(L_17);
		// this.source5 = source5;
		RuntimeObject* L_18 = ___source54;
		__this->set_source5_5(L_18);
		// this.source6 = source6;
		RuntimeObject* L_19 = ___source65;
		__this->set_source6_6(L_19);
		// this.source7 = source7;
		RuntimeObject* L_20 = ___source76;
		__this->set_source7_7(L_20);
		// this.resultSelector = resultSelector;
		ZipLatestFunc_8_t13537228185EA6EADD1988B0EA3197C9E7E3D0FA * L_21 = ___resultSelector7;
		__this->set_resultSelector_8(L_21);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipLatestObservable`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipLatestObservable_8_SubscribeCore_mB4F6AE126F214CA5B25861C1D724B7B805DB7AA2_gshared (ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new ZipLatest(7, this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821 * L_2 = (ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821 *, int32_t, ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_2, (int32_t)7, (ZipLatestObservable_8_tAED1BAAD7CB2BD494C44C09AC6133DD6352E20FA *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((ZipLatest_tC16EBF24E6795A25500F945A86CE14F7BB46C821 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipLatestObservable`1_ZipLatest_ZipLatestObserver<System.Object>::.ctor(UniRx.Operators.ZipLatestObservable`1_ZipLatest<T>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObserver__ctor_m3923D31D31FE8C57CAAF8B360B6CF498AD23588B_gshared (ZipLatestObserver_t1916CBEFD17AA1674F927430EF42C7EAEB835507 * __this, ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * ___parent0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		// public ZipLatestObserver(ZipLatest parent, int index)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405((RuntimeObject *)__this, /*hidden argument*/NULL);
		// this.parent = parent;
		ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		// this.index = index;
		int32_t L_1 = ___index1;
		__this->set_index_1(L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`1_ZipLatest_ZipLatestObserver<System.Object>::OnNext(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObserver_OnNext_m9032E98A3C43FCC442C0B284E20F30B7C76949E7_gshared (ZipLatestObserver_t1916CBEFD17AA1674F927430EF42C7EAEB835507 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (parent.gate)
		ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * L_0 = (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)__this->get_parent_0();
		RuntimeObject * L_1 = (RuntimeObject *)L_0->get_gate_3();
		V_0 = (RuntimeObject *)L_1;
		V_1 = (bool)0;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_2, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// parent.values[index] = value;
		ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * L_3 = (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)__this->get_parent_0();
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_4 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_3->get_values_5();
		int32_t L_5 = (int32_t)__this->get_index_1();
		RuntimeObject * L_6 = ___value0;
		(L_4)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5), (RuntimeObject *)L_6);
		// parent.Publish(index);
		ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * L_7 = (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)__this->get_parent_0();
		int32_t L_8 = (int32_t)__this->get_index_1();
		((  void (*) (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)L_7, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// }
		IL2CPP_LEAVE(0x4A, FINALLY_0040);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		{
			bool L_9 = V_1;
			if (!L_9)
			{
				goto IL_0049;
			}
		}

IL_0043:
		{
			RuntimeObject * L_10 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_10, /*hidden argument*/NULL);
		}

IL_0049:
		{
			IL2CPP_END_FINALLY(64)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_004a:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`1_ZipLatest_ZipLatestObserver<System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObserver_OnError_mC40D8D9EACDE9536FE25A5BC39305C45230205D4_gshared (ZipLatestObserver_t1916CBEFD17AA1674F927430EF42C7EAEB835507 * __this, Exception_t * ___ex0, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (parent.gate)
		ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * L_0 = (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)__this->get_parent_0();
		RuntimeObject * L_1 = (RuntimeObject *)L_0->get_gate_3();
		V_0 = (RuntimeObject *)L_1;
		V_1 = (bool)0;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_2, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// parent.OnError(ex);
		ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * L_3 = (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)__this->get_parent_0();
		Exception_t * L_4 = ___ex0;
		VirtActionInvoker1< Exception_t * >::Invoke(9 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Collections.Generic.IList`1<System.Object>,System.Collections.Generic.IList`1<System.Object>>::OnError(System.Exception) */, (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)L_3, (Exception_t *)L_4);
		// }
		IL2CPP_LEAVE(0x2E, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		{
			bool L_5 = V_1;
			if (!L_5)
			{
				goto IL_002d;
			}
		}

IL_0027:
		{
			RuntimeObject * L_6 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_6, /*hidden argument*/NULL);
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(36)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2E, IL_002e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_002e:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObservable`1_ZipLatest_ZipLatestObserver<System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObserver_OnCompleted_m8491BE57378CBB995DDD502B8C16E089DCBA717C_gshared (ZipLatestObserver_t1916CBEFD17AA1674F927430EF42C7EAEB835507 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	int32_t V_3 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (parent.gate)
		ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * L_0 = (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)__this->get_parent_0();
		RuntimeObject * L_1 = (RuntimeObject *)L_0->get_gate_3();
		V_0 = (RuntimeObject *)L_1;
		V_1 = (bool)0;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_2 = V_0;
			Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_2, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
			// parent.isCompleted[index] = true;
			ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * L_3 = (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)__this->get_parent_0();
			BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_4 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)L_3->get_isCompleted_7();
			int32_t L_5 = (int32_t)__this->get_index_1();
			(L_4)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5), (bool)1);
			// var allTrue = true;
			V_2 = (bool)1;
			// for (int i = 0; i < parent.length; i++)
			V_3 = (int32_t)0;
			goto IL_0046;
		}

IL_002f:
		{
			// if (!parent.isCompleted[i])
			ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * L_6 = (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)__this->get_parent_0();
			BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_7 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)L_6->get_isCompleted_7();
			int32_t L_8 = V_3;
			int32_t L_9 = L_8;
			uint8_t L_10 = (uint8_t)(L_7)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_9));
			if (L_10)
			{
				goto IL_0042;
			}
		}

IL_003e:
		{
			// allTrue = false;
			V_2 = (bool)0;
			// break;
			goto IL_0054;
		}

IL_0042:
		{
			// for (int i = 0; i < parent.length; i++)
			int32_t L_11 = V_3;
			V_3 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
		}

IL_0046:
		{
			// for (int i = 0; i < parent.length; i++)
			int32_t L_12 = V_3;
			ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * L_13 = (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)__this->get_parent_0();
			int32_t L_14 = (int32_t)L_13->get_length_4();
			if ((((int32_t)L_12) < ((int32_t)L_14)))
			{
				goto IL_002f;
			}
		}

IL_0054:
		{
			// if (allTrue)
			bool L_15 = V_2;
			if (!L_15)
			{
				goto IL_0062;
			}
		}

IL_0057:
		{
			// parent.OnCompleted();
			ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 * L_16 = (ZipLatest_tC9AD74780C01C91D1C079A8B1614C64BB80090C8 *)__this->get_parent_0();
			VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Collections.Generic.IList`1<System.Object>,System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)L_16);
		}

IL_0062:
		{
			// }
			IL2CPP_LEAVE(0x6E, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		{
			bool L_17 = V_1;
			if (!L_17)
			{
				goto IL_006d;
			}
		}

IL_0067:
		{
			RuntimeObject * L_18 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_18, /*hidden argument*/NULL);
		}

IL_006d:
		{
			IL2CPP_END_FINALLY(100)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_JUMP_TBL(0x6E, IL_006e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_006e:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// T UniRx.Operators.ZipLatestObserver`1<System.Object>::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ZipLatestObserver_1_get_Value_mD4D08438B458AEE81298D23D615D4D6763BAC1B3_gshared (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * __this, const RuntimeMethod* method)
{
	{
		// public T Value { get { return value; } }
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.Operators.ZipLatestObserver`1<System.Object>::.ctor(System.Object,UniRx.Operators.IZipLatestObservable,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObserver_1__ctor_mE08704AC5FDC58B16520BE2A22CE3EFD47A3038F_gshared (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * __this, RuntimeObject * ___gate0, RuntimeObject* ___parent1, int32_t ___index2, const RuntimeMethod* method)
{
	{
		// public ZipLatestObserver(object gate, IZipLatestObservable parent, int index)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405((RuntimeObject *)__this, /*hidden argument*/NULL);
		// this.gate = gate;
		RuntimeObject * L_0 = ___gate0;
		__this->set_gate_0(L_0);
		// this.parent = parent;
		RuntimeObject* L_1 = ___parent1;
		__this->set_parent_1(L_1);
		// this.index = index;
		int32_t L_2 = ___index2;
		__this->set_index_2(L_2);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObserver`1<System.Object>::OnNext(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObserver_1_OnNext_mA09C2EBC63D9C912D25DFC72BD868D15064C583A_gshared (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatestObserver_1_OnNext_mA09C2EBC63D9C912D25DFC72BD868D15064C583A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (gate)
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_0();
		V_0 = (RuntimeObject *)L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_1, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// this.value = value;
		RuntimeObject * L_2 = ___value0;
		__this->set_value_3(L_2);
		// parent.Publish(index);
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_parent_1();
		int32_t L_4 = (int32_t)__this->get_index_2();
		InterfaceActionInvoker1< int32_t >::Invoke(0 /* System.Void UniRx.Operators.IZipLatestObservable::Publish(System.Int32) */, IZipLatestObservable_tBC8E6DF6D33AD1BE66C2DD85334B17F8993603F7_il2cpp_TypeInfo_var, (RuntimeObject*)L_3, (int32_t)L_4);
		// }
		IL2CPP_LEAVE(0x35, FINALLY_002b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		{
			bool L_5 = V_1;
			if (!L_5)
			{
				goto IL_0034;
			}
		}

IL_002e:
		{
			RuntimeObject * L_6 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_6, /*hidden argument*/NULL);
		}

IL_0034:
		{
			IL2CPP_END_FINALLY(43)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0035:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObserver`1<System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObserver_1_OnError_mC4873B44B318D8F81C20FBD7D2856F6D01556415_gshared (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatestObserver_1_OnError_mC4873B44B318D8F81C20FBD7D2856F6D01556415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (gate)
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_0();
		V_0 = (RuntimeObject *)L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_1, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// parent.Fail(error);
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_parent_1();
		Exception_t * L_3 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void UniRx.Operators.IZipLatestObservable::Fail(System.Exception) */, IZipLatestObservable_tBC8E6DF6D33AD1BE66C2DD85334B17F8993603F7_il2cpp_TypeInfo_var, (RuntimeObject*)L_2, (Exception_t *)L_3);
		// }
		IL2CPP_LEAVE(0x29, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		{
			bool L_4 = V_1;
			if (!L_4)
			{
				goto IL_0028;
			}
		}

IL_0022:
		{
			RuntimeObject * L_5 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_5, /*hidden argument*/NULL);
		}

IL_0028:
		{
			IL2CPP_END_FINALLY(31)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x29, IL_0029)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0029:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipLatestObserver`1<System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipLatestObserver_1_OnCompleted_m03112B32B92F0EA008AABD5AA38B40CFC5D64CB0_gshared (ZipLatestObserver_1_tD21F0AF6C14B3D61C4EB1187F718ECE4919EA4C8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipLatestObserver_1_OnCompleted_m03112B32B92F0EA008AABD5AA38B40CFC5D64CB0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (gate)
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_0();
		V_0 = (RuntimeObject *)L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_1, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// parent.Done(index);
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_parent_1();
		int32_t L_3 = (int32_t)__this->get_index_2();
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.Operators.IZipLatestObservable::Done(System.Int32) */, IZipLatestObservable_tBC8E6DF6D33AD1BE66C2DD85334B17F8993603F7_il2cpp_TypeInfo_var, (RuntimeObject*)L_2, (int32_t)L_3);
		// }
		IL2CPP_LEAVE(0x2E, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		{
			bool L_4 = V_1;
			if (!L_4)
			{
				goto IL_002d;
			}
		}

IL_0027:
		{
			RuntimeObject * L_5 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_5, /*hidden argument*/NULL);
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(36)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2E, IL_002e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_002e:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`1<System.Object>::.ctor(System.IObservable`1<T>[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObservable_1__ctor_m9D6C75C40293BA45ABC0D90C787D4BCDFAD46AD8_gshared (ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B * __this, IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* ___sources0, const RuntimeMethod* method)
{
	{
		// : base(true)
		((  void (*) (OperatorObservableBase_1_tB16C98CA8ACB1947DC9798221FBEED187248DFB0 *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((OperatorObservableBase_1_tB16C98CA8ACB1947DC9798221FBEED187248DFB0 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// this.sources = sources;
		IObservable_1U5BU5D_t07E73D53D775BAD4A57C11557731BF9696E1329B* L_0 = ___sources0;
		__this->set_sources_1(L_0);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`1<System.Object>::SubscribeCore(System.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipObservable_1_SubscribeCore_mD1B3D40E7462A09ED4C1F15E44AF24137681D6F2_gshared (ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new Zip(this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * L_2 = (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *, ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_2, (ZipObservable_1_t8CC7A74801A18C6583DA46C75FFD98751124EA0B *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`3<System.Object,System.Object,System.Object>::.ctor(System.IObservable`1<TLeft>,System.IObservable`1<TRight>,System.Func`3<TLeft,TRight,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObservable_3__ctor_m61C1090CEC69CCAB97AD5D1BEA71780295C7D063_gshared (ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 * __this, RuntimeObject* ___left0, RuntimeObject* ___right1, Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * ___selector2, const RuntimeMethod* method)
{
	ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 * G_B2_0 = NULL;
	ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 * G_B3_1 = NULL;
	{
		// : base(left.IsRequiredSubscribeOnCurrentThread() || right.IsRequiredSubscribeOnCurrentThread())
		RuntimeObject* L_0 = ___left0;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		G_B1_0 = ((ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 *)(__this));
			goto IL_0011;
		}
	}
	{
		RuntimeObject* L_2 = ___right1;
		bool L_3 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		G_B3_0 = ((int32_t)(L_3));
		G_B3_1 = ((ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 *)(G_B1_0));
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 1;
		G_B3_1 = ((ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 *)(G_B2_0));
	}

IL_0012:
	{
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)G_B3_1, (bool)G_B3_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		// this.left = left;
		RuntimeObject* L_4 = ___left0;
		__this->set_left_1(L_4);
		// this.right = right;
		RuntimeObject* L_5 = ___right1;
		__this->set_right_2(L_5);
		// this.selector = selector;
		Func_3_tBBBFF266D23D5A9A7940D16DA73BCD5DE0753A27 * L_6 = ___selector2;
		__this->set_selector_3(L_6);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`3<System.Object,System.Object,System.Object>::SubscribeCore(System.IObserver`1<TResult>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipObservable_3_SubscribeCore_mAFD750D908F33DF87B2909FD1FBE76C578304D65_gshared (ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new Zip(this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 * L_2 = (Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4));
		((  void (*) (Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 *, ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)(L_2, (ZipObservable_3_t57798890D55C28E038157219D559DECF100EA838 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Zip_t5C91F64363B347FDB8BA7A83B4D6B0F2AF6B1F93 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.IObservable`1<T1>,System.IObservable`1<T2>,System.IObservable`1<T3>,UniRx.Operators.ZipFunc`4<T1,T2,T3,TR>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObservable_4__ctor_mC14F119281BB720EEE602C4C45F087EC23C52973_gshared (ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * __this, RuntimeObject* ___source10, RuntimeObject* ___source21, RuntimeObject* ___source32, ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 * ___resultSelector3, const RuntimeMethod* method)
{
	ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * G_B3_0 = NULL;
	ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * G_B1_0 = NULL;
	ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * G_B4_1 = NULL;
	{
		// : base(
		//     source1.IsRequiredSubscribeOnCurrentThread() ||
		//     source2.IsRequiredSubscribeOnCurrentThread() ||
		//     source3.IsRequiredSubscribeOnCurrentThread() ||
		//     false)
		RuntimeObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		G_B1_0 = ((ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 *)(__this));
		if (L_1)
		{
			G_B3_0 = ((ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 *)(__this));
			goto IL_0019;
		}
	}
	{
		RuntimeObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		G_B2_0 = ((ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 *)(G_B1_0));
		if (L_3)
		{
			G_B3_0 = ((ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 *)(G_B1_0));
			goto IL_0019;
		}
	}
	{
		RuntimeObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		G_B4_0 = ((int32_t)(L_5));
		G_B4_1 = ((ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 *)(G_B2_0));
		goto IL_001a;
	}

IL_0019:
	{
		G_B4_0 = 1;
		G_B4_1 = ((ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 *)(G_B3_0));
	}

IL_001a:
	{
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)G_B4_1, (bool)G_B4_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		// this.source1 = source1;
		RuntimeObject* L_6 = ___source10;
		__this->set_source1_1(L_6);
		// this.source2 = source2;
		RuntimeObject* L_7 = ___source21;
		__this->set_source2_2(L_7);
		// this.source3 = source3;
		RuntimeObject* L_8 = ___source32;
		__this->set_source3_3(L_8);
		// this.resultSelector = resultSelector;
		ZipFunc_4_t4B780A4E3E3227F03C8293177FBF72B8821892B5 * L_9 = ___resultSelector3;
		__this->set_resultSelector_4(L_9);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`4<System.Object,System.Object,System.Object,System.Object>::SubscribeCore(System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipObservable_4_SubscribeCore_m6AB89F8F2444D84C41D412DFD9AF7EB449447651_gshared (ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new Zip(this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1 * L_2 = (Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5));
		((  void (*) (Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1 *, ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)(L_2, (ZipObservable_4_tE93E0031101E455B39DC31B0CCE00B6B4D04CF39 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Zip_tED54EE240B89141B5CF984AF0CFECC124CB544B1 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.IObservable`1<T1>,System.IObservable`1<T2>,System.IObservable`1<T3>,System.IObservable`1<T4>,UniRx.Operators.ZipFunc`5<T1,T2,T3,T4,TR>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObservable_5__ctor_m9DA3A3F0C3402CB9CD2FA622A0542EDAD0A5F0A5_gshared (ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * __this, RuntimeObject* ___source10, RuntimeObject* ___source21, RuntimeObject* ___source32, RuntimeObject* ___source43, ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 * ___resultSelector4, const RuntimeMethod* method)
{
	ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * G_B4_0 = NULL;
	ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * G_B1_0 = NULL;
	ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * G_B2_0 = NULL;
	ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * G_B5_1 = NULL;
	{
		// : base(
		//     source1.IsRequiredSubscribeOnCurrentThread() ||
		//     source2.IsRequiredSubscribeOnCurrentThread() ||
		//     source3.IsRequiredSubscribeOnCurrentThread() ||
		//     source4.IsRequiredSubscribeOnCurrentThread() ||
		//     false)
		RuntimeObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		G_B1_0 = ((ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)(__this));
		if (L_1)
		{
			G_B4_0 = ((ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)(__this));
			goto IL_0022;
		}
	}
	{
		RuntimeObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		G_B2_0 = ((ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)(G_B1_0));
		if (L_3)
		{
			G_B4_0 = ((ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)(G_B1_0));
			goto IL_0022;
		}
	}
	{
		RuntimeObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		G_B3_0 = ((ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)(G_B2_0));
		if (L_5)
		{
			G_B4_0 = ((ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)(G_B2_0));
			goto IL_0022;
		}
	}
	{
		RuntimeObject* L_6 = ___source43;
		bool L_7 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((RuntimeObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		G_B5_0 = ((int32_t)(L_7));
		G_B5_1 = ((ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)(G_B3_0));
		goto IL_0023;
	}

IL_0022:
	{
		G_B5_0 = 1;
		G_B5_1 = ((ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)(G_B4_0));
	}

IL_0023:
	{
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)G_B5_1, (bool)G_B5_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		// this.source1 = source1;
		RuntimeObject* L_8 = ___source10;
		__this->set_source1_1(L_8);
		// this.source2 = source2;
		RuntimeObject* L_9 = ___source21;
		__this->set_source2_2(L_9);
		// this.source3 = source3;
		RuntimeObject* L_10 = ___source32;
		__this->set_source3_3(L_10);
		// this.source4 = source4;
		RuntimeObject* L_11 = ___source43;
		__this->set_source4_4(L_11);
		// this.resultSelector = resultSelector;
		ZipFunc_5_t6810769B8DC572662D8E37F3281AC7BBD515DD58 * L_12 = ___resultSelector4;
		__this->set_resultSelector_5(L_12);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`5<System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipObservable_5_SubscribeCore_mB96D89F23F0902CBEFE97D454C0ED421EA97DB27_gshared (ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new Zip(this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808 * L_2 = (Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6));
		((  void (*) (Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808 *, ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)(L_2, (ZipObservable_5_t2A40328BCEC065DF450DC77DCEC89E958D6B7E4E *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Zip_tF02E06D3B3119CFA336CB8161E20625BD1B9B808 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.IObservable`1<T1>,System.IObservable`1<T2>,System.IObservable`1<T3>,System.IObservable`1<T4>,System.IObservable`1<T5>,UniRx.Operators.ZipFunc`6<T1,T2,T3,T4,T5,TR>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObservable_6__ctor_m00842DBBFE00C9D15B14050F6718BDEEDE3BD993_gshared (ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * __this, RuntimeObject* ___source10, RuntimeObject* ___source21, RuntimeObject* ___source32, RuntimeObject* ___source43, RuntimeObject* ___source54, ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D * ___resultSelector5, const RuntimeMethod* method)
{
	ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * G_B5_0 = NULL;
	ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * G_B1_0 = NULL;
	ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * G_B2_0 = NULL;
	ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * G_B3_0 = NULL;
	ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * G_B6_1 = NULL;
	{
		// : base(
		//     source1.IsRequiredSubscribeOnCurrentThread() ||
		//     source2.IsRequiredSubscribeOnCurrentThread() ||
		//     source3.IsRequiredSubscribeOnCurrentThread() ||
		//     source4.IsRequiredSubscribeOnCurrentThread() ||
		//     source5.IsRequiredSubscribeOnCurrentThread() ||
		//     false)
		RuntimeObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		G_B1_0 = ((ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)(__this));
		if (L_1)
		{
			G_B5_0 = ((ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)(__this));
			goto IL_002b;
		}
	}
	{
		RuntimeObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		G_B2_0 = ((ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)(G_B1_0));
		if (L_3)
		{
			G_B5_0 = ((ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)(G_B1_0));
			goto IL_002b;
		}
	}
	{
		RuntimeObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		G_B3_0 = ((ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)(G_B2_0));
		if (L_5)
		{
			G_B5_0 = ((ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)(G_B2_0));
			goto IL_002b;
		}
	}
	{
		RuntimeObject* L_6 = ___source43;
		bool L_7 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((RuntimeObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		G_B4_0 = ((ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)(G_B3_0));
		if (L_7)
		{
			G_B5_0 = ((ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)(G_B3_0));
			goto IL_002b;
		}
	}
	{
		RuntimeObject* L_8 = ___source54;
		bool L_9 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		G_B6_0 = ((int32_t)(L_9));
		G_B6_1 = ((ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)(G_B4_0));
		goto IL_002c;
	}

IL_002b:
	{
		G_B6_0 = 1;
		G_B6_1 = ((ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)(G_B5_0));
	}

IL_002c:
	{
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)G_B6_1, (bool)G_B6_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		// this.source1 = source1;
		RuntimeObject* L_10 = ___source10;
		__this->set_source1_1(L_10);
		// this.source2 = source2;
		RuntimeObject* L_11 = ___source21;
		__this->set_source2_2(L_11);
		// this.source3 = source3;
		RuntimeObject* L_12 = ___source32;
		__this->set_source3_3(L_12);
		// this.source4 = source4;
		RuntimeObject* L_13 = ___source43;
		__this->set_source4_4(L_13);
		// this.source5 = source5;
		RuntimeObject* L_14 = ___source54;
		__this->set_source5_5(L_14);
		// this.resultSelector = resultSelector;
		ZipFunc_6_t7250C149CD5185E4BA38D3181CA2486168DB5B5D * L_15 = ___resultSelector5;
		__this->set_resultSelector_6(L_15);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipObservable_6_SubscribeCore_mDCDF80289543E5E7BBFA13A4E6448B82AE6485F4_gshared (ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new Zip(this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09 * L_2 = (Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 7));
		((  void (*) (Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09 *, ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)(L_2, (ZipObservable_6_tBF86697DD3953A3DD0A434236BB9C88C4E9D7B9F *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((Zip_t96405C8E05749FA122A94D520E5BE654EB0EBB09 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.IObservable`1<T1>,System.IObservable`1<T2>,System.IObservable`1<T3>,System.IObservable`1<T4>,System.IObservable`1<T5>,System.IObservable`1<T6>,UniRx.Operators.ZipFunc`7<T1,T2,T3,T4,T5,T6,TR>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObservable_7__ctor_mD3818E28DB404D74530F55F7909C787E594AF80F_gshared (ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * __this, RuntimeObject* ___source10, RuntimeObject* ___source21, RuntimeObject* ___source32, RuntimeObject* ___source43, RuntimeObject* ___source54, RuntimeObject* ___source65, ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 * ___resultSelector6, const RuntimeMethod* method)
{
	ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * G_B6_0 = NULL;
	ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * G_B1_0 = NULL;
	ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * G_B2_0 = NULL;
	ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * G_B3_0 = NULL;
	ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * G_B4_0 = NULL;
	ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * G_B7_1 = NULL;
	{
		// : base(
		//     source1.IsRequiredSubscribeOnCurrentThread() ||
		//     source2.IsRequiredSubscribeOnCurrentThread() ||
		//     source3.IsRequiredSubscribeOnCurrentThread() ||
		//     source4.IsRequiredSubscribeOnCurrentThread() ||
		//     source5.IsRequiredSubscribeOnCurrentThread() ||
		//     source6.IsRequiredSubscribeOnCurrentThread() ||
		//     false)
		RuntimeObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		G_B1_0 = ((ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)(__this));
		if (L_1)
		{
			G_B6_0 = ((ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)(__this));
			goto IL_0034;
		}
	}
	{
		RuntimeObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		G_B2_0 = ((ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)(G_B1_0));
		if (L_3)
		{
			G_B6_0 = ((ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)(G_B1_0));
			goto IL_0034;
		}
	}
	{
		RuntimeObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		G_B3_0 = ((ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)(G_B2_0));
		if (L_5)
		{
			G_B6_0 = ((ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)(G_B2_0));
			goto IL_0034;
		}
	}
	{
		RuntimeObject* L_6 = ___source43;
		bool L_7 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((RuntimeObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		G_B4_0 = ((ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)(G_B3_0));
		if (L_7)
		{
			G_B6_0 = ((ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)(G_B3_0));
			goto IL_0034;
		}
	}
	{
		RuntimeObject* L_8 = ___source54;
		bool L_9 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		G_B5_0 = ((ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)(G_B4_0));
		if (L_9)
		{
			G_B6_0 = ((ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)(G_B4_0));
			goto IL_0034;
		}
	}
	{
		RuntimeObject* L_10 = ___source65;
		bool L_11 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((RuntimeObject*)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		G_B7_0 = ((int32_t)(L_11));
		G_B7_1 = ((ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)(G_B5_0));
		goto IL_0035;
	}

IL_0034:
	{
		G_B7_0 = 1;
		G_B7_1 = ((ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)(G_B6_0));
	}

IL_0035:
	{
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)G_B7_1, (bool)G_B7_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		// this.source1 = source1;
		RuntimeObject* L_12 = ___source10;
		__this->set_source1_1(L_12);
		// this.source2 = source2;
		RuntimeObject* L_13 = ___source21;
		__this->set_source2_2(L_13);
		// this.source3 = source3;
		RuntimeObject* L_14 = ___source32;
		__this->set_source3_3(L_14);
		// this.source4 = source4;
		RuntimeObject* L_15 = ___source43;
		__this->set_source4_4(L_15);
		// this.source5 = source5;
		RuntimeObject* L_16 = ___source54;
		__this->set_source5_5(L_16);
		// this.source6 = source6;
		RuntimeObject* L_17 = ___source65;
		__this->set_source6_6(L_17);
		// this.resultSelector = resultSelector;
		ZipFunc_7_tBB54AA43BF40FE0253BFDAC4BDF85E326C54E204 * L_18 = ___resultSelector6;
		__this->set_resultSelector_7(L_18);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipObservable_7_SubscribeCore_m0FAC8D9C640BA3CC4462A03A8DF9243A88F4E847_gshared (ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new Zip(this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75 * L_2 = (Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 8));
		((  void (*) (Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75 *, ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)(L_2, (ZipObservable_7_t92F365143A130D8936F70D60E066639EA351D448 *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((Zip_tEAEA8E210160ED4E501914F7B6508B9752B7CE75 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.IObservable`1<T1>,System.IObservable`1<T2>,System.IObservable`1<T3>,System.IObservable`1<T4>,System.IObservable`1<T5>,System.IObservable`1<T6>,System.IObservable`1<T7>,UniRx.Operators.ZipFunc`8<T1,T2,T3,T4,T5,T6,T7,TR>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObservable_8__ctor_m777F3967A3A377923697EE36AF8728A672CCAFC6_gshared (ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * __this, RuntimeObject* ___source10, RuntimeObject* ___source21, RuntimeObject* ___source32, RuntimeObject* ___source43, RuntimeObject* ___source54, RuntimeObject* ___source65, RuntimeObject* ___source76, ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 * ___resultSelector7, const RuntimeMethod* method)
{
	ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * G_B7_0 = NULL;
	ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * G_B1_0 = NULL;
	ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * G_B2_0 = NULL;
	ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * G_B3_0 = NULL;
	ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * G_B4_0 = NULL;
	ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * G_B5_0 = NULL;
	ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * G_B6_0 = NULL;
	int32_t G_B8_0 = 0;
	ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * G_B8_1 = NULL;
	{
		// : base(
		//     source1.IsRequiredSubscribeOnCurrentThread() ||
		//     source2.IsRequiredSubscribeOnCurrentThread() ||
		//     source3.IsRequiredSubscribeOnCurrentThread() ||
		//     source4.IsRequiredSubscribeOnCurrentThread() ||
		//     source5.IsRequiredSubscribeOnCurrentThread() ||
		//     source6.IsRequiredSubscribeOnCurrentThread() ||
		//     source7.IsRequiredSubscribeOnCurrentThread() ||
		//     false)
		RuntimeObject* L_0 = ___source10;
		bool L_1 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		G_B1_0 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(__this));
		if (L_1)
		{
			G_B7_0 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(__this));
			goto IL_003d;
		}
	}
	{
		RuntimeObject* L_2 = ___source21;
		bool L_3 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		G_B2_0 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(G_B1_0));
		if (L_3)
		{
			G_B7_0 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(G_B1_0));
			goto IL_003d;
		}
	}
	{
		RuntimeObject* L_4 = ___source32;
		bool L_5 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		G_B3_0 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(G_B2_0));
		if (L_5)
		{
			G_B7_0 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(G_B2_0));
			goto IL_003d;
		}
	}
	{
		RuntimeObject* L_6 = ___source43;
		bool L_7 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((RuntimeObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		G_B4_0 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(G_B3_0));
		if (L_7)
		{
			G_B7_0 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(G_B3_0));
			goto IL_003d;
		}
	}
	{
		RuntimeObject* L_8 = ___source54;
		bool L_9 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		G_B5_0 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(G_B4_0));
		if (L_9)
		{
			G_B7_0 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(G_B4_0));
			goto IL_003d;
		}
	}
	{
		RuntimeObject* L_10 = ___source65;
		bool L_11 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((RuntimeObject*)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		G_B6_0 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(G_B5_0));
		if (L_11)
		{
			G_B7_0 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(G_B5_0));
			goto IL_003d;
		}
	}
	{
		RuntimeObject* L_12 = ___source76;
		bool L_13 = ((  bool (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((RuntimeObject*)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		G_B8_0 = ((int32_t)(L_13));
		G_B8_1 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(G_B6_0));
		goto IL_003e;
	}

IL_003d:
	{
		G_B8_0 = 1;
		G_B8_1 = ((ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)(G_B7_0));
	}

IL_003e:
	{
		((  void (*) (OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((OperatorObservableBase_1_tE8F06C764779526E49686496000B95E9695BC7CC *)G_B8_1, (bool)G_B8_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		// this.source1 = source1;
		RuntimeObject* L_14 = ___source10;
		__this->set_source1_1(L_14);
		// this.source2 = source2;
		RuntimeObject* L_15 = ___source21;
		__this->set_source2_2(L_15);
		// this.source3 = source3;
		RuntimeObject* L_16 = ___source32;
		__this->set_source3_3(L_16);
		// this.source4 = source4;
		RuntimeObject* L_17 = ___source43;
		__this->set_source4_4(L_17);
		// this.source5 = source5;
		RuntimeObject* L_18 = ___source54;
		__this->set_source5_5(L_18);
		// this.source6 = source6;
		RuntimeObject* L_19 = ___source65;
		__this->set_source6_6(L_19);
		// this.source7 = source7;
		RuntimeObject* L_20 = ___source76;
		__this->set_source7_7(L_20);
		// this.resultSelector = resultSelector;
		ZipFunc_8_t173B6DEFAA5D7C80F7FF82A5B4EB6961D0D35D03 * L_21 = ___resultSelector7;
		__this->set_resultSelector_8(L_21);
		// }
		return;
	}
}
// System.IDisposable UniRx.Operators.ZipObservable`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(System.IObserver`1<TR>,System.IDisposable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ZipObservable_8_SubscribeCore_mE52C539825CB1DDE0D9A6608B93C3638F4F4EA73_gshared (ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F * __this, RuntimeObject* ___observer0, RuntimeObject* ___cancel1, const RuntimeMethod* method)
{
	{
		// return new Zip(this, observer, cancel).Run();
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___cancel1;
		Zip_tFE288625F1A712CC45FAE011730910DD457970CC * L_2 = (Zip_tFE288625F1A712CC45FAE011730910DD457970CC *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (Zip_tFE288625F1A712CC45FAE011730910DD457970CC *, ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_2, (ZipObservable_8_tC0620FB37B631AAD2680B56273368960F7EEFD6F *)__this, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (Zip_tFE288625F1A712CC45FAE011730910DD457970CC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((Zip_tFE288625F1A712CC45FAE011730910DD457970CC *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObservable`1_Zip_ZipObserver<System.Object>::.ctor(UniRx.Operators.ZipObservable`1_Zip<T>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObserver__ctor_m66409D92BA2E6F0304716AD62774B4066AD06AD3_gshared (ZipObserver_tAE56C446B45ECA54A1360388514A7D8851F5CC31 * __this, Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * ___parent0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		// public ZipObserver(Zip parent, int index)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405((RuntimeObject *)__this, /*hidden argument*/NULL);
		// this.parent = parent;
		Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		// this.index = index;
		int32_t L_1 = ___index1;
		__this->set_index_1(L_1);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`1_Zip_ZipObserver<System.Object>::OnNext(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObserver_OnNext_mD549A64FC40CBA5D2F04B1583D51A75AF99B0F12_gshared (ZipObserver_tAE56C446B45ECA54A1360388514A7D8851F5CC31 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (parent.gate)
		Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * L_0 = (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)__this->get_parent_0();
		RuntimeObject * L_1 = (RuntimeObject *)L_0->get_gate_3();
		V_0 = (RuntimeObject *)L_1;
		V_1 = (bool)0;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_2, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// parent.queues[index].Enqueue(value);
		Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * L_3 = (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)__this->get_parent_0();
		Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40* L_4 = (Queue_1U5BU5D_t43C618669FBD9DCD6C5D7E717FB9A5B70ED49A40*)L_3->get_queues_4();
		int32_t L_5 = (int32_t)__this->get_index_1();
		int32_t L_6 = L_5;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_7 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)(L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		RuntimeObject * L_8 = ___value0;
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// parent.Dequeue(index);
		Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * L_9 = (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)__this->get_parent_0();
		int32_t L_10 = (int32_t)__this->get_index_1();
		((  void (*) (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		// }
		IL2CPP_LEAVE(0x4B, FINALLY_0041);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		{
			bool L_11 = V_1;
			if (!L_11)
			{
				goto IL_004a;
			}
		}

IL_0044:
		{
			RuntimeObject * L_12 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_12, /*hidden argument*/NULL);
		}

IL_004a:
		{
			IL2CPP_END_FINALLY(65)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_004b:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`1_Zip_ZipObserver<System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObserver_OnError_m4963FFDCFC612103AAABED25CB589CCF6A708068_gshared (ZipObserver_tAE56C446B45ECA54A1360388514A7D8851F5CC31 * __this, Exception_t * ___ex0, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (parent.gate)
		Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * L_0 = (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)__this->get_parent_0();
		RuntimeObject * L_1 = (RuntimeObject *)L_0->get_gate_3();
		V_0 = (RuntimeObject *)L_1;
		V_1 = (bool)0;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_2, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// parent.OnError(ex);
		Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * L_3 = (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)__this->get_parent_0();
		Exception_t * L_4 = ___ex0;
		VirtActionInvoker1< Exception_t * >::Invoke(9 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Collections.Generic.IList`1<System.Object>,System.Collections.Generic.IList`1<System.Object>>::OnError(System.Exception) */, (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)L_3, (Exception_t *)L_4);
		// }
		IL2CPP_LEAVE(0x2E, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		{
			bool L_5 = V_1;
			if (!L_5)
			{
				goto IL_002d;
			}
		}

IL_0027:
		{
			RuntimeObject * L_6 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_6, /*hidden argument*/NULL);
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(36)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2E, IL_002e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_002e:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObservable`1_Zip_ZipObserver<System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObserver_OnCompleted_m15E57AD7DE88A647DDC9F52AC5ABF15D6B2F40A2_gshared (ZipObserver_tAE56C446B45ECA54A1360388514A7D8851F5CC31 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	int32_t V_3 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (parent.gate)
		Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * L_0 = (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)__this->get_parent_0();
		RuntimeObject * L_1 = (RuntimeObject *)L_0->get_gate_3();
		V_0 = (RuntimeObject *)L_1;
		V_1 = (bool)0;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_2 = V_0;
			Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_2, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
			// parent.isDone[index] = true;
			Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * L_3 = (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)__this->get_parent_0();
			BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_4 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)L_3->get_isDone_5();
			int32_t L_5 = (int32_t)__this->get_index_1();
			(L_4)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5), (bool)1);
			// var allTrue = true;
			V_2 = (bool)1;
			// for (int i = 0; i < parent.length; i++)
			V_3 = (int32_t)0;
			goto IL_0046;
		}

IL_002f:
		{
			// if (!parent.isDone[i])
			Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * L_6 = (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)__this->get_parent_0();
			BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_7 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)L_6->get_isDone_5();
			int32_t L_8 = V_3;
			int32_t L_9 = L_8;
			uint8_t L_10 = (uint8_t)(L_7)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_9));
			if (L_10)
			{
				goto IL_0042;
			}
		}

IL_003e:
		{
			// allTrue = false;
			V_2 = (bool)0;
			// break;
			goto IL_0054;
		}

IL_0042:
		{
			// for (int i = 0; i < parent.length; i++)
			int32_t L_11 = V_3;
			V_3 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
		}

IL_0046:
		{
			// for (int i = 0; i < parent.length; i++)
			int32_t L_12 = V_3;
			Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * L_13 = (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)__this->get_parent_0();
			int32_t L_14 = (int32_t)L_13->get_length_6();
			if ((((int32_t)L_12) < ((int32_t)L_14)))
			{
				goto IL_002f;
			}
		}

IL_0054:
		{
			// if (allTrue)
			bool L_15 = V_2;
			if (!L_15)
			{
				goto IL_0062;
			}
		}

IL_0057:
		{
			// parent.OnCompleted();
			Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D * L_16 = (Zip_tB2FEA63E878C39429812290770BD1F62AB80BF0D *)__this->get_parent_0();
			VirtActionInvoker0::Invoke(10 /* System.Void UniRx.Operators.OperatorObserverBase`2<System.Collections.Generic.IList`1<System.Object>,System.Collections.Generic.IList`1<System.Object>>::OnCompleted() */, (OperatorObserverBase_2_t6250FABBAEB93A38D123AC3803250D91392B98F6 *)L_16);
		}

IL_0062:
		{
			// }
			IL2CPP_LEAVE(0x6E, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		{
			bool L_17 = V_1;
			if (!L_17)
			{
				goto IL_006d;
			}
		}

IL_0067:
		{
			RuntimeObject * L_18 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_18, /*hidden argument*/NULL);
		}

IL_006d:
		{
			IL2CPP_END_FINALLY(100)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_JUMP_TBL(0x6E, IL_006e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_006e:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.Operators.ZipObserver`1<System.Object>::.ctor(System.Object,UniRx.Operators.IZipObservable,System.Int32,System.Collections.Generic.Queue`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObserver_1__ctor_mCD49AC686858FD7E96CAC1A5A68A5AFCB9EE3D36_gshared (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * __this, RuntimeObject * ___gate0, RuntimeObject* ___parent1, int32_t ___index2, Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * ___queue3, const RuntimeMethod* method)
{
	{
		// public ZipObserver(object gate, IZipObservable parent, int index, Queue<T> queue)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405((RuntimeObject *)__this, /*hidden argument*/NULL);
		// this.gate = gate;
		RuntimeObject * L_0 = ___gate0;
		__this->set_gate_0(L_0);
		// this.parent = parent;
		RuntimeObject* L_1 = ___parent1;
		__this->set_parent_1(L_1);
		// this.index = index;
		int32_t L_2 = ___index2;
		__this->set_index_2(L_2);
		// this.queue = queue;
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_3 = ___queue3;
		__this->set_queue_3(L_3);
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObserver`1<System.Object>::OnNext(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObserver_1_OnNext_mAFBA00C1A35721E923AEC7A296BD7F11E9B5E905_gshared (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipObserver_1_OnNext_mAFBA00C1A35721E923AEC7A296BD7F11E9B5E905_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (gate)
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_0();
		V_0 = (RuntimeObject *)L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_1, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// queue.Enqueue(value);
		Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 * L_2 = (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)__this->get_queue_3();
		RuntimeObject * L_3 = ___value0;
		((  void (*) (Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Queue_1_t65333FCCA10D8CE1B441D400B6B94140BCB8BF64 *)L_2, (RuntimeObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// parent.Dequeue(index);
		RuntimeObject* L_4 = (RuntimeObject*)__this->get_parent_1();
		int32_t L_5 = (int32_t)__this->get_index_2();
		InterfaceActionInvoker1< int32_t >::Invoke(0 /* System.Void UniRx.Operators.IZipObservable::Dequeue(System.Int32) */, IZipObservable_t0ECBDF53FECFF9407F1BE5BA0FD1156D07B45496_il2cpp_TypeInfo_var, (RuntimeObject*)L_4, (int32_t)L_5);
		// }
		IL2CPP_LEAVE(0x3A, FINALLY_0030);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		{
			bool L_6 = V_1;
			if (!L_6)
			{
				goto IL_0039;
			}
		}

IL_0033:
		{
			RuntimeObject * L_7 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_7, /*hidden argument*/NULL);
		}

IL_0039:
		{
			IL2CPP_END_FINALLY(48)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3A, IL_003a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_003a:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObserver`1<System.Object>::OnError(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObserver_1_OnError_mC8AFE6282FDE7D11733BB41711DBA564553A68EE_gshared (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * __this, Exception_t * ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipObserver_1_OnError_mC8AFE6282FDE7D11733BB41711DBA564553A68EE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (gate)
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_0();
		V_0 = (RuntimeObject *)L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_1, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// parent.Fail(error);
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_parent_1();
		Exception_t * L_3 = ___error0;
		InterfaceActionInvoker1< Exception_t * >::Invoke(1 /* System.Void UniRx.Operators.IZipObservable::Fail(System.Exception) */, IZipObservable_t0ECBDF53FECFF9407F1BE5BA0FD1156D07B45496_il2cpp_TypeInfo_var, (RuntimeObject*)L_2, (Exception_t *)L_3);
		// }
		IL2CPP_LEAVE(0x29, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		{
			bool L_4 = V_1;
			if (!L_4)
			{
				goto IL_0028;
			}
		}

IL_0022:
		{
			RuntimeObject * L_5 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_5, /*hidden argument*/NULL);
		}

IL_0028:
		{
			IL2CPP_END_FINALLY(31)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x29, IL_0029)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0029:
	{
		// }
		return;
	}
}
// System.Void UniRx.Operators.ZipObserver`1<System.Object>::OnCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZipObserver_1_OnCompleted_m2C83E0107322DEEBE7FC6214DCAB4D098D43F67A_gshared (ZipObserver_1_tB28746943784DADD0186C76955CF856BF58FAA78 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipObserver_1_OnCompleted_m2C83E0107322DEEBE7FC6214DCAB4D098D43F67A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (gate)
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_gate_0();
		V_0 = (RuntimeObject *)L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_1, (bool*)(bool*)(&V_1), /*hidden argument*/NULL);
		// parent.Done(index);
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_parent_1();
		int32_t L_3 = (int32_t)__this->get_index_2();
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.Operators.IZipObservable::Done(System.Int32) */, IZipObservable_t0ECBDF53FECFF9407F1BE5BA0FD1156D07B45496_il2cpp_TypeInfo_var, (RuntimeObject*)L_2, (int32_t)L_3);
		// }
		IL2CPP_LEAVE(0x2E, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		{
			bool L_4 = V_1;
			if (!L_4)
			{
				goto IL_002d;
			}
		}

IL_0027:
		{
			RuntimeObject * L_5 = V_0;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_5, /*hidden argument*/NULL);
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(36)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2E, IL_002e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_002e:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
