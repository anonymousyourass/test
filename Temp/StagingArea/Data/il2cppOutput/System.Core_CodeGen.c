﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412 ();
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57 ();
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937 ();
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018 ();
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E ();
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31 ();
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1 ();
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6 ();
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93 ();
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6 ();
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460 ();
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45 ();
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B ();
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C ();
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268 ();
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF ();
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153 ();
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477 ();
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91 ();
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E ();
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265 ();
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4 ();
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502 ();
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1 ();
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465 ();
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34 ();
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB ();
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252 ();
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F ();
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058 ();
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218 ();
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D ();
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D ();
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63 ();
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD ();
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E ();
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF ();
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB ();
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E ();
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598 ();
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1 ();
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679 ();
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129 ();
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A ();
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD ();
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E ();
// 0x0000002F System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2 ();
// 0x00000030 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 ();
// 0x00000031 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 ();
// 0x00000032 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000033 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Skip(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::SkipIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000036 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000037 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000038 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000039 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003A TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003B TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003C TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Repeat(TResult,System.Int32)
// 0x0000003E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::RepeatIterator(TResult,System.Int32)
// 0x0000003F System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000040 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000041 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000042 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000043 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000044 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000045 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000046 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000047 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000048 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000049 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000004A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000004B System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000004C System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000004D System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000004E System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004F System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000050 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000051 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000052 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000053 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000054 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000055 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000056 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000057 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000058 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000059 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000005A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000005B System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000005C System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000005D System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::.ctor(System.Int32)
// 0x0000005E System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::System.IDisposable.Dispose()
// 0x0000005F System.Boolean System.Linq.Enumerable_<SkipIterator>d__31`1::MoveNext()
// 0x00000060 System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::<>m__Finally1()
// 0x00000061 TSource System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000062 System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.IEnumerator.Reset()
// 0x00000063 System.Object System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.IEnumerator.get_Current()
// 0x00000064 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000065 System.Collections.IEnumerator System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000066 System.Void System.Linq.Enumerable_<RepeatIterator>d__117`1::.ctor(System.Int32)
// 0x00000067 System.Void System.Linq.Enumerable_<RepeatIterator>d__117`1::System.IDisposable.Dispose()
// 0x00000068 System.Boolean System.Linq.Enumerable_<RepeatIterator>d__117`1::MoveNext()
// 0x00000069 TResult System.Linq.Enumerable_<RepeatIterator>d__117`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000006A System.Void System.Linq.Enumerable_<RepeatIterator>d__117`1::System.Collections.IEnumerator.Reset()
// 0x0000006B System.Object System.Linq.Enumerable_<RepeatIterator>d__117`1::System.Collections.IEnumerator.get_Current()
// 0x0000006C System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<RepeatIterator>d__117`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000006D System.Collections.IEnumerator System.Linq.Enumerable_<RepeatIterator>d__117`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000006E System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000006F System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000070 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000071 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000072 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000073 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000074 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000075 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000076 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x00000077 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000078 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000079 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x0000007A System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000007B System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000007C System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x0000007D System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x0000007E System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000007F System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000080 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000081 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000082 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000083 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000084 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000085 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000086 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000087 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000088 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000089 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000008A System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000008B System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000008C System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000008D System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000008E System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000008F System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000090 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000091 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000092 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000093 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000094 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000095 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000096 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000097 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000098 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000099 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000009A System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000009B System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000009C System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000009D System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000009E System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x0000009F System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000A0 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000A1 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000A2 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[162] = 
{
	SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412,
	AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57,
	AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937,
	AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018,
	AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E,
	AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31,
	AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1,
	AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6,
	AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93,
	AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6,
	AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460,
	AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45,
	AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B,
	AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C,
	AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268,
	AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF,
	AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153,
	AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477,
	AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91,
	AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E,
	AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265,
	AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4,
	AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502,
	AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1,
	AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465,
	AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34,
	AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB,
	AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252,
	AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F,
	AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058,
	AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218,
	AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D,
	AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D,
	AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63,
	AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD,
	AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E,
	AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF,
	AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB,
	AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E,
	AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598,
	AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1,
	AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679,
	AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129,
	AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A,
	AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD,
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[162] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	105,
	14,
	105,
	31,
	23,
	23,
	23,
	23,
	23,
	105,
	105,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	890,
	27,
	37,
	200,
	200,
	3,
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[34] = 
{
	{ 0x02000008, { 53, 4 } },
	{ 0x02000009, { 57, 9 } },
	{ 0x0200000A, { 66, 7 } },
	{ 0x0200000B, { 73, 10 } },
	{ 0x0200000C, { 83, 1 } },
	{ 0x0200000D, { 84, 8 } },
	{ 0x0200000E, { 92, 4 } },
	{ 0x02000010, { 96, 3 } },
	{ 0x02000011, { 101, 5 } },
	{ 0x02000012, { 106, 7 } },
	{ 0x02000013, { 113, 3 } },
	{ 0x02000014, { 116, 7 } },
	{ 0x02000015, { 123, 4 } },
	{ 0x02000016, { 127, 21 } },
	{ 0x02000018, { 148, 2 } },
	{ 0x06000032, { 0, 10 } },
	{ 0x06000033, { 10, 5 } },
	{ 0x06000034, { 15, 1 } },
	{ 0x06000035, { 16, 2 } },
	{ 0x06000036, { 18, 2 } },
	{ 0x06000037, { 20, 1 } },
	{ 0x06000038, { 21, 3 } },
	{ 0x06000039, { 24, 2 } },
	{ 0x0600003A, { 26, 4 } },
	{ 0x0600003B, { 30, 4 } },
	{ 0x0600003C, { 34, 3 } },
	{ 0x0600003D, { 37, 1 } },
	{ 0x0600003E, { 38, 2 } },
	{ 0x0600003F, { 40, 1 } },
	{ 0x06000040, { 41, 3 } },
	{ 0x06000041, { 44, 2 } },
	{ 0x06000042, { 46, 2 } },
	{ 0x06000043, { 48, 5 } },
	{ 0x06000072, { 99, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[150] = 
{
	{ (Il2CppRGCTXDataType)2, 18084 },
	{ (Il2CppRGCTXDataType)3, 16910 },
	{ (Il2CppRGCTXDataType)2, 18085 },
	{ (Il2CppRGCTXDataType)2, 18086 },
	{ (Il2CppRGCTXDataType)3, 16911 },
	{ (Il2CppRGCTXDataType)2, 18087 },
	{ (Il2CppRGCTXDataType)2, 18088 },
	{ (Il2CppRGCTXDataType)3, 16912 },
	{ (Il2CppRGCTXDataType)2, 18089 },
	{ (Il2CppRGCTXDataType)3, 16913 },
	{ (Il2CppRGCTXDataType)2, 18090 },
	{ (Il2CppRGCTXDataType)3, 16914 },
	{ (Il2CppRGCTXDataType)3, 16915 },
	{ (Il2CppRGCTXDataType)2, 7377 },
	{ (Il2CppRGCTXDataType)3, 16916 },
	{ (Il2CppRGCTXDataType)3, 16917 },
	{ (Il2CppRGCTXDataType)2, 18091 },
	{ (Il2CppRGCTXDataType)3, 16918 },
	{ (Il2CppRGCTXDataType)2, 18092 },
	{ (Il2CppRGCTXDataType)3, 16919 },
	{ (Il2CppRGCTXDataType)3, 16920 },
	{ (Il2CppRGCTXDataType)2, 18093 },
	{ (Il2CppRGCTXDataType)3, 16921 },
	{ (Il2CppRGCTXDataType)3, 16922 },
	{ (Il2CppRGCTXDataType)2, 7397 },
	{ (Il2CppRGCTXDataType)3, 16923 },
	{ (Il2CppRGCTXDataType)2, 18094 },
	{ (Il2CppRGCTXDataType)2, 18095 },
	{ (Il2CppRGCTXDataType)2, 7398 },
	{ (Il2CppRGCTXDataType)2, 18096 },
	{ (Il2CppRGCTXDataType)2, 18097 },
	{ (Il2CppRGCTXDataType)2, 18098 },
	{ (Il2CppRGCTXDataType)2, 7400 },
	{ (Il2CppRGCTXDataType)2, 18099 },
	{ (Il2CppRGCTXDataType)2, 7402 },
	{ (Il2CppRGCTXDataType)2, 18100 },
	{ (Il2CppRGCTXDataType)3, 16924 },
	{ (Il2CppRGCTXDataType)3, 16925 },
	{ (Il2CppRGCTXDataType)2, 18101 },
	{ (Il2CppRGCTXDataType)3, 16926 },
	{ (Il2CppRGCTXDataType)2, 7409 },
	{ (Il2CppRGCTXDataType)2, 7411 },
	{ (Il2CppRGCTXDataType)2, 18102 },
	{ (Il2CppRGCTXDataType)3, 16927 },
	{ (Il2CppRGCTXDataType)2, 18103 },
	{ (Il2CppRGCTXDataType)2, 7414 },
	{ (Il2CppRGCTXDataType)2, 18104 },
	{ (Il2CppRGCTXDataType)3, 16928 },
	{ (Il2CppRGCTXDataType)3, 16929 },
	{ (Il2CppRGCTXDataType)2, 18105 },
	{ (Il2CppRGCTXDataType)2, 7418 },
	{ (Il2CppRGCTXDataType)2, 18106 },
	{ (Il2CppRGCTXDataType)2, 7420 },
	{ (Il2CppRGCTXDataType)3, 16930 },
	{ (Il2CppRGCTXDataType)3, 16931 },
	{ (Il2CppRGCTXDataType)2, 7423 },
	{ (Il2CppRGCTXDataType)3, 16932 },
	{ (Il2CppRGCTXDataType)3, 16933 },
	{ (Il2CppRGCTXDataType)2, 7432 },
	{ (Il2CppRGCTXDataType)2, 18107 },
	{ (Il2CppRGCTXDataType)3, 16934 },
	{ (Il2CppRGCTXDataType)3, 16935 },
	{ (Il2CppRGCTXDataType)2, 7434 },
	{ (Il2CppRGCTXDataType)2, 17602 },
	{ (Il2CppRGCTXDataType)3, 16936 },
	{ (Il2CppRGCTXDataType)3, 16937 },
	{ (Il2CppRGCTXDataType)3, 16938 },
	{ (Il2CppRGCTXDataType)2, 7441 },
	{ (Il2CppRGCTXDataType)2, 18108 },
	{ (Il2CppRGCTXDataType)3, 16939 },
	{ (Il2CppRGCTXDataType)3, 16940 },
	{ (Il2CppRGCTXDataType)3, 15866 },
	{ (Il2CppRGCTXDataType)3, 16941 },
	{ (Il2CppRGCTXDataType)3, 16942 },
	{ (Il2CppRGCTXDataType)2, 7450 },
	{ (Il2CppRGCTXDataType)2, 18109 },
	{ (Il2CppRGCTXDataType)3, 16943 },
	{ (Il2CppRGCTXDataType)3, 16944 },
	{ (Il2CppRGCTXDataType)3, 16945 },
	{ (Il2CppRGCTXDataType)3, 16946 },
	{ (Il2CppRGCTXDataType)3, 16947 },
	{ (Il2CppRGCTXDataType)3, 15872 },
	{ (Il2CppRGCTXDataType)3, 16948 },
	{ (Il2CppRGCTXDataType)3, 16949 },
	{ (Il2CppRGCTXDataType)3, 16950 },
	{ (Il2CppRGCTXDataType)2, 7470 },
	{ (Il2CppRGCTXDataType)2, 7465 },
	{ (Il2CppRGCTXDataType)3, 16951 },
	{ (Il2CppRGCTXDataType)2, 7464 },
	{ (Il2CppRGCTXDataType)2, 18110 },
	{ (Il2CppRGCTXDataType)3, 16952 },
	{ (Il2CppRGCTXDataType)3, 16953 },
	{ (Il2CppRGCTXDataType)2, 7474 },
	{ (Il2CppRGCTXDataType)2, 18111 },
	{ (Il2CppRGCTXDataType)3, 16954 },
	{ (Il2CppRGCTXDataType)3, 16955 },
	{ (Il2CppRGCTXDataType)2, 18112 },
	{ (Il2CppRGCTXDataType)3, 16956 },
	{ (Il2CppRGCTXDataType)3, 16957 },
	{ (Il2CppRGCTXDataType)2, 18113 },
	{ (Il2CppRGCTXDataType)3, 16958 },
	{ (Il2CppRGCTXDataType)2, 18114 },
	{ (Il2CppRGCTXDataType)3, 16959 },
	{ (Il2CppRGCTXDataType)3, 16960 },
	{ (Il2CppRGCTXDataType)3, 16961 },
	{ (Il2CppRGCTXDataType)2, 7500 },
	{ (Il2CppRGCTXDataType)3, 16962 },
	{ (Il2CppRGCTXDataType)2, 7508 },
	{ (Il2CppRGCTXDataType)3, 16963 },
	{ (Il2CppRGCTXDataType)2, 18115 },
	{ (Il2CppRGCTXDataType)2, 18116 },
	{ (Il2CppRGCTXDataType)3, 16964 },
	{ (Il2CppRGCTXDataType)3, 16965 },
	{ (Il2CppRGCTXDataType)3, 16966 },
	{ (Il2CppRGCTXDataType)3, 16967 },
	{ (Il2CppRGCTXDataType)3, 16968 },
	{ (Il2CppRGCTXDataType)3, 16969 },
	{ (Il2CppRGCTXDataType)2, 7524 },
	{ (Il2CppRGCTXDataType)2, 18117 },
	{ (Il2CppRGCTXDataType)3, 16970 },
	{ (Il2CppRGCTXDataType)3, 16971 },
	{ (Il2CppRGCTXDataType)2, 7528 },
	{ (Il2CppRGCTXDataType)3, 16972 },
	{ (Il2CppRGCTXDataType)2, 18118 },
	{ (Il2CppRGCTXDataType)2, 7538 },
	{ (Il2CppRGCTXDataType)2, 7536 },
	{ (Il2CppRGCTXDataType)2, 18119 },
	{ (Il2CppRGCTXDataType)3, 16973 },
	{ (Il2CppRGCTXDataType)2, 18120 },
	{ (Il2CppRGCTXDataType)3, 16974 },
	{ (Il2CppRGCTXDataType)3, 16975 },
	{ (Il2CppRGCTXDataType)3, 16976 },
	{ (Il2CppRGCTXDataType)2, 7542 },
	{ (Il2CppRGCTXDataType)3, 16977 },
	{ (Il2CppRGCTXDataType)3, 16978 },
	{ (Il2CppRGCTXDataType)2, 7545 },
	{ (Il2CppRGCTXDataType)3, 16979 },
	{ (Il2CppRGCTXDataType)1, 18121 },
	{ (Il2CppRGCTXDataType)2, 7544 },
	{ (Il2CppRGCTXDataType)3, 16980 },
	{ (Il2CppRGCTXDataType)1, 7544 },
	{ (Il2CppRGCTXDataType)1, 7542 },
	{ (Il2CppRGCTXDataType)2, 18122 },
	{ (Il2CppRGCTXDataType)2, 7544 },
	{ (Il2CppRGCTXDataType)3, 16981 },
	{ (Il2CppRGCTXDataType)3, 16982 },
	{ (Il2CppRGCTXDataType)3, 16983 },
	{ (Il2CppRGCTXDataType)2, 7543 },
	{ (Il2CppRGCTXDataType)3, 16984 },
	{ (Il2CppRGCTXDataType)2, 7556 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	162,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	34,
	s_rgctxIndices,
	150,
	s_rgctxValues,
	NULL,
};
