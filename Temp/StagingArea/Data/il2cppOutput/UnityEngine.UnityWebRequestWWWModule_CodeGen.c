﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,System.Int32)
extern void WWW_LoadFromCacheOrDownload_mCEBC8CAFFCF02955704F2434FF021E00CA25ECF9 ();
// 0x00000002 UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,System.Int32,System.UInt32)
extern void WWW_LoadFromCacheOrDownload_m517BA65B5127CD3ACD4E7FD631842E7520911E66 ();
// 0x00000003 UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,UnityEngine.Hash128)
extern void WWW_LoadFromCacheOrDownload_m62474289B95FD023EB8E967ABD6C17213FC2264C ();
// 0x00000004 UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,UnityEngine.Hash128,System.UInt32)
extern void WWW_LoadFromCacheOrDownload_mF035045BEF97EFBB19305F66B72165854716E583 ();
// 0x00000005 System.Void UnityEngine.WWW::.ctor(System.String,UnityEngine.WWWForm)
extern void WWW__ctor_m2F58987EB716A6D1B9B2425464E5C42FB6CF7DE6 ();
// 0x00000006 System.Void UnityEngine.WWW::.ctor(System.String,System.Byte[])
extern void WWW__ctor_mFB9E9E1DE62E5826F643B25B3AB6BE33B681B0F4 ();
// 0x00000007 System.Void UnityEngine.WWW::.ctor(System.String,System.Byte[],System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void WWW__ctor_m6686CBC878BB9EB28F4C4171C203CA4E3DE3656C ();
// 0x00000008 System.Void UnityEngine.WWW::.ctor(System.String,System.String,UnityEngine.Hash128,System.UInt32)
extern void WWW__ctor_m443FACCAB63C95EB81924E1FF3B5ADE2D7875186 ();
// 0x00000009 UnityEngine.AssetBundle UnityEngine.WWW::get_assetBundle()
extern void WWW_get_assetBundle_mE523FA754E97AAECE72EA84CBC5D1888E445DE9F ();
// 0x0000000A System.Byte[] UnityEngine.WWW::get_bytes()
extern void WWW_get_bytes_m378FCCD8E91FB7FE7FA22E05BA3FE528CD7EAF1A ();
// 0x0000000B System.String UnityEngine.WWW::get_error()
extern void WWW_get_error_mB278F5EC90EF99FEF70D80112940CFB49E79C9BC ();
// 0x0000000C System.Boolean UnityEngine.WWW::get_isDone()
extern void WWW_get_isDone_m916B54D53395990DB59C64413798FBCAFB08E0E3 ();
// 0x0000000D System.Single UnityEngine.WWW::get_progress()
extern void WWW_get_progress_m53C94C51D328A968EC132163D1F8B87654B5E073 ();
// 0x0000000E System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::get_responseHeaders()
extern void WWW_get_responseHeaders_m41BDD7540714ED01ED6B0F63F3062AE1D4C8B598 ();
// 0x0000000F System.String UnityEngine.WWW::get_text()
extern void WWW_get_text_m0D2EF7BBFB58E37FE30A665389355ACA65804138 ();
// 0x00000010 System.String UnityEngine.WWW::get_url()
extern void WWW_get_url_m1D75D492D78A7AA8F607C5D7700497B8FE5E9526 ();
// 0x00000011 System.Boolean UnityEngine.WWW::get_keepWaiting()
extern void WWW_get_keepWaiting_m231A6A7A835610182D78FC414665CC75195ABD70 ();
// 0x00000012 System.Void UnityEngine.WWW::Dispose()
extern void WWW_Dispose_mF5A8B944281564903043545BC1E7F1CAD941519F ();
// 0x00000013 System.Boolean UnityEngine.WWW::WaitUntilDoneIfPossible()
extern void WWW_WaitUntilDoneIfPossible_m8D6B638F661CBD13B442F392BF42F5C9BDF0E84D ();
static Il2CppMethodPointer s_methodPointers[19] = 
{
	WWW_LoadFromCacheOrDownload_mCEBC8CAFFCF02955704F2434FF021E00CA25ECF9,
	WWW_LoadFromCacheOrDownload_m517BA65B5127CD3ACD4E7FD631842E7520911E66,
	WWW_LoadFromCacheOrDownload_m62474289B95FD023EB8E967ABD6C17213FC2264C,
	WWW_LoadFromCacheOrDownload_mF035045BEF97EFBB19305F66B72165854716E583,
	WWW__ctor_m2F58987EB716A6D1B9B2425464E5C42FB6CF7DE6,
	WWW__ctor_mFB9E9E1DE62E5826F643B25B3AB6BE33B681B0F4,
	WWW__ctor_m6686CBC878BB9EB28F4C4171C203CA4E3DE3656C,
	WWW__ctor_m443FACCAB63C95EB81924E1FF3B5ADE2D7875186,
	WWW_get_assetBundle_mE523FA754E97AAECE72EA84CBC5D1888E445DE9F,
	WWW_get_bytes_m378FCCD8E91FB7FE7FA22E05BA3FE528CD7EAF1A,
	WWW_get_error_mB278F5EC90EF99FEF70D80112940CFB49E79C9BC,
	WWW_get_isDone_m916B54D53395990DB59C64413798FBCAFB08E0E3,
	WWW_get_progress_m53C94C51D328A968EC132163D1F8B87654B5E073,
	WWW_get_responseHeaders_m41BDD7540714ED01ED6B0F63F3062AE1D4C8B598,
	WWW_get_text_m0D2EF7BBFB58E37FE30A665389355ACA65804138,
	WWW_get_url_m1D75D492D78A7AA8F607C5D7700497B8FE5E9526,
	WWW_get_keepWaiting_m231A6A7A835610182D78FC414665CC75195ABD70,
	WWW_Dispose_mF5A8B944281564903043545BC1E7F1CAD941519F,
	WWW_WaitUntilDoneIfPossible_m8D6B638F661CBD13B442F392BF42F5C9BDF0E84D,
};
static const int32_t s_InvokerIndices[19] = 
{
	119,
	196,
	1716,
	1717,
	27,
	27,
	200,
	1598,
	14,
	14,
	14,
	89,
	700,
	14,
	14,
	14,
	89,
	23,
	89,
};
extern const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule = 
{
	"UnityEngine.UnityWebRequestWWWModule.dll",
	19,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
