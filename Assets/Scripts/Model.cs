﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Model
{
    private const string SCORE = "SCORE";
    private const string LVL = "LEVEL";
    private const string MAPS = "MAPS";

    public static string MAP(int id)
    {
        if(PlayerPrefs.GetString(id.ToString(),string.Empty) == string.Empty)
            PlayerPrefs.SetString(MAPS + id.ToString(), MapHelper.GenerateMap(id));

       return PlayerPrefs.GetString(MAPS + id.ToString(),string.Empty);
    }

    public static int Score
    {
        get
        {
            return PlayerPrefs.GetInt(SCORE, 0);

        }
        set
        {
            PlayerPrefs.SetInt(SCORE, value);
        }
    }

    public static int Level
    {
        get
        {
            return PlayerPrefs.GetInt(LVL, 0);

        }
        set
        {
            PlayerPrefs.SetInt(LVL, value);
        }
    }
}
