﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using TMPro;
using System.Linq;

public class Controller : MonoBehaviour
{
    //Model
    private GameSettings model;
    //VIEW

    //ui
    public TMPro.TextMeshProUGUI scoreView;
    public TMPro.TextMeshProUGUI lvlView;
    //model


    //locals
    private List<(GameObject,GameObject)> tiles;
    private GameObject player;

    private int mapProgress = 0;
    private int startProgress = 0;


    // Start is called before the first frame update
    void Start()
    {
        model = Resources.Load("Settings", typeof(GameSettings)) as GameSettings;

        //PlayerPrefs.DeleteAll();

        //initialize view
        scoreView.text = "Score "+Model.Score.ToString();
        lvlView.text = "Level " + Model.Level.ToString();
        //initialize start platz
        InitializeMap(Model.Level);

        //logics
        //camera follow
        Observable.EveryUpdate().Where(_=>player.gameObject.activeSelf).Subscribe(_ => {
            Camera.main.transform.position = player.transform.position + Vector3.up * model.CamH;
        });

        //death 
        Observable.EveryUpdate().Where(_ => player.gameObject.activeSelf).Subscribe(_ => {
            if(player.transform.position.y < model.DeathH)
                DeinitializeMap();
        });

        //restart
        Observable.EveryUpdate().Where(_ => player.gameObject.activeSelf == false).Subscribe(_ => {
            if (Input.GetMouseButtonDown(0))
                InitializeMap(++Model.Level);
        });

        //input
        Observable.EveryUpdate().Where(_ => player.gameObject.activeSelf).Subscribe(_ =>
        {
            Vector3 directionMove = Vector3.zero;
            
            if (Input.GetKey(KeyCode.W))
                directionMove = (directionMove + Vector3.forward).normalized;
            if (Input.GetKey(KeyCode.S))
                directionMove = (directionMove + Vector3.back).normalized;
            if (Input.GetKey(KeyCode.A))
                directionMove = (directionMove + Vector3.left).normalized;
            if (Input.GetKey(KeyCode.D))
                directionMove = (directionMove + Vector3.right).normalized;

            var vel = new Vector3(0f, player.GetComponent<Rigidbody>().velocity.y,0f);

            player.GetComponent<Rigidbody>().velocity = vel + model.MaxVelPl* directionMove;
        });

        //observe player progress
        Observable.EveryUpdate().Where(_ => player.gameObject.activeSelf).Subscribe(_ =>
        {
            foreach (var tile in tiles.Skip(9))
            {
                if(Vector3.Distance(player.transform.position, tile.Item1.transform.position) < 2f && tiles.IndexOf(tile) > mapProgress)
                    mapProgress = tiles.IndexOf(tile);
            }
        });

        //change color of tiles
        Observable.EveryUpdate().Where(_ => player.gameObject.activeSelf).Subscribe(_ =>
        {
            foreach(var tile in tiles)
            {
                if(mapProgress >= tiles.IndexOf(tile))
                {
                    tile.Item1.GetComponent<Renderer>().material.SetColor("_Color", Color.grey);  //те которые прошли отображаем серым

                    if(tile.Item2)
                        tile.Item2.GetComponent<Renderer>().material.SetColor("_Color", Color.cyan);    //те которые прошли отображаем
                }
                   
                if (mapProgress >= tiles.IndexOf(tile) + startProgress && mapProgress > startProgress * 2)
                {
                    tile.Item1.GetComponent<Renderer>().material.SetColor("_Color", new Color(1f, 1f, 1f, 0f));  //те которые прошли более чем на startProgress (9 стартовых тайлов) не отображаем

                    if (tile.Item2)
                        tile.Item2.GetComponent<Renderer>().material.SetColor("_Color", new Color(1f, 1f, 1f, 0f));
                }
            }
        });
    }

    void DeinitializeMap()
    {
        tiles.ForEach((tile) =>
        {
            tile.Item1.GetComponent<Renderer>().material.SetColor("_Color", Color.grey); 

            if (tile.Item2)
                tile.Item2.GetComponent<Renderer>().material.SetColor("_Color", Color.cyan);   

            tile.Item1.GetComponent<PoolObject>().ReturnToPool();
            if (tile.Item2)
            {
                tile.Item2.GetComponent<PoolObject>().ReturnToPool();
                tile.Item2.GetComponent<CollisionNotifier>().notify = null;
            }
        });


        tiles.Clear();
        player.GetComponent<Rigidbody>().velocity = Vector3.zero;
        player.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        player.GetComponent<PoolObject>().ReturnToPool();
       
    }

    void InitializeMap(int id)
    {
        tiles = new List<(GameObject, GameObject)>();
     

        for (int i = -1; i <= 1; i++)
            for (int j = -1; j <= 1; j++)
            {
                var quad = PoolManager.GetObject("Quad", new Vector3(i, 0f, j), Quaternion.identity * Quaternion.Euler(90f, 0f, 0f));
                tiles.Add((quad,null));
            }

        player = PoolManager.GetObject("Sphere", Vector3.zero + Vector3.up, Quaternion.identity);

        //initialize map

        System.Func<int, Vector3, Vector3> getTilePos = (tileId, posPrev) =>
        {
            if (tileId == 0) return MapHelper.TransformToDir(Model.MAP(id).Substring(0, 1)) * 2f;
            var pos = posPrev + MapHelper.TransformToDir(Model.MAP(id).Substring(tileId, 1)) * 1f;
            return pos;
        };

        mapProgress = startProgress = tiles.Count;

        for (int k = 1; k < Model.MAP(id).Length; k++)
        {
            var posTile = getTilePos(k, tiles[tiles.Count - 1].Item1.gameObject.transform.position);

            var quad = PoolManager.GetObject("Quad", posTile, Quaternion.identity * Quaternion.Euler(90f, 0f, 0f));

            GameObject cube = null;
            if (MapHelper.IsBonusCode(Model.MAP(id).Substring(k, 1)))
            {
                cube = PoolManager.GetObject("Cube", posTile + Vector3.up, Quaternion.identity * Quaternion.Euler(45f, 45f, 45f));

                cube.GetComponent<CollisionNotifier>().notify = (crystalCollided, collision) =>
                {
                    if (collision == player)
                    {
                        crystalCollided.GetComponent<CollisionNotifier>().notify = null;
                        crystalCollided.GetComponent<PoolObject>().ReturnToPool();
                        scoreView.text = "Score "+(++Model.Score).ToString();
                    }
                };
            }

            tiles.Add((quad,cube));
            lvlView.text = "Level " + Model.Level.ToString();
        }

       
    }
}
