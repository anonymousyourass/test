﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Pool/PoolSetup")]
public class PoolSetup : MonoBehaviour
{//обертка для управления статическим классом PoolManager

#pragma warning disable 0649
	#region Unity scene settings
	[SerializeField] private PoolManager.PoolPart[] pools;
	#endregion
#pragma warning restore 0649

	#region Methods
	void OnValidate()
	{
		for (int i = 0; i < pools.Length; i++)
		{
			pools[i].name = pools[i].prefab.name;
		}
	}

	void Awake()
	{
		Initialize();
	}

	void Initialize()
	{
		PoolManager.Initialize(pools);
	}
	#endregion
}