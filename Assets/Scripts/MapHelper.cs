﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MapHelper 
{
    public static bool IsBonusCode(string code)
    {
        switch (code)
        {
            case ("4"):
                return true;
            case ("5"):
                return true;
            case ("6"):
                return true;
            case ("0"):
                return UnityEngine.Random.Range(0, 2) == 0 ? false : true;
            case ("1"):
                return false;
            case ("2"):
                return false;
            case ("3"):
                return false;
            default:
                return false;
        }
    }


    public static Vector3 TransformToDir(string code)
    {
        switch (code)
        {
            case ("0"):
                return Vector3.forward;
            case ("1"):
                return Vector3.right;
            case ("2"):
                return Vector3.left;
            case ("3"):
                return Vector3.forward;
            case ("4"):
                return Vector3.right;
            case ("5"):
                return Vector3.left;
            case ("6"):
                return Vector3.back;
            case ("7"):
                return Vector3.back;
            default:
                return Vector3.zero;
        }
    }

    public static string GenerateMap(int id)
    {
        UnityEngine.Random.InitState(id);

        var mapLen = UnityEngine.Random.Range(100, 1000);
        var mapStr = "0000";

        for (int i = 4; i < mapLen; i += 4)
        {
            var rnd = UnityEngine.Random.Range(0, 6).ToString();

            if (i % 8 == 0)
            {
                rnd = "0";
            }

            mapStr = string.Concat(mapStr, rnd);
            mapStr = string.Concat(mapStr, rnd);
            mapStr = string.Concat(mapStr, rnd);
            mapStr = string.Concat(mapStr, rnd);
        }

        return mapStr;
    }
}
