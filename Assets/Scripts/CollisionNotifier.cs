﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionNotifier : MonoBehaviour
{
    public System.Action<GameObject,GameObject> notify;


    private void OnCollisionEnter(Collision collision)
    {
        if (notify != null)
            notify(this.gameObject,collision.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (notify != null)
            notify(this.gameObject, other.gameObject);
    }
}
