﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New GameSettings", menuName = "GameSettings", order = 51)]
public class GameSettings : ScriptableObject
{
    [SerializeField]
    private float DEATH_HEIGHT = -10f;
    [SerializeField]
    private float CAM_HEIGHT = 5f;
    [SerializeField]
    private float MAX_VEL_PL = 1f;

    public float DeathH
    {
        get
        {
            return DEATH_HEIGHT;
        }
    }

    public float CamH
    {
        get
        {
            return CAM_HEIGHT;
        }
    }

    public float MaxVelPl
    {
        get
        {
            return MAX_VEL_PL;
        }
    }

}
